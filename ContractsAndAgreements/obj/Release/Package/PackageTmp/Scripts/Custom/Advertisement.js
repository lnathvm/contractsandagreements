﻿var ProjectInfoId = localStorage.getItem("ProjectInfoId");
var ProjectAdvId = localStorage.getItem("ProjectAdvId");
var subOptionText;
var selected = [];
$(document).ready(function () {
    GetCompensationTypeList();
    GetAllConsultantProviderList();
    GetAllContractTypeList();
    GetAllFeeTypeList();
    GetAllParishList();
    GetAllSelectionList();
    GetAllProjectManagerList();
    GetAllSupervisorList();
    $('#AdvertisementInfoForm').validate({});
    if (ProjectInfoId != null) {
        GetProjectGeneralInformation();
    }
    $('#btnSaveAdvertisement').on('click', function (e) {

        e.preventDefault();
        if (!$("#AdvertisementInfoForm").valid()) {
            alert("Please fill all the required fields.");
            return false;
        }
        ProjectAdvertisement();
    });
    //$('#btnSaveAndAssignAdvertisement').on('click', function (e) {

    //    e.preventDefault();
    //    if (!$("#AdvertisementInfoForm").valid()) {
    //        return false;
    //    }
    //    ProjectAdvertisement();
    //});
    $('#btnCancel').on('click', function (e) {

        window.location.href = localStorage.getItem("BaseUrl") + "Dashboard/AdvertisementDashboard";
    });
    $('#btnAdvertisementDocument').on('click', function (e) {
        window.location.href = localStorage.getItem("BaseUrl") + "Advertisement/AdvertisementDocument";
        return false;
    });
    $('#btnEditAdvertisement').on('click', function (e) {
        $('#btnSaveAdvertisement').prop('disabled', false);
        DisableControls(false);
        $("#btnEditAdvertisement").css("visibility", "hidden");
        // $('#btnEditAdvertisement').prop('disabled', true);
    });

    $('#btnReadMode').on('click', function (e) {
        DisableControls(true);

    });
    $('#btnEditMode').on('click', function (e) {
        DisableControls(false);
        $('#btnSave').prop('disabled', false);
    });


    $('#btnSendEmail').on('click', function (event) {
        event.preventDefault();

        var url = "../Advertisement/SendEmail?SPN=" + $("#txtStateProjectNo").val() + "&SupId=" + $("#ddlSupervisor").val();
        $.ajax({
            type: "POST",
            url: url,
            contentType: "application/json",
            dataType: "json",
            success: function (data) {

            },
            error: function (err) { }
        });
    });

    $('#btnAddendum').on('click', function (e) {
        window.location.href = localStorage.getItem("BaseUrl") + "Advertisement/Addendum";
        return false;
    });
    $('#btnAward').on('click', function (e) {
        window.location.href = "../Advertisement/Award";
        return false;
    });

    $('#ddlCompensationType').multiselect({
        //templates: { // Use the Awesome Bootstrap Checkbox structure
        //    li: '<li class="checkList"><a tabindex="0"><div class="aweCheckbox aweCheckbox-danger"><label for=""></label></div></a></li>'
        //},
        onChange: function (option, checked) {
            debugger;
            if (checked) {
                selected.push(option[0].value);
            }
            else {
                selected = selected.filter(function (ele) {
                    return ele != option[0].value;
                });
            }
        },
    });
    $('.multiselect-container div.aweCheckbox').each(function (index) {

        var id = 'multiselect-' + index,
            $input = $(this).find('input');

        // Associate the label and the input
        $(this).find('label').attr('for', id);
        $input.attr('id', id);

        // Remove the input from the label wrapper
        $input.detach();

        // Place the input back in before the label
        $input.prependTo($(this));

        $(this).click(function (e) {
            // Prevents the click from bubbling up and hiding the dropdown
            e.stopPropagation();
        });

    });
});


function GetProjectGeneralInformation() {
    $.ajax({
        type: "GET",
        url: localStorage.getItem("BaseUrl") + "Advertisement/GetProjectAdvertisementDetails?ProjectInfoId=" + ProjectInfoId,
        contentType: "application/json",
        dataType: "json",
        success: function (response) {
            if (response.Result.Status == 1) {
                if (response.Result.Result != null) {
                    localStorage.setItem("ProjectAdvId", response.Result.Result.ProjectAdvertisementDetailsId);
                    if (response.Result.Result.ProjectAdvertisementDetailsId != 0) {
                        alertify.alert('Success!', 'Going to update.', function () {
                        });
                    }
                    var result = response.Result.Result;
                    $("#txtStateProjectNo").val(result.Information.StateProjectNo);
                    $("#txtContractNo").val(result.Information.ContractNo);

                    $("#txtFAPNo").val(result.Information.FAPNo);
                    $("#txtProjectName").val(result.Information.ProjectName);
                    $("#txtRoute").val(result.Information.Route);
                    $("#ddlParish").val(result.Information.Parish);
                    $("#txtGeneralDescription").val(result.Information.GeneralDescription);
                    $("#ddlSelection").val(result.Information.Selection);
                    result.Information.FHWAFullOversight == true ? $('#rdoFHWAFullOversight1').prop('checked', true) : $('#rdoFHWAFullOversight1').prop('checked', false);
                    $("#ddlContractType").val(result.Information.ContractType);
                    $("#ddlFeeType").val(result.Information.FeeType);
                    $("#txtEstContractAmount").val("$" + result.Information.EStContractAmount);
                    if (result.AdvertisedContractAmount != null) {
                        $("#txtAdvertisedContractAmount").val("$" + result.AdvertisedContractAmount);
                    }

                    $("#ddlProjectManager").val(result.ProjectManagerId);
                    $("#txtManagerPhone").val(result.ManagerPhone);

                    //$("#txtProjectDetails").val(result.ProjectDetails);
                    //$("#txtMinimumManPower").val(result.MinimumManPower);
                    //$("#txtFundingSource").val(result.FundingSource);
                    $("#txtContractTime").val(result.ContractTime);
                    $("#txtManagerPhone").val(result.ManagerPhone);
                    $("#txtDBEGoel").val(result.Information.DBEGoel + "%");
                    if (result.Information.CompensationType != null && result.Information.CompensationType.length != 0) {
                        $.each(result.Information.CompensationType.split(","), function (i, e) {
                            if (e != "") {
                                $("#ddlCompensationType option[value='" + e + "']").attr("selected", 1);
                                $("#ddlCompensationType").multiselect("refresh");
                                selected.push(e);
                            }
                        });
                    }
                    //WorkFlow Tracking
                    if (result.ProjectWork != null) {
                        $("#ddlSupervisor").val(result.ProjectWork.Supervisor);
                        $("#txtProcessor").val(result.ProjectWork.Processor);
                        $("#txtPhone").val(result.ProjectWork.Phone);
                        $("#txtComments").val(result.ProjectWork.Comments);
                        result.ProjectWork.Suspended == true ? $('#rdoSuspended').prop('checked', true) : $('#rdoSuspended').prop('checked', false);
                        $("#txtInfoRecAndLogged").val(result.ProjectWork.InfoReceived);

                        if (result.ProjectWork.TaskAssignedToStaff != null) {
                            $("#txtTaskAssignedToStaff").val(result.ProjectWork.TaskAssignedToStaff.split(' ')[0]);
                        }
                        $("#txtChiefApprovaltoRetain").val(result.ProjectWork.ChiefApprovalToRetain);
                        $("#txtDrafttoSupervisor").val(result.ProjectWork.DraftToSupervisor);
                        $("#txtApprovedfromSupervisor").val(result.ProjectWork.ApprovedFromSupervisor);
                        $("#txtDrafttoProjectManager").val(result.ProjectWork.DraftToProjectManager);
                        $("#txtApprovedFromProjectManager").val(result.ProjectWork.ApprovedFromProjectManager);
                        //$("#txtDrafttoChiefEngineer").val(result.ProjectWork.DraftToChiefEngineer);
                        //$("#txtApprovedfromChiefEngineer").val(result.ProjectWork.ApprovedFromChiefEngineer);
                        $("#txtDrafttoFHWA").val(result.ProjectWork.DraftToFHWA);
                        $("#txtApprovedfromFHWA").val(result.ProjectWork.ApprovedFromFHWA);
                        // $("#txtFedAideFHWAAuth").val(result.ProjectWork.FedAideFHWA);
                        $("#txtAdvertisementDate").val(result.ProjectWork.AdvertisementDate);
                        $("#txtClosingDate").val(result.ProjectWork.ClosingDate);
                        $("#txtAmendedClosingDate").val(result.ProjectWork.AmendedClosingDate);
                        // $("#txtSecretaryShortlist").val(result.ProjectWork.SecretaryShortlist);
                        $("#txtAwardNotification").val(result.ProjectWork.AwardNotification);
                        // $("#txtTo3rdFloor").val(result.ProjectWork.To3Floor);
                    }
                }
            }
        },
        error: function (result) {

        }
    });
}



function ProjectAdvertisement() {

    var projectAdvertisement = {};
    projectAdvertisement.ProjectGeneralInformationId = ProjectInfoId;
    //projectAdvertisement.ProjectDetails = $("#txtProjectDetails").val();
    //projectAdvertisement.MinimumManPower = $("#txtMinimumManPower").val();
    //projectAdvertisement.FundingSource = $("#txtFundingSource").val();
    projectAdvertisement.ContractTime = $("#txtContractTime").val();
    projectAdvertisement.ProjectManagerId = $("#ddlProjectManager").val();
    projectAdvertisement.ManagerPhone = $("#txtManagerPhone").val();
    projectAdvertisement.AdvertisedContractAmount = $("#txtAdvertisedContractAmount").val().replace("$", " ");

    //Save ProjectWork
    projectAdvertisement.ProjectWork = {};

    if ($("#ddlSupervisor").val() != "") {
        var projectwork = {};
        projectwork.Supervisor = $("#ddlSupervisor").val();
        projectwork.Processor = $("#txtProcessor").val();
        projectwork.Phone = $("#txtPhone").val();
        projectwork.Comments = $("#txtComments").val();
        projectwork.Suspended = $("#Suspended1").prop('checked');
        projectwork.InfoReceived = $("#txtInfoRecAndLogged").val();
        projectwork.TaskAssignedToStaff = $("#txtTaskAssignedToStaff").val();
        projectwork.ChiefApprovalToRetain = $("#txtChiefApprovaltoRetain").val();
        projectwork.DraftToSupervisor = $("#txtDrafttoSupervisor").val();
        projectwork.ApprovedFromSupervisor = $("#txtApprovedfromSupervisor").val();
        projectwork.DraftToProjectManager = $("#txtDrafttoProjectManager").val();
        projectwork.ApprovedFromProjectManager = $("#txtApprovedFromProjectManager").val();
        //projectwork.DraftToChiefEngineer = $("#txtDrafttoChiefEngineer").val();
        //projectwork.ApprovedFromChiefEngineer = $("#txtApprovedfromChiefEngineer").val();
        projectwork.DraftToFHWA = $("#txtDrafttoFHWA").val();
        projectwork.ApprovedFromFHWA = $("#txtApprovedfromFHWA").val();
        // projectwork.FedAideFHWA = $("#txtFedAideFHWAAuth").val();
        projectwork.AdvertisementDate = $("#txtAdvertisementDate").val();
        projectwork.ClosingDate = $("#txtClosingDate").val();
        projectwork.AmendedClosingDate = $("#txtAmendedClosingDate").val();
        // projectwork.SecretaryShortlist = $("#txtSecretaryShortlist").val();
        projectwork.AwardNotification = $("#txtAwardNotification").val();
        //  projectwork.To3Floor = $("#txtTo3rdFloor").val();
        projectAdvertisement.ProjectWork = projectwork;
    }
    projectAdvertisement.Information = {};
    if ($("#txtStateProjectNo").val() != "") {
        var information = {};
        information.ProjectGeneralInformationId = ProjectInfoId;
        information.ContractType = $("#ddlContractType").val();
        information.FAPNo = $("#txtFAPNo").val();
        information.FeeType = $("#ddlFeeType").val();
        information.FHWAFullOversight = $("#rdoFHWAFullOversight1").prop('checked');
        information.GeneralDescription = $("#txtGeneralDescription").val();
        information.Parish = $("#ddlParish").val();
        information.ProjectName = $("#txtProjectName").val();
        information.Route = $("#txtRoute").val();
        information.Selection = $("#ddlSelection").val();
        information.StateProjectNo = $("#txtStateProjectNo").val();
        information.ContractNo = $("#txtContractNo").val();
        information.DBEGoel = $("#txtDBEGoel").val().replace("%", "");
        projectAdvertisement.Information = information;
    }
    var url = localStorage.getItem("BaseUrl") + "Advertisement/CreateProjectAdvertisementDetails";


    $.ajax({
        type: "POST",
        url: url,
        contentType: "application/json",
        dataType: "json",
        data: JSON.stringify(projectAdvertisement),
        success: function (data) {
            if (data.Result.Status == 1) {
                $('#divLogInformationPanel').css('display', 'block');
                alertify.alert('Success!', 'Record has been saved successfully.', function () {
                    window.location.href = localStorage.getItem("BaseUrl") + "Dashboard/AdvertisementDashboard";


                });
            }
            else if (data.Result.Status == 3) {
                $('#divLogInformationPanel').css('display', 'block');
                alertify.alert('Success!', 'Record has been updated successfully.', function () {
                    window.location.href = localStorage.getItem("BaseUrl") + "Dashboard/AdvertisementDashboard";
                });
            }
        },
        error: function (err) { }
    });
    return false;
}
function DisableControls(value) {
    //$("#txtStateProjectNo").prop('disabled', value);
    //$("#txtContractNo").prop('disabled', value);
    $("#txtFAPNo").prop('disabled', value);
    //$("#txtProjectName").prop('disabled', value);
    $("#txtRoute").prop('disabled', value);
    $("#ddlParish").prop('disabled', value);
    $("#txtGeneralDescription").prop('disabled', value);
    //$("#ddlSelection").prop('disabled', value);
    //$("#ddlContractType").prop('disabled', value);
    //$("#ddlFeeType").prop('disabled', value);
    $("#txtAdvertisedContractAmount").prop('disabled', value);
    $("#ddlProjectManager").prop('disabled', value);
    $("#txtManagerPhone").prop('disabled', value);
    $("#txtProjectDetails").prop('disabled', value);
    //$("#txtMinimumManPower").prop('disabled', value);
    $("#txtFundingSource").prop('disabled', value);
    $("#txtContractTime").prop('disabled', value);
    $("#ddlProjectManager").prop('disabled', value);
    $("#txtManagerPhone").prop('disabled', value);

    //WorkFlow Tracking
    $("#txtSupervisor").prop('disabled', value);
    $("#txtProcessor").prop('disabled', value);
    $("#txtPhone").prop('disabled', value);
    $("#txtComments").prop('disabled', value);
    $("#txtInfoRecAndLogged").prop('disabled', value);

    $("#txtTaskAssignedToStaff").prop('disabled', value);
    $("#txtChiefApprovaltoRetain").prop('disabled', value);
    $("#txtDrafttoSupervisor").prop('disabled', value);
    $("#txtApprovedfromSupervisor").prop('disabled', value);
    $("#txtDrafttoProjectManager").prop('disabled', value);
    $("#txtApprovedFromProjectManager").prop('disabled', value);
    //$("#txtDrafttoChiefEngineer").prop('disabled', value);
    // $("#txtApprovedfromChiefEngineer").prop('disabled', value);
    $("#txtDrafttoFHWA").prop('disabled', value);
    $("#txtApprovedfromFHWA").prop('disabled', value);
    // $("#txtFedAideFHWAAuth").prop('disabled', value);
    $("#txtAdvertisementDate").prop('disabled', value);
    $("#txtClosingDate").prop('disabled', value);
    $("#txtAmendedClosingDate").prop('disabled', value);
    //  $("#txtSecretaryShortlist").prop('disabled', value);
    $("#txtAwardNotification").prop('disabled', value);
    // $("#txtTo3rdFloor").prop('disabled', value);
}





//var ProjectInfoId = localStorage.getItem("ProjectInfoId");
//var ProjectAdvId = localStorage.getItem("ProjectAdvId");
//var subOptionText;

//$(document).ready(function () {
//    $('#AdvertisementInfoForm').validate({});
//    if (ProjectInfoId != null) {
//        GetProjectGeneralInformation();
//    }
//    $('#btnSaveAdvertisement').on('click', function (e) {
//       
//        e.preventDefault();
//        if (!$("#AdvertisementInfoForm").valid()) {
//            return false;
//        }
//        ProjectAdvertisement();
//    });
//    //$('#btnSaveAndAssignAdvertisement').on('click', function (e) {
//    //   
//    //    e.preventDefault();
//    //    if (!$("#AdvertisementInfoForm").valid()) {
//    //        return false;
//    //    }
//    //    ProjectAdvertisement();
//    //});
//    $('#btnCancel').on('click', function (e) {
//        window.location.href = "../Dashboard/AdvertisementDashboard";
//    });

//    $('#btnEditAdvertisement').on('click', function (e) {
//        $('#btnSaveAdvertisement').prop('disabled', false);
//        EnableDisable(false);
//        $("#btnEditAdvertisement").css("visibility", "hidden");
//        // $('#btnEditAdvertisement').prop('disabled', true);
//    });


$('#btnSaveAndAssignAdvertisement').on('click', function (event) {
    event.preventDefault();
    debugger;
    var url = "../Advertisement/SendEmail?SPN=" + $("#txtStateProjectNo").val() + "&SupId=" + $("#ddlSupervisor").val();
    $.ajax({
        type: "POST",
        url: url,
        contentType: "application/json",
        dataType: "json",
        success: function (data) {

        },
        error: function (err) { }
    });
});



function GetAllSuperVisorList() {
    $.ajax({
        type: "GET",
        url: "../Advertisement/GetAllSuperVisorList",
        contentType: "application/json",
        dataType: "json",
        success: function (response) {

            var options = "<option value='' selected='selected'>--Select Sub--</option>";
            if (response.Result.Result != undefined && response.Result.Result.length > 0) {
                $.each(response.Result.Result, function (key, value) {
                    options += '<option value="' + value.SuperVisorId + '" taxId="' + value.TaxId + '">' + value.SuperVisorName + '</option>';
                });
            }
            subOptionText = options;
            $('#txtSupervisor').html(options);
        },
        error: function (result) {
            subOptionText = options;
            var options = "<option value='' selected='selected'>--Select Sub--</option>";
            $('#txtSupervisor').html(options);
        }
    });

}
