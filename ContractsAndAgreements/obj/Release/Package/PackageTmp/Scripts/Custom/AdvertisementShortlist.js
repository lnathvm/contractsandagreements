﻿$(document).ready(function () {
    var table = $('#tblPjrojectInfo').DataTable({
        "ajax": localStorage.getItem("BaseUrl") + "Advertisement/GetAllAdvertisementShortlistDetailsList",
        "columns": [
            {
                "data": "Date",
                "render": function (data) {                   
                    if (data != null) {

                        var d = new Date(data.split("/").reverse().join("-"));
                        var dd = d.getDate();
                        var mm = d.getMonth() + 1;
                        var yy = d.getFullYear();
                        var newdate = mm + "-" + dd + "-" + yy;
                        return newdate;

                    }
                    return data;
                    
                }
            },
            { "data": "ProjectAdvertisementShortlistId" },
            { "data": "StateProjectNo" },
            {
                "data": "Type",
                "className": 'RedirectToAdertisement',
                "render": function (data, type, row, meta) {
                    return "<a href='#'>ShortList</a>"
                }
            },
            { "data": "ProjectName" },
            { "data": "Parish" },
            //    { "data": "ClosingDate" },
            //   { "data": "Type", "className":"Hide" },

        ],
        "paging": false,
        "ordering": false,
        "info": false,
        "filter": false,
        "columnDefs": [
            {
                "targets": [1],
                "visible": false,
                "searchable": false
            }
        ],
        //"order": [[1, 'asc']]
    });

    // Add event listener for opening and closing details
    $('#tblPjrojectInfo tbody').on('click', 'td.RedirectToAdertisement', function () {

        var tr = $(this).closest('tr');
        var row = table.row(tr);
        var data = row.data();
        // alert(data);
        //    localStorage.setItem("ProjectInfoId", data.ProjectGeneralInformationId);


        // window.location.href = localStorage.getItem("BaseUrl") + "DashBoard/ContractAdvertisementShortlistDocument?projectadvertisementShortlistId=" + data.ProjectAdDetailsId;
        window.location.href = localStorage.getItem("BaseUrl") + "Advertisement/AdvertisementSortlistDoc?ProjectInfoId=" + data.ProjectAdvertisementShortlistId;

        return false;
    });


});

