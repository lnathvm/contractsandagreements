﻿var ConsultantProviderId = localStorage.getItem("ConsultantProviderId");
$(document).ready(function () {
    $('#AddUpdateConsultantProviderInfoForm').validate({});
    if (ConsultantProviderId != null) {
        GetConsultantProviderInfo();
    }
    $('input[type="checkbox"]').on('change', function () {
        $('input[name="' + this.name + '"]').not(this).prop('checked', false);
    });
    $('#btnSave').on('click', function (e) {

        e.preventDefault();
        if (!$("#AddUpdateConsultantProviderInfoForm").valid()) {
            alert("Please fill all the required fields.");
            return false;
        }
        SaveConsultantProvider();
    });

    $('#btnCancel').on('click', function (e) {
        window.location.href = localStorage.getItem("BaseUrl") + "Common/ConsultantProvider";
        return false;
    });

});

function GetConsultantProviderInfo() {
    $.ajax({
        type: "GET",
        url: localStorage.getItem("BaseUrl") + "Common/GetConsultantProviderDetail?ConsultantProviderId=" + ConsultantProviderId,
        contentType: "application/json",
        dataType: "json",
        success: function (response) {

            if (response.Result.Status == 1) {
                if (response.Result.Result != null) {
                    debugger;
                    var result = response.Result.Result;
                    $("#txtConsultantProviderName").val(result.ConsultantProviderName);
                    $("#txtContactName").val(result.ContactName);
                    $("#txtTitle").val(result.Title);
                    $("#txtJobTitle").val(result.JobTitle);
                    $("#txtFax").val(result.Fax);
                    $("#txtTaxId").val(result.TaxId);
                    $("#txtEmail").val(result.Email);
                    $("#txtPhone").val(result.Phone);
                    $("#txtOfficeAddress").val(result.OfficeAddress);
                    $("#txtCity").val(result.City);
                    $("#txtState").val(result.State);
                    $("#txtZip").val(result.Zip);
                    result.ProofofInsurance == true ? $('#rdoProofofInsurance1').prop('checked', true) : $('#rdoProofofInsurance2').prop('checked', true);
                    result.TypeofInsurance == true ? $('#rdoTypeofInsurance1').prop('checked', true) : $('#rdoTypeofInsurance2').prop('checked', true);
                    $("#txtInsuranceCompany").val(result.InsuranceCompany);
                    $("#txtEffectiveDateofCoverage").val(result.EffectiveDateofCoverage);
                    $("#txtExpirationDateofCoverage").val(result.ExpirationDateofCoverage);
                    result.DisclosureofOwnership == true ? $('#rdoDisclosureofOwnership1').prop('checked', true) : $('#rdoDisclosureofOwnership2').prop('checked', true);
                    result.CertificateofAuthority == true ? $('#rdoCertificateofAuthority1').prop('checked', true) : $('#rdoCertificateofAuthority2').prop('checked', true);
                    result.ResolutionCertificate == true ? $('#rdoResolutionCertificate1').prop('checked', true) : $('#rdoResolutionCertificate2').prop('checked', true);
                    $("#txtEffectiveDate").val(result.EffectiveDate);
                    $("#txtPersonAuthorizedToSign").val(result.PersonAuthorizedToSign);
                    $("#txtComments").val(result.Comments);
                    $.each($('input[type="checkbox"]'), function (key, value) {
                        if ($(this).val() == result.Type) {
                            $('input[name="' + this.name + '"]').prop('checked', true);
                            $('input[name="' + this.name + '"]').not(this).prop('checked', false);
                        }
                    });

                }
            }
        },
        error: function (result) {

        }
    });
}


function SaveConsultantProvider() {
    var project = {};
    project.ConsultantProviderId = ConsultantProviderId;
    project.ConsultantProviderName = $("#txtConsultantProviderName").val();
    project.ContactName = $("#txtContactName").val();
    project.Title = $("#txtTitle").val();
    project.JobTitle = $("#txtJobTitle").val();
    project.Fax = $("#txtFax").val();
    project.TaxId = $("#txtTaxId").val();
    project.Email = $("#txtEmail").val();
    project.Phone = $("#txtPhone").val();
    project.OfficeAddress = $("#txtOfficeAddress").val();
    project.City = $("#txtCity").val();
    project.State = $("#txtState").val();
    project.Zip = $("#txtZip").val();
    project.IsActive = true;
    project.ProofofInsurance = $("#rdoTypeofInsurance1").prop('checked');
    project.InsuranceCompany = $("#txtInsuranceCompany").val();
    project.EffectiveDateofCoverage = $("#txtEffectiveDateofCoverage").val();
    project.ExpirationDateofCoverage = $("#txtExpirationDateofCoverage").val();
    project.DisclosureofOwnership = $("#rdoDisclosureofOwnership1").prop('checked');
    project.CertificateofAuthority = $("#rdoCertificateofAuthority1").prop('checked');
    project.ResolutionCertificate = $("#rdoResolutionCertificate1").prop('checked');
    project.EffectiveDate = $("#txtEffectiveDate").val();
    project.PersonAuthorizedToSign = $("#txtPersonAuthorizedToSign").val();
    project.Comments = $("#txtComments").val();
    $.each($('input[type="checkbox"]'), function (key, value) {
        if ($(this).prop('checked')) {
            project.Type = $(this).val();
        }
    });


    var url = localStorage.getItem("BaseUrl") + "Common/SaveConsultantProvider";

    $.ajax({
        type: "POST",
        url: url,
        contentType: "application/json",
        dataType: "json",
        data: JSON.stringify(project),
        success: function (data) {
            if (data.Result.Status == 1) {
                alertify.alert('Success!', 'Record has been saved successfully.', function () {
                });
            }
            else if (data.Result.Status == 3) {
                alertify.alert('Success!', 'Record has been updated successfully.', function () {
                });
            }
        },
        error: function (err) { }
    });
    return false;
}
