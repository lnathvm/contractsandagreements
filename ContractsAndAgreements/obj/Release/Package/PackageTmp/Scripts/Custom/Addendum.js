﻿var ProjectInfoId = localStorage.getItem("ProjectInfoId");
var ProjectAdvId = localStorage.getItem("ProjectAdvId");
var baseURL = localStorage.getItem("BaseUrl");


$(document).ready(function () {
    GetAllParishList();
    GetAllSupervisorList();
    GetAllSelectionList();
    $('#AddendumInfoForm').validate({});
    if (ProjectInfoId != null) {
        GetProjectGeneralInformation();
    }
    $('#btnSaveAddendum').on('click', function (e) {
        
        e.preventDefault();
        //if (!$("#AddendumInfoForm").valid()) {
        //    return false;
        //}
        alert("Please fill all the required fields.");
        SaveProjectAddendum();
    });

    $('#btnCancel').on('click', function (e) {
        window.location.href = localStorage.getItem("BaseUrl") + "Dashboard/AdvertisementDashboard";
        return false;
    });
    $('#btnAddendumDocument').on('click', function (e) {
        

        window.location.href = localStorage.getItem("BaseUrl") + "Advertisement/AddendumDocumentDetails";
        return false;
    });
});

function GetProjectGeneralInformation() {
    $.ajax({
        type: "GET",
        url: localStorage.getItem("BaseUrl") + "Advertisement/GetProjectAdvertisementDetails?ProjectInfoId=" + ProjectInfoId,
        contentType: "application/json",
        dataType: "json",
        success: function (response) {
            debugger;
            if (response.Result.Status == 1) {
                if (response.Result.Result != null) {
                    localStorage.setItem("ProjectAddendumId", response.Result.Result.ProjectAddendumId);
                    var result = response.Result.Result;
                    if (result.Information.StateProjectNo != null) {
                        $("#txtStateProjectNo").val(result.Information.StateProjectNo);
                    }
                    $("#txtContractNo").val(result.Information.ContractNo);
                    $("#txtFAPNo").val(result.Information.FAPNo);
                    $("#txtProjectName").val(result.Information.ProjectName);
                    $("#txtRoute").val(result.Information.Route);
                    $("#ddlParish").val(result.Information.Parish);
                    $("#txtGeneralDescription").val(result.Information.GeneralDescription);
                    $("#ddlSelection").val(result.Information.Selection);
                    //result.Information.FHWAFullOversight == true ? $('#rdoFHWAFullOversight1').prop('checked', true) : $('#rdoFHWAFullOversight1').prop('checked', false);
                    //$("#ddlContractType").val(result.Information.ContractType);
                    //$("#ddlFeeType").val(result.Information.FeeType);
                    if (result.Information.AdvertisedContractAmount) {
                        $("#txtContractAmountAdjustment").val("$" + result.Information.AdvertisedContractAmount);
                    }
                    $("#txtDBEGoel").val(result.Information.DBEGoel + "%");
                    //$("#ddlProjectManager").val(result.ProjectManagerId);
                    //$("#txtManagerPhone").val(result.ManagerPhone);

                    $("#txtProjectDetails").val(result.ProjectDetails);
                    //$("#txtMinimumManPower").val(result.MinimumManPower);
                    //$("#txtFundingSource").val(result.FundingSource);
                    //  $("#txtContractTime").val(result.ContractTime);

                    //WorkFlow Tracking

                    $("#ddlSupervisor").val(result.ProjectWork.Supervisor);
                    $("#txtProcessor").val(result.ProjectWork.Processor);
                    $("#txtPhone").val(result.ProjectWork.Phone);
                    $("#txtComments").val(result.ProjectWork.Comments);
                    $("#txtInfoRecAndLogged").val(result.ProjectWork.InfoReceived);
                    if (result.ProjectWork.TaskAssignedToStaff != null) {
                        $("#txtTaskAssignedToStaff").val(result.ProjectWork.TaskAssignedToStaff.split(' ')[0]);
                    }
                    //$("#txtAddendumDate").val(result.ProjectWork.AddendumDate);
                    //$("#txtClosingDate").val(result.ProjectWork.ClosingDate);
                }
            }
        },
        error: function (result) {

        }
    });
}
function SaveProjectAddendum() {


    var projectAddendum = {};
    projectAddendum.ProjectGeneralInformationId = ProjectInfoId;
    // projectAddendum.ProjectDetails = $("#txtProjectDetails").val();
    //projectAddendum.MinimumManPower = $("#txtMinimumManPower").val();
    //projectAddendum.FundingSource = $("#txtFundingSource").val();
    //projectAddendum.ContractTime = $("#txtContractTime").val();
    //projectAddendum.ProjectManagerId = $("#ddlProjectManager").val();
    //projectAddendum.ManagerPhone = $("#txtManagerPhone").val();
    projectAddendum.AddendumComments = $("#txtAddendumComments").val();
    projectAddendum.FAPNo = $("#txtFAPNo").val();
    projectAddendum.Route = $("#txtRoute").val();
    projectAddendum.GeneralDescription = $("#txtGeneralDescription").val();
    projectAddendum.AddenDescription = $("#txtAddenDescription").val();
    //projectAddendum.FeeType = $("#ddlFeeType").val();
    //projectAddendum.ContractType = $("#ddlContractType").val();
    projectAddendum.ContractAmountAdjustment = $("#txtContractAmountAdjustment").val().replace("$", "");
    projectAddendum.DBEGoel = $("#txtDBEGoel").val().replace("%", "");
    //Save ProjectWork
    projectAddendum.ProjectWork = {};
    if ($("#ddlSupervisor").val() != "") {
        var projectwork = {};
        projectwork.SupervisorId = $("#ddlSupervisor").val();
        projectwork.Processor = $("#txtProcessor").val();
        projectwork.Phone = $("#txtPhone").val();
        projectwork.Comments = $("#txtComments").val();
        projectwork.InfoReceived = $("#txtInfoRecAndLogged").val();
        projectwork.TaskAssignedToStaff = $("#txtTaskAssignedToStaff").val();
        projectwork.AddendumDate = $("#txtAddendumDate").val();
        projectwork.ClosingDate = $("#txtClosingDate").val();
        projectAddendum.ProjectWork = projectwork;
    }


    var url = localStorage.getItem("BaseUrl") + "Advertisement/CreateProjectAddendumDetails";

    $.ajax({
        type: "POST",
        url: url,
        contentType: "application/json",
        dataType: "json",
        data: JSON.stringify(projectAddendum),
        success: function (data) {
            if (data.Result.Status == 1) {
                alertify.alert('Success!', 'Record has been saved successfully.', function () {
                    localStorage.setItem("ProjectAddendumId", data.Result.Result);
                    window.location.href = localStorage.getItem("BaseUrl")+"/DashBoard/AdvertisementDashBoard";
                });
            }
        },
        error: function (err) { }
    });
    return false;
}


