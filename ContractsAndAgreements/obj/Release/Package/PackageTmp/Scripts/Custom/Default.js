﻿var defaultText = "Phone and Intranet";

function WaterMark(txt, evt) {
    if (txt.value.length == 0 && evt.type == "blur") {
        txt.style.color = "gray";
        txt.value = defaultText;
    }
    if (txt.value == defaultText && evt.type == "focus") {
        txt.style.color = "black";
        txt.value = "";
    }
}