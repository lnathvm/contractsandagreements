﻿var ProjectInfoId = localStorage.getItem("ProjectInfoId");
var ProjectAdvSelectionId = localStorage.getItem("ProjectAdvSelectionId");
var subOptionText;

$(document).ready(function () {
    GetShortlistsList();
    $('#SelectionForm').validate({});
    if (ProjectInfoId != null) {
        GetAdvertisementDetail(ProjectInfoId);
    }
    $('#btnSaveSelection').on('click', function (e) {
        
        e.preventDefault();
        if (!$("#SelectionForm").valid()) {
            alert("Please fill all the required fields.");
            return false;
        }
        ProjectAdvertisementSelection();
    });

    $('#btnCancel').on('click', function (e) {
        window.location.href = localStorage.getItem("BaseUrl") + "Dashboard/AdvertisementDashboard";
        return false;
    });
    $('#btnAdvertisementDocument').on('click', function (e) {
        window.location.href = localStorage.getItem("BaseUrl") + "Advertisement/AdvertisementDocument";
        return false;
    });

    $('#btnAddendum').on('click', function (e) {
        window.location.href = localStorage.getItem("BaseUrl") + "Advertisement/Addendum";
        return false;
    });
    $('#btnAdd').on('click', function (e) {
        AddNewRow();
    });

    $('#btnEditSelection').on('click', function (e) {
        $('#btnSaveSelection').prop('disabled', false);
        EnableDisable(false);
        $("#btnEditSelection").css("visibility", "hidden");
        // $('#btnEditAdvertisement').prop('disabled', true);
    });


});

function GetAdvertisementDetail(ProjectGeneralInformationId) {
    $.ajax({
        type: "GET",
        url: localStorage.getItem("BaseUrl") + "Advertisement/GetAdvertisementDetail?ProjectGeneralInformationId=" + ProjectGeneralInformationId,
        contentType: "application/json",
        dataType: "json",
        success: function (response) {

            if (response.data != null) {
                var result = response.data;
                $("#txtAdvertisementDate").val(result.AdvertisementDate);
                $("#txtClosingDate").val(result.ClosingDate);
                $("#txtContractNo").val(result.ProjectGeneralInformationId);
                $("#txtStateProjectno").val(result.StateProjectno);
            }
        },
        error: function (result) {

        }
    });
}


function GetShortlistsList() {

    $.ajax({
        type: "GET",
        url: localStorage.getItem("BaseUrl") + "Advertisement/GetShortListConsultantProviderList?ProjectGeneralInformationId=" + ProjectInfoId,
        contentType: "application/json",
        dataType: "json",
        success: function (response) {

            var options = "<option value='' selected='selected'>--Select --</option>";
            if (response.Result.Result != undefined && response.Result.Result.length > 0) {
                $.each(response.Result.Result, function (key, value) {
                    options += '<option value="' + value.Id + '">' + value.Name + '</option>';
                });
            }
            $('#ddlSelection1').html(options);
        },
        error: function (result) {
            var options = "<option value='' selected='selected'>--Select --</option>";
            $('#ddlSelection1').html(options);
        }
    });
}

function GetProjectSelectionInformation() {
    $.ajax({
        type: "GET",
        url: localStorage.getItem("BaseUrl") + "Advertisement/GetProjectAdvertisementSelectionDetailsByProjectInfoId?ProjectInfoId=" + ProjectInfoId,
        contentType: "application/json",
        dataType: "json",
        success: function (response) {

            if (response.Result.Status == 1) {
                if (response.Result.Result != null) {
                    var result = response.Result.Result;


                    // $("#txtTitle39").val(result.Title39);
                    $("#txtAdvertisementDate").val(result.AdvertisementDate);
                    $("#txtClosingDate").val(result.ClosingDate);
                    $("#txtAmmendedClosingDate").val(result.AmmendedClosingDate);
                    $("#txtSelectionDate").val(result.SelectionDate);
                    $("#txtContractNo").val(result.ContractNo);
                    $("#txtStateProjectno").val(result.StateProjectno);
                    $("#txtSubject").val(result.Subject);
                    $("#txtComment1").val(result.Comment1);
                    $("#txtComment2").val(result.Comment1);

                }
            }

        },
        error: function (result) {

        }
    });
}
function ProjectAdvertisementSelection() {

    var ProjectAdvSelection = {};
    ProjectAdvSelection.ProjectGeneralInformationId = ProjectInfoId;
    // ProjectAdvSelection.Title39 = $("#txtTitle39").val();
    ProjectAdvSelection.AdvertisementDate = $("#txtAdvertisementDate").val();
    ProjectAdvSelection.ClosingDate = $("#txtClosingDate").val();
    ProjectAdvSelection.AmmendedClosingDate = $("#txtAmmendedClosingDate").val();
    ProjectAdvSelection.SelectionDate = $("#txtSelectionDate").val();
    ProjectAdvSelection.ContractNo = $("#txtContractNo").val();
    ProjectAdvSelection.StateProjectno = $("#txtStateProjectno").val();
    ProjectAdvSelection.Subject = $("#txtSubject").val();
    ProjectAdvSelection.Selection = $("#ddlSelection1").val();
    ProjectAdvSelection.Comment1 = $("#txtComment1").val();
    ProjectAdvSelection.Comment2 = $("#txtComment2").val();

    var url = localStorage.getItem("BaseUrl") + "Advertisement/CreateProjectAdvertisementSelectionDetails";

    $.ajax({
        type: "POST",
        url: url,
        contentType: "application/json",
        dataType: "json",
        data: JSON.stringify(ProjectAdvSelection),
        success: function (data) {
            if (data.Result.Status == 1 || data.Result.Status == 3) {
                $('#divLogInformationPanel').css('display', 'block');
                alertify.alert('Success!', 'Record has been saved successfully.', function () {
                    window.location.href = localStorage.getItem("BaseUrl") + "Dashboard/AdvertisementDashboard";
                    // $('#btnSaveAdvertisement').prop('disabled', true);
                    // EnableDisable(true);
                    //// $('#btnEditAdvertisement').prop('disabled', false);
                    // $("#btnEditAdvertisement").css("visibility", "visible");


                });
            }
        },
        error: function (err) { }
    });
    return false;
}
//function EnableDisable(value) {
//    $("#txtTitle39").prop('disabled', value);
//    $("#txtAdvertisementDate").prop('disabled', value);
//    $("#txtClosingDate").prop('disabled', value);
//    $("#txtAmmendedClosingDate").prop('disabled', value);
//    $("#txtSelectionDate").prop('disabled', value);
//    $("#txtContractNo").prop('disabled', value);
//    $("#txtStateProjectno").prop('disabled', value);
//    $("#txtSubject").prop('disabled', value);
//    $("#ddlSelection").prop('disabled', value);
//}