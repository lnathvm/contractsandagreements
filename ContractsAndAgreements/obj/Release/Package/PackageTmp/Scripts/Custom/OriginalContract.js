﻿var ProjectInfoId = localStorage.getItem("ProjectInfoId");
var ProjectAdvId = localStorage.getItem("ProjectAdvId");
var subOptionText;

$(document).ready(function () {
    GetAllConsultantProviderList();
    GetAllContractTypeList();
    GetAllFeeTypeList();
    GetAllParishList();
    GetAllSelectionList();
    GetAllProjectManagerList();
    GetAllSupervisorList();
    GetAllConsultantAreaSubTaxList();
    $('#OriginalContractForm').validate({});
    if (ProjectInfoId != null) {
        GetProjectGeneralInformation();
    }
    $('#btnOriginalContract').on('click', function (e) {

        e.preventDefault();
        if (!$("#OriginalContractForm").valid()) {
            alert("Please fill all the required fields.");
            return false;
        }
        SaveOriginalContract();
    });

    $('#btnCancel').on('click', function (e) {
        window.location.href = localStorage.getItem("BaseUrl") + "Dashboard/AdvertisementDashboard";
        return false;
    });

    $('#btnAdd').on('click', function (e) {
        AddNewRow();
    });

});

function GetProjectGeneralInformation() {
    $.ajax({
        type: "GET",
        url: localStorage.getItem("BaseUrl") + "Advertisement/GetProjectAdvertisementDetails?ProjectInfoId=" + ProjectInfoId,
        contentType: "application/json",
        dataType: "json",
        success: function (response) {

            if (response.Result.Status == 1) {
                if (response.Result.Result != null) {
                    localStorage.setItem("ProjectAdvId", response.Result.Result.ProjectAdvertisementDetailsId);
                    var result = response.Result.Result;
                    $("#txtStateProjectNo").val(result.Information.StateProjectNo);
                    $("#txtContractNo").val(result.Information.ContractNo);

                    $("#txtFAPNo").val(result.Information.FAPNo);
                    $("#txtProjectName").val(result.Information.ProjectName);
                    $("#txtRoute").val(result.Information.Route);
                    $("#ddlParish").val(result.Information.Parish);
                    $("#txtGeneralDescription").val(result.Information.GeneralDescription);
                    $("#ddlSelection").val(result.Information.Selection);
                    result.Information.FHWAFullOversight == true ? $('#rdoFHWAFullOversight1').prop('checked', true) : $('#rdoFHWAFullOversight1').prop('checked', false);
                    $("#ddlContractType").val(result.Information.ContractType);
                    $("#ddlFeeType").val(result.Information.FeeType);
                    $("#txtAdvertisedContractAmount").val("$" + result.AdvertisedContractAmount);
                    $("#ddlProjectManager").val(result.ProjectManagerId);
                    $("#txtManagerPhone").val(result.ManagerPhone);

                    $("#txtProjectDetails").val(result.ProjectDetails);
                    //$("#txtMinimumManPower").val(result.MinimumManPower);
                    //$("#txtFundingSource").val(result.FundingSource);
                    $("#txtContractTime").val(result.ContractTime);
                    $("#txtManagerPhone").val(result.ManagerPhone);
                    $("#txtDBEGoel").val(result.Information.DBEGoel + "%");
                    //WorkFlow Tracking
                    if (result.ProjectWork != null) {
                        $("#ddlSupervisor").val(result.ProjectWork.Supervisor);
                        $("#txtProcessor").val(result.ProjectWork.Processor);
                        $("#txtPhone").val(result.ProjectWork.Phone);
                        $("#txtComments").val(result.ProjectWork.Comments);
                        result.ProjectWork.Suspended == true ? $('#rdoSuspended').prop('checked', true) : $('#rdoSuspended').prop('checked', false);
                        $("#txtInfoRecAndLogged").val(result.ProjectWork.InfoReceived);

                        if ($("#txtTaskAssignedToStaff").val(result.ProjectWork.TaskAssignedToStaff != null)) {
                            $("#txtTaskAssignedToStaff").val(result.ProjectWork.TaskAssignedToStaff.split(' ')[0]);
                        }
                        $("#txtChiefApprovaltoRetain").val(result.ProjectWork.ChiefApprovalToRetain);
                        $("#txtDrafttoSupervisor").val(result.ProjectWork.DraftToSupervisor);
                        $("#txtApprovedfromSupervisor").val(result.ProjectWork.ApprovedFromSupervisor);
                        $("#txtDrafttoProjectManager").val(result.ProjectWork.DraftToProjectManager);
                        $("#txtApprovedFromProjectManager").val(result.ProjectWork.ApprovedFromProjectManager);
                        //$("#txtDrafttoChiefEngineer").val(result.ProjectWork.DraftToChiefEngineer);
                        //$("#txtApprovedfromChiefEngineer").val(result.ProjectWork.ApprovedFromChiefEngineer);
                        $("#txtDrafttoFHWA").val(result.ProjectWork.DraftToFHWA);
                        $("#txtApprovedfromFHWA").val(result.ProjectWork.ApprovedFromFHWA);
                        // $("#txtFedAideFHWAAuth").val(result.ProjectWork.FedAideFHWA);
                        $("#txtAdvertisementDate").val(result.ProjectWork.AdvertisementDate);
                        $("#txtClosingDate").val(result.ProjectWork.ClosingDate);
                        $("#txtAmendedClosingDate").val(result.ProjectWork.AmendedClosingDate);
                        // $("#txtSecretaryShortlist").val(result.ProjectWork.SecretaryShortlist);
                        $("#txtAwardNotification").val(result.ProjectWork.AwardNotification);
                        // $("#txtTo3rdFloor").val(result.ProjectWork.To3Floor);
                    }
                }
            }
        },
        error: function (result) {

        }
    });
}


function SaveOriginalContract() {
    var project = {};
    project.ProjectGeneralInformationId = ProjectInfoId;
    project.ContractAction = $("#ContractAction").val();
    project.ContractExpries = $("#ContractExpries").val();
    project.Commnets = $("#Commnets").val();
    project.InfoRecAndLogged = $("#InfoRecAndLogged").val();
    project.TaskAssignedToStaff = $("#TaskAssignedToStaff").val();
    project.DrafttoSupervisor = $("#DrafttoSupervisor").val();
    project.ApprovedfromSupervisor = $("#ApprovedfromSupervisor").val();
    project.FeePackagetoChiefEngineer = $("#FeePackagetoChiefEngineer").val();
    project.ApprovedFromFeePackagetoChiefEngineer = $("#ApprovedFromFeePackagetoChiefEngineer").val();
    project.FeeNegotiationsTOProjMgr = $("#FeeNegotiationsTOProjMgr").val();
    project.Complete = $("#Complete").val();
    project.FedAideFHWAANDAuth = $("#FedAideFHWAANDAuth").val();
    project.Authorized = $("#Authorized").val();
    project.DraftToManagerForReview = $("#DraftToManagerForReview").val();
    project.DraftToManagerForReviewConcurred = $("#DraftToManagerForReviewConcurred").val();
    project.DraftToConsultantForReview = $("#DraftToConsultantForReview").val();
    project.DraftToConsultantForReviewConcurred = $("#DraftToConsultantForReviewConcurred").val();
    project.ContractToConsultant = $("#ContractToConsultant").val();
    project.ContractTo3rdFloor = $("#ContractTo3rdFloor").val();
    project.SignedExecuted = $("#SignedExecuted").val();
    project.NTPLetter = $("#NTPLetter").val();
    project.NTPDate = $("#NTPDate").val();
    project.SubTaxInformation = {};
    if ($("#txtStateProjectNo").val() != "") {
        var information = {};
        information.ContractAmount = $("#txtContractAmount").val().replace("$", "");
        //information.TOAmount = $("#txtTOAmount").val().replace("$", "");
        //information.Prime = $("#txtPrime").val();
        //information.PrimeAmount = $("#txtPrimeAmount").val().replace("$", "");
        information.SubTaxDetailsDto = [];
        $("#tbodySubTab tr.item").each(function () {
            var TaxDetailsDto = {};
            $(this).find('td :input').each(function () {
                if ($(this).attr('id').indexOf('ddlSub') > -1) {
                    TaxDetailsDto.SubId = $(this).val();
                }
                else if ($(this).attr('id').indexOf('txtTaxId') > -1) {
                    TaxDetailsDto.TaxId = $(this).val();
                }
                else if ($(this).attr('id').indexOf('txtCAmount') > -1) {
                    TaxDetailsDto.ContractAmount = $(this).val();
                }
            });
            if (TaxDetailsDto.SubId != 0 && TaxDetailsDto.SubId != "" && TaxDetailsDto.SubId != undefined) {
                information.SubTaxDetailsDto.push(TaxDetailsDto);
            }
        });
        project.SubTaxInformation = information;
    }

    var url = localStorage.getItem("BaseUrl") + "Advertisement/CreateUpdateProjectOriginalContractDetails";

    $.ajax({
        type: "POST",
        url: url,
        contentType: "application/json",
        dataType: "json",
        data: JSON.stringify(project),
        success: function (data) {
            if (data.Result.Status == 1 || data.Result.Status == 3) {
                alertify.alert('Success!', 'Record has been saved successfully.', function () {
                });
            }
        },
        error: function (err) { }
    });
    return false;
}
