﻿/* Formatting function for row details - modify as you need */
function format(d) {
    var html = '';
    // `d` is the original data object for the row
    var url = localStorage.getItem("BaseUrl") + "Project/GetAllProjectContractAndTaskOrderList?ProjectGeneralInformationId=" + d.ProjectGeneralInformationId;
    $.ajax({
        type: 'GET',
        url: url,
        async: false,
        success: function (obj) {
            if (obj.data != null) {
                debugger;
                $.each(obj.data, function (i, r) {
                    if (r.ProjectContractDetailsId != null) {
                        html += '<tr>';
                        html += '<td>ContractInfo</td>';
                        html += '<td></td>';
                        html += '<td><a href="" class="editor_edit" onclick="OpenUrl(' + d.ProjectGeneralInformationId + ',\'Contract\'); return false;">Edit</a></td>';
                        html += '</tr>';
                    }
                    if (r.ProjectOriginalContractDetailId != null) {
                        html += '<tr>';
                        html += '<td>TaskOrderInfo</td>';
                        html += '<td></td>';
                        html += '<td><a href="" class="editor_edit" onclick="OpenUrl(' + d.ProjectGeneralInformationId + ',\'TaskOrder\'); return false;">Edit</a></td>';
                        html += '</tr>';
                    }
                    if (r.ProjectSupplementalAgreementDetailId != null) {
                        html += '<tr>';
                        html += '<td>SupplementalAgreementInfo</td>';
                        html += '<td></td>';
                        html += '<td><a href="" class="editor_edit" onclick="OpenUrl(' + d.ProjectGeneralInformationId + ',\SupplementalAgreement\'); return false;">Edit</a></td>';
                        html += '</tr>';
                    }
                    if (r.ProjectSupplementalAgreementForIDIQId != null) {
                        html += '<tr>';
                        html += '<td>SupplementalAgreementForIDIQInfo</td>';
                        html += '<td></td>';
                        html += '<td><a href="" class="editor_edit" onclick="OpenUrl(' + d.ProjectGeneralInformationId + ',\'SupplementalAgreementForIDIQ\'); return false;">Edit</a></td>';
                        html += '</tr>';
                    }

                });
            }

        }
    })
    return '<table cellpadding="5" cellspacing="0" class="dataTableSubTable" border="0" style="padding-left:50px;">' +
        '<tr>' +
        '<td>ProjectInfo</td>' +
        '<td>' + d.StateProjectNo + '</td>' +
        '<td><a href="" class="editor_edit" onclick="OpenUrl(' + d.ProjectGeneralInformationId + ',\'Project\'); return false;">Edit</a></td>' +
        '<td><a href="" class="editor_Delete" onclick="DeleteProject(' + d.ProjectGeneralInformationId + ',\'Project\'); return false;">Delete</a></td>' +
        '</tr>' +
        html
    '</table>';
}



function OpenUrl(id, type) {
    var url;
    if (id == null) {
        localStorage.removeItem("ProjectInfoId");
    }
    else {
        localStorage.setItem("ProjectInfoId", id);
    }
    switch (type) {
        case "Project":
            url = localStorage.getItem("BaseUrl") + "Project/ProjectInfo";
            break;
        case "Contract":
            url = ".../Advertisement/Contract";
            break;
        case "OriginalContract":
            url = localStorage.getItem("BaseUrl") + "Advertisement/OriginalContract";
            break;
        case "TaskOrder":
            url = localStorage.getItem("BaseUrl") + "Advertisement/TaskOrder";
            break;
        case "SupplementalAgreement":
            url = localStorage.getItem("BaseUrl") + "Advertisement/SupplementalAgreement";
            break;
        case "SupplementalAgreementForIDIQ":
            url = localStorage.getItem("BaseUrl") + "Advertisement/SupplementalAgreementForIDIQ";
            break;
    }
    window.location.href = url;
    return false;
}
function DeleteProject(id, type) {
    debugger;
    alertify.confirm('Alert!', 'Do you want to delete this?', function () {
        var url = localStorage.getItem("BaseUrl") + "Project/DeleteProjectInformation?ProjectInfoId=" + id;
        $.ajax({
            type: 'GET',
            url: url,
            async: false,
            success: function (obj) {
                debugger;
                if (data.Result.Status == 1 || data.Result.Status == 3) {
                    alertify.alert('Success!', 'Record has been deleted successfully.', function () {
                        window.location.reload();
                    });
                }
            },
            error: function (err) { }
        });
    }
        , function () { });

}


//function RedirectToProjectInfo(id) {
//    localStorage.setItem("ProjectInfoId", id);
//    window.location.href = "../Project/ProjectInfo";
//    return false;
//}
//function RedirectToContractDetail(contractid, ProjectId) {
//    localStorage.setItem("ProjectInfoId", ProjectId);
//    localStorage.setItem("ProjectContractDetailId", contractid);
//    window.location.href = "../Advertisement/Contract";
//    return false;
//}
//function RedirectToTaskOrder(taskOrdertid, ProjectId) {
//    localStorage.setItem("ProjectInfoId", ProjectId);
//    localStorage.setItem("ProjectTaskOrderId", taskOrdertid);
//    window.location.href = "../Advertisement/TaskOrder";
//    return false;
//}

$(document).ready(function () {
    var table = $('#tblPjrojectInfo').DataTable({
        "ajax": localStorage.getItem("BaseUrl") + "Project/GetAllProjectGeneralInformationList",
        "columns": [
            {
                "className": 'details-control',
                "orderable": false,
                "data": null,
                "defaultContent": ''
            },
            { "data": "ProjectGeneralInformationId" },
            { "data": "ContractNo" },
            { "data": "StateProjectNo" },
            { "data": "FAPNo" },
            { "data": "ProjectName" },
            { "data": "EStContractAmount" },
            { "data": "DBEGoel" },
            //{
            //    data: null,
            //    className: "center editor_edit",
            //    defaultContent: '<a href="" class="editor_edit">Edit</a>'
            //}
        ],
        "columnDefs": [
            {
                "targets": [1],
                "visible": false,
                "searchable": false
            }
        ],
        "order": [[1, 'asc']]
    });

    // Add event listener for opening and closing details
    $('#tblPjrojectInfo tbody').on('click', 'td.editor_edit', function () {
        var tr = $(this).closest('tr');
        var row = table.row(tr);
        var data = row.data();
        localStorage.setItem("ProjectInfoId", data.ProjectGeneralInformationId);
        window.location.href = "Project/ProjectInfo";
        return false;
    });

    $('#tblPjrojectInfo tbody').on('click', 'td.details-control', function () {
        var tr = $(this).closest('tr');
        var row = table.row(tr);
        if (row.child.isShown()) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        }
        else {
            // Open this row
            row.child(format(row.data())).show();
            tr.addClass('shown');
        }
    });
});




