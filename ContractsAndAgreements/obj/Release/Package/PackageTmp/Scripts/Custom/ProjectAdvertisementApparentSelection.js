﻿var ProjectInfoId = localStorage.getItem("ProjectInfoId");
var ProjectAdvApparentSelectionId = localStorage.getItem("ProjectAdvApparentSelectionId");
var subOptionText;

$(document).ready(function () {
    GetShortlistsList();
    $('#ApparentSelectionForm').validate({});
    if (ProjectInfoId != null) {
        GetAdvertisementDetail(ProjectInfoId);
    }
    $('#btnSaveApparentSelection').on('click', function (e) {
        e.preventDefault();
        if (!$("#ApparentSelectionForm").valid()) {
            alert("Please fill all the required fields.");
            return false;
        }
        ProjectAdvertisementApparentSelection();
    });

    $('#btnCancel').on('click', function (e) {
        window.location.href = localStorage.getItem("BaseUrl") + "Dashboard/AdvertisementDashboard";
        return false;
    });
    $('#btnAdvertisementDocument').on('click', function (e) {
        window.location.href = localStorage.getItem("BaseUrl") + "Advertisement/AdvertisementDocument";
        return false;
    });

    $('#btnAddendum').on('click', function (e) {
        window.location.href = localStorage.getItem("BaseUrl") + "Advertisement/Addendum";
        return false;
    });
    $('#btnAdd').on('click', function (e) {
        AddNewRow();
    });

    $('#btnEditApparentSelection').on('click', function (e) {
        $('#btnSaveApparentSelection').prop('disabled', false);
        EnableDisable(false);
        $("#btnEditApparentSelection").css("visibility", "hidden");
        // $('#btnEditAdvertisement').prop('disabled', true);
    });


});

function GetAdvertisementDetail(ProjectGeneralInformationId) {
    $.ajax({
        type: "GET",
        url: localStorage.getItem("BaseUrl") + "Advertisement/GetAdvertisementDetail?ProjectGeneralInformationId=" + ProjectGeneralInformationId,
        contentType: "application/json",
        dataType: "json",
        success: function (response) {
            if (response.data != null) {
                var result = response.data;
                $("#txtAdvertisementDate").val(result.AdvertisementDate);
                $("#txtClosingDate").val(result.ClosingDate);
                $("#txtContractNo").val(result.ProjectGeneralInformationId);
                $("#txtStateProjectno").val(result.StateProjectno);
            }
        },
        error: function (result) {

        }
    });
}

function GetShortlistsList() {
    $.ajax({
        type: "GET",
        url: localStorage.getItem("BaseUrl") + "Advertisement/GetShortListConsultantProviderList?ProjectGeneralInformationId=" + ProjectInfoId,
        contentType: "application/json",
        dataType: "json",
        success: function (response) {
            var options = "<option value='' selected='selected'>--Select --</option>";
            if (response.Result.Result != undefined && response.Result.Result.length > 0) {
                $.each(response.Result.Result, function (key, value) {
                    options += '<option value="' + value.Id + '">' + value.Name + '</option>';
                });
            }
            $('#ddlApparentSelection').html(options);
        },
        error: function (result) {
            var options = "<option value='' selected='selected'>--Select --</option>";
            $('#ddlApparentSelection').html(options);
        }
    });
}

function GetProjectApparentSelectionInformation() {
    $.ajax({
        type: "GET",
        url: localStorage.getItem("BaseUrl") + "Advertisement/GetAllAdvertisementApparentSelectionDetailsList?ProjectInfoId=" + ProjectInfoId,
        contentType: "application/json",
        dataType: "json",
        success: function (response) {

            if (response.Result.Status == 1) {
                if (response.Result.Result != null) {
                    var result = response.Result.Result;


                    //  $("#txtTitle39").val(result.Title39);
                    $("#txtAdvertisementDate").val(result.AdvertisementDate);
                    $("#txtClosingDate").val(result.ClosingDate);
                    $("#txtAmmendedClosingDate").val(result.AmmendedClosingDate);
                    $("#txtApparentSelectionDate").val(result.ApparentSelectionDate);
                    $("#txtContractNo").val(result.ContractNo);
                    $("#txtStateProjectno").val(result.StateProjectno);
                    $("#txtSubject").val(result.Subject);
                    $("#ddlApparentSelection").val(result.ApparentSelection);
                    $("#txtComment1").val(result.Comment1);
                    $("#txtComment2").val(result.Comment1);

                }
            }

        },
        error: function (result) {

        }
    });
}
function ProjectAdvertisementApparentSelection() {

    var ProjectAdvApparentSelection = {};
    ProjectAdvApparentSelection.ProjectGeneralInformationId = ProjectInfoId;
    //   ProjectAdvApparentSelection.Title39 = $("#txtTitle39").val();
    ProjectAdvApparentSelection.AdvertisementDate = $("#txtAdvertisementDate").val();
    ProjectAdvApparentSelection.ClosingDate = $("#txtClosingDate").val();
    ProjectAdvApparentSelection.AmmendedClosingDate = $("#txtAmmendedClosingDate").val();
    ProjectAdvApparentSelection.ApparentSelectionDate = $("#txtApparentSelectionDate").val();
    ProjectAdvApparentSelection.ContractNo = $("#txtContractNo").val();
    ProjectAdvApparentSelection.StateProjectno = $("#txtStateProjectno").val();
    ProjectAdvApparentSelection.Subject = $("#txtSubject").val();
    ProjectAdvApparentSelection.ApparentSelection = $("#ddlApparentSelection").val();
    ProjectAdvApparentSelection.Comment1 = $("#txtComment1").val();
    ProjectAdvApparentSelection.Comment2 = $("#txtComment2").val();

    var url = localStorage.getItem("BaseUrl") + "Advertisement/CreateUpdateProjectAdvertisementApparentSelectionDetails";

    $.ajax({
        type: "POST",
        url: url,
        contentType: "application/json",
        dataType: "json",
        data: JSON.stringify(ProjectAdvApparentSelection),
        success: function (data) {
            if (data.Result.Status == 1 || data.Result.Status == 3) {
                $('#divLogInformationPanel').css('display', 'block');
                alertify.alert('Success!', 'Record has been saved successfully.', function () {
                    window.location.href = localStorage.getItem("BaseUrl") + "Dashboard/AdvertisementDashboard";
                    // $('#btnSaveAdvertisement').prop('disabled', true);
                    // EnableDisable(true);
                    //// $('#btnEditAdvertisement').prop('disabled', false);
                    // $("#btnEditAdvertisement").css("visibility", "visible");


                });
            }
        },
        error: function (err) { }
    });
    return false;
}
//function EnableDisable(value) {
//    $("#txtTitle39").prop('disabled', value);
//    $("#txtAdvertisementDate").prop('disabled', value);
//    $("#txtClosingDate").prop('disabled', value);
//    $("#txtAmmendedClosingDate").prop('disabled', value);
//    $("#txtApparentSelectionDate").prop('disabled', value);
//    $("#txtContractNo").prop('disabled', value);
//    $("#txtStateProjectno").prop('disabled', value);
//    $("#txtSubject").prop('disabled', value);
//    $("#ddlApparentSelection").prop('disabled', value);
//}
