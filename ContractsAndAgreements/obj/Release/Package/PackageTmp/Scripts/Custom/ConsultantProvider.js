﻿$(document).ready(function () {

    $("#btnAdd").on("click", function () {
        window.location.href = localStorage.getItem("BaseUrl") + "Common/AddUpdateConsultantProvider";
        localStorage.removeItem("ConsultantProviderId");

    });

    var table = $('#tblConsultantProviderInfo').DataTable({
        "ajax": localStorage.getItem("BaseUrl") + "Common/GetAllConsultantProviderList",
        "columns": [
            {
                "className": 'center editor_edit',
                "orderable": false,
                "data": null,
                "defaultContent": '<a href="" class="editor_edit">Edit</a><a href="" class="editor_Delete">Delete</a>'
            },
            { "data": "ConsultantProviderId" },
            { "data": "ConsultantProviderName" },
            { "data": "ContactName" },
            { "data": "Title" },
            { "data": "JobTitle" },
            { "data": "Email" },
            { "data": "OfficeAddress" },
        ],
        "columnDefs": [
            {
                "targets": [1],
                "visible": false,
                "searchable": false
            }
        ],
        "order": [[1, 'asc']]
    });

    // Add event listener for opening and closing details
    $('#tblConsultantProviderInfo tbody').on('click', 'td.editor_edit', function () {
        var tr = $(this).closest('tr');
        var row = table.row(tr);
        var data = row.data();
        localStorage.setItem("ConsultantProviderId", data.ConsultantProviderId);
        window.location.href = localStorage.getItem("BaseUrl") + "Common/AddUpdateConsultantProvider";
        return false;
    });
    $('#tblConsultantProviderInfo tbody').on('click', 'a.editor_Delete', function () {
        var tr = $(this).closest('tr');
        var row = table.row(tr);
        var data = row.data();
        alertify.confirm('Alert!', 'Do you want to delete this. ?', function () {
            debugger;

            var url = localStorage.getItem("BaseUrl") + "Common/DeleteConsultantProvider?ConsultantProviderId=" + data.ConsultantProviderId;

            $.ajax({
                type: "Delete",
                url: url,
                contentType: "application/json",
                dataType: "json",
                success: function (data) {
                    if (data.Result.Status == 2) {
                        alertify.alert('Success!', 'Record has been deleted successfully.', function () {
                            window.location.reload();
                        });
                    }
                },
                error: function (err) { }
            });
        }, function () { });
        return false;
    });
});
