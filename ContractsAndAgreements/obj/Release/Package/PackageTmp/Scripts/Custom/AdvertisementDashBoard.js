﻿/* Formatting function for row details - modify as you need */
function format(d) {

    var html = "";
    if (d.AdvertisementDate != null && d.AdvertisementDate != "") {
        html += '<tr>';
        html += '<td>Advertisement Record</td>';
        html += '<td>' + d.AdvertisementDate + '</td>';
        html += '<td><a href="" class="editor_edit" onclick="RedirectToPage(' + d.ProjectGeneralInformationId + ',\'Advertisement\'); return false;">Edit</a></td>';
        html += '</tr>';

    }
    var htmlwork = "";
    $.ajax({
        type: "GET",
        url: localStorage.getItem("BaseUrl") + "Advertisement/GetProjectAddendumDetails?ProjectInfoId=" + d.ProjectGeneralInformationId,
        contentType: "application/json",
        dataType: "json",
        async: false,
        success: function (response) {
            debugger;
            if (response.Result.Status == 1) {
                if (response.Result.Result != undefined && response.Result.Result.length > 0) {
                    $.each(response.Result.Result, function (key, value) {
                        htmlwork += '<tr>';
                        htmlwork += '<td>Addendum' + (key + 1) + '</td>';
                        htmlwork += '<td>' + value.ProjectWork.AddendumDate + '</td>';
                        htmlwork += '<td><a href="" class="editor_edit" onclick="RedirectToPage(' + value.ProjectGeneralInformationId + ',\'Addendum\'); return false;">Edit</a></td>';
                        htmlwork += '<td><a href="" class="editor_edit" onclick="DeleteAddendum(' + value.ProjectGeneralInformationId + '); return false;">Delete</a></td>';
                        htmlwork += '</tr>';
                    });
                }

            }
        },
        error: function (err) { }
    });
    // `d` is the original data object for the row
    return '<table cellpadding="3" cellspacing="0" class="dataTableSubTable" border="0" style="padding-left:50px;">' +
        '<tr>' +
        '<td>Project Record</td>' +
        '<td>' + d.ProjectDate + '</td>' +
        '<td><a href="" class="editor_edit" onclick="RedirectToPage(' + d.ProjectGeneralInformationId + ',\'Project\'); return false;">Edit</a></td>' +
        '</tr>' +
        html +
        htmlwork +
        '</table>';
}
function RedirectToPage(id, type) {
    debugger;
    var url;
    if (id == null) {
        localStorage.removeItem("ProjectInfoId");
    }
    else {
        localStorage.setItem("ProjectInfoId", id);
    }
    switch (type) {
        case "Project":
            url = localStorage.getItem("BaseUrl") + "Project/ProjectInfo";
            break;
        case "Addendum":
            url = localStorage.getItem("BaseUrl") + "/Advertisement/Addendum";
            break;
        case "Advertisement":
            url = localStorage.getItem("BaseUrl") + "Advertisement/Advertisement";
            break;

    }
    window.location.href = url;
    return false;
}

function DeleteAddendum(id) {
    debugger;
    alertify.confirm('Alert!', 'Do you want to delete this?', function () {
        var url = localStorage.getItem("BaseUrl") + "Advertisement/DeleteProjectAddendum?AddendumId=" + id;
        $.ajax({
            type: 'GET',
            url: url,
            async: false,
            success: function (resp) {
                debugger;
                if (resp.data == 1 || resp.data == 3) {
                    alertify.alert('Success!', 'Record has been deleted successfully.', function () {
                        window.location.reload();
                    });
                }
            },
            error: function (err) { }
        });
    }
        , function () { });
}

//function OpenUrl(type) {
//    var url;
//    switch (type) {
//        case "Project":
//            localStorage.removeItem("ProjectInfoId");
//            url = localStorage.getItem("BaseUrl") + "Project/ProjectInfo";
//    }
//    window.location.href = url;
//}

//function Redirect(id) {
//    //localStorage.setItem("ProjectInfoId", id);
//    //window.location.href = localStorage.getItem("BaseUrl") + "Project/ProjectInfo";
//    return false;
//}

$(document).ready(function () {
    var table = $('#tblPjrojectAdvertisementInfo').DataTable({
        "ajax": localStorage.getItem("BaseUrl") + "Project/GetAllProjectGeneralInformationList",
        "columns": [
            {
                "className": 'details-control',
                "orderable": false,
                "data": null,
                "defaultContent": ''
            },
            { "data": "ProjectGeneralInformationId" },
            { "data": "ContractNo" },
            { "data": "StateProjectNo" },
            { "data": "FAPNo" },
            { "data": "ProjectName" },
            { "data": "EStContractAmount" },
            { "data": "DBEGoel" },
            { "data": "AwardNotification" }
        ],
        "columnDefs": [
            {
                "targets": [1],
                "visible": false,
                "searchable": false
            }
        ],
        "order": [[1, 'asc']]
    });

    // Add event listener for opening and closing details
    //$('#tblPjrojectAdvertisementInfo tbody').on('click', 'td.editor_edit', function () {
    //    var tr = $(this).closest('tr');
    //    var row = table.row(tr);
    //    var data = row.data();
    //    localStorage.setItem("ProjectInfoId", data.ProjectGeneralInformationId);
    //    window.location.href = "Project/ProjectInfo";
    //    return false;
    //});

    $('#tblPjrojectAdvertisementInfo tbody').on('click', 'td.details-control', function () {
        var tr = $(this).closest('tr');
        var row = table.row(tr);
        if (row.child.isShown()) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        }
        else {
            // Open this row
            row.child(format(row.data())).show();
            tr.addClass('shown');
        }
    });
});
