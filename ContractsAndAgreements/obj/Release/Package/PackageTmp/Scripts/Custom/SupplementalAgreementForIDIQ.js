﻿var ProjectInfoId = localStorage.getItem("ProjectInfoId");
var ProjectAdvId = localStorage.getItem("ProjectSupplementalAgreementId");
var subOptionText;

$(document).ready(function () {
    $('#SupplementalAgreementForIDIQForm').validate({});
    GetAllSupervisorList();
    GetAllParishList();
    GetAllSelectionList();
    GetAllProjectManagerList();
    GetAllConsultantAreaSubTaxList();
    if (ProjectInfoId != null) {
        GetProjectGeneralInformation();
    }
    $('#btnSaveSupplementalAgreement').on('click', function (e) {
        debugger;
        e.preventDefault();
        if (!$("#SupplementalAgreementForIDIQForm").valid()) {
            alert("Please fill all the required fields.");
            return false;
        }
        ProjectSupplementalAgreement();
    });

    $('#btnCancel').on('click', function (e) {
        window.location.href = localStorage.getItem("BaseUrl") + "Dashboard/AdvertisementDashboard";
        return false;
    });
    $('#btnAdvertisementDocument').on('click', function (e) {
        window.location.href = localStorage.getItem("BaseUrl") + "Advertisement/AdvertisementDocument";
        return false;
    });

    $('#btnAddendum').on('click', function (e) {
        window.location.href = localStorage.getItem("BaseUrl") + "Advertisement/Addendum";
        return false;
    });
    $('#btnAdd').on('click', function (e) {
        //AddNewRowForIDIQ();
        AddNewRow();
    });

    $('#btnEditContract').on('click', function (e) {
        $('#btnSaveContract').prop('disabled', false);
        $("#btnEditContract").css("visibility", "hidden");
        // $('#btnEditAdvertisement').prop('disabled', true);
    });


    $('#btnSaveAndAssignAdvertisement').on('click', function (event) {
        event.preventDefault();

        var url = localStorage.getItem("BaseUrl") + "Advertisement/SendEmail?SPN=" + $("#txtStateProjectNo").val() + "&SupId=" + $("#ddlSupervisor").val();
        $.ajax({
            type: "POST",
            url: url,
            contentType: "application/json",
            dataType: "json",
            success: function (data) {

            },
            error: function (err) { }
        });
    });
});
//function AddNewRowForIDIQ() {
//    var rows = (document.getElementById("tableSubTab").getElementsByTagName("tbody")[0].getElementsByTagName("tr").length);
//    var html = '';
//    html += '<tr class="odd gradeX item" id="tr' + rows + '">';
//    html += '<td> <div class="col-lg-12">';
//    html += ' <div class="form-group">';
//    html += '     <label>Sub #' + rows + '</label>';
//    html += '       <select id="ddlSub' + rows + '" name="Sub' + rows + '" required class="form-control" onchange="SetTaxId(this); return false;">';
//    html += '           ' + subOptionText + '';
//    html += '       </select>';
//    html += '       </div >';
//    html += '    </div></td>';
//    html += '<td><div class="col-lg-12">';
//    html += '   <div class="form-group">';
//    html += '      <label>TaxID #' + rows + '</label>';
//    html += '     <input id="txtTaxId' + rows + '" name="TaxId' + rows + '" type="text" class="form-control">';
//    html += '      </div>';
//    html += '  </div></td>';
//    html += '<td><div class="col-lg-12">';
//    html += '   <div class="form-group">';
//    html += '      <label>Information Amount #' + rows + '</label>';
//    // html += '     <input id="txtCAmount' + rows + '" name="CAmount' + rows + '" type="text" class="form-control">';
//    html += '<input id="txtCAmount' + rows + '" pattern="^\$\d{1,3}(,\d{3})*(\.\d+)?$" value="" data-type="currency" placeholder="$1,000,000.00" type="text" name="CAmount' + rows + '" class="form-control">';
//    html += '      </div>';
//    html += '  </div></td>';
//    html += '<td style="width: 9%"><div class="col-lg-12"><input id="btnremove' + rows + '" type="buttom" class="btn btn-danger" value="Remove" style="width: 80px;" onclick="remove(tr' + rows + '); return false;"></div> </td>';
//    html += '</tr>';
//    $('#tbodySubTab').append(html);
//}
//function remove(id) {
//    var dfdf = '#' + id.id;
//    alertify.confirm('Alert!', 'Do you want to delete this?', function () { id.remove(); }
//        , function () { });
//}
//function SetTaxId(ConsultantInfo) {
//    var seletedTaxId = $("#" + ConsultantInfo.id).find('option:selected').attr("taxId");
//    var id = ConsultantInfo.id.split('ddlSub')[1];
//    $("#txtTaxId" + id).val(seletedTaxId);
//}
//function GetAllConsultantAreaSubTaxList() {
//    debugger
//    $.ajax({
//        type: "GET",
//        url: "../Common/GetAllConsultantProviderList",
//        contentType: "application/json",
//        dataType: "json",
//        async: false,
//        success: function (response) {

//            var options = "<option value='' selected='selected'>--Select Sub--</option>";
//            if (response.data != undefined && response.data.length > 0) {
//                $.each(response.data, function (key, value) {
//                    options += '<option value="' + value.ConsultantProviderId + '" taxId="' + value.TaxId + '">' + value.ConsultantProviderName + '</option>';
//                });
//            }
//            subOptionText = options;
//            $('#ddlSub1').html(options);
//        },
//        error: function (result) {
//            subOptionText = options;
//            var options = "<option value='' selected='selected'>--Select Sub--</option>";
//            $('#ddlSub1').html(options);
//        }
//    });
//}

function GetProjectGeneralInformation() {
    $.ajax({
        type: "GET",
        url: localStorage.getItem("BaseUrl") + "Advertisement/GetProjectSupplementalAgreementForIDIQ?ProjectInfoId=" + ProjectInfoId,
        contentType: "application/json",
        dataType: "json",
        success: function (response) {

            if (response.Result.Status == 1) {
                if (response.Result.Result != null) {
                    var result = response.Result.Result;
                    $("#txtStateProjectNo").val(result.Information.StateProjectNo);
                    $("#txtFAPNo").val(result.Information.FAPNo);
                    $("#txtProjectName").val(result.Information.ProjectName);
                    $("#txtRoute").val(result.Information.Route);
                    $("#ddlParish").val(result.Information.Parish);
                    $("#txtGeneralDescription").val(result.Information.GeneralDescription);
                    $("#ddlSelection").val(result.Information.Selection);
                    result.Information.FHWAFullOversight == true ? $('#rdoFHWAFullOversight1').prop('checked', true) : $('#rdoFHWAFullOversight1').prop('checked', false);
                    $("#ddlContractType").val(result.Information.ContractType);
                    $("#ddlFeeType").val(result.Information.FeeType);
                    $("#txtEstContractAmount").val("$" + result.Information.EstContractAmount);
                    $("#ddlProjectManager").val(result.ProjectManagerId);
                    $("#txtManagerPhone").val(result.ManagerPhone);

                    $("#txtTaskForProject").val(result.TaskForProject);
                    $("#txtTaskScope").val(result.TaskScope);
                    $("#txtTaxClassification").val(result.TaxClassification);
                    $("#txtTaskExpirationDate").val(result.TaskExpirationDate);
                    $("#txtContractExpires").val(result.ContractExpires);

                    //WorkFlow Tracking
                    if (result.ProjectWork != null) {
                        //$("#ddlSupervisor").val(result.ProjectWork.Supervisor);
                        //$("#txtProcessor").val(result.ProjectWork.Processor);
                        //$("#txtPhone").val(result.ProjectWork.Phone);
                        //$("#txtComments").val(result.ProjectWork.Comments);
                        //result.ProjectWork.Suspended == true ? $('#rdoSuspended').prop('checked', true) : $('#rdoSuspended').prop('checked', false);
                        //$("#txtInfoRecAndLogged").val(result.ProjectWork.InfoReceived);

                        //$("#txtTaskAssignedToStaff").val(result.ProjectWork.TaskAssignedToStaff.split(' ')[0]);
                        //$("#txtDraftandFeePacktoSupervisor").val(result.ProjectWork.DraftandFeePacktoSupervisor);
                        //$("#txtApprovedfromSupervisor").val(result.ProjectWork.ApprovedfromSupervisor);
                        //$("#txtDraftandFeePacktoProjectManager").val(result.ProjectWork.DraftandFeePacktoProjectManager);
                        //$("#txtConcurredFromProjectManager").val(result.ProjectWork.ConcurredFromProjectManager);
                        //$("#txtDrafttoConsultant").val(result.ProjectWork.DrafttoConsultant);
                        //$("#txtConcurredFromConsultant").val(result.ProjectWork.ConcurredFromConsultant);
                        //$("#txtFedAideFHWAAuth").val(result.ProjectWork.FedAideFHWAAuth);
                        //$("#txtAuthorized").val(result.ProjectWork.Authorized);
                        //$("#txtContractToConsultant").val(result.ProjectWork.ContractToConsultant);
                        //$("#txtSigned").val(result.ProjectWork.Signed);
                        //$("#txtContractTo3rdFloor").val(result.ProjectWork.ContractTo3rdFloor);
                        //$("#txtSignedExecuted").val(result.ProjectWork.SignedExecuted);
                        //$("#txtNTPLetter").val(result.ProjectWork.NTPLetter);
                        //$("#txtEffectiveDate").val(result.ProjectWork.EffectiveDate);
                        //Contract Information
                    }
                    if (result.TaskOrderInformation != null) {
                        $("#txtContractAmount").val("$" + result.TaskOrderInformation.ContractAmount);
                        $("#txtTOAmount").val("$" + result.TaskOrderInformation.TOAmount);
                        $("#txtPrime").val(result.TaskOrderInformation.Prime);
                        $("#txtPrimeAmount").val("$" + result.TaskOrderInformation.PrimeAmount);
                        if (result.TaskOrderInformation.SubTaxDetailsDto != null) {
                            $("#ddlSub0").val(result.TaskOrderInformation.SubTaxDetailsDto[0].SubId);
                            $("#txtTaxId0").val(result.TaskOrderInformation.SubTaxDetailsDto[0].TaxId);
                            $("#txtCAmount0").val("$" + result.TaskOrderInformation.SubTaxDetailsDto[0].ContractAmount);
                            if (result.TaskOrderInformation.SubTaxDetailsDto.length > 1) {
                                for (i = 0; i < result.TaskOrderInformation.SubTaxDetailsDto.length; i++) {
                                    AddNewRow();
                                    $("#ddlSub" + (i + 1) + "").val(result.TaskOrderInformation.SubTaxDetailsDto[i].SubId);
                                    $("#txtTaxId" + (i + 1) + "").val(result.TaskOrderInformation.SubTaxDetailsDto[i].TaxId);
                                    $("#txtCAmount" + (i + 1) + "").val("$" + result.TaskOrderInformation.SubTaxDetailsDto[i].ContractAmount);
                                }
                            }
                        }
                    }
                }
            }
        },
        error: function (result) {

        }
    });
}
function ProjectSupplementalAgreement() {

    var ProjectSupplementalAgreement = {};
    ProjectSupplementalAgreement.ProjectGeneralInformationId = ProjectInfoId;
    ProjectSupplementalAgreement.TaskForProject = $("#txtTaskForProject").val();
    ProjectSupplementalAgreement.TaskScope = $("#txtTaskScope").val();
    ProjectSupplementalAgreement.TaxClassification = $("#txtTaxClassification").val();
    ProjectSupplementalAgreement.TaskExpirationDate = $("#txtTaskExpirationDate").val();
    ProjectSupplementalAgreement.ContractExpires = $("#txtContractExpires").val();
    ProjectSupplementalAgreement.ProjectManagerId = $("#ddlProjectManager").val();
    ProjectSupplementalAgreement.ManagerPhone = $("#txtManagerPhone").val();

    //Save ProjectWork
    ProjectSupplementalAgreement.ProjectWork = {};

    if ($("#ddlSupervisor").val() != "") {
        var projectwork = {};
        projectwork.Supervisor = $("#ddlSupervisor").val();
        projectwork.Processor = $("#txtProcessor").val();
        projectwork.Phone = $("#txtPhone").val();
        projectwork.Comments = $("#txtComments").val();
        projectwork.Suspended = $("#Suspended1").prop('checked');
        projectwork.InfoRecAndLogged = $("#txtInfoRecAndLogged").val();
        projectwork.TaskAssignedToStaff = $("#txtTaskAssignedToStaff").val();
        projectwork.DraftandFeePacktoSupervisor = $("#txtDraftandFeePacktoSupervisor").val();
        projectwork.ApprovedfromSupervisor = $("#txtApprovedfromSupervisor").val();
        projectwork.DraftandFeePacktoProjectManager = $("#txtDraftandFeePacktoProjectManager").val();
        projectwork.ConcurredFromProjectManager = $("#txtConcurredFromProjectManager").val();
        projectwork.DrafttoConsultant = $("#txtDrafttoConsultant").val();
        projectwork.ConcurredFromConsultant = $("#txtConcurredFromConsultant").val();
        //  projectwork.FedAideFHWAAuth = $("#txtFedAideFHWAAuth").val();
        projectwork.Authorized = $("#txtAuthorized").val();
        projectwork.ContractToConsultant = $("#txtContractToConsultant").val();
        projectwork.Signed = $("#txtSigned").val();
        projectwork.ContractTo3rdFloor = $("#txtContractTo3rdFloor").val();
        projectwork.SignedExecuted = $("#txtSignedExecuted").val();
        projectwork.NTPLetter = $("#txtNTPLetter").val();
        projectwork.EffectiveDate = $("#txtEffectiveDate").val();

        ProjectSupplementalAgreement.ProjectWork = projectwork;
    }
    ProjectSupplementalAgreement.TaskOrderInformation = {};
    if ($("#txtStateProjectNo").val() != "") {
        var information = {};
        // information.ProjectGeneralInformationId = ProjectInfoId;
        information.ContractAmount = $("#txtContractAmount").val().replace("$", "");
        information.TOAmount = $("#txtTOAmount").val().replace("$", "");
        //information.Prime = $("#txtPrime").val();
        //information.PrimeAmount = $("#txtPrimeAmount").val().replace("$", "");
        information.SubTaxDetailsDto = [];
        $("#tbodySubTab tr.item").each(function () {
            var TaxDetailsDto = {};
            $(this).find('td :input').each(function () {
                if ($(this).attr('id').indexOf('ddlSub') > -1) {
                    TaxDetailsDto.SubId = $(this).val();
                }
                else if ($(this).attr('id').indexOf('txtTaxId') > -1) {
                    TaxDetailsDto.TaxId = $(this).val();
                }
                else if ($(this).attr('id').indexOf('txtCAmount') > -1) {
                    TaxDetailsDto.ContractAmount = $(this).val();
                }
            });
            if (TaxDetailsDto.SubId != 0 && TaxDetailsDto.SubId != "" && TaxDetailsDto.SubId != undefined) {
                information.SubTaxDetailsDto.push(TaxDetailsDto);
            }
        });
        ProjectSupplementalAgreement.TaskOrderInformation = information;
    }
    var url = localStorage.getItem("BaseUrl") + "Advertisement/CreateProjectSupplementalAgreementForIDIQ";

    $.ajax({
        type: "POST",
        url: url,
        contentType: "application/json",
        dataType: "json",
        data: JSON.stringify(ProjectSupplementalAgreement),
        success: function (data) {
            if (data.Result.Status == 1 || data.Result.Status == 3) {
                $('#divLogInformationPanel').css('display', 'block');
                alertify.alert('Success!', 'Record has been saved successfully.', function () {
                    window.location.href = localStorage.getItem("BaseUrl") + "Dashboard/AdvertisementDashboard";
                });
            }
        },
        error: function (err) { }
    });
    return false;
}
