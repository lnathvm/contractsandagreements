﻿var ProjectAddendumId = localStorage.getItem("ProjectAddendumId");
var ProjectInfoId = localStorage.getItem("ProjectInfoId");
$(document).ready(function () {
    $('#AddendumDocumentForm').validate({});
    GetAllParishList();
    if (ProjectInfoId != null) {
        
        GetProjectGeneralInformation();

        GetAddendumList();
    }
    else
    {
        $("#txtAddendumno").val(1);


        $("#txtStateProjectNo").prop("disabled", true);
        $("#txtContractNo").prop("disabled", true);
        $("#txtFAPNo").prop("disabled", true);
        $("#txtProjectName").prop("disabled", true);
        $("#txtRoute").prop("disabled", true);
        $("#ddlParish").prop("disabled", true);
        $("#txtDBEGoel").prop("disabled", true);
    }
    $('#btnSaveAddendumDocument').on('click', function (e) {
        e.preventDefault();
        if (!$("#AddendumDocumentForm").valid()) {
            alert("Please fill all the required fields.");
            return false;
        }
        ProjectAddendumDocument();
    });
});


function ProjectAddendumDocument() {

    var addDocument = {};
    addDocument.ProjectAddendumId = ProjectAddendumId;
    addDocument.AdvertisementDate = $("#txtAdvertisementDate").val();
    addDocument.ClosingDate = $("#txtClosingDate").val();
    addDocument.AddendumDate = $("#txtAddendumDate").val();
    addDocument.Addendumno = $("#txtAddendumno").val();
    //addDocument.StateProjectNo = $("#txtStateProjectNo").val();
    //addDocument.FederalAidProjectNo = $("#txtFederalAidProjectNo").val();
    //addDocument.Idiqcpm = $("#txtIdiqcpm").val();
    //addDocument.Route = $("#txtRoute").val();
    //addDocument.StateWideParish = $("#txtStateWideParish").val();
    addDocument.ContractNo = $("#txtContractNo").val();
    addDocument.ProjectName = $("#txtProjectName").val();
    addDocument.StateProjectNo = $("#txtStateProjectNo").val();
    addDocument.FAPNo = $("#txtFAPNo").val();
    addDocument.Parish = $("#ddlParish").val();
    addDocument.Comment1 = $("#txtComment1").val();
    addDocument.Comment2 = $("#txtComment2").val();
    addDocument.Comment3 = $("#txtComment3").val();
    addDocument.DBEGoel = $("#txtDBEGoel").val().replace("%", "");
    var url = localStorage.getItem("BaseUrl") + "Advertisement/CreateProjectAddendumDocumentDetails";
    $.ajax({
        type: "POST",
        url: url,
        contentType: "application/json",
        dataType: "json",
        data: JSON.stringify(addDocument),
        success: function (data) {
            if (data.Result.Status == 1 || data.Result.Status == 3) {
                $('#divLogInformationPanel').css('display', 'block');
                alertify.alert('Success!', 'Record has been saved successfully.', function () { });
                window.location.href = "Dashboard/AdvertisementDashboard";
            }
        },
        error: function (err) { }
    });
    return false;
}

function GetProjectGeneralInformation() {
    $.ajax({
        type: "GET",
        url: localStorage.getItem("BaseUrl") + "Advertisement/GetProjectAdvertisementDetails?ProjectInfoId=" + ProjectInfoId,
        contentType: "application/json",
        dataType: "json",
        success: function (response) {

            if (response.Result.Status == 1) {
                if (response.Result.Result != null) {
                    var result = response.Result.Result;
                    $("#txtStateProjectNo").val(result.Information.StateProjectNo);
                    $("#txtContractNo").val(result.Information.ContractNo);
                    $("#txtFAPNo").val(result.Information.FAPNo);
                    $("#txtProjectName").val(result.Information.ProjectName);
                    $("#txtRoute").val(result.Information.Route);
                    $("#ddlParish").val(result.Information.Parish);
                    $("#txtDBEGoel").val(result.Information.DBEGoel + "%");
                    if (result.ProjectWork != null) {
                        $("#txtAdvertisementDate").val(result.ProjectWork.AdvertisementDate);
                        $("#txtClosingDate").val(result.ProjectWork.ClosingDate);
                    }
                }
            }
        },
        error: function (result) {

        }
    });
}

function GetAddendumList() {
    $.ajax({
        type: "GET",
        url: localStorage.getItem("BaseUrl") + "Advertisement/GetProjectAddendumDetails?ProjectInfoId=" + ProjectInfoId,
        contentType: "application/json",
        dataType: "json",
        async: false,
        success: function (response) {
            var len = 0;
            if (response.Result != null) {
                if (response.Result.Status == 1) {
                    if (response.Result.Result != undefined && response.Result.Result.length > 0) {
                        len = response.Result.Result.length;
                    }
                }
            }
            $("#txtAddendumno").val(len + 1);
        },
        error: function (err) { }
    });
}