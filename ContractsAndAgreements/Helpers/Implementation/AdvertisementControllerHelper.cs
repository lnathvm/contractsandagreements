﻿using AutoMapper;
using ContractsAndAgreements.Helpers.Interfaces;
using ContractsAndAgreements_Domain.Dtos;
using ContractsAndAgreements_Domain.Entity;
using ContractsAndAgreements_Domain.Enums;
using ContractsAndAgreements_DomainLogic.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace ContractsAndAgreements.Helpers.Implementation
{
    public class AdvertisementControllerHelper : IAdvertisementControllerHelper
    {
        private readonly IMapper _mapper;
        private readonly IProjectService _projectService;
        private readonly IAdvertisementService _advertisement;
        private readonly ICommunicationControllerHelper _communication;
        public AdvertisementControllerHelper(IMapper mapper, IProjectService projectService, IAdvertisementService advertisement, ICommunicationControllerHelper communication)
        {
            _mapper = mapper;
            _projectService = projectService;
            _advertisement = advertisement;
            _communication = communication;
        }

        public async Task<ResponseDto<StatusCode>> CreateUpdateProjectAdvertisementDetails(ProjectAdvertisementDetailsDto projectAdvertisement)
        {
            try
            {
                if (projectAdvertisement.Information != null && projectAdvertisement.ProjectGeneralInformationId != 0)
                {
                    var mapProInfo = _mapper.Map<ProjectGeneralInformationDto, ProjectGeneralInformation>(projectAdvertisement.Information);
                    var getProInfo = await _projectService.GetProjectGeneralInformation(projectAdvertisement.ProjectGeneralInformationId);
                    if (getProInfo != null)
                    {
                        getProInfo.ContractType = projectAdvertisement.Information.ContractType;
                        //getProInfo.EstContractAmount = projectAdvertisement.Information.EstContractAmount;
                        getProInfo.FAPNo = projectAdvertisement.Information.FAPNo;
                        getProInfo.FeeType = projectAdvertisement.Information.FeeType;
                        getProInfo.FHWAFullOversight = projectAdvertisement.Information.FHWAFullOversight;
                        getProInfo.GeneralDescription = projectAdvertisement.Information.GeneralDescription;
                        getProInfo.Parish = projectAdvertisement.Information.Parish;
                        getProInfo.ProjectName = projectAdvertisement.Information.ProjectName;
                        getProInfo.Route = projectAdvertisement.Information.Route;
                        getProInfo.Selection = projectAdvertisement.Information.Selection;
                        getProInfo.StateProjectNo = projectAdvertisement.Information.StateProjectNo;
                        getProInfo.DBEGoel = projectAdvertisement.Information.DBEGoel;
                        var updateInfo = await _projectService.UpdateProjectGeneralInformation(getProInfo);
                        if (updateInfo != null)
                        {
                            var existingArea = await _projectService.GetProjectConsultantAreaByProjectInfoId(projectAdvertisement.ProjectGeneralInformationId);
                            if (existingArea != null)
                            {
                                existingArea.ManagerPhone = projectAdvertisement.ManagerPhone;
                                existingArea.ProjectManagerId = projectAdvertisement.ProjectManagerId;
                                var updateAre = await _projectService.UpdateProjectConsultantArea(existingArea);
                            }
                            var resAdv = await _advertisement.GetProjectAdvertisementDetailsByProjectInfoId(projectAdvertisement.ProjectGeneralInformationId);
                            if (resAdv != null)
                            {
                                int ProjectWorkFlowTrackingId = 0;
                                if (Convert.ToInt32(resAdv.ProjectWorkFlowTrackingId) != 0)
                                {
                                    ProjectWorkFlowTrackingId = resAdv.ProjectWorkFlowTrackingId;
                                    var resProjectWork = await _advertisement.GetProjectWorkFlowTracking(resAdv.ProjectWorkFlowTrackingId);
                                    if (resProjectWork != null)
                                    {
                                        resProjectWork.AdvertisementDate = projectAdvertisement.ProjectWork.AdvertisementDate;
                                        resProjectWork.AmendedClosingDate = projectAdvertisement.ProjectWork.AmendedClosingDate;
                                        // resProjectWork.ApprovedFromChiefEngineer = projectAdvertisement.ProjectWork.ApprovedFromChiefEngineer;
                                        resProjectWork.ApprovedFromFHWA = projectAdvertisement.ProjectWork.ApprovedFromFHWA;
                                        resProjectWork.ApprovedFromProjectManager = projectAdvertisement.ProjectWork.ApprovedFromProjectManager;
                                        resProjectWork.ApprovedFromSupervisor = projectAdvertisement.ProjectWork.ApprovedFromSupervisor;
                                        resProjectWork.AwardNotification = projectAdvertisement.ProjectWork.AwardNotification;
                                        resProjectWork.ChiefApprovalToRetain = projectAdvertisement.ProjectWork.ChiefApprovalToRetain;
                                        resProjectWork.ClosingDate = projectAdvertisement.ProjectWork.ClosingDate;
                                        resProjectWork.Comments = projectAdvertisement.ProjectWork.Comments;
                                        // resProjectWork.DraftToChiefEngineer = projectAdvertisement.ProjectWork.DraftToChiefEngineer;
                                        resProjectWork.DraftToFHWA = projectAdvertisement.ProjectWork.DraftToFHWA;
                                        resProjectWork.DraftToProjectManager = projectAdvertisement.ProjectWork.DraftToProjectManager;
                                        resProjectWork.DraftToSupervisor = projectAdvertisement.ProjectWork.DraftToSupervisor;
                                        //resProjectWork.FedAideFHWA = projectAdvertisement.ProjectWork.FedAideFHWA;
                                        resProjectWork.InfoReceived = projectAdvertisement.ProjectWork.InfoReceived;
                                        resProjectWork.Phone = projectAdvertisement.ProjectWork.Phone;
                                        resProjectWork.Processor = projectAdvertisement.ProjectWork.Processor;
                                        resProjectWork.Shortlist = projectAdvertisement.ProjectWork.Shortlist;
                                        resProjectWork.Supervisor = projectAdvertisement.ProjectWork.Supervisor;
                                        resProjectWork.Suspended = projectAdvertisement.ProjectWork.Suspended;
                                        resProjectWork.TaskAssignedToStaff = projectAdvertisement.ProjectWork.TaskAssignedToStaff;
                                        resProjectWork.PackettoSecretary = projectAdvertisement.ProjectWork.PackettoSecretary;
                                        var updateWork = await _advertisement.UpdateProjectWorkFlowTracking(resProjectWork);
                                    }
                                }
                                else
                                {
                                    var mapWork = _mapper.Map<ProjectWorkFlowTrackingDto, ProjectWorkFlowTracking>(projectAdvertisement.ProjectWork);
                                    var saveWork = await _advertisement.CreateProjectWorkFlowTracking(mapWork);
                                    if (saveWork != null)
                                    {
                                        ProjectWorkFlowTrackingId = saveWork.ProjectWorkFlowTrackingId;
                                    }
                                }
                                if (projectAdvertisement.ContractTime != null)
                                {
                                    resAdv.ContractTime = projectAdvertisement.ContractTime;
                                    //resAdv.ContractTime = TimeSpan.Parse(projectAdvertisement.ContractTime);
                                }
                                //resAdv.FundingSource = projectAdvertisement.FundingSource;
                                // resAdv.MinimumManPower = projectAdvertisement.MinimumManPower;
                                //resAdv.ProjectDetails = projectAdvertisement.ProjectDetails;
                                resAdv.ProjectWorkFlowTrackingId = ProjectWorkFlowTrackingId;
                                resAdv.AdvertisedContractAmount = projectAdvertisement.AdvertisedContractAmount;
                                var updateAdv = _advertisement.UpdateProjectAdvertisementDetails(resAdv);
                                return new ResponseDto<StatusCode>()
                                {
                                    Status = StatusCode.Updated
                                };
                            }
                            else
                            {
                                var mapWork = _mapper.Map<ProjectWorkFlowTrackingDto, ProjectWorkFlowTracking>(projectAdvertisement.ProjectWork);
                                var saveWork = await _advertisement.CreateProjectWorkFlowTracking(mapWork);
                                if (saveWork != null)
                                {
                                    var mapAdv = _mapper.Map<ProjectAdvertisementDetailsDto, ProjectAdvertisementDetails>(projectAdvertisement);
                                    mapAdv.ProjectWorkFlowTrackingId = saveWork.ProjectWorkFlowTrackingId;
                                    var saveAdv = _advertisement.CreateProjectAdvertisementDetails(mapAdv);
                                }
                                return new ResponseDto<StatusCode>()
                                {
                                    Status = StatusCode.Success
                                };
                            }
                        }

                    }
                    else
                    {
                        return new ResponseDto<StatusCode>()
                        {
                            Status = StatusCode.NotFound
                        };
                    }
                }
                return new ResponseDto<StatusCode>()
                {
                    Status = StatusCode.BadRequest
                };
            }
            catch (Exception ex)
            {
                return new ResponseDto<StatusCode>()
                {
                    Status = StatusCode.BadRequest,
                    Error = ex.InnerException.ToString()
                };
            }
        }

        public async Task<ResponseDto<StatusCode>> CreateProjectWorkFlowTracking(ProjectWorkFlowTrackingDto projectWork)
        {
            try
            {
                var map = _mapper.Map<ProjectWorkFlowTrackingDto, ProjectWorkFlowTracking>(projectWork);
                var save = await _advertisement.CreateProjectWorkFlowTracking(map);
                return new ResponseDto<StatusCode>()
                {
                    Status = StatusCode.Success
                };
            }
            catch (Exception ex)
            {
                return new ResponseDto<StatusCode>()
                {
                    Status = StatusCode.BadRequest,
                    Error = ex.InnerException.ToString()
                };
            }
        }

        public async Task<ResponseDto<ProjectAdvertisementDetailsDto>> GetProjectAdvertisementDetails(int ProjectInfoId)
        {
            try
            {
                ProjectAdvertisementDetailsDto detailsDto = new ProjectAdvertisementDetailsDto();
                var resInfo = await _projectService.GetProjectGeneralInformation(ProjectInfoId);
                var map = _mapper.Map<ProjectGeneralInformation, ProjectGeneralInformationDto>(resInfo);
                if (map != null)
                {
                    detailsDto.Information = map;
                    var resAdv = await _advertisement.GetProjectAdvertisementDetailsByProjectInfoId(ProjectInfoId);
                    if (resAdv != null)
                    {
                        detailsDto.ContractTime = Convert.ToString(resAdv.ContractTime);
                        detailsDto.ProjectAdvertisementDetailsId = resAdv.ProjectAdvertisementDetailsId;
                        detailsDto.ProjectGeneralInformationId = map.ProjectGeneralInformationId;
                        detailsDto.AdvertisedContractAmount = resAdv.AdvertisedContractAmount;

                        var resArea = await _projectService.GetProjectConsultantAreaByProjectInfoId(ProjectInfoId);
                        if (resArea != null)
                        {
                            detailsDto.ProjectManagerId = resArea.ProjectManagerId;
                            detailsDto.ManagerPhone = resArea.ManagerPhone;
                            var maparea = _mapper.Map<ProjectConsultantArea, ProjectConsultantAreaDto>(resArea);
                            map.AreaDto = maparea;
                            var resSubDetail = await _projectService.GetConsultantAreaSubTaxDetails(resArea.ProjectConsultantAreaId);
                            if (resSubDetail.Count > 0)
                            {
                                var mapareasub = _mapper.Map<IList<ConsultantAreaSubTaxDetailsDto>>(resSubDetail);
                                map.SubTaxDetailsDto = mapareasub;
                            }
                        }
                        var resProjectWork = await _advertisement.GetProjectWorkFlowTracking(resAdv.ProjectWorkFlowTrackingId);
                        if (resProjectWork != null)
                        {
                            var mapWork = _mapper.Map<ProjectWorkFlowTracking, ProjectWorkFlowTrackingDto>(resProjectWork);
                            detailsDto.ProjectWork = mapWork;
                        }
                    }
                }
                return new ResponseDto<ProjectAdvertisementDetailsDto>()
                {
                    Status = StatusCode.Success,
                    Result = detailsDto
                };
            }
            catch (Exception ex)
            {
                return new ResponseDto<ProjectAdvertisementDetailsDto>()
                {
                    Status = StatusCode.BadRequest,
                    Error = ex.InnerException.ToString()
                };
            }
        }


        public async Task<ResponseDto<int>> CreateProjectAddendumDetails(ProjectAddendumDetailsDto projectAddendum)
        {
            try
            {
                var mapWorkFlow = _mapper.Map<ProjectAddendumWorkFlowTrackingDto, ProjectAddendumWorkFlowTracking>(projectAddendum.ProjectWork);
                var createWorkflow = await _advertisement.CreateProjectAddendumWorkFlowTracking(mapWorkFlow);
                if (createWorkflow != null)
                {
                    var objAddenm = _mapper.Map<ProjectAddendumDetailsDto, ProjectAddendumDetails>(projectAddendum);
                    //var objAddenm = new ProjectAddendumDetails()
                    //{
                    //    AddendumComments = projectAddendum.AddendumComments,
                    //    ProjectAddendumWorkFlowId = createWorkflow.ProjectAddendumWorkFlowId,
                    //    ProjectGeneralInformationId = projectAddendum.ProjectGeneralInformationId
                    //};
                    objAddenm.ProjectAddendumWorkFlowId = createWorkflow.ProjectAddendumWorkFlowId;
                    var createAdden = await _advertisement.CreateProjectAddendumDetails(objAddenm);
                    if (createAdden != null)
                    {
                        var objPublished = new ProjectPublishedDetails()
                        {
                            AdvertismentId = createAdden.ProjectAddendumId,
                            AdvertismentType = "Addendum",
                            IsAdvertisment = false,
                            ProjectGeneralInformationId = projectAddendum.ProjectGeneralInformationId
                        };

                        var CreatePublished = await _advertisement.CreateProjectPublishedDetails(objPublished);
                        return new ResponseDto<int>()
                        {
                            Status = StatusCode.Success,
                            Result = createAdden.ProjectAddendumId
                        };
                    }

                }
                return new ResponseDto<int>()
                {
                    Status = StatusCode.BadRequest
                };
            }
            catch (Exception ex)
            {
                return new ResponseDto<int>()
                {
                    Status = StatusCode.BadRequest,
                    Error = ex.InnerException.ToString()
                };
            }
        }

        public async Task<ResponseDto<IList<ProjectAddendumDetailsDto>>> GetProjectAddendumDetails(int ProjectInfoId)
        {
            try
            {
                var getAddendum = await _advertisement.GetProjectAddendumDetails(ProjectInfoId);
                var map = _mapper.Map<IList<ProjectAddendumDetailsDto>>(getAddendum);
                foreach (var item in map)
                {
                    var getworkFlow = await _advertisement.GetProjectAddendumWorkFlowDetails(item.ProjectAddendumWorkFlowId);
                    var mapworkflow = _mapper.Map<ProjectAddendumWorkFlowTracking, ProjectAddendumWorkFlowTrackingDto>(getworkFlow);
                    item.ProjectWork = mapworkflow;
                }
                return new ResponseDto<IList<ProjectAddendumDetailsDto>>()
                {
                    Status = StatusCode.Success,
                    Result = map
                };

            }
            catch (Exception ex)
            {
                return new ResponseDto<IList<ProjectAddendumDetailsDto>>()
                {
                    Status = StatusCode.BadRequest
                };
            }
        }

        public async Task<ResponseDto<StatusCode>> CreateUpdateProjectContractDetails(ProjectContractDetailsDto projectContract)
        {
            try
            {
                if (projectContract.ContractInformation != null && projectContract.ProjectGeneralInformationId != 0)
                {
                    //var mapProInfo = _mapper.Map<ProjectGeneralInformationDto, ProjectGeneralInformation>(projectContract.Information);
                    var mapSubTaxDetails = _mapper.Map<IList<ContractInformationSubTaxDetails>>(projectContract.ContractInformation.SubTaxDetailsDto);


                    var existingArea = await _projectService.GetProjectConsultantAreaByProjectInfoId(projectContract.ProjectGeneralInformationId);
                    if (existingArea != null)
                    {
                        existingArea.ManagerPhone = projectContract.ManagerPhone;
                        existingArea.ProjectManagerId = projectContract.ProjectManagerId;
                        var updateAre = await _projectService.UpdateProjectConsultantArea(existingArea);
                    }
                    var resAdv = await _advertisement.GetProjectContractDetailsByProjectInfoId(projectContract.ProjectGeneralInformationId);
                    if (resAdv != null)
                    {
                        int ProjectContractWorkFlowTrackingId = 0;
                        int ProjectContractInformationId = 0;
                        if (Convert.ToInt32(resAdv.ProjectContractWorkFlowTrackingId) != 0)
                        {
                            ProjectContractWorkFlowTrackingId = resAdv.ProjectContractWorkFlowTrackingId;
                            var resProjectWork = await _advertisement.GetProjectContractWorkFlowTracking(resAdv.ProjectContractWorkFlowTrackingId);
                            if (resProjectWork != null)
                            {
                                resProjectWork.Supervisor = projectContract.ProjectWork.Supervisor;
                                resProjectWork.Suspended = projectContract.ProjectWork.Suspended;
                                resProjectWork.TaskAssignedToStaff = projectContract.ProjectWork.TaskAssignedToStaff;
                                resProjectWork.InfoReceived = projectContract.ProjectWork.InfoReceived;
                                resProjectWork.Phone = projectContract.ProjectWork.Phone;
                                resProjectWork.Processor = projectContract.ProjectWork.Processor;
                                resProjectWork.Comments = projectContract.ProjectWork.Comments;

                                resProjectWork.DraftandFeePacktoSupervisor = projectContract.ProjectWork.DraftandFeePacktoSupervisor;
                                resProjectWork.ApprovedfromSupervisor = projectContract.ProjectWork.ApprovedfromSupervisor;
                                resProjectWork.DraftandFeePacktoProjectManager = projectContract.ProjectWork.DraftandFeePacktoProjectManager;
                                resProjectWork.ConcurredFromProjectManager = projectContract.ProjectWork.ConcurredFromProjectManager;
                                resProjectWork.DrafttoConsultant = projectContract.ProjectWork.DrafttoConsultant;
                                resProjectWork.ConcurredFromConsultant = projectContract.ProjectWork.ConcurredFromConsultant;
                                resProjectWork.FedAideFHWAAuth = projectContract.ProjectWork.FedAideFHWAAuth;
                                resProjectWork.Authorized = projectContract.ProjectWork.Authorized;
                                resProjectWork.ContractToConsultant = projectContract.ProjectWork.ContractToConsultant;
                                resProjectWork.Signed = projectContract.ProjectWork.Signed;
                                resProjectWork.ContractTo3rdFloor = projectContract.ProjectWork.ContractTo3rdFloor;
                                resProjectWork.SignedExecuted = projectContract.ProjectWork.SignedExecuted;
                                resProjectWork.NTPLetter = projectContract.ProjectWork.NTPLetter;
                                resProjectWork.EffectiveDate = projectContract.ProjectWork.EffectiveDate;
                                var updateWork = await _advertisement.UpdateProjectContrcatWorkFlowTracking(resProjectWork);
                            }
                        }
                        else
                        {
                            var mapWork = _mapper.Map<ProjectContractWorkFlowTrackingDto, ProjectContractWorkFlowTracking>(projectContract.ProjectWork);
                            var saveWork = await _advertisement.CreateProjectContractWorkFlowTracking(mapWork);
                            if (saveWork != null)
                            {
                                ProjectContractWorkFlowTrackingId = saveWork.ProjectContractWorkFlowTrackingId;
                            }
                        }
                        if (Convert.ToInt32(resAdv.ProjectContractInformationId) != 0)
                        {
                            ProjectContractInformationId = resAdv.ProjectContractInformationId;
                            var resInfo = await _advertisement.GetProjectContractInformation(resAdv.ProjectContractInformationId);
                            if (resInfo != null)
                            {
                                resInfo.ContractAmount = Convert.ToDecimal(projectContract.ContractInformation.ContractAmount); ;
                                resInfo.Description = projectContract.ContractInformation.Description;
                                resInfo.Prime = projectContract.ContractInformation.Prime;
                                resInfo.PrimeAmount = Convert.ToDecimal(projectContract.ContractInformation.PrimeAmount);
                                var updateInformation = await _advertisement.UpdateProjectContractatInformation(resInfo);

                                var mapInfo = _mapper.Map<IList<ContractInformationSubTaxDetails>>(projectContract.ContractInformation.SubTaxDetailsDto);
                                if (ProjectContractInformationId != 0 && mapInfo.Count > 0)
                                {
                                    var delete = await _advertisement.DeleteContractInformationSubTaxDetails(ProjectContractInformationId);
                                    foreach (var item in mapInfo)
                                    {

                                        item.CreatedAt = DateTime.Now;
                                        item.ProjectContractInformationId = ProjectContractInformationId;
                                    }
                                    var respconsultantDetails = await _advertisement.CreateContractInformationSubTaxDetails(mapInfo);
                                }
                            }
                        }
                        else
                        {
                            var mapInfo = _mapper.Map<ProjectContractInformationDto, ProjectContractInformation>(projectContract.ContractInformation);
                            var saveInfo = await _advertisement.CreateProjectContractInformation(mapInfo);
                            var mapSubTax = _mapper.Map<IList<ContractInformationSubTaxDetails>>(projectContract.ContractInformation.SubTaxDetailsDto);

                            foreach (var item in mapSubTax)
                            {
                                item.CreatedAt = DateTime.Now;
                                item.ProjectContractInformationId = saveInfo.ProjectContractInformationId;
                            }
                            var respContractDetails = await _advertisement.CreateContractInformationSubTaxDetails(mapSubTax);

                            if (saveInfo != null)
                            {
                                ProjectContractInformationId = saveInfo.ProjectContractInformationId;
                            }
                        }



                        //if (projectAdvertisement.ContractTime != null)
                        //{
                        //    resAdv.ContractTime = TimeSpan.Parse(projectAdvertisement.ContractTime);
                        //}
                        resAdv.ContractAction = projectContract.ContractAction;
                        resAdv.ExpDate = projectContract.ExpDate;
                        resAdv.ProjectDetails = projectContract.ProjectDetails;
                        resAdv.ProjectContractWorkFlowTrackingId = ProjectContractWorkFlowTrackingId;
                        resAdv.ProjectContractInformationId = ProjectContractInformationId;

                        var updateAdv = _advertisement.UpdateProjectContractDetails(resAdv);
                    }
                    else
                    {
                        var mapWork = _mapper.Map<ProjectContractWorkFlowTrackingDto, ProjectContractWorkFlowTracking>(projectContract.ProjectWork);
                        if (mapWork != null)
                        {
                            var saveWork = await _advertisement.CreateProjectContractWorkFlowTracking(mapWork);
                            var mapInfo = _mapper.Map<ProjectContractInformationDto, ProjectContractInformation>(projectContract.ContractInformation);
                            if (mapInfo != null)
                            {
                                var saveInfo = await _advertisement.CreateProjectContractInformation(mapInfo);
                                var mapSubTax = _mapper.Map<IList<ContractInformationSubTaxDetails>>(projectContract.ContractInformation.SubTaxDetailsDto);

                                foreach (var item in mapSubTax)
                                {
                                    item.CreatedAt = DateTime.Now;
                                    item.ProjectContractInformationId = saveInfo.ProjectContractInformationId;
                                }
                                var respContractDetails = await _advertisement.CreateContractInformationSubTaxDetails(mapSubTax);

                                if (saveWork != null && saveInfo != null)
                                {
                                    var mapAdv = _mapper.Map<ProjectContractDetailsDto, ProjectContractDetails>(projectContract);
                                    mapAdv.ProjectContractWorkFlowTrackingId = saveWork.ProjectContractWorkFlowTrackingId;
                                    mapAdv.ProjectContractInformationId = saveInfo.ProjectContractInformationId;

                                    var saveAdv = _advertisement.CreateProjectContractDetails(mapAdv);
                                }
                            }
                        }
                    }

                    return new ResponseDto<StatusCode>()
                    {
                        Status = StatusCode.Success
                    };
                }
                else
                {
                    return new ResponseDto<StatusCode>()
                    {
                        Status = StatusCode.NotFound
                    };
                }
            }
            catch (Exception ex)
            {
                return new ResponseDto<StatusCode>()
                {
                    Status = StatusCode.BadRequest,
                    Error = ex.InnerException.ToString()
                };
            }
        }
        public async Task<ResponseDto<ProjectContractDetailsDto>> GetProjectContractDetails(int ProjectInfoId)
        {
            try
            {
                ProjectContractDetailsDto detailsDto = new ProjectContractDetailsDto();

                var resAdv = await _advertisement.GetProjectContractDetailsByProjectInfoId(ProjectInfoId);
                if (resAdv != null)
                {
                    detailsDto.ExpDate = Convert.ToString(resAdv.ExpDate);
                    detailsDto.ContractAction = resAdv.ContractAction;
                    detailsDto.ExpDate = resAdv.ExpDate;

                    detailsDto.ProjectContractDetailsId = resAdv.ProjectContractDetailsId;
                    detailsDto.ProjectGeneralInformationId = ProjectInfoId;
                    detailsDto.ProjectDetails = resAdv.ProjectDetails;

                    var resArea = await _projectService.GetProjectConsultantAreaByProjectInfoId(ProjectInfoId);
                    detailsDto.ProjectManagerId = resArea.ProjectManagerId;
                    detailsDto.ManagerPhone = resArea.ManagerPhone;

                    var resProjectWork = await _advertisement.GetProjectContractWorkFlowTracking(resAdv.ProjectContractWorkFlowTrackingId);
                    if (resProjectWork != null)
                    {
                        var mapWork = _mapper.Map<ProjectContractWorkFlowTracking, ProjectContractWorkFlowTrackingDto>(resProjectWork);
                        detailsDto.ProjectWork = mapWork;
                    }
                    var resContractInfo = await _advertisement.GetProjectContractInformation(resAdv.ProjectContractInformationId);
                    if (resContractInfo != null)
                    {
                        var mapInfo = _mapper.Map<ProjectContractInformation, ProjectContractInformationDto>(resContractInfo);
                        detailsDto.ContractInformation = mapInfo;

                    }
                    var resSubDetail = await _advertisement.GetContractInformationSubTaxDetails(resAdv.ProjectContractInformationId);
                    if (resSubDetail.Count > 0)
                    {
                        var mapareasub = _mapper.Map<IList<ContractInformationSubTaxDetailsDto>>(resSubDetail);
                        detailsDto.ContractInformation.SubTaxDetailsDto = mapareasub;
                    }
                }
                //}
                return new ResponseDto<ProjectContractDetailsDto>()
                {
                    Status = StatusCode.Success,
                    Result = detailsDto
                };
            }
            catch (Exception ex)
            {
                return new ResponseDto<ProjectContractDetailsDto>()
                {
                    Status = StatusCode.BadRequest,
                    Error = ex.InnerException.ToString()
                };
            }
        }
        public async Task<ResponseDto<StatusCode>> CreateUpdateProjectTaskOrderDetails(ProjectTaskOrderDetailsDto projectTaskOrder)
        {
            try
            {
                if (projectTaskOrder.TaskOrderInformation != null && projectTaskOrder.ProjectGeneralInformationId != 0)
                {
                    var resAdv = await _advertisement.GetProjectTaskOrderDetailsByProjectInfoId(projectTaskOrder.ProjectGeneralInformationId);
                    if (resAdv != null)
                    {
                        //  int ProjectTaskOrderWorkFlowTrackingId = 0;
                        int ProjectTaskOrderInformationId = 0;

                        if (Convert.ToInt32(resAdv.ProjectTaskOrderInformationId) != 0)
                        {
                            ProjectTaskOrderInformationId = resAdv.ProjectTaskOrderInformationId;
                            var resInfo = await _advertisement.GetProjectTaskOrderInformation(resAdv.ProjectTaskOrderInformationId);
                            if (resInfo != null)
                            {
                                resInfo.ContractAmount = Convert.ToDecimal(projectTaskOrder.TaskOrderInformation.ContractAmount);
                                resInfo.TOAmount = Convert.ToDecimal(projectTaskOrder.TaskOrderInformation.TOAmount);
                                resInfo.Prime = projectTaskOrder.TaskOrderInformation.Prime;
                                resInfo.PrimeAmount = Convert.ToDecimal(projectTaskOrder.TaskOrderInformation.PrimeAmount);
                                var updateInformation = await _advertisement.UpdateProjectTaskOrderInformation(resInfo);

                                var mapInfo = _mapper.Map<IList<TaskOrderInformationSubTaxDetails>>(projectTaskOrder.TaskOrderInformation.SubTaxDetailsDto);
                                if (ProjectTaskOrderInformationId != 0 && mapInfo.Count > 0)
                                {
                                    var delete = await _advertisement.DeleteTaskOrderInformationSubTaxDetails(ProjectTaskOrderInformationId);
                                    foreach (var item in mapInfo)
                                    {

                                        item.CreatedAt = DateTime.Now;
                                        item.ProjectTaskOrderInformationId = ProjectTaskOrderInformationId;
                                    }
                                    var respconsultantDetails = await _advertisement.CreateTaskOrderInformationSubTaxDetails(mapInfo);
                                }
                            }
                        }
                        else
                        {
                            var mapInfo = _mapper.Map<ProjectInformationDto, ProjectTaskOrderInformation>(projectTaskOrder.TaskOrderInformation);
                            var saveInfo = await _advertisement.CreateProjectTaskOrderInformation(mapInfo);
                            var mapSubTax = _mapper.Map<IList<TaskOrderInformationSubTaxDetails>>(projectTaskOrder.TaskOrderInformation.SubTaxDetailsDto);

                            foreach (var item in mapSubTax)
                            {
                                item.CreatedAt = DateTime.Now;
                                item.ProjectTaskOrderInformationId = saveInfo.ProjectTaskOrderInformationId;
                            }
                            var respContractDetails = await _advertisement.CreateTaskOrderInformationSubTaxDetails(mapSubTax);

                            if (saveInfo != null)
                            {
                                ProjectTaskOrderInformationId = saveInfo.ProjectTaskOrderInformationId;
                            }
                        }
                        resAdv.TaskForProject = projectTaskOrder.TaskForProject;
                        resAdv.TaskScope = projectTaskOrder.TaskScope;
                        resAdv.TaxClassification = projectTaskOrder.TaxClassification;
                        resAdv.TaskExpirationDate = projectTaskOrder.TaskExpirationDate;
                        resAdv.ContractExpires = projectTaskOrder.ContractExpires;

                        //  resAdv.ProjectTaskOrderWorkFlowTrackingId = ProjectTaskOrderWorkFlowTrackingId;
                        resAdv.ProjectTaskOrderInformationId = ProjectTaskOrderInformationId;

                        var updateAdv = _advertisement.UpdateProjectTaskOrderDetails(resAdv);
                    }
                    else
                    {
                        var mapInfo = _mapper.Map<ProjectInformationDto, ProjectTaskOrderInformation>(projectTaskOrder.TaskOrderInformation);
                        var saveInfo = await _advertisement.CreateProjectTaskOrderInformation(mapInfo);
                        var mapSubTax = _mapper.Map<IList<TaskOrderInformationSubTaxDetails>>(projectTaskOrder.TaskOrderInformation.SubTaxDetailsDto);

                        foreach (var item in mapSubTax)
                        {
                            item.CreatedAt = DateTime.Now;
                            item.ProjectTaskOrderInformationId = saveInfo.ProjectTaskOrderInformationId;
                        }
                        var respTaskOrderDetails = await _advertisement.CreateTaskOrderInformationSubTaxDetails(mapSubTax);

                        if (/*saveWork != null &&*/ saveInfo != null)
                        {
                            var mapAdv = _mapper.Map<ProjectTaskOrderDetailsDto, ProjectTaskOrderDetails>(projectTaskOrder);
                            mapAdv.ProjectTaskOrderInformationId = saveInfo.ProjectTaskOrderInformationId;
                            var saveAdv = _advertisement.CreateProjectTaskOrderDetails(mapAdv);
                        }
                    }
                    return new ResponseDto<StatusCode>()
                    {
                        Status = StatusCode.Success
                    };

                }
                return new ResponseDto<StatusCode>()
                {
                    Status = StatusCode.NotFound
                };
            }
            catch (Exception ex)
            {
                return new ResponseDto<StatusCode>()
                {
                    Status = StatusCode.BadRequest,
                    Error = ex.InnerException.ToString()
                };
            }
        }
        public async Task<ResponseDto<ProjectTaskOrderDetailsDto>> GetProjectTaskOrderDetails(int ProjectInfoId)
        {
            try
            {
                ProjectTaskOrderDetailsDto detailsDto = new ProjectTaskOrderDetailsDto();
                var resAdv = await _advertisement.GetProjectTaskOrderDetailsByProjectInfoId(ProjectInfoId);
                if (resAdv != null)
                {
                    detailsDto.TaskForProject = Convert.ToString(resAdv.TaskForProject);
                    detailsDto.TaskScope = resAdv.TaskScope;
                    detailsDto.TaxClassification = resAdv.TaxClassification;
                    detailsDto.TaskExpirationDate = resAdv.TaskExpirationDate;
                    detailsDto.ContractExpires = resAdv.ContractExpires;
                    detailsDto.ProjectTaskOrderDetailsId = resAdv.ProjectTaskOrderDetailsId;
                    detailsDto.ProjectGeneralInformationId = ProjectInfoId;


                    //var resArea = await _projectService.GetProjectConsultantAreaByProjectInfoId(ProjectInfoId);
                    //if (resArea != null)
                    //{
                    //    detailsDto.ProjectManagerId = resArea.ProjectManagerId;
                    //    detailsDto.ManagerPhone = resArea.ManagerPhone;
                    //}
                    //var resProjectWork = await _advertisement.GetProjectTaskOrderWorkFlowTracking(resAdv.ProjectTaskOrderWorkFlowTrackingId);
                    //if (resProjectWork != null)
                    //{
                    //    var mapWork = _mapper.Map<ProjectTaskOrderWorkFlowTracking, ProjectTaskOrderWorkFlowTrackingDto>(resProjectWork);
                    //    detailsDto.ProjectWork = mapWork;
                    //}
                    var resContractInfo = await _advertisement.GetProjectTaskOrderInformation(resAdv.ProjectTaskOrderInformationId);
                    var resAdv1 = await _advertisement.GetProjectContractDetailsByProjectInfoId(ProjectInfoId);
                    if (resContractInfo != null)
                    {
                        var mapInfo = _mapper.Map<ProjectTaskOrderInformation, ProjectInformationDto>(resContractInfo);
                        detailsDto.TaskOrderInformation = mapInfo;

                    }
                    else
                    {
                        var resContractInfo1 = await _advertisement.GetProjectContractInformation(resAdv1.ProjectContractInformationId);
                        if (resContractInfo1 != null)
                        {
                            var mapInfo = _mapper.Map<ProjectContractInformation, ProjectInformationDto>(resContractInfo1);
                            detailsDto.TaskOrderInformation = mapInfo;
                        }
                    }
                    var resSubDetail = await _advertisement.GetTaskOrderInformationSubTaxDetails(resAdv.ProjectTaskOrderInformationId);
                    if (resSubDetail.Count > 0)
                    {
                        var mapareasub = _mapper.Map<IList<ProjectInformationSubTaxDetailsDto>>(resSubDetail);
                        detailsDto.TaskOrderInformation.SubTaxDetailsDto = mapareasub;
                    }
                    else
                    {
                        var resSubDetail1 = await _advertisement.GetContractInformationSubTaxDetails(resAdv1.ProjectContractInformationId);
                        if (resSubDetail1.Count > 0)
                        {
                            var mapareasub = _mapper.Map<IList<ProjectInformationSubTaxDetailsDto>>(resSubDetail1);
                            detailsDto.TaskOrderInformation.SubTaxDetailsDto = mapareasub;
                        }
                    }
                }
                else
                {
                    var resAdv1 = await _advertisement.GetProjectContractDetailsByProjectInfoId(ProjectInfoId);
                    if (resAdv1 != null)
                    {
                        var resContractInfo1 = await _advertisement.GetProjectContractInformation(resAdv1.ProjectContractInformationId);
                        if (resContractInfo1 != null)
                        {
                            var mapInfo = _mapper.Map<ProjectContractInformation, ProjectInformationDto>(resContractInfo1);
                            detailsDto.TaskOrderInformation = mapInfo;
                        }

                        var resSubDetail1 = await _advertisement.GetContractInformationSubTaxDetails(resAdv1.ProjectContractInformationId);
                        if (resSubDetail1.Count > 0)
                        {
                            var mapareasub = _mapper.Map<IList<ProjectInformationSubTaxDetailsDto>>(resSubDetail1);
                            detailsDto.TaskOrderInformation.SubTaxDetailsDto = mapareasub;
                        }
                    }
                }

                return new ResponseDto<ProjectTaskOrderDetailsDto>()
                {
                    Status = StatusCode.Success,
                    Result = detailsDto
                };
            }
            catch (Exception ex)
            {
                return new ResponseDto<ProjectTaskOrderDetailsDto>()
                {
                    Status = StatusCode.BadRequest,
                    Error = ex.InnerException.ToString()
                };
            }
        }

        public async Task<ResponseDto<IList<AllAdvertisementDetailsDto>>> GetAllAdvertisementDetailsList()
        {
            try
            {
                var resp = await _advertisement.GetAllAdvertisementDetailsList();
                var map = _mapper.Map<IList<AllAdvertisementDetailsDto>>(resp);
                return new ResponseDto<IList<AllAdvertisementDetailsDto>>()
                {
                    Status = StatusCode.Success,
                    Result = map
                };
            }
            catch (Exception ex)
            {
                return new ResponseDto<IList<AllAdvertisementDetailsDto>>()
                {
                    Status = StatusCode.BadRequest
                };
            }
        }

        public async Task<ResponseDto<GetAddendunDocumentDetailDto>> GetAddendunDocumentDetail(int ProjectAddendumId)
        {
            try
            {
                var resp = await _advertisement.GetAddendunDocumentDetail(ProjectAddendumId);
                var map = _mapper.Map<GetAddendunDocumentDetailDto>(resp);
                return new ResponseDto<GetAddendunDocumentDetailDto>()
                {
                    Status = StatusCode.Success,
                    Result = map
                };
            }
            catch (Exception ex)
            {
                return new ResponseDto<GetAddendunDocumentDetailDto>()
                {
                    Status = StatusCode.BadRequest
                };
            }
        }

        public async Task<ResponseDto<AllAdvertisementDetailsDto>> GetAdvertisementDocumentDetail(int projectadvertisementdetails)
        {
            try
            {
                var resp = await _advertisement.GetAdvertisementDocumentDetail(projectadvertisementdetails);
                var map = _mapper.Map<AllAdvertisementDetailsDto>(resp);
                return new ResponseDto<AllAdvertisementDetailsDto>()
                {
                    Status = StatusCode.Success,
                    Result = map
                };
            }
            catch (Exception ex)
            {
                return new ResponseDto<AllAdvertisementDetailsDto>()
                {
                    Status = StatusCode.BadRequest
                };
            }
        }


        public async Task<ResponseDto<StatusCode>> CreateUpdateProjectAdvertisementSelectionDetails(ProjectAdvertisementSelectionDto projectAdvSel)
        {
            try
            {
                if (projectAdvSel.ProjectGeneralInformationId != 0 /*&& projectAdvSel.ProjectGeneralInformationId != 0*/)
                {
                    var resAdvSel = await _advertisement.GetProjectAdvertisementSelectionDetailsByProjectInfoId(projectAdvSel.ProjectGeneralInformationId);
                    if (resAdvSel != null)
                    {
                        //resAdvSel.Title39 = projectAdvSel.Title39;
                        resAdvSel.AdvertisementDate = projectAdvSel.AdvertisementDate;
                        resAdvSel.ClosingDate = projectAdvSel.ClosingDate;
                        resAdvSel.AmmendedClosingDate = projectAdvSel.AmmendedClosingDate;
                        resAdvSel.SelectionDate = projectAdvSel.SelectionDate;
                        //resAdvSel.ContractNo = projectAdvSel.ContractNo;
                        //resAdvSel.StateProjectno = projectAdvSel.StateProjectno;
                        resAdvSel.Subject = projectAdvSel.Subject;
                        resAdvSel.Selection = projectAdvSel.Selection;
                        resAdvSel.Comment1 = projectAdvSel.Comment1;
                        resAdvSel.Comment2 = projectAdvSel.Comment2;
                        resAdvSel.IsPostToWeb = projectAdvSel.IsPostToWeb;
                        resAdvSel.Rank = projectAdvSel.Rank;
                        var updateAdv = _advertisement.UpdateProjectAdvertisementSelectionDetails(resAdvSel);
                    }
                    else
                    {
                        var mapAdv = _mapper.Map<ProjectAdvertisementSelectionDto, ProjectAdvertisementSelection>(projectAdvSel);
                        var saveAdv = _advertisement.CreateProjectAdvertisementSelectionDetails(mapAdv);

                    }
                    return new ResponseDto<StatusCode>()
                    {
                        Status = StatusCode.Success
                    };
                }
                else
                {
                    return new ResponseDto<StatusCode>()
                    {
                        Status = StatusCode.BadRequest
                    };
                }
            }
            catch (Exception ex)
            {
                return new ResponseDto<StatusCode>()
                {
                    Status = StatusCode.BadRequest,
                    Error = ex.InnerException.ToString()
                };
            }
        }
        public async Task<ResponseDto<IList<AllAdvertisementDetailsDto>>> GetAllAdvertisementSelectionDetailsList()
        {
            try
            {
                var resp = await _advertisement.GetAllAdvertisementSelectionDetailsList();
                var map = _mapper.Map<IList<AllAdvertisementDetailsDto>>(resp);
                return new ResponseDto<IList<AllAdvertisementDetailsDto>>()
                {
                    Status = StatusCode.Success,
                    Result = map
                };
            }
            catch (Exception ex)
            {
                return new ResponseDto<IList<AllAdvertisementDetailsDto>>()
                {
                    Status = StatusCode.BadRequest
                };
            }
        }


        public async Task<ResponseDto<StatusCode>> CreateUpdateProjectAdvertisementApparentSelectionDetails(ProjectAdvertisementApparentSelectionDto projectAdvSel)
        {
            try
            {
                if (projectAdvSel.ProjectGeneralInformationId != 0 /*&& projectAdvSel.ProjectGeneralInformationId != 0*/)
                {
                    var resAdvSel = await _advertisement.GetProjectAdvertisementApparentSelectionDetailsByProjectInfoId(projectAdvSel.ProjectGeneralInformationId);
                    if (resAdvSel != null)
                    {

                        //resAdvSel.ProjectAdvertisementApparentSelectionId = projectAdvSel.ProjectAdvertisementApparentSelectionId;
                        //resAdvSel.Title39 = projectAdvSel.Title39;
                        resAdvSel.AdvertisementDate = projectAdvSel.AdvertisementDate;
                        resAdvSel.ClosingDate = projectAdvSel.ClosingDate;
                        resAdvSel.AmmendedClosingDate = projectAdvSel.AmmendedClosingDate;
                        resAdvSel.ApparentSelectionDate = projectAdvSel.ApparentSelectionDate;
                        //resAdvSel.ContractNo = projectAdvSel.ContractNo;
                        //resAdvSel.StateProjectno = projectAdvSel.StateProjectno;
                        resAdvSel.Subject = projectAdvSel.Subject;
                        resAdvSel.ApparentSelection = projectAdvSel.ApparentSelection;
                        resAdvSel.Comment1 = projectAdvSel.Comment1;
                        resAdvSel.Comment2 = projectAdvSel.Comment2;
                        resAdvSel.IsPostToWeb = projectAdvSel.IsPostToWeb;
                        resAdvSel.Rank = projectAdvSel.Rank;
                        var updateAdv = _advertisement.UpdateProjectAdvertisementApparentSelectionDetails(resAdvSel);
                    }
                    else
                    {
                        var mapAdv = _mapper.Map<ProjectAdvertisementApparentSelectionDto, ProjectAdvertisementApparentSelection>(projectAdvSel);
                        var saveAdv = _advertisement.CreateProjectAdvertisementApparentSelectionDetails(mapAdv);

                    }
                    return new ResponseDto<StatusCode>()
                    {
                        Status = StatusCode.Success
                    };
                }
                return new ResponseDto<StatusCode>()
                {
                    Status = StatusCode.BadRequest
                };
            }
            catch (Exception ex)
            {
                return new ResponseDto<StatusCode>()
                {
                    Status = StatusCode.BadRequest,
                    Error = ex.InnerException.ToString()
                };
            }
        }
        public async Task<ResponseDto<IList<AllAdvertisementDetailsDto>>> GetAllAdvertisementApparentSelectionDetailsList()
        {
            try
            {
                var resp = await _advertisement.GetAllAdvertisementApparentSelectionDetailsList();
                var map = _mapper.Map<IList<AllAdvertisementDetailsDto>>(resp);
                return new ResponseDto<IList<AllAdvertisementDetailsDto>>()
                {
                    Status = StatusCode.Success,
                    Result = map
                };
            }
            catch (Exception ex)
            {
                return new ResponseDto<IList<AllAdvertisementDetailsDto>>()
                {
                    Status = StatusCode.BadRequest
                };
            }
        }


        public async Task<ResponseDto<int>> CreateUpdateProjectAdvertisementShortlistDetails(ProjectAdvertisementShortlistDto projectAdvSel)
        {
            try
            {
                if (projectAdvSel.ProjectGeneralInformationId != 0 /*&& projectAdvSel.ProjectGeneralInformationId != 0*/)
                {
                    var resAdvSel = await _advertisement.GetProjectAdvertisementShortlistDetailsByProjectInfoId(projectAdvSel.ProjectGeneralInformationId);
                    if (resAdvSel != null)
                    {
                        //resAdvSel.ProjectAdvertisementShortlistId = projectAdvSel.ProjectAdvertisementShortlistId;
                        //resAdvSel.Title39 = projectAdvSel.Title39;
                        resAdvSel.AdvertisementDate = projectAdvSel.AdvertisementDate;
                        resAdvSel.ClosingDate = projectAdvSel.ClosingDate;
                        resAdvSel.AmmendedClosingDate = projectAdvSel.AmmendedClosingDate;
                        resAdvSel.ShortListDate = projectAdvSel.ShortListDate;
                        //resAdvSel.ContractNo = projectAdvSel.ContractNo;
                        //resAdvSel.StateProjectno = projectAdvSel.StateProjectno;
                        resAdvSel.Subject = projectAdvSel.Subject;
                        // resAdvSel.ShortList = projectAdvSel.ShortList;
                        resAdvSel.Comment1 = projectAdvSel.Comment1;
                        resAdvSel.Comment2 = projectAdvSel.Comment2;
                        

                        var updateAdv = await _advertisement.UpdateProjectAdvertisementShortlistDetails(resAdvSel);
                        return new ResponseDto<int>()
                        {
                            Status = StatusCode.Updated,
                            Result = resAdvSel.ProjectAdvertisementShortlistId
                        };
                    }
                    else
                    {
                        if (resAdvSel == null)
                        {
                            var mapAdv = _mapper.Map<ProjectAdvertisementShortlistDto, ProjectAdvertisementShortlist>(projectAdvSel);
                            var saveAdv = await _advertisement.CreateProjectAdvertisementShortlistDetails(mapAdv);
                            foreach (var item in projectAdvSel.ShortList)
                            {
                                ProjectAdvertisementShortlistConsultantProvider obj = new ProjectAdvertisementShortlistConsultantProvider();
                                obj.ProjectAdvertisementShortListId = saveAdv.ProjectAdvertisementShortlistId;
                                obj.SelectedShortlistConsultantProviderId = Convert.ToInt32(item.ShortList);
                                obj.Rank = item.Rank;
                                var saveAdvPro = await _advertisement.CreateProjectAdvertisementShortlistConsultantProviderDetails(obj);

                                var mapSubTax = _mapper.Map<IList<ShortlistSubDetails>>(item.SubTaxDetailsDto);
                                foreach (var item1 in mapSubTax)
                                {
                                    item1.CreatedAt = DateTime.Now;
                                    item1.ProjectAdvertisementShortlistConsultantProviderId = saveAdvPro.ProjectAdvertisementShortlistConsultantProviderId;
                                }
                                var respDetails = await _advertisement.CreateShortlistSubDetails(mapSubTax);
                            }
                            return new ResponseDto<int>()
                            {
                                Status = StatusCode.Success,
                                Result = saveAdv.ProjectAdvertisementShortlistId
                            };
                        }
                        else
                        {
                            return new ResponseDto<int>()
                            {
                                Status = StatusCode.RecordAlreadyExist,
                                Result = resAdvSel.ProjectAdvertisementShortlistId
                            };
                        }
                    }

                }
                else
                {
                    return new ResponseDto<int>()
                    {
                        Status = StatusCode.BadRequest
                    };
                }

            }
            catch (Exception ex)
            {
                return new ResponseDto<int>()
                {
                    Status = StatusCode.BadRequest,
                    Error = ex.InnerException.ToString()
                };
            }
        }
        public async Task<ResponseDto<IList<AllAdvertisementDetailsDto>>> GetAllAdvertisementShortlistDetailsList()
        {
            try
            {
                var resp = await _advertisement.GetAllAdvertisementShortlistDetailsList();
                var map = _mapper.Map<IList<AllAdvertisementDetailsDto>>(resp);
                return new ResponseDto<IList<AllAdvertisementDetailsDto>>()
                {
                    Status = StatusCode.Success,
                    Result = map
                };
            }
            catch (Exception ex)
            {
                return new ResponseDto<IList<AllAdvertisementDetailsDto>>()
                {
                    Status = StatusCode.BadRequest
                };
            }
        }


        public async Task<ResponseDto<IList<CommonDropdownDto>>> GetShortListConsultantProviderList(int ProjectInfoId)
        {
            try
            {
                var get = await _advertisement.GetShortListConsultantProviderList(ProjectInfoId);
                var map = _mapper.Map<IList<CommonDropdownDto>>(get);

                return new ResponseDto<IList<CommonDropdownDto>>()
                {
                    Status = StatusCode.Success,
                    Result = map
                };

            }
            catch (Exception ex)
            {
                return new ResponseDto<IList<CommonDropdownDto>>()
                {
                    Status = StatusCode.BadRequest
                };
            }
        }

        public async Task<ResponseDto<StatusCode>> CreateProjectAdvertisementDocument(ProjectAdvertisementDocumentDto documentDto)
        {
            try
            {
                var get = await _advertisement.GetProjectAdvertisementDocument(documentDto.ProjectAdvertisementDetailsId);
                if (get == null)
                {
                    var map = _mapper.Map<ProjectAdvertisementDocumentDto, AdvertisementDocumentMaster>(documentDto);
                    var save = await _advertisement.CreateProjectAdvertisementDocument(map);
                    return new ResponseDto<StatusCode>()
                    {
                        Status = StatusCode.Success
                    };
                }
                else
                {
                    if (!string.IsNullOrEmpty(documentDto.AdvertDateGoesHere))
                    {
                        get.AdvertDateGoesHere = Convert.ToDateTime(documentDto.AdvertDateGoesHere);
                    }
                    if (!string.IsNullOrEmpty(documentDto.AdvertisementDate))
                    {
                        get.AdvertisementDate = Convert.ToDateTime(documentDto.AdvertisementDate);
                    }
                    if (!string.IsNullOrEmpty(documentDto.ClosingDate))
                    {
                        get.ClosingDate = Convert.ToDateTime(documentDto.ClosingDate);
                    }
                    //get.ContractNo = documentDto.ContractNo;
                    //get.DBEGoal = documentDto.DBEGoal;
                    get.Details = documentDto.Details;
                    //get.Document = documentDto.Document;
                    //get.FAPNo = documentDto.FAPNo;
                    //get.IsPostToWeb = documentDto.IsPostToWeb;
                    //get.Parish = documentDto.Parish;
                    //get.ProjectName = documentDto.ProjectName;
                    //get.StateProjectNo = documentDto.StateProjectNo;
                    get.TypeOfAdvertGoesHere = documentDto.TypeOfAdvertGoesHere;
                    await _advertisement.UpdateProjectAdvertisementDocument(get);
                    return new ResponseDto<StatusCode>()
                    {
                        Status = StatusCode.Updated
                    };
                }
            }
            catch (Exception ex)
            {
                return new ResponseDto<StatusCode>()
                {
                    Status = StatusCode.BadRequest,
                    Error = ex.InnerException.ToString()
                };
            }
        }
        public async Task<ResponseDto<StatusCode>> CreateUpdateProjectAdvertisementDocument(ProjectAdvertisementDocumentDto documentDto)
        {
            try
            {
                var get = await _advertisement.GetProjectAdvertisementDocument(documentDto.ProjectAdvertisementDetailsId);
                if (get == null)
                {
                    var map = _mapper.Map<ProjectAdvertisementDocumentDto, AdvertisementDocumentMaster>(documentDto);
                    var save = await _advertisement.CreateProjectAdvertisementDocument(map);
                    return new ResponseDto<StatusCode>()
                    {
                        Status = StatusCode.Success
                    };
                }
                else
                {
                    if (!string.IsNullOrEmpty(documentDto.AdvertDateGoesHere))
                    {
                        get.AdvertDateGoesHere = Convert.ToDateTime(documentDto.AdvertDateGoesHere);
                    }
                    if (!string.IsNullOrEmpty(documentDto.AdvertisementDate))
                    {
                        get.AdvertisementDate = Convert.ToDateTime(documentDto.AdvertisementDate);
                    }
                    if (!string.IsNullOrEmpty(documentDto.ClosingDate))
                    {
                        get.ClosingDate = Convert.ToDateTime(documentDto.ClosingDate);
                    }
                    get.Comment1 = documentDto.Comment1;
                    get.Comment2 = documentDto.Comment2;
                    get.Comment3 = documentDto.Comment3;
                    //get.ContractNo = documentDto.ContractNo;
                    //get.DBEGoal = documentDto.DBEGoal;
                    get.Details = documentDto.Details;
                    //get.FAPNo = documentDto.FAPNo;
                    //get.Parish = documentDto.Parish;
                    get.TypeOfAdvertGoesHere = documentDto.TypeOfAdvertGoesHere;
                    //get.StateProjectNo = documentDto.StateProjectNo;
                    get.IsPostToWeb = documentDto.IsPostToWeb;

                    var updatedoc = _advertisement.UpdateProjectAdvertisementDocument(get);

                    return new ResponseDto<StatusCode>()
                    {
                        Status = StatusCode.Updated
                    };
                }
            }
            catch (Exception ex)
            {
                return new ResponseDto<StatusCode>()
                {
                    Status = StatusCode.BadRequest,
                    Error = ex.InnerException.ToString()
                };
            }
        }

        public async Task<ResponseDto<AdvertisementShortListDocDto>> GetAdvertisementShortListDocDetail(int ProjectInfoId)
        {
            try
            {
                var resp = await _advertisement.GetAdvertisementShortListDocDetail(ProjectInfoId);
                var map = _mapper.Map<AdvertisementShortListDocDto>(resp);
                var get = await _advertisement.GetSelectedShortListConsultantProviderDetail(resp.ProjectAdvertisementShortlistId);
                var map1 = _mapper.Map<IList<CommonDropdownDto>>(get);
                IList<ShortListDto> obj = new List<ShortListDto>();
                for (int i = 0; i < map1.Count; i++)
                {
                    ShortListDto shortList = new ShortListDto();
                    shortList.ShortList = map1[i].Name;
                    IList<ShortlistSubDetailsDto> shortlistSubDetails = new List<ShortlistSubDetailsDto>();
                    var getSub = await _advertisement.GetSubByShortListId(map1[i].Id);
                    foreach (var item in getSub)
                    {
                        ShortlistSubDetailsDto detailsDto = new ShortlistSubDetailsDto();
                        detailsDto.ConsultantProviderName = item.ConsultantProviderName;
                        detailsDto.SubId = item.SubId;
                        detailsDto.ShortlistSubDetailsId = item.ShortlistSubDetailsId;
                        detailsDto.ProjectAdvertisementShortlistConsultantProviderId = item.ProjectAdvertisementShortlistConsultantProviderId;
                        shortlistSubDetails.Add(detailsDto);
                    }
                    shortList.SubTaxDetailsDto = shortlistSubDetails;
                    obj.Add(shortList);
                }
                map.ShortList = obj;


                return new ResponseDto<AdvertisementShortListDocDto>()
                {
                    Status = StatusCode.Success,
                    Result = map
                };
            }
            catch (Exception ex)
            {
                return new ResponseDto<AdvertisementShortListDocDto>()
                {
                    Status = StatusCode.BadRequest
                };
            }
        }

        public async Task<ResponseDto<AllAdvertisementDetailsDto>> GetAdvertisementSelectionDocDetail(int ProjectSelId)
        {
            try
            {
                var resp = await _advertisement.GetAdvertisementSelectionDocDetail(ProjectSelId);
                var map = _mapper.Map<AllAdvertisementDetailsDto>(resp);
                var getSub = await _advertisement.GetSubByShortListId(Convert.ToInt32(map.Selection));
                map.SubTaxDetailsDto = getSub;
                return new ResponseDto<AllAdvertisementDetailsDto>()
                {
                    Status = StatusCode.Success,
                    Result = map
                };
            }
            catch (Exception ex)
            {
                return new ResponseDto<AllAdvertisementDetailsDto>()
                {
                    Status = StatusCode.BadRequest
                };
            }
        }

        public async Task<ResponseDto<AllAdvertisementDetailsDto>> GetAdvertisementApparentSelectionDocDetail(int ProjectAppSelId)
        {
            try
            {
                var resp = await _advertisement.GetAdvertisementApparentSelectionDocDetail(ProjectAppSelId);

                var map = _mapper.Map<AllAdvertisementDetailsDto>(resp);
                if (map != null)
                {
                    var getSub = await _advertisement.GetSubByShortListId(Convert.ToInt32(map.ApparentSelection));
                    map.SubTaxDetailsDto = getSub;
                }
                return new ResponseDto<AllAdvertisementDetailsDto>()
                {
                    Status = StatusCode.Success,
                    Result = map
                };
            }
            catch (Exception ex)
            {
                return new ResponseDto<AllAdvertisementDetailsDto>()
                {
                    Status = StatusCode.BadRequest
                };
            }
        }
        public async Task<ResponseDto<AdvertisementCommonDetailDto>> GetAdvertisementDetail(int ProjectGeneralInformationId)
        {
            try
            {
                var resp = await _advertisement.GetAdvertisementDetail(ProjectGeneralInformationId);
                var map = _mapper.Map<AdvertisementCommonDetailDto>(resp);
                return new ResponseDto<AdvertisementCommonDetailDto>()
                {
                    Status = StatusCode.Success,
                    Result = map
                };
            }
            catch (Exception ex)
            {
                return new ResponseDto<AdvertisementCommonDetailDto>()
                {
                    Status = StatusCode.BadRequest
                };
            }
        }

        public async Task<ResponseDto<ProjectAdvertisementApparentSelectionDto>> GetAdvertisementApparentSelection(int ProjectGeneralInformationId)
        {
            try
            {
                var resp = await _advertisement.GetProjectAdvertisementApparentSelectionDetailsByProjectInfoId(ProjectGeneralInformationId);
                if (resp != null)
                {
                    var map = _mapper.Map<ProjectAdvertisementApparentSelectionDto>(resp);
                    return new ResponseDto<ProjectAdvertisementApparentSelectionDto>()
                    {
                        Status = StatusCode.Success,
                        Result = map
                    };
                }
                return new ResponseDto<ProjectAdvertisementApparentSelectionDto>()
                {
                    Status = StatusCode.NotFound
                };
                //else
                //{
                //    var respad = await _advertisement.GetAdvertisementDetail(ProjectGeneralInformationId);
                //    if (respad != null)
                //    {
                //        var obj = new ProjectAdvertisementApparentSelectionDto()
                //        {
                //            ClosingDate = respad.ClosingDate,
                //            AdvertisementDate = respad.AdvertisementDate,
                //            ProjectGeneralInformationId = respad.ProjectGeneralInformationId,
                //            //StateProjectno = respad.StateProjectno,
                //            //ContractNo = respad.ContractNo
                //        };
                //        return new ResponseDto<ProjectAdvertisementApparentSelectionDto>()
                //        {
                //            Result = obj,
                //            Status = StatusCode.Success
                //        };
                //    }
                //    return new ResponseDto<ProjectAdvertisementApparentSelectionDto>()
                //    {
                //        Status = StatusCode.NotFound
                //    };
                //}
            }
            catch (Exception ex)
            {
                return new ResponseDto<ProjectAdvertisementApparentSelectionDto>()
                {
                    Status = StatusCode.BadRequest
                };
            }
        }
        public async Task<ResponseDto<StatusCode>> CreateProjectAddendumDocumentDetails(AddendumDocumentMasterDto documentDto)
        {
            try
            {
                var map = _mapper.Map<AddendumDocumentMasterDto, AddendumDocumentMaster>(documentDto);
                var save = await _advertisement.CreateProjectAddendumDocumentDetails(map);
                return new ResponseDto<StatusCode>()
                {
                    Status = StatusCode.Success
                };
            }
            catch (Exception ex)
            {
                return new ResponseDto<StatusCode>()
                {
                    Status = StatusCode.BadRequest,
                    Error = ex.InnerException.ToString()
                };
            }
        }

        public async Task<ResponseDto<StatusCode>> CreateUpdateProjectSupplementalAgreementDetails(ProjectSupplementalAgreementDetailsDto project)
        {
            try
            {
                //if (project.ProjectGeneralInformationId != 0)
                //{
                //    var getProInfo = await _projectService.GetProjectGeneralInformation(project.ProjectGeneralInformationId);
                //    if (getProInfo != null)
                //    {
                //        // getProInfo.ContractType = project.ContractType;
                //        //getProInfo.EstContractAmount = projectAdvertisement.Information.EstContractAmount;

                //        var updateInfo = await _projectService.UpdateProjectGeneralInformation(getProInfo);
                //        if (updateInfo != null)
                //        {


                //        }
                //        return new ResponseDto<StatusCode>()
                //        {
                //            Status = StatusCode.Success
                //        };
                //    }
                //    else
                //    {
                //        return new ResponseDto<StatusCode>()
                //        {
                //            Status = StatusCode.NotFound
                //        };
                //    }
                //}
                int ProjectSupplementalAgreementDetailId = 0;
                var resAdv = await _advertisement.GetProjectSupplementalAgreementDetails(project.ProjectSupplementalAgreementDetailId);
                if (resAdv != null)
                {
                    //resAdv.ProjectSupplementalAgreementDetailId = project.ProjectSupplementalAgreementDetailId;
                    //resAdv.ProjectGeneralInformationId = project.ProjectGeneralInformationId;
                    resAdv.ReasonForSA = project.ReasonForSA;
                    resAdv.SADescription = project.SADescription;
                    resAdv.SAAction = project.SAAction;
                    resAdv.SAFeeType = project.SAFeeType;
                    resAdv.SAExpries = project.SAExpries;
                    resAdv.ContractExpries = project.ContractExpries;
                    resAdv.Commnets = project.Commnets;
                    resAdv.InfoRecAndLogged = project.InfoRecAndLogged;
                    resAdv.TaskAssignedToStaff = project.TaskAssignedToStaff;
                    resAdv.DrafttoSupervisor = project.DrafttoSupervisor;
                    resAdv.ApprovedfromSupervisor = project.ApprovedfromSupervisor;
                    resAdv.FeePackagetoChiefEngineer = project.FeePackagetoChiefEngineer;
                    resAdv.ApprovedFromFeePackagetoChiefEngineer = project.ApprovedFromFeePackagetoChiefEngineer;
                    resAdv.FeeNegotiationsTOProjMgr = project.FeeNegotiationsTOProjMgr;
                    resAdv.Complete = project.Complete;
                    resAdv.FedAideFHWA = project.FedAideFHWA;
                    resAdv.Authorized = project.Authorized;
                    resAdv.DraftToManagerForReview = project.DraftToManagerForReview;
                    resAdv.DraftToManagerForReviewConcurred = project.DraftToManagerForReviewConcurred;
                    resAdv.DraftToConsultantForReview = project.DraftToConsultantForReview;
                    resAdv.DraftToConsultantForReviewConcurred = project.DraftToConsultantForReviewConcurred;
                    resAdv.ContractToConsultant = project.ContractToConsultant;
                    resAdv.Signed = project.Signed;
                    resAdv.ContractTo3rdFloor = project.ContractTo3rdFloor;
                    resAdv.SignedExecuted = project.SignedExecuted;
                    resAdv.NTPLetter = project.NTPLetter;
                    resAdv.NTPDate = project.NTPDate;
                    var updateWork = await _advertisement.UpdateProjectSupplementalAgreementDetails(resAdv);
                    ProjectSupplementalAgreementDetailId = resAdv.ProjectSupplementalAgreementDetailId;
                }
                else
                {
                    var mapAdv = _mapper.Map<ProjectSupplementalAgreementDetailsDto, ProjectSupplementalAgreementDetails>(project);
                    var saveAdv = await _advertisement.CreateProjectSupplementalAgreementDetails(mapAdv);
                    ProjectSupplementalAgreementDetailId = saveAdv.ProjectSupplementalAgreementDetailId;
                }
                var getinfo = await _advertisement.GetProjectSupplementalAgreementInformationDetails(ProjectSupplementalAgreementDetailId);
                if (getinfo != null)
                {
                    getinfo.SAAmount = Convert.ToDecimal(project.ProjectInformation.TOAmount);
                    await _advertisement.UpdateProjectSupplementalAgreementInformationDetails(getinfo);
                }
                else
                {
                    getinfo = new ProjectSupplementalAgreementInformationDetails();
                    getinfo.SAAmount = Convert.ToDecimal(project.ProjectInformation.TOAmount);
                    getinfo.ProjectSupplementalAgreementDetailId = ProjectSupplementalAgreementDetailId;
                    await _advertisement.CreateProjectSupplementalAgreementInformationDetails(getinfo);
                }
                var get = await _advertisement.GetProjectSupplementalAgreementSubTaxDetails(getinfo.ProjectSupplementalAgreementInformationId);
                if (get.Count > 0)
                {
                    await _advertisement.DeleteProjectSupplementalAgreementSubTaxDetails(get);
                }
                var mapSubTax = _mapper.Map<IList<ProjectSupplementalAgreementSubTaxDetails>>(project.ProjectInformation.SubTaxDetailsDto);

                foreach (var item in mapSubTax)
                {
                    item.CreatedAt = DateTime.Now;
                    item.ProjectSupplementalAgreementInformationId = getinfo.ProjectSupplementalAgreementInformationId;
                }
                var respTaskOrderDetails = await _advertisement.CreateProjectSupplementalAgreementSubTaxDetails(mapSubTax);
                return new ResponseDto<StatusCode>()
                {
                    Status = StatusCode.Success
                };
            }
            catch (Exception ex)
            {
                return new ResponseDto<StatusCode>()
                {
                    Status = StatusCode.BadRequest,
                    Error = ex.InnerException.ToString()
                };
            }
        }
        public async Task<ResponseDto<StatusCode>> CreateUpdateProjectExtraWorkLetterDetails(ProjectExtraWorkLetterDetailsDto project)
        {
            try
            {
                //if (project.ProjectGeneralInformationId != 0)
                //{
                //    var getProInfo = await _projectService.GetProjectGeneralInformation(project.ProjectGeneralInformationId);
                //    if (getProInfo != null)
                //    {
                //        // getProInfo.ContractType = project.ContractType;
                //        //getProInfo.EstContractAmount = projectAdvertisement.Information.EstContractAmount;

                //        var updateInfo = await _projectService.UpdateProjectGeneralInformation(getProInfo);
                //        if (updateInfo != null)
                //        {


                //        }
                //        return new ResponseDto<StatusCode>()
                //        {
                //            Status = StatusCode.Success
                //        };
                //    }
                //    else
                //    {
                //        return new ResponseDto<StatusCode>()
                //        {
                //            Status = StatusCode.NotFound
                //        };
                //    }
                //}

                var resAdv = await _advertisement.GetProjectExtraWorkLetterDetails(project.ProjectExtraWorkLetterDetailId);
                if (resAdv != null)
                {
                    resAdv.ProjectExtraWorkLetterDetailId = project.ProjectExtraWorkLetterDetailId;
                    resAdv.ProjectGeneralInformationId = project.ProjectGeneralInformationId;
                    resAdv.ReasonforEWL = project.ReasonforEWL;
                    resAdv.ExtraWorkAction = project.ExtraWorkAction;
                    resAdv.ContractExpries = project.ContractExpries;
                    resAdv.Commnets = project.Commnets;
                    resAdv.InformationAmount = project.InformationAmount;
                    resAdv.ContractExpries = project.ContractExpries;
                    resAdv.Commnets = project.Commnets;
                    resAdv.InfoRecAndLogged = project.InfoRecAndLogged;
                    resAdv.TaskAssignedToStaff = project.TaskAssignedToStaff;
                    resAdv.InfoRecAndLogged = project.InfoRecAndLogged;
                    resAdv.TaskAssignedToStaff = project.TaskAssignedToStaff;
                    resAdv.NTPLetter = project.NTPLetter;
                    resAdv.NTPDate = project.NTPDate;
                    var updateWork = await _advertisement.UpdateProjectExtraWorkLetterDetails(resAdv);
                }
                else
                {
                    resAdv = _mapper.Map<ProjectExtraWorkLetterDetailsDto, ProjectExtraWorkLetterDetails>(project);
                    var saveAdv = _advertisement.CreateProjectExtraWorkLetterDetails(resAdv);
                }
                var getinfo = await _advertisement.GetProjectExtraWorkLetterInformationDetails(resAdv.ProjectExtraWorkLetterDetailId);
                if (getinfo != null)
                {
                    getinfo.EWLAmount = Convert.ToDecimal(project.ProjectInformation.TOAmount); 
                    getinfo.ContractAmount = Convert.ToDecimal(project.ProjectInformation.ContractAmount);
                    await _advertisement.UpdateProjectExtraWorkLetterInformationDetails(getinfo);
                }
                else
                {
                    getinfo = new ProjectExtraWorkLetterInformationDetails();
                    getinfo.EWLAmount = Convert.ToDecimal(project.ProjectInformation.TOAmount);
                    getinfo.ContractAmount = Convert.ToDecimal(project.ProjectInformation.ContractAmount);
                    getinfo.ProjectExtraWorkLetterDetailId = resAdv.ProjectExtraWorkLetterDetailId;
                    await _advertisement.CreateProjectExtraWorkLetterInformationDetails(getinfo);
                }
                var get = await _advertisement.GetProjectExtraWorkLetterSubTaxDetails(getinfo.ProjectExtraWorkLetterInformationId);
                if (get.Count > 0)
                {
                    await _advertisement.DeleteProjectExtraWorkLetterSubTaxDetails(get);
                }
                var mapSubTax = _mapper.Map<IList<ProjectExtraWorkLetterSubTaxDetails>>(project.ProjectInformation.SubTaxDetailsDto);

                foreach (var item in mapSubTax)
                {
                    item.CreatedAt = DateTime.Now;
                    item.ProjectExtraWorkLetterInformationId = getinfo.ProjectExtraWorkLetterInformationId;
                }
                var respTaskOrderDetails = await _advertisement.CreateProjectExtraWorkLetterSubTaxDetails(mapSubTax);

                return new ResponseDto<StatusCode>()
                {
                    Status = StatusCode.Success
                };
            }
            catch (Exception ex)
            {
                return new ResponseDto<StatusCode>()
                {
                    Status = StatusCode.BadRequest,
                    Error = ex.InnerException.ToString()
                };
            }
        }
        public async Task<ResponseDto<StatusCode>> CreateUpdateProjectOriginalContractDetails(ProjectOriginalContractDetailsDto project)
        {
            try
            {
                //if (project.ProjectGeneralInformationId != 0)
                //{
                //    var getProInfo = await _projectService.GetProjectGeneralInformation(project.ProjectGeneralInformationId);
                //    if (getProInfo != null)
                //    {
                //        var updateInfo = await _projectService.UpdateProjectGeneralInformation(getProInfo);
                //        if (updateInfo != null)
                //        {


                //        }
                //        return new ResponseDto<StatusCode>()
                //        {
                //            Status = StatusCode.Success
                //        };
                //    }
                //    else
                //    {
                //        return new ResponseDto<StatusCode>()
                //        {
                //            Status = StatusCode.NotFound
                //        };
                //    }
                //}
                var resAdv = await _advertisement.GetProjectOriginalContractDetails(project.ProjectOriginalContractDetailId);
                if (resAdv != null)
                {

                    // resAdv.ProjectOriginalContractDetailId = project.ProjectOriginalContractDetailId;
                    resAdv.ProjectGeneralInformationId = project.ProjectGeneralInformationId;
                    resAdv.ContractAction = project.ContractAction;
                    resAdv.ContractExpries = project.ContractExpries;
                    resAdv.Commnets = project.Commnets;
                    resAdv.InfoRecAndLogged = project.InfoRecAndLogged;
                    resAdv.TaskAssignedToStaff = project.TaskAssignedToStaff;
                    resAdv.DrafttoSupervisor = project.DrafttoSupervisor;
                    resAdv.ApprovedfromSupervisor = project.ApprovedfromSupervisor;
                    resAdv.FeePackagetoChiefEngineer = project.FeePackagetoChiefEngineer;
                    resAdv.ApprovedFromFeePackagetoChiefEngineer = project.ApprovedFromFeePackagetoChiefEngineer;
                    resAdv.FeeNegotiationsTOProjMgr = project.FeeNegotiationsTOProjMgr;
                    resAdv.Complete = project.Complete;
                    resAdv.FedAideFHWAANDAuth = project.FedAideFHWAANDAuth;
                    resAdv.Authorized = project.Authorized;
                    resAdv.DraftToManagerForReview = project.DraftToManagerForReview;
                    resAdv.DraftToManagerForReviewConcurred = project.DraftToManagerForReviewConcurred;
                    resAdv.DraftToConsultantForReview = project.DraftToConsultantForReview;
                    resAdv.DraftToConsultantForReviewConcurred = project.DraftToConsultantForReviewConcurred;
                    resAdv.ContractToConsultant = project.ContractToConsultant;
                    resAdv.ContractTo3rdFloor = project.ContractTo3rdFloor;
                    resAdv.SignedExecuted = project.SignedExecuted;
                    resAdv.NTPLetter = project.NTPLetter;
                    resAdv.NTPDate = project.NTPDate;
                    var updateWork = await _advertisement.UpdateProjectOriginalContractDetails(resAdv);
                }
                else
                {
                    resAdv = _mapper.Map<ProjectOriginalContractDetailsDto, ProjectOriginalContractDetails>(project);
                    var saveAdv = await _advertisement.CreateProjectOriginalContractDetails(resAdv);
                }

                var getinfo = await _advertisement.GetProjectOriginalContractInformationDetails(resAdv.ProjectOriginalContractDetailId);
                if (getinfo != null)
                {
                    getinfo.ContractAmount = Convert.ToDecimal(project.ProjectInformation.ContractAmount);
                    await _advertisement.UpdateProjectOriginalContractInformationDetails(getinfo);
                }
                else
                {
                    getinfo = new ProjectOriginalContractInformationDetails();
                    getinfo.ContractAmount = Convert.ToDecimal(project.ProjectInformation.ContractAmount);
                    getinfo.ProjectOriginalContractDetailId = resAdv.ProjectOriginalContractDetailId;
                    await _advertisement.CreateProjectOriginalContractInformationDetails(getinfo);
                }
                var get = await _advertisement.GetProjectOriginalContractSubTaxDetails(getinfo.ProjectOriginalContractDetailId);
                if (get.Count > 0)
                {
                    await _advertisement.DeleteProjectOriginalContractSubTaxDetails(get);
                }
                var mapSubTax = _mapper.Map<IList<ProjectOriginalContractSubTaxDetails>>(project.ProjectInformation.SubTaxDetailsDto);

                foreach (var item in mapSubTax)
                {
                    item.CreatedAt = DateTime.Now;
                    item.ProjectOriginalContractInformationId = getinfo.ProjectOriginalContractInformationId;
                }
                var respTaskOrderDetails = await _advertisement.CreateProjectOriginalContractSubTaxDetails(mapSubTax);

                return new ResponseDto<StatusCode>()
                {
                    Status = StatusCode.Success
                };
            }
            catch (Exception ex)
            {
                return new ResponseDto<StatusCode>()
                {
                    Status = StatusCode.BadRequest,
                    Error = ex.InnerException.ToString()
                };
            }
        }

        public async Task<ResponseDto<StatusCode>> CreateUpdateSupplementalAgreementForIDIQ(ProjectSupplementalAgreementForIDIQDto projectTaskOrder)
        {
            try
            {
                if (projectTaskOrder.TaskOrderInformation != null && projectTaskOrder.ProjectGeneralInformationId != 0)
                {
                    var resAdv = await _advertisement.GetProjectSupplementalAgreementForIDIQDetailsByProjectInfoId(projectTaskOrder.ProjectGeneralInformationId);
                    if (resAdv != null)
                    {
                        //  int ProjectTaskOrderWorkFlowTrackingId = 0;
                        int ProjectSupplementalAgreementInformationForIDIQId = 0;

                        if (Convert.ToInt32(resAdv.ProjectSupplementalAgreementInformationForIDIQId) != 0)
                        {
                            ProjectSupplementalAgreementInformationForIDIQId = resAdv.ProjectSupplementalAgreementInformationForIDIQId;
                            var resInfo = await _advertisement.GetProjectSupplementalAgreementForIDIQInformation(resAdv.ProjectSupplementalAgreementInformationForIDIQId);
                            if (resInfo != null)
                            {
                                resInfo.ContractAmount = Convert.ToDecimal(projectTaskOrder.TaskOrderInformation.ContractAmount);
                                resInfo.TOAmount = Convert.ToDecimal(projectTaskOrder.TaskOrderInformation.TOAmount);
                                resInfo.Prime = projectTaskOrder.TaskOrderInformation.Prime;
                                resInfo.PrimeAmount = Convert.ToDecimal(projectTaskOrder.TaskOrderInformation.PrimeAmount);
                                var updateInformation = await _advertisement.UpdateProjectSupplementalAgreementForIDIQInformation(resInfo);

                                var mapInfo = _mapper.Map<IList<ProjectSupplementalAgreementInformationForIDIQSubTaxDetails>>(projectTaskOrder.TaskOrderInformation.SubTaxDetailsDto);
                                if (ProjectSupplementalAgreementInformationForIDIQId != 0 && mapInfo.Count > 0)
                                {
                                    var delete = await _advertisement.DeleteTaskOrderInformationSubTaxDetails(ProjectSupplementalAgreementInformationForIDIQId);
                                    foreach (var item in mapInfo)
                                    {

                                        item.CreatedAt = DateTime.Now;
                                        item.ProjectSupplementalAgreementInformationForIDIQId = ProjectSupplementalAgreementInformationForIDIQId;
                                    }
                                    var respconsultantDetails = await _advertisement.CreateSupplementalAgreementForIDIQInformationSubTaxDetails(mapInfo);
                                }
                            }
                        }
                        else
                        {
                            var mapInfo = _mapper.Map<ProjectSupplementalAgreementInformationForIDIQDto, ProjectSupplementalAgreementInformationForIDIQ>(projectTaskOrder.TaskOrderInformation);
                            var saveInfo = await _advertisement.CreateProjectSupplementalAgreementForIDIQInformation(mapInfo);
                            var mapSubTax = _mapper.Map<IList<ProjectSupplementalAgreementInformationForIDIQSubTaxDetails>>(projectTaskOrder.TaskOrderInformation.SubTaxDetailsDto);

                            foreach (var item in mapSubTax)
                            {
                                item.CreatedAt = DateTime.Now;
                                item.ProjectSupplementalAgreementInformationForIDIQId = saveInfo.ProjectSupplementalAgreementInformationForIDIQId;
                            }
                            var respContractDetails = await _advertisement.CreateSupplementalAgreementForIDIQInformationSubTaxDetails(mapSubTax);

                            if (saveInfo != null)
                            {
                                ProjectSupplementalAgreementInformationForIDIQId = saveInfo.ProjectSupplementalAgreementInformationForIDIQId;
                            }
                        }
                        resAdv.SAForProject = projectTaskOrder.SAForProject;
                        resAdv.SAScope = projectTaskOrder.SAScope;
                        resAdv.TaxClassification = projectTaskOrder.TaxClassification;
                        resAdv.SAExpirationDate = projectTaskOrder.SAExpirationDate;
                        resAdv.ContractExpires = projectTaskOrder.ContractExpires;

                        //  resAdv.ProjectTaskOrderWorkFlowTrackingId = ProjectTaskOrderWorkFlowTrackingId;
                        resAdv.ProjectSupplementalAgreementInformationForIDIQId = ProjectSupplementalAgreementInformationForIDIQId;

                        var updateAdv = _advertisement.UpdateProjectSupplementalAgreementForIDIQDetails(resAdv);
                    }
                    else
                    {
                        //var mapWork = _mapper.Map<ProjectTaskOrderWorkFlowTrackingDto, ProjectTaskOrderWorkFlowTracking>(projectContract.ProjectWork);
                        //var saveWork = await _advertisement.CreateProjectTaskOrderWorkFlowTracking(mapWork);
                        var mapInfo = _mapper.Map<ProjectSupplementalAgreementInformationForIDIQDto, ProjectSupplementalAgreementInformationForIDIQ>(projectTaskOrder.TaskOrderInformation);
                        var saveInfo = await _advertisement.CreateProjectSupplementalAgreementForIDIQInformation(mapInfo);
                        var mapSubTax = _mapper.Map<IList<ProjectSupplementalAgreementInformationForIDIQSubTaxDetails>>(projectTaskOrder.TaskOrderInformation.SubTaxDetailsDto);

                        foreach (var item in mapSubTax)
                        {
                            item.CreatedAt = DateTime.Now;
                            item.ProjectSupplementalAgreementInformationForIDIQId = saveInfo.ProjectSupplementalAgreementInformationForIDIQId;
                        }
                        var respTaskOrderDetails = await _advertisement.CreateSupplementalAgreementForIDIQInformationSubTaxDetails(mapSubTax);

                        if (/*saveWork != null &&*/ saveInfo != null)
                        {
                            var mapAdv = _mapper.Map<ProjectSupplementalAgreementForIDIQDto, ProjectSupplementalAgreementForIDIQ>(projectTaskOrder);
                            mapAdv.ProjectSupplementalAgreementInformationForIDIQId = saveInfo.ProjectSupplementalAgreementInformationForIDIQId;
                            var saveAdv = _advertisement.CreateProjectSupplementalAgreementForIDIQDetails(mapAdv);
                        }
                    }

                    return new ResponseDto<StatusCode>()
                    {
                        Status = StatusCode.Success
                    };

                }
                return new ResponseDto<StatusCode>()
                {
                    Status = StatusCode.BadRequest
                };
            }
            catch (Exception ex)
            {
                return new ResponseDto<StatusCode>()
                {
                    Status = StatusCode.BadRequest,
                    Error = ex.InnerException.ToString()
                };
            }
        }
        public async Task<ResponseDto<ProjectSupplementalAgreementForIDIQDto>> GetProjectTSupplementalAgreementForIDIQ(int ProjectInfoId)
        {
            try
            {
                ProjectSupplementalAgreementForIDIQDto detailsDto = new ProjectSupplementalAgreementForIDIQDto();
                var resInfo = await _projectService.GetProjectGeneralInformation(ProjectInfoId);
                var map = _mapper.Map<ProjectGeneralInformation, ProjectGeneralInformationDto>(resInfo);
                if (map != null)
                {
                    detailsDto.Information = map;
                    var resAdv = await _advertisement.GetProjectSupplementalAgreementForIDIQDetailsByProjectInfoId(ProjectInfoId);
                    if (resAdv != null)
                    {
                        detailsDto.SAForProject = Convert.ToString(resAdv.SAForProject);
                        detailsDto.SAScope = resAdv.SAScope;
                        detailsDto.TaxClassification = resAdv.TaxClassification;
                        detailsDto.SAExpirationDate = resAdv.SAExpirationDate;
                        detailsDto.ContractExpires = resAdv.ContractExpires;
                        detailsDto.ProjectSupplementalAgreementForIDIQId = resAdv.ProjectSupplementalAgreementForIDIQId;
                        detailsDto.ProjectGeneralInformationId = map.ProjectGeneralInformationId;


                        var resArea = await _projectService.GetProjectConsultantAreaByProjectInfoId(ProjectInfoId);
                        if (resArea != null)
                        {
                            detailsDto.ProjectManagerId = resArea.ProjectManagerId;
                            detailsDto.ManagerPhone = resArea.ManagerPhone;
                        }
                        var resContractInfo = await _advertisement.GetProjectSupplementalAgreementForIDIQInformation(resAdv.ProjectSupplementalAgreementInformationForIDIQId);

                        var resAdv1 = await _advertisement.GetProjectContractDetailsByProjectInfoId(ProjectInfoId);
                        if (resContractInfo != null)
                        {
                            var mapInfo = _mapper.Map<ProjectSupplementalAgreementInformationForIDIQ, ProjectSupplementalAgreementInformationForIDIQDto>(resContractInfo);
                            detailsDto.TaskOrderInformation = mapInfo;

                        }
                        else
                        {
                            var resContractInfo1 = await _advertisement.GetProjectContractInformation(resAdv1.ProjectContractInformationId);
                            if (resContractInfo1 != null)
                            {
                                var mapInfo = _mapper.Map<ProjectContractInformation, ProjectSupplementalAgreementInformationForIDIQDto>(resContractInfo1);
                                detailsDto.TaskOrderInformation = mapInfo;
                            }
                        }
                        var resSubDetail = await _advertisement.GetSupplementalAgreementForIDIQInformationSubTaxDetails(resAdv.ProjectSupplementalAgreementInformationForIDIQId);
                        if (resSubDetail.Count > 0)
                        {
                            var mapareasub = _mapper.Map<IList<ProjectSupplementalAgreementInformationForIDIQSubTaxDetailsDto>>(resSubDetail);
                            detailsDto.TaskOrderInformation.SubTaxDetailsDto = mapareasub;
                        }
                        else
                        {
                            var resSubDetail1 = await _advertisement.GetContractInformationSubTaxDetails(resAdv1.ProjectContractInformationId);
                            if (resSubDetail1.Count > 0)
                            {
                                var mapareasub = _mapper.Map<IList<ProjectSupplementalAgreementInformationForIDIQSubTaxDetailsDto>>(resSubDetail1);
                                detailsDto.TaskOrderInformation.SubTaxDetailsDto = mapareasub;
                            }
                        }
                    }
                    else
                    {
                        var resAdv1 = await _advertisement.GetProjectContractDetailsByProjectInfoId(ProjectInfoId);
                        if (resAdv1 != null)
                        {
                            var resContractInfo1 = await _advertisement.GetProjectContractInformation(resAdv1.ProjectContractInformationId);
                            if (resContractInfo1 != null)
                            {
                                var mapInfo = _mapper.Map<ProjectContractInformation, ProjectSupplementalAgreementInformationForIDIQDto>(resContractInfo1);
                                detailsDto.TaskOrderInformation = mapInfo;
                            }

                            var resSubDetail1 = await _advertisement.GetContractInformationSubTaxDetails(resAdv1.ProjectContractInformationId);
                            if (resSubDetail1.Count > 0)
                            {
                                var mapareasub = _mapper.Map<IList<ProjectSupplementalAgreementInformationForIDIQSubTaxDetailsDto>>(resSubDetail1);
                                detailsDto.TaskOrderInformation.SubTaxDetailsDto = mapareasub;
                            }
                        }
                    }
                }
                return new ResponseDto<ProjectSupplementalAgreementForIDIQDto>()
                {
                    Status = StatusCode.Success,
                    Result = detailsDto
                };
            }
            catch (Exception ex)
            {
                return new ResponseDto<ProjectSupplementalAgreementForIDIQDto>()
                {
                    Status = StatusCode.BadRequest,
                    Error = ex.InnerException.ToString()
                };
            }
        }

        public async Task<ResponseDto<StatusCode>> DeleteProjectAddendum(int AddendumId)
        {
            try
            {
                var resp = await _advertisement.GetProjectAddendumDetailsById(AddendumId);
                if (resp != null)
                {
                    resp.IsDeleted = true;
                    await _advertisement.UpdateProjectAddendumDetails(resp);
                    return new ResponseDto<StatusCode>()
                    {
                        Status = StatusCode.Success,
                    };
                }
                else
                {
                    return new ResponseDto<StatusCode>()
                    {
                        Status = StatusCode.NotFound,
                    };
                }
            }
            catch (Exception ex)
            {
                return new ResponseDto<StatusCode>()
                {
                    Status = StatusCode.BadRequest
                };
            }
        }


        public async Task<ResponseDto<StatusCode>> DeleteProjectShorlist(int ShorlistId)
        {
            try
            {
                var resp = await _advertisement.GetProjectAdvertisementShortlistDetailsById(ShorlistId);
                if (resp != null)
                {
                    //resp.IsDeleted = true;
                    resp.IsPostToWeb = false;
                    await _advertisement.UpdateProjectAdvertisementShortlistDetails(resp);
                    return new ResponseDto<StatusCode>()
                    {
                        Status = StatusCode.Success,
                    };
                }
                else
                {
                    return new ResponseDto<StatusCode>()
                    {
                        Status = StatusCode.NotFound,
                    };
                }
            }
            catch (Exception ex)
            {
                return new ResponseDto<StatusCode>()
                {
                    Status = StatusCode.BadRequest
                };
            }
        }

        public async Task<ResponseDto<StatusCode>> PostToWebShorlist(int ShorlistId)
        {
            try
            {
                var resp = await _advertisement.GetProjectAdvertisementShortlistDetailsById(ShorlistId);
                if (resp != null)
                {
                    //resp.IsDeleted = true;
                    resp.IsPostToWeb = true;
                    await _advertisement.UpdateProjectAdvertisementShortlistDetails(resp);
                    return new ResponseDto<StatusCode>()
                    {
                        Status = StatusCode.Success,
                    };
                }
                else
                {
                    return new ResponseDto<StatusCode>()
                    {
                        Status = StatusCode.NotFound,
                    };
                }
            }
            catch (Exception ex)
            {
                return new ResponseDto<StatusCode>()
                {
                    Status = StatusCode.BadRequest
                };
            }
        }

        public async Task<ResponseDto<ProjectAdvertisementShortlistDto>> GetProjectAdvertisementShortlistDetailsByProjectInfoId(int ProjectInfoId)
        {
            try
            {
                var resp = await _advertisement.GetProjectAdvertisementShortlistDetailsByProjectInfoId(ProjectInfoId);
                var map = _mapper.Map<ProjectAdvertisementShortlist, ProjectAdvertisementShortlistDto>(resp);
                if (map != null)
                {
                    var get = await _advertisement.GetProjectAdvertisementShortlistConsultantProviderDetails(map.ProjectAdvertisementShortlistId);
                    IList<ShortListDto> ShortList = new List<ShortListDto>();
                    foreach (var item in get)
                    {
                        ShortListDto obj = new ShortListDto();
                        obj.ShortList = Convert.ToString(item.SelectedShortlistConsultantProviderId);
                        var sub = await _advertisement.GetShortlistSubDetails(item.ProjectAdvertisementShortlistConsultantProviderId);
                        var mapsub = _mapper.Map<IList<ShortlistSubDetailsDto>>(sub);
                        obj.SubTaxDetailsDto = mapsub;
                        ShortList.Add(obj);
                    }
                    map.ShortList = ShortList;
                }
                return new ResponseDto<ProjectAdvertisementShortlistDto>()
                {
                    Status = StatusCode.Success,
                    Result = map
                };
            }
            catch (Exception ex)
            {
                return new ResponseDto<ProjectAdvertisementShortlistDto>()
                {
                    Status = StatusCode.BadRequest
                };
            }
        }

        public async Task<ResponseDto<ProjectAdvertisementSelectionDto>> GetAdvertisementSelection(int ProjectGeneralInformationId)
        {
            try
            {
                var resp = await _advertisement.GetProjectAdvertisementSelectionDetailsByProjectInfoId(ProjectGeneralInformationId);
                if (resp != null)
                {
                    var map = _mapper.Map<ProjectAdvertisementSelectionDto>(resp);
                    return new ResponseDto<ProjectAdvertisementSelectionDto>()
                    {
                        Status = StatusCode.Success,
                        Result = map
                    };
                }
                return new ResponseDto<ProjectAdvertisementSelectionDto>()
                {
                    Status = StatusCode.NotFound
                };
                //else
                //{
                //    var respad = await _advertisement.GetAdvertisementDetail(ProjectGeneralInformationId);
                //    if (respad != null)
                //    {
                //        var obj = new ProjectAdvertisementSelectionDto()
                //        {
                //            ClosingDate = respad.ClosingDate,
                //            AdvertisementDate = respad.AdvertisementDate,
                //            ProjectGeneralInformationId = respad.ProjectGeneralInformationId,
                //            //StateProjectno = respad.StateProjectno,
                //            //ContractNo = respad.ContractNo
                //        };
                //        return new ResponseDto<ProjectAdvertisementSelectionDto>()
                //        {
                //            Result = obj,
                //            Status = StatusCode.Success
                //        };
                //    }
                //    return new ResponseDto<ProjectAdvertisementSelectionDto>()
                //    {
                //        Status = StatusCode.NotFound
                //    };
                //}
            }
            catch (Exception ex)
            {
                return new ResponseDto<ProjectAdvertisementSelectionDto>()
                {
                    Status = StatusCode.BadRequest
                };
            }
        }


        public async Task<ResponseDto<ProjectAdvertisementDocumentDto>> GetAdvertisementDocumentDetails(int ProjectGeneralInformationId)
        {
            try
            {
                var respD = await _advertisement.GetProjectAdvertisementDetailsByProjectInfoId(ProjectGeneralInformationId);
                if (respD == null)
                {
                    return new ResponseDto<ProjectAdvertisementDocumentDto>()
                    {
                        Status = StatusCode.NotFound
                    };
                }
                else
                {
                    var respad = await GetProjectAdvertisementDetails(ProjectGeneralInformationId);
                    if (respad != null)
                    {
                        var obj = new ProjectAdvertisementDocumentDto()
                        {
                            ProjectAdvertisementDetailsId = respad.Result.ProjectAdvertisementDetailsId,
                            AdvertisementDate = respad.Result.ProjectWork.AdvertisementDate,
                            ClosingDate = respad.Result.ProjectWork.ClosingDate,
                            DBEGoal = respad.Result.Information.DBEGoel,
                            ContractNo = respad.Result.Information.ContractNo,
                            ProjectName = respad.Result.Information.ProjectName,
                            StateProjectNo = respad.Result.Information.StateProjectNo,
                            FAPNo = respad.Result.Information.FAPNo,
                            Parish = Convert.ToString(respad.Result.Information.Parish),
                        };
                        var resp = await _advertisement.GetProjectAdvertisementDocument(respD.ProjectAdvertisementDetailsId);
                        if (resp != null)
                        {
                            obj.AdvertDateGoesHere = Convert.ToString(resp.AdvertDateGoesHere);
                            obj.Details = resp.Details;
                            obj.TypeOfAdvertGoesHere = resp.TypeOfAdvertGoesHere;
                        }
                        return new ResponseDto<ProjectAdvertisementDocumentDto>()
                        {
                            Result = obj,
                            Status = StatusCode.Success
                        };
                    }
                    return new ResponseDto<ProjectAdvertisementDocumentDto>()
                    {
                        Status = StatusCode.NotFound
                    };



                }
            }
            catch (Exception ex)
            {
                return new ResponseDto<ProjectAdvertisementDocumentDto>()
                {
                    Status = StatusCode.BadRequest
                };
            }
        }


        public async Task<ResponseDto<StatusCode>> CreateProcessor(ProcessorMasterDto processorDto)
        {
            try
            {
                var map = _mapper.Map<ProcessorMasterDto, ProcessorMaster>(processorDto);
                var save = await _advertisement.CreateProcessor(map);
                return new ResponseDto<StatusCode>()
                {
                    Status = StatusCode.Success
                };
            }
            catch (Exception ex)
            {
                return new ResponseDto<StatusCode>()
                {
                    Status = StatusCode.BadRequest,
                    Error = ex.InnerException.ToString()
                };
            }
        }


    }
}