﻿using AutoMapper;
using ContractsAndAgreements.Helpers.Interfaces;
using ContractsAndAgreements_Domain.Dtos;
using ContractsAndAgreements_Domain.Entity;
using ContractsAndAgreements_Domain.Enums;
using ContractsAndAgreements_DomainLogic.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace ContractsAndAgreements.Helpers.Implementation
{
    public class CommonControllerHelper : ICommonControllerHelper
    {
        private readonly IMapper _mapper;
        private readonly ICommonService _commonService;
       
        public CommonControllerHelper(IMapper mapper, ICommonService commonService)
        {
            _mapper = mapper;
            _commonService = commonService;
        }

        public async Task<ResponseDto<IList<CommonDropdownDto>>> GetAllContractTypeList()
        {
            try
            {
                var resp = await _commonService.GetAllContractTypeList();
                var map = _mapper.Map<IList<CommonDropdownDto>>(resp);
                return new ResponseDto<IList<CommonDropdownDto>>()
                {
                    Status = StatusCode.Success,
                    Result = map
                };
            }
            catch (Exception ex)
            {
                return new ResponseDto<IList<CommonDropdownDto>>()
                {
                    Status = StatusCode.BadRequest
                };
            }
        }

        public async Task<ResponseDto<IList<CommonDropdownDto>>> GetAllFeeTypeList()
        {
            try
            {
                var resp = await _commonService.GetAllFeeTypeList();
                var map = _mapper.Map<IList<CommonDropdownDto>>(resp);
                return new ResponseDto<IList<CommonDropdownDto>>()
                {
                    Status = StatusCode.Success,
                    Result = map
                };
            }
            catch (Exception ex)
            {
                return new ResponseDto<IList<CommonDropdownDto>>()
                {
                    Status = StatusCode.BadRequest
                };
            }
        }
        public async Task<ResponseDto<IList<CommonDropdownDto>>> GetAllCompensationTypeList()
        {
            try
            {
                var resp = await _commonService.GetAllCompensationTypeList();
                var map = _mapper.Map<IList<CommonDropdownDto>>(resp);
                return new ResponseDto<IList<CommonDropdownDto>>()
                {
                    Status = StatusCode.Success,
                    Result = map
                };
            }
            catch (Exception ex)
            {
                return new ResponseDto<IList<CommonDropdownDto>>()
                {
                    Status = StatusCode.BadRequest
                };
            }
        }

        public async Task<ResponseDto<IList<CommonDropdownDto>>> GetAllParishList()
        {
            try
            {
                var resp = await _commonService.GetAllParishList();
                var map = _mapper.Map<IList<CommonDropdownDto>>(resp);
                return new ResponseDto<IList<CommonDropdownDto>>()
                {
                    Status = StatusCode.Success,
                    Result = map
                };
            }
            catch (Exception ex)
            {
                return new ResponseDto<IList<CommonDropdownDto>>()
                {
                    Status = StatusCode.BadRequest
                };
            }
        }

        public async Task<ResponseDto<IList<CommonDropdownDto>>> GetAllSelectionList()
        {
            try
            {
                var resp = await _commonService.GetAllSelectionList();
                var map = _mapper.Map<IList<CommonDropdownDto>>(resp);
                return new ResponseDto<IList<CommonDropdownDto>>()
                {
                    Status = StatusCode.Success,
                    Result = map
                };
            }
            catch (Exception ex)
            {
                return new ResponseDto<IList<CommonDropdownDto>>()
                {
                    Status = StatusCode.BadRequest
                };
            }
        }

        public async Task<ResponseDto<IList<CommonDropdownDto>>> GetAllProjectManagerList()
        {
            try
            {
                var resp = await _commonService.GetAllProjectManagerList();
                var map = _mapper.Map<IList<CommonDropdownDto>>(resp);
                return new ResponseDto<IList<CommonDropdownDto>>()
                {
                    Status = StatusCode.Success,
                    Result = map
                };
            }
            catch (Exception ex)
            {
                return new ResponseDto<IList<CommonDropdownDto>>()
                {
                    Status = StatusCode.BadRequest
                };
            }
        }
        //public async Task<ResponseDto<IList<CommonDropdownDto>>> GetAllConsultantAreaSubTaxList()
        //{
        //    try
        //    {
        //        var resp = await _commonService.GetAllConsultantAreaSubTaxList();
        //        var map = _mapper.Map<IList<CommonDropdownDto>>(resp);
        //        return new ResponseDto<IList<CommonDropdownDto>>()
        //        {
        //            Status = StatusCode.Success,
        //            Result = map
        //        };
        //    }
        //    catch (Exception ex)
        //    {
        //        return new ResponseDto<IList<CommonDropdownDto>>()
        //        {
        //            Status = StatusCode.BadRequest
        //        };
        //    }
        //}
        public async Task<ResponseDto<IList<ConsultantProviderDto>>> GetAllConsultantProviderList()
        {
            try
            {
                var resp = await _commonService.GetAllConsultantProviderList();
                var map = _mapper.Map<IList<ConsultantProviderDto>>(resp);
                return new ResponseDto<IList<ConsultantProviderDto>>()
                {
                    Status = StatusCode.Success,
                    Result = map
                };
            }
            catch (Exception ex)
            {
                return new ResponseDto<IList<ConsultantProviderDto>>()
                {
                    Status = StatusCode.BadRequest
                };
            }
        }
        public async Task<ResponseDto<ConsultantProviderDto>> GetConsultantProviderDetail(int ConsultantProviderId)
        {
            try
            {
                var resp = await _commonService.GetConsultantProviderDetail(ConsultantProviderId);
                var map = _mapper.Map<ConsultantProviderMaster, ConsultantProviderDto>(resp);
                return new ResponseDto<ConsultantProviderDto>()
                {
                    Status = StatusCode.Success,
                    Result = map
                };
            }
            catch (Exception ex)
            {
                return new ResponseDto<ConsultantProviderDto>()
                {
                    Status = StatusCode.BadRequest
                };
            }
        }

        public async Task<ResponseDto<IList<CommonDropdownDto>>> GetAllSupervisorList()
        {
            try
            {
                var resp = await _commonService.GetAllSupervisorList();
                var map = _mapper.Map<IList<CommonDropdownDto>>(resp);
                return new ResponseDto<IList<CommonDropdownDto>>()
                {
                    Status = StatusCode.Success,
                    Result = map
                };
            }
            catch (Exception ex)
            {
                return new ResponseDto<IList<CommonDropdownDto>>()
                {
                    Status = StatusCode.BadRequest
                };
            }
        }
        public async Task<ResponseDto<IList<CommonDropdownDto>>> GetAllProcessorList()
        {
            try
            {
                var resp = await _commonService.GetAllProcessorList();
                var map = _mapper.Map<IList<CommonDropdownDto>>(resp);
                return new ResponseDto<IList<CommonDropdownDto>>()
                {
                    Status = StatusCode.Success,
                    Result = map
                };
            }
            catch (Exception ex)
            {
                return new ResponseDto<IList<CommonDropdownDto>>()
                {
                    Status = StatusCode.BadRequest
                };
            }
        }

        public async Task<ResponseDto<StatusCode>> SaveConsultantProvider(ConsultantProviderDto informationDto)
        {
            try
            {
                var exist = await _commonService.GetConsultantProviderDetail(informationDto.ConsultantProviderId);
                if (exist != null)
                {
                    exist.City = informationDto.City;
                    exist.ConsultantProviderName = informationDto.ConsultantProviderName;
                    exist.ContactName = informationDto.ContactName;
                    exist.Email = informationDto.Email;
                    exist.Fax = informationDto.Fax;
                    exist.IsActive = informationDto.IsActive;
                    exist.JobTitle = informationDto.JobTitle;
                    exist.OfficeAddress = informationDto.OfficeAddress;
                    exist.ProofofInsurance = informationDto.ProofofInsurance;
                    exist.InsuranceCompany = informationDto.InsuranceCompany;
                    exist.EffectiveDateofCoverage = informationDto.EffectiveDateofCoverage;
                    exist.ExpirationDateofCoverage = informationDto.ExpirationDateofCoverage;
                    exist.DisclosureofOwnership = informationDto.DisclosureofOwnership;
                    exist.CertificateofAuthority = informationDto.CertificateofAuthority;
                    exist.ResolutionCertificate = informationDto.ResolutionCertificate;
                    exist.EffectiveDate = informationDto.EffectiveDate;
                    exist.PersonAuthorizedToSign = informationDto.PersonAuthorizedToSign;
                    exist.Comments = informationDto.Comments;
                    exist.Type = informationDto.Type;
                    var update = await _commonService.UpdateConsultantProvider(exist);
                    
                    return new ResponseDto<StatusCode>()
                    {
                        Status = StatusCode.Updated
                    };
                }
                else
                {
                    var map = _mapper.Map<ConsultantProviderDto, ConsultantProviderMaster>(informationDto);
                    var save = await _commonService.SaveConsultantProvider(map);
                    return new ResponseDto<StatusCode>()
                    {
                        Status = StatusCode.Success
                    };
                }
            }
            catch (Exception ex)
            {
                return new ResponseDto<StatusCode>()
                {
                    Status = StatusCode.BadRequest
                };
            }
        }
        public async Task<ResponseDto<StatusCode>> DeleteConsultantProvider(int ConsultantProviderId)
        {
            try
            {
                var get = await _commonService.GetConsultantProviderDetail(ConsultantProviderId);
                if (get != null)
                {
                    await _commonService.DeleteConsultantProvider(get);
                }
                return new ResponseDto<StatusCode>()
                {
                    Status = StatusCode.Deleted
                };
            }
            catch (Exception ex)
            {
                return new ResponseDto<StatusCode>()
                {
                    Status = StatusCode.BadRequest
                };
            }

        }
        public async Task<ResponseDto<IList<CommonDropdownDto>>> GetCompensationTypeList()
        {
            try
            {
                var resp = await _commonService.GetCompensationTypeList();
                var map = _mapper.Map<IList<CommonDropdownDto>>(resp);
                return new ResponseDto<IList<CommonDropdownDto>>()
                {
                    Status = StatusCode.Success,
                    Result = map
                };
            }
            catch (Exception ex)
            {
                return new ResponseDto<IList<CommonDropdownDto>>()
                {
                    Status = StatusCode.BadRequest
                };
            }
        }
        public async Task<ResponseDto<IList<CommonDropdownDto>>> GetAllContractAgencyList()
        {
            try
            {
                var resp = await _commonService.GetAllContractAgencyList();
                var map = _mapper.Map<IList<CommonDropdownDto>>(resp);
                return new ResponseDto<IList<CommonDropdownDto>>()
                {
                    Status = StatusCode.Success,
                    Result = map
                };
            }
            catch (Exception ex)
            {
                return new ResponseDto<IList<CommonDropdownDto>>()
                {
                    Status = StatusCode.BadRequest
                };
            }
        }

    }
}