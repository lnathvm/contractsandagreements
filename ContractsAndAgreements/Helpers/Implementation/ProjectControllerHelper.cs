﻿using AutoMapper;
using ContractsAndAgreements.Helpers.Interfaces;
using ContractsAndAgreements_Domain.Dtos;
using ContractsAndAgreements_Domain.Entity;
using ContractsAndAgreements_Domain.Enums;
using ContractsAndAgreements_DomainLogic.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace ContractsAndAgreements.Helpers.Implementation
{
    public class ProjectControllerHelper : IProjectControllerHelper
    {
        private readonly IMapper _mapper;
        private readonly IProjectService _projectService;

        public ProjectControllerHelper(IMapper mapper, IProjectService projectService)
        {
            _mapper = mapper;
            _projectService = projectService;
        }
        public async Task<ResponseDto<int>> CreateProjectGeneralInformation(ProjectGeneralInformationDto generalInformationDto)
        {
            try
            {
                var mapprojectinfo = _mapper.Map<ProjectGeneralInformationDto, ProjectGeneralInformation>(generalInformationDto);
                var mapconsultant = _mapper.Map<ProjectConsultantAreaDto, ProjectConsultantArea>(generalInformationDto.AreaDto);
                var mapconsultantDetails = _mapper.Map<IList<ConsultantAreaSubTaxDetails>>(generalInformationDto.SubTaxDetailsDto);
                var maptracking = _mapper.Map<ProjectTrackingDto, ProjectTracking>(generalInformationDto.TrackingDto);
                //string type = "";
                //foreach (var item in generalInformationDto.CompensationType)
                //{
                //    type = type + "," + item;
                //}
                //mapprojectinfo.CompensationType = type;
                var respProjectinfo = await _projectService.CreateProjectGeneralInformation(mapprojectinfo);
                if (respProjectinfo != null)
                {
                    if (mapconsultant != null)
                    {
                        mapconsultant.ProjectGeneralInformationId = respProjectinfo.ProjectGeneralInformationId;
                        var respconsultant = await _projectService.CreateProjectConsultantArea(mapconsultant);
                        if (respconsultant != null)
                        {
                            foreach (var item in mapconsultantDetails)
                            {
                                item.CreatedAt = DateTime.Now;
                                item.ProjectConsultantAreaId = respconsultant.ProjectConsultantAreaId;
                            }
                            var respconsultantDetails = await _projectService.CreateConsultantAreaSubTaxDetails(mapconsultantDetails);
                            if (respconsultantDetails == StatusCode.Success)
                            {
                                if (maptracking != null)
                                {
                                    maptracking.ProjectGeneralInformationId = respProjectinfo.ProjectGeneralInformationId;
                                    var respTracking = await _projectService.CreateProjectTracking(maptracking);
                                    return new ResponseDto<int>()
                                    {
                                        Status = StatusCode.Success,
                                        Result = respProjectinfo.ProjectGeneralInformationId
                                    };
                                }
                                else
                                {
                                    return new ResponseDto<int>()
                                    {
                                        Status = StatusCode.Success,
                                        Result = respProjectinfo.ProjectGeneralInformationId
                                    };
                                }
                            }
                        }
                    }
                    else
                    {
                        return new ResponseDto<int>()
                        {
                            Status = StatusCode.Success,
                            Result = respProjectinfo.ProjectGeneralInformationId
                        };
                    }
                }
                return new ResponseDto<int>()
                {
                    Status = StatusCode.BadRequest,
                };
            }
            catch (Exception ex)
            {
                return new ResponseDto<int>()
                {
                    Status = StatusCode.BadRequest,
                    Error = Convert.ToString(ex.InnerException)
                };
            }
        }

        //public async Task<ResponseDto<IList<ConsultantProviderDto>>> GetAllConsultantProviderList()
        //{
        //    try
        //    {
        //        var resp = await _projectService.GetAllConsultantProviderList();
        //        var map = _mapper.Map<IList<ConsultantProviderDto>>(resp);
        //        return new ResponseDto<IList<ConsultantProviderDto>>()
        //        {
        //            Status = StatusCode.Success,
        //            Result = map
        //        };
        //    }
        //    catch (Exception ex)
        //    {
        //        return new ResponseDto<IList<ConsultantProviderDto>>()
        //        {
        //            Status = StatusCode.BadRequest
        //        };
        //    }
        //}
        //public async Task<ResponseDto<ConsultantProviderDto>> GetConsultantProviderDetail(int ConsultantProviderId)
        //{
        //    try
        //    {
        //        var resp = await _projectService.GetConsultantProviderDetail(ConsultantProviderId);
        //        var map = _mapper.Map<ConsultantProviderMaster, ConsultantProviderDto>(resp);
        //        return new ResponseDto<ConsultantProviderDto>()
        //        {
        //            Status = StatusCode.Success,
        //            Result = map
        //        };
        //    }
        //    catch (Exception ex)
        //    {
        //        return new ResponseDto<ConsultantProviderDto>()
        //        {
        //            Status = StatusCode.BadRequest
        //        };
        //    }
        //}

        public async Task<ResponseDto<IList<DashboardInfoDto>>> GetAllProjectGeneralInformationList()
        {
            try
            {
                var resp = await _projectService.GetAllProjectGeneralInformationList();
                var map = _mapper.Map<IList<DashboardInfoDto>>(resp);
                return new ResponseDto<IList<DashboardInfoDto>>()
                {
                    Status = StatusCode.Success,
                    Result = map
                };
            }
            catch (Exception ex)
            {
                return new ResponseDto<IList<DashboardInfoDto>>()
                {
                    Status = StatusCode.BadRequest
                };
            }
        }
        public async Task<ResponseDto<ProjectGeneralInformationDto>> GetProjectGeneralInformation(int ProjectInfoId)
        {
            try
            {
                var res = await _projectService.GetProjectGeneralInformation(ProjectInfoId);
                //List<int> cType = new List<int>();
                //if (!string.IsNullOrEmpty(res.CompensationType))
                //{
                //    var sSplit = res.CompensationType.Split(',');

                //    foreach (var item in sSplit)
                //    {
                //        if (!string.IsNullOrEmpty(item))
                //        {
                //            cType.Add(Convert.ToInt32(item));
                //        }
                //    }
                //}
                //res.CompensationType = cType;
                var map = _mapper.Map<ProjectGeneralInformation, ProjectGeneralInformationDto>(res);
                var resArea = await _projectService.GetProjectConsultantAreaByProjectInfoId(ProjectInfoId);
                if (resArea != null)
                {
                    var maparea = _mapper.Map<ProjectConsultantArea, ProjectConsultantAreaDto>(resArea);
                    map.AreaDto = maparea;
                    var resSubDetail = await _projectService.GetConsultantAreaSubTaxDetails(resArea.ProjectConsultantAreaId);
                    if (resSubDetail.Count > 0)
                    {
                        var mapareasub = _mapper.Map<IList<ConsultantAreaSubTaxDetailsDto>>(resSubDetail);
                        map.SubTaxDetailsDto = mapareasub;
                    }
                    var resTracking = await _projectService.GetProjectTrackingByProjectGeneralInformationId(ProjectInfoId);
                    if (resTracking != null)
                    {
                        var maptracking = _mapper.Map<ProjectTracking, ProjectTrackingDto>(resTracking);
                        map.TrackingDto = maptracking;
                    }
                }
                return new ResponseDto<ProjectGeneralInformationDto>()
                {
                    Status = StatusCode.Success,
                    Result = map
                };
            }
            catch (Exception ex)
            {
                return new ResponseDto<ProjectGeneralInformationDto>()
                {
                    Status = StatusCode.BadRequest
                };
            }
        }

        public async Task<ResponseDto<int>> UpdateProjectGeneralInformation(ProjectGeneralInformationDto generalInformationDto)
        {
            try
            {
                var existing = await _projectService.GetProjectGeneralInformation(generalInformationDto.ProjectGeneralInformationId);
                if (existing != null)
                {
                    existing.Comment = generalInformationDto.Comment;
                    existing.ContractAgency = generalInformationDto.ContractAgency;
                    existing.ContractType = generalInformationDto.ContractType;
                    // existing.EstContractAmount = generalInformationDto.EstContractAmount;
                    existing.FAPNo = generalInformationDto.FAPNo;
                    existing.FeeType = generalInformationDto.FeeType;
                    existing.FHWAFullOversight = generalInformationDto.FHWAFullOversight;
                    existing.GeneralDescription = generalInformationDto.GeneralDescription;
                    existing.ContractNo = generalInformationDto.ContractNo;
                    existing.Parish = generalInformationDto.Parish;
                    existing.ProjectName = generalInformationDto.ProjectName;
                    existing.Route = generalInformationDto.Route;
                    existing.Selection = generalInformationDto.Selection;
                    existing.StateProjectNo = generalInformationDto.StateProjectNo;
                    existing.DBEGoel = generalInformationDto.DBEGoel;
                    existing.EStContractAmount = generalInformationDto.EStContractAmount;
                    //string type = "";
                    //foreach (var item in generalInformationDto.CompensationType)
                    //{
                    //    type = type + "," + item;
                    //}
                    //existing.CompensationType = type;
                    var update = await _projectService.UpdateProjectGeneralInformation(existing);
                    if (update != null)
                    {
                        var existingArea = await _projectService.GetProjectConsultantAreaByProjectInfoId(existing.ProjectGeneralInformationId);
                        int ProjectConsultantAreaId = 0;
                        if (existingArea != null)
                        {
                            existingArea.ConsultantProviderId = generalInformationDto.AreaDto.ConsultantProviderId;
                            existingArea.ContactPerson = generalInformationDto.AreaDto.ContactPerson;
                            existingArea.Email = generalInformationDto.AreaDto.Email;
                            existingArea.Fax = generalInformationDto.AreaDto.Fax;
                            existingArea.JobTitle = generalInformationDto.AreaDto.JobTitle;
                            existingArea.ManagerPhone = generalInformationDto.AreaDto.ManagerPhone;
                            existingArea.OfficeAddress = generalInformationDto.AreaDto.OfficeAddress;
                            existingArea.Phone = generalInformationDto.AreaDto.Phone;
                            existingArea.ProjectManagerId = generalInformationDto.AreaDto.ProjectManagerId;
                            existingArea.TaxId = generalInformationDto.AreaDto.TaxId;
                            existingArea.Title = generalInformationDto.AreaDto.Title;
                            var updateAre = await _projectService.UpdateProjectConsultantArea(existingArea);
                            ProjectConsultantAreaId = updateAre.ProjectConsultantAreaId;
                        }
                        else
                        {
                            var mapconsultant = _mapper.Map<ProjectConsultantAreaDto, ProjectConsultantArea>(generalInformationDto.AreaDto);
                            if (mapconsultant != null)
                            {
                                mapconsultant.ProjectGeneralInformationId = update.ProjectGeneralInformationId;
                                var respconsultant = await _projectService.CreateProjectConsultantArea(mapconsultant);
                                ProjectConsultantAreaId = respconsultant.ProjectConsultantAreaId;
                            }
                        }
                        var mapconsultantDetails = _mapper.Map<IList<ConsultantAreaSubTaxDetails>>(generalInformationDto.SubTaxDetailsDto);
                        if (ProjectConsultantAreaId != 0 && mapconsultantDetails.Count > 0)
                        {
                            var delete = await _projectService.DeleteConsultantAreaSubTaxDetails(ProjectConsultantAreaId);
                            foreach (var item in mapconsultantDetails)
                            {

                                item.CreatedAt = DateTime.Now;
                                item.ProjectConsultantAreaId = ProjectConsultantAreaId;
                            }
                            var respconsultantDetails = await _projectService.CreateConsultantAreaSubTaxDetails(mapconsultantDetails);
                        }
                        var ExistingProjectTracking = await _projectService.GetProjectTrackingByProjectGeneralInformationId(existing.ProjectGeneralInformationId);
                        if (ExistingProjectTracking != null)
                        {
                            ExistingProjectTracking.AuditReport = generalInformationDto.TrackingDto.AuditReport;
                            ExistingProjectTracking.AwardNotification = generalInformationDto.TrackingDto.AwardNotification;
                            ExistingProjectTracking.Closed = generalInformationDto.TrackingDto.Closed;
                            ExistingProjectTracking.FinalCost = generalInformationDto.TrackingDto.FinalCost;
                            ExistingProjectTracking.FinalInvoicedRecvd = generalInformationDto.TrackingDto.FinalInvoicedRecvd;
                            ExistingProjectTracking.FinalToAcct = generalInformationDto.TrackingDto.FinalToAcct;
                            ExistingProjectTracking.Received = generalInformationDto.TrackingDto.Received;
                            var updateTracking = await _projectService.UpdateProjectTracking(ExistingProjectTracking);
                        }
                        else
                        {
                            var maptracking = _mapper.Map<ProjectTrackingDto, ProjectTracking>(generalInformationDto.TrackingDto);
                            if (maptracking != null)
                            {
                                maptracking.ProjectGeneralInformationId = update.ProjectGeneralInformationId;
                                var respTracking = await _projectService.CreateProjectTracking(maptracking);
                            }
                        }
                    }
                }
                else
                {
                    return new ResponseDto<int>()
                    {
                        Status = StatusCode.NotFound
                    };
                }
                return new ResponseDto<int>()
                {
                    Status = StatusCode.Updated,
                    Result = existing.ProjectGeneralInformationId
                };
            }
            catch (Exception ex)
            {
                return new ResponseDto<int>()
                {
                    Status = StatusCode.BadRequest,
                    Error = Convert.ToString(ex.InnerException)
                };
            }
        }

        public async Task<ResponseDto<ProjectExternalServiceDto>> GetProjectInfoFromLaGov(string ContractNo)
        {
            try
            {
                ServiceReference1.WebServiceSoapClient obj = new ServiceReference1.WebServiceSoapClient();
                var res = await obj.GetbyProjectIdAsync(ContractNo);
                if (res != null)
                {
                    // List<DataRow> list = res.AsEnumerable().ToList();                 
                    var RespRow = (from DataRow row in res.Rows
                                   select new ProjectExternalServiceDto
                                   {
                                       ContractNumber = Convert.ToString(row["CONTRACT_NUMBER"]),
                                       ContractName = Convert.ToString(row["CONTRACT_NAME"]),
                                       ContractorConsultant = Convert.ToString(row["CONTRACTOR_CONSULTANT"]),
                                       ContractorId = Convert.ToString(row["CONTRACTOR_ID"]),
                                       FederalAidProjectNumber = Convert.ToString(row["FEDERAL_AID_PROJECT_NUMBER"]),
                                       FinalAcceptanceDate = Convert.ToString(row["FINAL_ACCEPTANCE_DATE"]),
                                       ParishNumber = Convert.ToString(row["PARISH_NUMBER"]),
                                       ParishName = Convert.ToString(row["PARISH_NAME"]),
                                       ProjectManager = Convert.ToString(row["PROJECT_MANAGER"]),
                                       ProjectName = Convert.ToString(row["PROJECT_NAME"]),
                                       RouteNumber = Convert.ToString(row["ROUTE_NUMBER"]),
                                       StateProjectNumber = Convert.ToString(row["STATE_PROJECT_NUMBER"]),
                                   }).FirstOrDefault();
                    if (RespRow != null)
                    {
                        return new ResponseDto<ProjectExternalServiceDto>()
                        {
                            Status = StatusCode.Success,
                            Result = RespRow
                        };

                    }
                    else
                    {
                        return new ResponseDto<ProjectExternalServiceDto>()
                        {
                            Status = StatusCode.NotFound,
                        };
                    }
                }
                else
                {
                    return new ResponseDto<ProjectExternalServiceDto>()
                    {
                        Status = StatusCode.NotFound,
                    };
                }
                //return new ResponseDto<ProjectExternalServiceDto>()
                //{
                //    Status = StatusCode.NotFound,
                //};
            }
            catch (Exception ex)
            {
                return new ResponseDto<ProjectExternalServiceDto>()
                {
                    Status = StatusCode.BadRequest,
                };

            }
        }

        public async Task<ResponseDto<IList<ContractAndTaskOrderListDto>>> GetAllProjectContractAndTaskOrderList(int ProjectGeneralInformationId)
        {
            try
            {
                var resp = await _projectService.GetAllProjectContractAndTaskOrderList(ProjectGeneralInformationId);
                var map = _mapper.Map<IList<ContractAndTaskOrderListDto>>(resp);
                return new ResponseDto<IList<ContractAndTaskOrderListDto>>()
                {
                    Status = StatusCode.Success,
                    Result = map
                };
            }
            catch (Exception ex)
            {
                return new ResponseDto<IList<ContractAndTaskOrderListDto>>()
                {
                    Status = StatusCode.BadRequest
                };
            }
        }
        public async Task<ResponseDto<IList<ProjectContractTaskOrderMoneyBalanceDto>>> GetAllProjectMoneyBalanceList()
        {
            try
            {
                var resp = await _projectService.GetAllProjectMoneyBalanceList();
                var map = _mapper.Map<IList<ProjectContractTaskOrderMoneyBalanceDto>>(resp);
                return new ResponseDto<IList<ProjectContractTaskOrderMoneyBalanceDto>>()
                {
                    Status = StatusCode.Success,
                    Result = map
                };
            }
            catch (Exception ex)
            {
                return new ResponseDto<IList<ProjectContractTaskOrderMoneyBalanceDto>>()
                {
                    Status = StatusCode.BadRequest
                };
            }
        }
        public async Task<ResponseDto<IList<ProjectContractTaskOrderMoneyBalanceDto>>> GetProjectContractTaskOrderMoneyBalanceList(int ProjectGeneralInformationId)
        {
            try
            {
                var resp = await _projectService.GetProjectContractTaskOrderMoneyBalanceList(ProjectGeneralInformationId);
                var map = _mapper.Map<IList<ProjectContractTaskOrderMoneyBalanceDto>>(resp);
                return new ResponseDto<IList<ProjectContractTaskOrderMoneyBalanceDto>>()
                {
                    Status = StatusCode.Success,
                    Result = map
                };
            }
            catch (Exception ex)
            {
                return new ResponseDto<IList<ProjectContractTaskOrderMoneyBalanceDto>>()
                {
                    Status = StatusCode.BadRequest
                };
            }
        }

        public async Task<ResponseDto<StatusCode>> DeleteProjectInformation(int ProjectGeneralInformationId)
        {
            try
            {
                var resp = await _projectService.GetProjectGeneralInformation(ProjectGeneralInformationId);
                if (resp != null)
                {
                    resp.IsDelete = true;
                    await _projectService.UpdateProjectGeneralInformation(resp);
                    return new ResponseDto<StatusCode>()
                    {
                        Status = StatusCode.Success,
                    };
                }
                else
                {
                    return new ResponseDto<StatusCode>()
                    {
                        Status = StatusCode.NotFound,
                    };
                }
            }
            catch (Exception ex)
            {
                return new ResponseDto<StatusCode>()
                {
                    Status = StatusCode.BadRequest
                };
            }
        }


       


    }
}