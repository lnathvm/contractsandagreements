﻿using ContractsAndAgreements.Helpers.Interfaces;
using ContractsAndAgreements_Domain.Dtos;
using ContractsAndAgreements_DomainLogic.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using System.Web;
using Outlook = Microsoft.Office.Interop.Outlook;

namespace ContractsAndAgreements.Helpers.Implementation
{
    public class CommunicationControllerHelper : ICommunicationControllerHelper
    {
        private readonly ICommonService _CommonService;
        private readonly ICommunicationService _CommunicationService;

        public CommunicationControllerHelper(ICommonService CommonService, ICommunicationService communication)
        {
            _CommonService = CommonService;
            _CommunicationService = communication;
        }

        public async Task<bool> SendEmail(string SPN, int SupId)
        {
            try
            {
                var getSettingDetail = await _CommonService.GetCommunicationSettingMaster("Outlook");
                if (getSettingDetail != null)
                {

                    var getsupervisor = await _CommonService.GetSupervisorDetails(SupId);
                    if (getsupervisor != null)
                    {
                        var obj = new EmailDto()
                        {
                            Body = getSettingDetail.Message + ' ' + SPN,
                            Subject = getSettingDetail.Subject + ' ' + SPN,
                            CcEmail = getSettingDetail.BccEmail,
                            ToEmail = getsupervisor.SupervisorEmail
                        };
                        var showoutlook = await _CommunicationService.SendEmail(obj);
                    }
                }
                return true;
            }
            catch (Exception objEx)
            {
                return false;
            }

        }



    }
}