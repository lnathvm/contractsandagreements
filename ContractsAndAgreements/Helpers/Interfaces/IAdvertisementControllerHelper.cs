﻿using ContractsAndAgreements_Domain.Dtos;
using ContractsAndAgreements_Domain.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContractsAndAgreements.Helpers.Interfaces
{
    public interface IAdvertisementControllerHelper
    {
        Task<ResponseDto<StatusCode>> CreateUpdateProjectAdvertisementDetails(ProjectAdvertisementDetailsDto projectAdvertisement);
        Task<ResponseDto<StatusCode>> CreateProjectWorkFlowTracking(ProjectWorkFlowTrackingDto projectWork);
        Task<ResponseDto<ProjectAdvertisementDetailsDto>> GetProjectAdvertisementDetails(int ProjectInfoId);

        Task<ResponseDto<int>> CreateProjectAddendumDetails(ProjectAddendumDetailsDto projectAddendum);

        //Task<<ResponseDto<IList<ProjectAddendumDetailsDto>> GetProjectAddendumDetails(int ProjectInfoId);

        Task<ResponseDto<IList<ProjectAddendumDetailsDto>>> GetProjectAddendumDetails(int ProjectInfoId);

        Task<ResponseDto<StatusCode>> CreateUpdateProjectContractDetails(ProjectContractDetailsDto projectAdvertisement);
        Task<ResponseDto<ProjectContractDetailsDto>> GetProjectContractDetails(int ProjectInfoId);
        Task<ResponseDto<StatusCode>> CreateUpdateProjectTaskOrderDetails(ProjectTaskOrderDetailsDto projectAdvertisement);
        Task<ResponseDto<ProjectTaskOrderDetailsDto>> GetProjectTaskOrderDetails(int ProjectInfoId);

        Task<ResponseDto<IList<AllAdvertisementDetailsDto>>> GetAllAdvertisementDetailsList();

        Task<ResponseDto<GetAddendunDocumentDetailDto>> GetAddendunDocumentDetail(int ProjectAddendumId);
        Task<ResponseDto<AllAdvertisementDetailsDto>> GetAdvertisementDocumentDetail(int projectadvertisementdetails);

        Task<ResponseDto<StatusCode>> CreateUpdateProjectAdvertisementSelectionDetails(ProjectAdvertisementSelectionDto projectAdvertisement);
        Task<ResponseDto<IList<AllAdvertisementDetailsDto>>> GetAllAdvertisementSelectionDetailsList();


        Task<ResponseDto<StatusCode>> CreateUpdateProjectAdvertisementApparentSelectionDetails(ProjectAdvertisementApparentSelectionDto projectAdvertisement);
        Task<ResponseDto<IList<AllAdvertisementDetailsDto>>> GetAllAdvertisementApparentSelectionDetailsList();

        Task<ResponseDto<int>> CreateUpdateProjectAdvertisementShortlistDetails(ProjectAdvertisementShortlistDto projectAdvertisement);
        Task<ResponseDto<IList<AllAdvertisementDetailsDto>>> GetAllAdvertisementShortlistDetailsList();
        //Task<ResponseDto<IList<ProjectAdvertisementShortlistDto>>> GetProjectAdvertisementShortlistDetailsByProjectInfoId(int ProjectInfoId);

        Task<ResponseDto<StatusCode>> CreateProjectAdvertisementDocument(ProjectAdvertisementDocumentDto documentDto);

        Task<ResponseDto<IList<CommonDropdownDto>>> GetShortListConsultantProviderList(int ProjectInfoId);




        Task<ResponseDto<AdvertisementShortListDocDto>> GetAdvertisementShortListDocDetail(int ProjectInfoId);

        Task<ResponseDto<AllAdvertisementDetailsDto>> GetAdvertisementSelectionDocDetail(int ProjectSelId);

        Task<ResponseDto<AllAdvertisementDetailsDto>> GetAdvertisementApparentSelectionDocDetail(int ProjectAppSelId);


        Task<ResponseDto<AdvertisementCommonDetailDto>> GetAdvertisementDetail(int ProjectGeneralInformationId);
        Task<ResponseDto<ProjectAdvertisementApparentSelectionDto>> GetAdvertisementApparentSelection(int ProjectGeneralInformationId);
        Task<ResponseDto<StatusCode>> CreateProjectAddendumDocumentDetails(AddendumDocumentMasterDto documentDto);

        Task<ResponseDto<StatusCode>> CreateUpdateProjectSupplementalAgreementDetails(ProjectSupplementalAgreementDetailsDto project);
        Task<ResponseDto<StatusCode>> CreateUpdateProjectExtraWorkLetterDetails(ProjectExtraWorkLetterDetailsDto project);
        Task<ResponseDto<StatusCode>> CreateUpdateProjectOriginalContractDetails(ProjectOriginalContractDetailsDto project);
        Task<ResponseDto<StatusCode>> CreateUpdateSupplementalAgreementForIDIQ(ProjectSupplementalAgreementForIDIQDto projectTaskOrder);
        Task<ResponseDto<ProjectSupplementalAgreementForIDIQDto>> GetProjectTSupplementalAgreementForIDIQ(int ProjectInfoId);

        Task<ResponseDto<StatusCode>> DeleteProjectAddendum(int AddendumId);
        Task<ResponseDto<StatusCode>> DeleteProjectShorlist(int ShorlistId);

        Task<ResponseDto<StatusCode>> PostToWebShorlist(int ShorlistId);
        Task<ResponseDto<ProjectAdvertisementShortlistDto>> GetProjectAdvertisementShortlistDetailsByProjectInfoId(int ProjectInfoId);

        Task<ResponseDto<ProjectAdvertisementSelectionDto>> GetAdvertisementSelection(int ProjectGeneralInformationId);

        Task<ResponseDto<ProjectAdvertisementDocumentDto>> GetAdvertisementDocumentDetails(int ProjectGeneralInformationId);

        Task<ResponseDto<StatusCode>> CreateProcessor(ProcessorMasterDto processorDto);

    }

}



    

