﻿using ContractsAndAgreements_Domain.Dtos;
using ContractsAndAgreements_Domain.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContractsAndAgreements.Helpers.Interfaces
{
    public interface IProjectControllerHelper
    {

        Task<ResponseDto<int>> CreateProjectGeneralInformation(ProjectGeneralInformationDto generalInformationDto);
        //Task<ResponseDto<IList<ConsultantProviderDto>>> GetAllConsultantProviderList();
        //Task<ResponseDto<ConsultantProviderDto>> GetConsultantProviderDetail(int ConsultantProviderId);
        //Task<ResponseDto<IList<ProjectGeneralInformationDto>>> GetAllProjectGeneralInformationList();
        Task<ResponseDto<IList<DashboardInfoDto>>> GetAllProjectGeneralInformationList();
        Task<ResponseDto<ProjectGeneralInformationDto>> GetProjectGeneralInformation(int ProjectInfoId);
        Task<ResponseDto<int>> UpdateProjectGeneralInformation(ProjectGeneralInformationDto generalInformationDto);

        Task<ResponseDto<ProjectExternalServiceDto>> GetProjectInfoFromLaGov(string StateProjectNo);

        Task<ResponseDto<IList<ContractAndTaskOrderListDto>>> GetAllProjectContractAndTaskOrderList(int ProjectGeneralInformationId);

        Task<ResponseDto<IList<ProjectContractTaskOrderMoneyBalanceDto>>> GetAllProjectMoneyBalanceList();

        Task<ResponseDto<IList<ProjectContractTaskOrderMoneyBalanceDto>>> GetProjectContractTaskOrderMoneyBalanceList(int ProjectGeneralInformationId);
        Task<ResponseDto<StatusCode>> DeleteProjectInformation(int ProjectGeneralInformationId);

        

    }
}
