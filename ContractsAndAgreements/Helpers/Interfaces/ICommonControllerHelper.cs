﻿using ContractsAndAgreements_Domain.Dtos;
using ContractsAndAgreements_Domain.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContractsAndAgreements.Helpers.Interfaces
{
    public interface ICommonControllerHelper
    {
        Task<ResponseDto<IList<CommonDropdownDto>>> GetAllContractTypeList();
        Task<ResponseDto<IList<CommonDropdownDto>>> GetAllFeeTypeList();
        Task<ResponseDto<IList<CommonDropdownDto>>> GetAllCompensationTypeList();
        Task<ResponseDto<IList<CommonDropdownDto>>> GetAllParishList();
        Task<ResponseDto<IList<CommonDropdownDto>>> GetAllSelectionList();
        Task<ResponseDto<IList<CommonDropdownDto>>> GetAllProjectManagerList();
        //Task<ResponseDto<IList<CommonDropdownDto>>> GetAllConsultantAreaSubTaxList();
        Task<ResponseDto<IList<ConsultantProviderDto>>> GetAllConsultantProviderList();
        Task<ResponseDto<ConsultantProviderDto>> GetConsultantProviderDetail(int ConsultantProviderId);
        Task<ResponseDto<IList<CommonDropdownDto>>> GetAllSupervisorList();
        Task<ResponseDto<IList<CommonDropdownDto>>> GetAllProcessorList();

        Task<ResponseDto<StatusCode>> SaveConsultantProvider(ConsultantProviderDto informationDto);

        Task<ResponseDto<StatusCode>> DeleteConsultantProvider(int ConsultantProviderId);

        Task<ResponseDto<IList<CommonDropdownDto>>> GetCompensationTypeList();

        Task<ResponseDto<IList<CommonDropdownDto>>> GetAllContractAgencyList();
    }

}
