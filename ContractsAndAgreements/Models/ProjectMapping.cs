﻿using AutoMapper;
using ContractsAndAgreements_Domain.Dtos;
using ContractsAndAgreements_Domain.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ContractsAndAgreements.Models
{
    public class ProjectMapping :Profile
    {

        public ProjectMapping()
        {
            CreateMap<ProjectGeneralInformationDto, ProjectGeneralInformation>();

            CreateMap<ProjectConsultantAreaDto, ProjectConsultantArea>();

            CreateMap<ConsultantAreaSubTaxDetailsDto, ConsultantAreaSubTaxDetails>();

            CreateMap<ProjectTrackingDto, ProjectTracking>();

            CreateMap<ConsultantProviderMaster, ConsultantProviderDto>();

            CreateMap<ParishMaster, CommonDropdownDto>()
              .ForMember(d => d.Id, m => m.MapFrom(s => s.ParishId))
            .ForMember(d => d.Name, m => m.MapFrom(s => s.Name));

            CreateMap<ContractTypeMaster, CommonDropdownDto>()
              .ForMember(d => d.Id, m => m.MapFrom(s => s.ContractTypeId))
            .ForMember(d => d.Name, m => m.MapFrom(s => s.Name));

            CreateMap<FeeTypeMaster, CommonDropdownDto>()
              .ForMember(d => d.Id, m => m.MapFrom(s => s.FeeTypeId))
            .ForMember(d => d.Name, m => m.MapFrom(s => s.Name));

            CreateMap<FeeTypeMasterr, CommonDropdownDto>()
             .ForMember(d => d.Id, m => m.MapFrom(s => s.FeeTypeeId))
           .ForMember(d => d.Name, m => m.MapFrom(s => s.Name));

            CreateMap<SelectionMaster, CommonDropdownDto>()
              .ForMember(d => d.Id, m => m.MapFrom(s => s.SelectionId))
            .ForMember(d => d.Name, m => m.MapFrom(s => s.Name));

            CreateMap<ProjectManagerMaster, CommonDropdownDto>()
           .ForMember(d => d.Id, m => m.MapFrom(s => s.ProjectManagerId))
         .ForMember(d => d.Name, m => m.MapFrom(s => s.Name));

            CreateMap<SuperVisorMaster, CommonDropdownDto>()
             .ForMember(d => d.Id, m => m.MapFrom(s => s.SupervisorId))
           .ForMember(d => d.Name, m => m.MapFrom(s => s.Name));


            CreateMap<ProcessorMaster, CommonDropdownDto>()
             .ForMember(d => d.Id, m => m.MapFrom(s => s.ProcessorId))
           .ForMember(d => d.Name, m => m.MapFrom(s => s.Name));

            //CreateMap<ConsultantAreaSubTaxMaster, CommonDropdownDto>()
            //  .ForMember(d => d.Id, m => m.MapFrom(s => s.ConsultantAreaSubTaxMasterId))
            //.ForMember(d => d.Name, m => m.MapFrom(s => s.SubName));

            CreateMap<ProjectGeneralInformation, ProjectGeneralInformationDto>();

            CreateMap<ProjectConsultantArea, ProjectConsultantAreaDto>();
            CreateMap<ProjectTracking, ProjectTrackingDto>();
            CreateMap<ConsultantAreaSubTaxDetails, ConsultantAreaSubTaxDetailsDto>()
                .ForMember(d => d.ConsultantAreaSubTaxDetailsId, m => m.MapFrom(s => s.ConsultantAreaSubTaxDetailsId))
            .ForMember(d => d.ProjectConsultantAreaId, m => m.MapFrom(s => s.ProjectConsultantAreaId))
            .ForMember(d => d.SubId, m => m.MapFrom(s => s.SubId))
            .ForMember(d => d.TaxId, m => m.MapFrom(s => s.TaxId));

            CreateMap<ProjectAdvertisementDocumentDto, AdvertisementDocumentMaster>();
            CreateMap<AdvertisementDocumentMaster, ProjectAdvertisementDocumentDto>();

            CreateMap<ProcessorMasterDto, ProcessorMaster>();

            #region ProjectAdv
            CreateMap<ProjectWorkFlowTrackingDto, ProjectWorkFlowTracking>();
            CreateMap<ProjectAdvertisementDetailsDto, ProjectAdvertisementDetails>();

            CreateMap<ProjectAdvertisementDetails, ProjectAdvertisementDetailsDto>();
            CreateMap<ProjectWorkFlowTracking, ProjectWorkFlowTrackingDto>();
            #endregion

            CreateMap<ProjectAddendumWorkFlowTrackingDto, ProjectAddendumWorkFlowTracking>();
            CreateMap<ProjectAddendumDetailsDto, ProjectAddendumDetails>();

            CreateMap<ProjectAddendumWorkFlowTracking, ProjectAddendumWorkFlowTrackingDto>();
            CreateMap<ProjectAddendumDetails, ProjectAddendumDetailsDto>();


            CreateMap<ProjectAddendumWorkFlowTracking, ProjectAddendumWorkFlowTrackingDto>();
            CreateMap<ProjectAddendumDetails, ProjectAddendumDetailsDto>();

            CreateMap<ProjectContractWorkFlowTrackingDto, ProjectContractWorkFlowTracking>();
            CreateMap<ProjectContractInformationDto, ProjectContractInformation>();
            CreateMap<ContractInformationSubTaxDetailsDto, ContractInformationSubTaxDetails>();
            CreateMap<ProjectContractDetailsDto, ProjectContractDetails>();
            CreateMap<ProjectGeneralInformation, ProjectGeneralInformationDto>();
            CreateMap<ProjectContractWorkFlowTracking, ProjectContractWorkFlowTrackingDto>();
            CreateMap<ProjectContractInformation, ProjectContractInformationDto>();
            CreateMap<ContractInformationSubTaxDetails, ContractInformationSubTaxDetailsDto>();
            CreateMap<ProjectGeneralInformationDto, ProjectGeneralInformation>();
            CreateMap<ProjectInformationSubTaxDetailsDto, TaskOrderInformationSubTaxDetails>();
            CreateMap<TaskOrderInformationSubTaxDetails, ProjectInformationSubTaxDetailsDto>();
            CreateMap<ProjectInformationDto, ProjectTaskOrderInformation>();
            CreateMap<ProjectTaskOrderInformation, ProjectInformationDto>();
            CreateMap<ProjectTaskOrderDetailsDto, ProjectTaskOrderDetails>();
            CreateMap<ContractInformationSubTaxDetails, ProjectInformationSubTaxDetailsDto>();
            CreateMap<ProjectContractInformation, ProjectSupplementalAgreementInformationForIDIQDto>();
            CreateMap<ContractInformationSubTaxDetails, ProjectSupplementalAgreementInformationForIDIQSubTaxDetailsDto>();

            #region Advertisement Selection
            CreateMap<ProjectAdvertisementSelectionDto, ProjectAdvertisementSelection>();
            CreateMap<ProjectAdvertisementSelection, ProjectAdvertisementSelectionDto>();
            CreateMap<ProjectAdvertisementApparentSelectionDto, ProjectAdvertisementApparentSelection>();
            CreateMap<ProjectAdvertisementApparentSelection, ProjectAdvertisementApparentSelectionDto>();
            CreateMap<ProjectAdvertisementShortlistDto, ProjectAdvertisementShortlist>();
            CreateMap<ProjectAdvertisementShortlist, ProjectAdvertisementShortlistDto>();

            CreateMap<ShortlistSubDetailsDto, ShortlistSubDetails>();
            CreateMap<ShortlistSubDetails, ShortlistSubDetailsDto>();



            #endregion

            CreateMap<AddendumDocumentMasterDto, AddendumDocumentMaster>();
            CreateMap<ConsultantProviderDto, ConsultantProviderMaster>();

            CreateMap<ProjectSupplementalAgreementInformationForIDIQSubTaxDetailsDto, ProjectSupplementalAgreementInformationForIDIQSubTaxDetails>();
            CreateMap<ProjectSupplementalAgreementInformationForIDIQDto, ProjectSupplementalAgreementInformationForIDIQ>();
            CreateMap<ProjectSupplementalAgreementForIDIQDto, ProjectSupplementalAgreementForIDIQ>();

            CreateMap<ProjectInformationSubTaxDetailsDto, ProjectSupplementalAgreementSubTaxDetails>();
            CreateMap<ProjectInformationSubTaxDetailsDto, ProjectExtraWorkLetterSubTaxDetails>();
            CreateMap<ProjectInformationSubTaxDetailsDto, ProjectOriginalContractSubTaxDetails>();
            CreateMap<ProjectSupplementalAgreementInformationForIDIQ, ProjectSupplementalAgreementInformationForIDIQDto>();
            CreateMap<ProjectSupplementalAgreementInformationForIDIQSubTaxDetails, ProjectSupplementalAgreementInformationForIDIQSubTaxDetailsDto>();

            CreateMap<ProjectOriginalContractDetailsDto, ProjectOriginalContractDetails>();

            CreateMap<CompensationTypeMaster, CommonDropdownDto>()
             .ForMember(d => d.Id, m => m.MapFrom(s => s.CompensationTypeId))
           .ForMember(d => d.Name, m => m.MapFrom(s => s.Name));
            CreateMap<ProjectContractInformation, ProjectInformationDto>();

            CreateMap<ContractAgencyMaster, CommonDropdownDto>()
            .ForMember(d => d.Id, m => m.MapFrom(s => s.ContractAgencyId))
          .ForMember(d => d.Name, m => m.MapFrom(s => s.ContractAgency));

            CreateMap<ProjectOriginalContractDetailsDto, ProjectOriginalContractDetails>();
            CreateMap<ProjectSupplementalAgreementDetailsDto, ProjectSupplementalAgreementDetails>();
            CreateMap<ProjectExtraWorkLetterDetailsDto, ProjectExtraWorkLetterDetails>();
        }
    }
}