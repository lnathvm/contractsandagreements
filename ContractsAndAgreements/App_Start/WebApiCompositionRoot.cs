﻿using AutoMapper;
using ContractsAndAgreements.Helpers.Implementation;
using ContractsAndAgreements.Helpers.Interfaces;
using ContractsAndAgreements.Models;
using ContractsAndAgreements_DataAccess;
using ContractsAndAgreements_DomainLogic;
using LightInject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ContractsAndAgreements.App_Start
{
    public class WebCompositionRoot : ICompositionRoot
    {
        public void Compose(IServiceRegistry serviceRegistry)
        {
            serviceRegistry.RegisterFrom<DataAccessEFCompositionRoot>();
            serviceRegistry.RegisterFrom<DomainLogicCompositionRoot>();

            #region Helper
            serviceRegistry.Register<IProjectControllerHelper, ProjectControllerHelper>();
            serviceRegistry.Register<ICommonControllerHelper, CommonControllerHelper>();
            serviceRegistry.Register<IAdvertisementControllerHelper, AdvertisementControllerHelper>();
            serviceRegistry.Register<ICommunicationControllerHelper, CommunicationControllerHelper>();
            #endregion

            #region Mapper         

            var mapconfig = new MapperConfiguration(
                c =>
                {
                    c.AddProfile(new ProjectMapping());

                });
            var mapper = mapconfig.CreateMapper();
            serviceRegistry.Register<MapperConfiguration>(factory => mapconfig, new PerContainerLifetime());
            serviceRegistry.Register<IMapper>(factory => factory.GetInstance<MapperConfiguration>().CreateMapper(factory.GetInstance), new PerContainerLifetime());

            #endregion

        }
    }
}