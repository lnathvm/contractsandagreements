﻿using LightInject;
using LightInject.ServiceLocation;
using Microsoft.Practices.ServiceLocation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ContractsAndAgreements.App_Start
{
    public class DependencyInjectionConfig
    {


        public static void RegisteredDependencies()
        {
            var container = new ServiceContainer();
            container.RegisterControllers();
            container.EnableMvc();
            container.RegisterApiControllers();
            container.EnableWebApi(System.Web.Http.GlobalConfiguration.Configuration);

            container.EnablePerWebRequestScope();

            container.RegisterFrom<WebCompositionRoot>();

            ServiceLocator.SetLocatorProvider(() => new LightInjectServiceLocator(container));

        }

    }
}