﻿using ContractsAndAgreements.Helpers.Interfaces;
using ContractsAndAgreements_Domain.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace ContractsAndAgreements.Controllers
{
    public class DashBoardController : Controller
    {
        private readonly IAdvertisementControllerHelper _advertisement;

        public DashBoardController(IAdvertisementControllerHelper advertisementController)
        {
            _advertisement = advertisementController;
        }
        // GET: DashBoard
        public ActionResult Home()
        {
            return View();
        }
        public ActionResult AdvertisementDashBoard()
        {
            return View();
        }
        
        public ActionResult MoneyViewPortfolioBalance()
        {
            return View();
        }
        public ActionResult WebSiteAdvertisementDashboard()
        {
            return View();
        }

        public async Task<ActionResult> ContractAdvertisementDocument(int projectadvertisementdetails)
        {
            ResponseDto<AllAdvertisementDetailsDto> response = await _advertisement.GetAdvertisementDocumentDetail(projectadvertisementdetails);

            return View(response.Result);
        }
        public async Task<ActionResult> AddendumDocument(int ProjectAddendumId)
        {
            ResponseDto<GetAddendunDocumentDetailDto> response = await _advertisement.GetAddendunDocumentDetail(ProjectAddendumId);
            return View(response.Result);
        }

        

    }
}