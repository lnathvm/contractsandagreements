﻿using ContractsAndAgreements.Helpers.Interfaces;
using ContractsAndAgreements_Domain.Dtos;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace ContractsAndAgreements.Controllers
{
    public class ProjectController : Controller
    {
        private readonly IProjectControllerHelper _projectControllerHelper;

        public ProjectController(IProjectControllerHelper projectControllerHelper)
        {
            _projectControllerHelper = projectControllerHelper;

        }
        // GET: Project
        public ActionResult ProjectInfo()
        {
            return View();
        }


        [HttpPost]
        [ActionName("CreateProjectGeneralInformation")]
        public async Task<ActionResult> CreateProjectGeneralInformation(ProjectGeneralInformationDto informationDto)
        {
            var response = await _projectControllerHelper.CreateProjectGeneralInformation(informationDto);
            return Json(new { Result = response });
        }

        //[HttpGet]
        //[ActionName("GetAllConsultantProviderList")]
        //public async Task<ActionResult> GetAllConsultantProviderList()
        //{
        //    var response = await _projectControllerHelper.GetAllConsultantProviderList();
        //    return Json(new { Result = response }, JsonRequestBehavior.AllowGet);
        //}
        //[HttpGet]
        //[ActionName("GetConsultantProviderDetail")]
        //public async Task<ActionResult> GetConsultantProviderDetail(int ConsultantProviderId)
        //{
        //    var response = await _projectControllerHelper.GetConsultantProviderDetail(ConsultantProviderId);
        //    return Json(new { Result = response }, JsonRequestBehavior.AllowGet);
        //}
        [HttpGet]
        [ActionName("GetAllProjectGeneralInformationList")]
        public async Task<ActionResult> GetAllProjectGeneralInformationList()
        {
            var response = await _projectControllerHelper.GetAllProjectGeneralInformationList();
            return Json(new { data = response.Result }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [ActionName("GetProjectGeneralInformation")]
        public async Task<ActionResult> GetProjectGeneralInformation(int ProjectInfoId)
        {
            var response = await _projectControllerHelper.GetProjectGeneralInformation(ProjectInfoId);
            return Json(new { data = response.Result }, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        [ActionName("UpdateProjectGeneralInformation")]
        public async Task<ActionResult> UpdateProjectGeneralInformation(ProjectGeneralInformationDto informationDto)
        {
            var response = await _projectControllerHelper.UpdateProjectGeneralInformation(informationDto);
            return Json(new { Result = response });
        }
        [HttpGet]
        [ActionName("GetProjectInfoFromLaGov")]
        public async Task<ActionResult> GetProjectInfoFromLaGov(string ContractNo)
        {
            var response = await _projectControllerHelper.GetProjectInfoFromLaGov(ContractNo);
            return Json(new { data = response.Result }, JsonRequestBehavior.AllowGet);

        }

        [HttpGet]
        [ActionName("GetAllProjectMoneyBalanceList")]
        public async Task<ActionResult> GetAllProjectMoneyBalanceList()
        {
            var response = await _projectControllerHelper.GetAllProjectMoneyBalanceList();
            return Json(new { data = response.Result }, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        [ActionName("GetProjectContractTaskOrderMoneyBalanceList")]
        public async Task<ActionResult> GetProjectContractTaskOrderMoneyBalanceList(int ProjectGeneralInformationId)
        {
            var response = await _projectControllerHelper.GetProjectContractTaskOrderMoneyBalanceList(ProjectGeneralInformationId);
            return Json(new { data = response.Result }, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        [ActionName("DeleteProjectInformation")]
        public async Task<ActionResult> DeleteProjectInformation(int ProjectInfoId)
        {
            var response = await _projectControllerHelper.DeleteProjectInformation(ProjectInfoId);
            return Json(new { data = response.Status }, JsonRequestBehavior.AllowGet);
        }

        


    }
}