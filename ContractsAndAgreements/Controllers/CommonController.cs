﻿using ContractsAndAgreements.Helpers.Interfaces;
using ContractsAndAgreements_Domain.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace ContractsAndAgreements.Controllers
{
    public class CommonController : Controller
    {
        // GET: Common
        private readonly ICommonControllerHelper _commonControllerHelper;
        public CommonController(ICommonControllerHelper commonControllerHelper)
        {
            _commonControllerHelper = commonControllerHelper;
        }

        [HttpGet]
        [ActionName("GetAllContractTypeList")]
        public async Task<ActionResult> GetAllContractTypeList()
        {
            var response = await _commonControllerHelper.GetAllContractTypeList();
            return Json(new { Result = response }, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        [ActionName("GetAllFeeTypeList")]
        public async Task<ActionResult> GetAllFeeTypeList()
        {
            var response = await _commonControllerHelper.GetAllFeeTypeList();
            return Json(new { Result = response }, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        [ActionName("GetAllCompensationTypeList")]
        public async Task<ActionResult> GetAllCompensationTypeList()
        {
            var response = await _commonControllerHelper.GetAllCompensationTypeList();
            return Json(new { Result = response }, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        [ActionName("GetAllParishList")]
        public async Task<ActionResult> GetAllParishList()
        {
            var response = await _commonControllerHelper.GetAllParishList();
            return Json(new { Result = response }, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        [ActionName("GetAllSelectionList")]
        public async Task<ActionResult> GetAllSelectionList()
        {
            var response = await _commonControllerHelper.GetAllSelectionList();
            return Json(new { Result = response }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [ActionName("GetAllProjectManagerList")]
        public async Task<ActionResult> GetAllProjectManagerList()
        {
            var response = await _commonControllerHelper.GetAllProjectManagerList();
            return Json(new { Result = response }, JsonRequestBehavior.AllowGet);
        }
        //[HttpGet]
        //[ActionName("GetAllConsultantAreaSubTaxList")]
        //public async Task<ActionResult> GetAllConsultantAreaSubTaxList()
        //{
        //    var response = await _commonControllerHelper.GetAllConsultantAreaSubTaxList();
        //    return Json(new { Result = response }, JsonRequestBehavior.AllowGet);
        //}
        [HttpGet]
        [ActionName("GetAllConsultantProviderList")]
        public async Task<ActionResult> GetAllConsultantProviderList()
        {
            var response = await _commonControllerHelper.GetAllConsultantProviderList();
            return Json(new { data = response.Result }, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        [ActionName("GetConsultantProviderDetail")]
        public async Task<ActionResult> GetConsultantProviderDetail(int ConsultantProviderId)
        {
            var response = await _commonControllerHelper.GetConsultantProviderDetail(ConsultantProviderId);
            return Json(new { Result = response }, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        [ActionName("GetAllSupervisorList")]
        public async Task<ActionResult> GetAllSupervisorList()
        {
            var response = await _commonControllerHelper.GetAllSupervisorList();
            return Json(new { Result = response }, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        [ActionName("GetAllProcessorList")]
        public async Task<ActionResult> GetAllProcessorList()
        {
            var response = await _commonControllerHelper.GetAllProcessorList();
            return Json(new { Result = response }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ConsultantProvider()
        {
            return View();
        }
        public ActionResult AddUpdateConsultantProvider()
        {
            return View();
        }
        [HttpPost]
        [ActionName("SaveConsultantProvider")]
        public async Task<ActionResult> SaveConsultantProvider(ConsultantProviderDto informationDto)
        {
            var response = await _commonControllerHelper.SaveConsultantProvider(informationDto);
            return Json(new { Result = response });
        }

        [HttpDelete]
        [ActionName("DeleteConsultantProvider")]
        public async Task<ActionResult> DeleteConsultantProvider(int ConsultantProviderId)
        {
            var response = await _commonControllerHelper.DeleteConsultantProvider(ConsultantProviderId);
            return Json(new { Result = response });
        }
        [HttpGet]
        [ActionName("GetCompensationTypeList")]
        public async Task<ActionResult> GetCompensationTypeList()
        {
            var response = await _commonControllerHelper.GetCompensationTypeList();
            return Json(new { Result = response.Result }, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        [ActionName("GetAllContractAgencyList")]
        public async Task<ActionResult> GetAllContractAgencyList()
        {
            var response = await _commonControllerHelper.GetAllContractAgencyList();
            return Json(new { Result = response }, JsonRequestBehavior.AllowGet);
        }
    }
}
