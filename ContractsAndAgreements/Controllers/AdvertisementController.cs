﻿using ContractsAndAgreements.Helpers.Interfaces;
using ContractsAndAgreements_Domain.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace ContractsAndAgreements.Controllers
{
    public class AdvertisementController : Controller
    {
        private readonly IAdvertisementControllerHelper _advertisement;
        private readonly ICommunicationControllerHelper _communication;

        public AdvertisementController(IAdvertisementControllerHelper advertisement, ICommunicationControllerHelper communication)
        {
            _advertisement = advertisement;
            _communication = communication;
        }
        public ActionResult Advertisement()
        {
            return View();
        }
        public ActionResult AdvertisementDocument()
        {
            return View();
        }
       public ActionResult Addendum()
        {
            return View();
        }
        public ActionResult AddendumDocumentDetails()
        {
            return View();
        }
        public ActionResult AddendumDocument()
        {
            return View();
        }
        public ActionResult Award()
        {
            return View();
        }


        [HttpPost]
        [ActionName("CreateProjectAddendumDetails")]
        public async Task<ActionResult> CreateProjectAddendumDetails(ProjectAddendumDetailsDto projectAddendum)
        {
            var response = await _advertisement.CreateProjectAddendumDetails(projectAddendum);
            return Json(new { Result = response });
        }
        [HttpPost]
        [ActionName("CreateProjectAdvertisementDetails")]
        public async Task<ActionResult> CreateProjectAdvertisementDetails(ProjectAdvertisementDetailsDto projectAdvertisement)
        {
            var response = await _advertisement.CreateUpdateProjectAdvertisementDetails(projectAdvertisement);
            return Json(new { Result = response });
        }

        [HttpGet]
        [ActionName("GetProjectAdvertisementDetails")]
        public async Task<ActionResult> GetProjectAdvertisementDetails(int ProjectInfoId)
        {
            var response = await _advertisement.GetProjectAdvertisementDetails(ProjectInfoId);
            return Json(new { Result = response }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [ActionName("SendEmail")]
        public async Task<ActionResult> SendEmail(string SPN, int SupId)
        {
            var response = await _communication.SendEmail(SPN, SupId);
            return Json(new { Result = response });
        }
        [HttpGet]
        [ActionName("GetProjectAddendumDetails")]
        public async Task<ActionResult> GetProjectAddendumDetails(int ProjectInfoId)
        {
            var response = await _advertisement.GetProjectAddendumDetails(ProjectInfoId);
            return Json(new { Result = response }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult Contract()
        {
            return View();
        }

        [HttpPost]
        [ActionName("CreateProjectContractDetails")]
        public async Task<ActionResult> CreateProjectContractDetails(ProjectContractDetailsDto projectContract)
        {
            var response = await _advertisement.CreateUpdateProjectContractDetails(projectContract);
            return Json(new { Result = response });
        }
        [HttpGet]
        [ActionName("GetProjectContractDetails")]
        public async Task<ActionResult> GetProjectContractDetails(int ProjectInfoId)
        {
            var response = await _advertisement.GetProjectContractDetails(ProjectInfoId);
            return Json(new { Result = response }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult TaskOrder()
        {
            return View();
        }
        [ActionName("CreateProjectTaskOrderDetails")]
        public async Task<ActionResult> CreateProjectTaskOrderDetails(ProjectTaskOrderDetailsDto projectContract)
        {
            var response = await _advertisement.CreateUpdateProjectTaskOrderDetails(projectContract);
            return Json(new { Result = response });
        }
        [HttpGet]
        [ActionName("GetProjectTaskOrderDetails")]
        public async Task<ActionResult> GetProjectTaskOrderDetails(int ProjectInfoId)
        {
            var response = await _advertisement.GetProjectTaskOrderDetails(ProjectInfoId);
            return Json(new { Result = response }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [ActionName("GetAllAdvertisementDetailsList")]
        public async Task<ActionResult> GetAllAdvertisementDetailsList()
        {
            var response = await _advertisement.GetAllAdvertisementDetailsList();
            return Json(new { data = response.Result }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ProjectAdvertisementSelection()
        {
            return View();
        }
        public ActionResult ProjectAdvertisementApparentSelection()
        {
            return View();
        }
        public ActionResult ProjectAdvertisementShortlist()
        {
            return View();
        }
        [HttpPost]
        [ActionName("CreateProjectAdvertisementSelectionDetails")]
        public async Task<ActionResult> CreateProjectAdvertisementSelectionDetails(ProjectAdvertisementSelectionDto projectAdvSel)
        {
            var response = await _advertisement.CreateUpdateProjectAdvertisementSelectionDetails(projectAdvSel);
            return Json(new { Result = response });
        }

        [HttpPost]
        [ActionName("CreateUpdateProjectAdvertisementApparentSelectionDetails")]
        public async Task<ActionResult> CreateUpdateProjectAdvertisementApparentSelectionDetails(ProjectAdvertisementApparentSelectionDto projectAdvSel)
        {
            var response = await _advertisement.CreateUpdateProjectAdvertisementApparentSelectionDetails(projectAdvSel);
            return Json(new { Result = response });
        }

        [HttpPost]
        [ActionName("CreateUpdateProjectAdvertisementShortlistDetails")]
        public async Task<ActionResult> CreateUpdateProjectAdvertisementShortlistDetails(ProjectAdvertisementShortlistDto projectAdvSel)
        {
            var response = await _advertisement.CreateUpdateProjectAdvertisementShortlistDetails(projectAdvSel);
            return Json(new { Result = response });
        }
        [HttpGet]
        [ActionName("GetShortListConsultantProviderList")]
        public async Task<ActionResult> GetShortListConsultantProviderList(int ProjectGeneralInformationId)
        {
            var response = await _advertisement.GetShortListConsultantProviderList(ProjectGeneralInformationId);
            return Json(new { Result = response }, JsonRequestBehavior.AllowGet);
        }

        #region Website
        public ActionResult AdvertisementSelection()
        {
            return View();
        }
        [HttpGet]
        [ActionName("GetAllAdvertisementSelectionDetailsList")]
        public async Task<ActionResult> GetAllAdvertisementSelectionDetailsList()
        {
            var response = await _advertisement.GetAllAdvertisementSelectionDetailsList();
            return Json(new { data = response.Result }, JsonRequestBehavior.AllowGet);
        }


        public ActionResult AdvertisementSortlist()
        {
            return View();
        }

        [HttpGet]
        [ActionName("GetAllAdvertisementShortlistDetailsList")]
        public async Task<ActionResult> GetAllAdvertisementShortlistDetailsList()
        {
            var response = await _advertisement.GetAllAdvertisementShortlistDetailsList();
            return Json(new { data = response.Result }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult AdvertisementApparentSelection()
        {
            return View();
        }
        [HttpGet]
        [ActionName("GetAllAdvertisementApparentSelectionDetailsList")]
        public async Task<ActionResult> GetAllAdvertisementApparentSelectionDetailsList()
        {
            var response = await _advertisement.GetAllAdvertisementApparentSelectionDetailsList();
            return Json(new { data = response.Result }, JsonRequestBehavior.AllowGet);
        }

        public async Task<ActionResult> AdvertisementSortlistDoc(int ProjectInfoId)
        {
            ResponseDto<AdvertisementShortListDocDto> response = await _advertisement.GetAdvertisementShortListDocDetail(ProjectInfoId);
            return View(response.Result);
        }
        public async Task<ActionResult> AdvertisementSelectionDoc(int ProjectSelId)
        {
            ResponseDto<AllAdvertisementDetailsDto> response = await _advertisement.GetAdvertisementSelectionDocDetail(ProjectSelId);
            return View(response.Result);
        }
        public async Task<ActionResult> AdvertisementApparentSelectionDoc(int ProjectAppSelId)
        {
            ResponseDto<AllAdvertisementDetailsDto> response = await _advertisement.GetAdvertisementApparentSelectionDocDetail(ProjectAppSelId);
            return View(response.Result);
        }
        [HttpPost]
        [ActionName("CreateProjectAdvertisementDocument")]
        public async Task<ActionResult> CreateProjectAdvertisementDocument(ProjectAdvertisementDocumentDto documentDto)
        {
            var response = await _advertisement.CreateProjectAdvertisementDocument(documentDto);
            return Json(new { Result = response });
        }

        [HttpGet]
        [ActionName("GetAdvertisementDetail")]
        public async Task<ActionResult> GetAdvertisementDetail(int ProjectGeneralInformationId)
        {
            var response = await _advertisement.GetAdvertisementDetail(ProjectGeneralInformationId);
            return Json(new { data = response.Result }, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        [ActionName("GetAdvertisementApparentSelection")]
        public async Task<ActionResult> GetAdvertisementApparentSelection(int ProjectGeneralInformationId)
        {
            var response = await _advertisement.GetAdvertisementApparentSelection(ProjectGeneralInformationId);
            return Json(new { data = response.Result }, JsonRequestBehavior.AllowGet);
        }
        #endregion
        [HttpPost]
        [ActionName("CreateProjectAddendumDocumentDetails")]
        public async Task<ActionResult> CreateProjectAddendumDocumentDetails(AddendumDocumentMasterDto documentDto)
        {
            var response = await _advertisement.CreateProjectAddendumDocumentDetails(documentDto);
            return Json(new { Result = response });
        }
        public ActionResult OriginalContract()
        {
            return View();
        }
        public ActionResult SupplementalAgreement()
        {
            return View();
        }
        public ActionResult ExtraWorkLetter()
        {
            return View();
        }
        [HttpPost]
        [ActionName("CreateUpdateProjectSupplementalAgreementDetails")]
        public async Task<ActionResult> CreateUpdateProjectSupplementalAgreementDetails(ProjectSupplementalAgreementDetailsDto project)
        {
            var response = await _advertisement.CreateUpdateProjectSupplementalAgreementDetails(project);
            return Json(new { Result = response });
        }
        [HttpPost]
        [ActionName("CreateUpdateProjectExtraWorkLetterDetails")]
        public async Task<ActionResult> CreateUpdateProjectExtraWorkLetterDetails(ProjectExtraWorkLetterDetailsDto project)
        {
            var response = await _advertisement.CreateUpdateProjectExtraWorkLetterDetails(project);
            return Json(new { Result = response });
        }
        [HttpPost]
        [ActionName("CreateUpdateProjectOriginalContractDetails")]
        public async Task<ActionResult> CreateUpdateProjectOriginalContractDetails(ProjectOriginalContractDetailsDto project)
        {
            var response = await _advertisement.CreateUpdateProjectOriginalContractDetails(project);
            return Json(new { Result = response });
        }

        public ActionResult SupplementalAgreementForIDIQ()
        {
            return View();
        }
        [HttpPost]
        [ActionName("CreateProjectSupplementalAgreementForIDIQ")]
        public async Task<ActionResult> CreateProjectSupplementalAgreementForIDIQ(ProjectSupplementalAgreementForIDIQDto projectContract)
        {
            var response = await _advertisement.CreateUpdateSupplementalAgreementForIDIQ(projectContract);
            return Json(new { Result = response });
        }
        [HttpGet]
        [ActionName("GetProjectSupplementalAgreementForIDIQ")]
        public async Task<ActionResult> GetProjectSupplementalAgreementForIDIQ(int ProjectInfoId)
        {
            var response = await _advertisement.GetProjectTSupplementalAgreementForIDIQ(ProjectInfoId);
            return Json(new { Result = response }, JsonRequestBehavior.AllowGet);
        }
        //[HttpGet]
        //[ActionName("GetProjectOriginalContractDetails")]
        //public async Task<ActionResult> GetProjectOriginalContractDetails(int ProjectInfoId)
        //{
        //    var response = await _advertisement.GetProjectOriginalContractDetails(ProjectInfoId);
        //    return Json(new { Result = response }, JsonRequestBehavior.AllowGet);
        //}


        [HttpGet]
        [ActionName("DeleteProjectAddendum")]
        public async Task<ActionResult> DeleteProjectAddendum(int AddendumId)
        {
            var response = await _advertisement.DeleteProjectAddendum(AddendumId);
            return Json(new { data = response.Status }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [ActionName("DeleteProjectShorlist")]
        public async Task<ActionResult> DeleteProjectShorlist(int ShorlistId)
        {
            var response = await _advertisement.DeleteProjectShorlist(ShorlistId);
            return Json(new { data = response.Status }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [ActionName("PostToWebShorlist")]
        public async Task<ActionResult> PostToWebShorlist(int ShorlistId)
        {
            var response = await _advertisement.PostToWebShorlist(ShorlistId);
            return Json(new { data = response.Status }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [ActionName("GetProjectAdvertisementShortlistDetailsByProjectInfoId")]
        public async Task<ActionResult> GetProjectAdvertisementShortlistDetailsByProjectInfoId(int ProjectInfoId)
        {
            var response = await _advertisement.GetProjectAdvertisementShortlistDetailsByProjectInfoId(ProjectInfoId);
            return Json(new { data = response.Result }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [ActionName("GetAdvertisementSelection")]
        public async Task<ActionResult> GetAdvertisementSelection(int ProjectGeneralInformationId)
        {
            var response = await _advertisement.GetAdvertisementSelection(ProjectGeneralInformationId);
            return Json(new { data = response.Result }, JsonRequestBehavior.AllowGet);
        }

        //[HttpPost]
        //[ActionName("CreateUpdateProjectAdvertisementDocument")]
        //public async Task<ActionResult>CreateUpdateProjectAdvertisementDocument(ProjectAdvertisementDocumentDto documentDto)
        //{
        //    var response = await _advertisement.CreateUpdateProjectAdvertisementDocument(documentDto);
        //    return Json(new { data = response.Result }, JsonRequestBehavior.AllowGet);
        //}
        [HttpGet]
        [ActionName("GetAdvertisementDocumentDetails")]
        public async Task<ActionResult> GetAdvertisementDocumentDetails(int ProjectGeneralInformationId)
        {
            var response = await _advertisement.GetAdvertisementDocumentDetails(ProjectGeneralInformationId);
            return Json(new { data = response.Result }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [ActionName("CreateProcessor")]
        public async Task<ActionResult> CreateProcessor(ProcessorMasterDto processorDto)
        {
            var response = await _advertisement.CreateProcessor(processorDto);
            return Json(new { Result = response });
        }
    }
}