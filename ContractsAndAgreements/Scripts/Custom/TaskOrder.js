﻿var ProjectInfoId = localStorage.getItem("ProjectInfoId");
var ProjectAdvId = localStorage.getItem("ProjectTaskOrderId");
//var subOptionText;

$(document).ready(function () {
    $('#TaskOrderForm').validate({});
    GetAllSupervisorList();
    GetAllParishList();
    GetAllSelectionList();
    GetAllProjectManagerList();
    GetAllConsultantAreaSubTaxList();
    if (ProjectInfoId != null) {
        GetProjectTaskOrderDetails();
        GetAdvertisementDetail(ProjectInfoId);
    }
    $('#btnSaveTaskOrder').on('click', function (e) {

        e.preventDefault();
        if (!$("#TaskOrderForm").valid()) {
            alert("Please fill all the required fields.");
            return false;
        }
        ProjectTaskOrder();
    });
    //$('#btnSaveAndAssignAdvertisement').on('click', function (e) {
    //    
    //    e.preventDefault();
    //    if (!$("#AdvertisementInfoForm").valid()) {
    //        return false;
    //    }
    //    ProjectAdvertisement();
    //});
    $('#btnCancel').on('click', function (e) {
        window.location.href = localStorage.getItem("BaseUrl") + "Dashboard/AdvertisementDashboard";
        return false;
    });
    $('#btnAdvertisementDocument').on('click', function (e) {
        window.location.href = localStorage.getItem("BaseUrl") + "Advertisement/AdvertisementDocument";
        return false;
    });

    $('#btnAddendum').on('click', function (e) {
        window.location.href = localStorage.getItem("BaseUrl") + "Advertisement/Addendum";
        return false;
    });
    $('#btnAdd').on('click', function (e) {
        AddNewRow();
    });

    $('#btnEditContract').on('click', function (e) {
        $('#btnSaveContract').prop('disabled', false);
        EnableDisable(false);
        $("#btnEditContract").css("visibility", "hidden");
        // $('#btnEditAdvertisement').prop('disabled', true);
    });


    $('#btnSaveAndAssignAdvertisement').on('click', function (event) {
        event.preventDefault();

        var url = localStorage.getItem("BaseUrl") + "Advertisement/SendEmail?SPN=" + $("#txtStateProjectNo").val() + "&SupId=" + $("#ddlSupervisor").val();
        $.ajax({
            type: "POST",
            url: url,
            contentType: "application/json",
            dataType: "json",
            success: function (data) {

            },
            error: function (err) { }
        });
    });
});

function GetProjectTaskOrderDetails() {
    $.ajax({
        type: "GET",
        url: localStorage.getItem("BaseUrl") + "Advertisement/GetProjectTaskOrderDetails?ProjectInfoId=" + ProjectInfoId,
        contentType: "application/json",
        dataType: "json",
        success: function (response) {

            if (response.Result.Status == 1) {
                if (response.Result.Result != null) {
                    var result = response.Result.Result;

                    $("#txtTaskForProject").val(result.TaskForProject);
                    $("#txtTaskScope").val(result.TaskScope);
                    $("#txtTaxClassification").val(result.TaxClassification);
                    $("#txtTaskExpirationDate").val(result.TaskExpirationDate);
                    $("#txtContractExpires").val(result.ContractExpires);

                    if (result.TaskOrderInformation != null) {
                        if (result.TaskOrderInformation.ContractAmount != null) {
                            $("#txtContractAmount").val("$" + result.TaskOrderInformation.ContractAmount);
                        }
                        if (result.TaskOrderInformation.TOAmount != null) {
                            $("#txtTOAmount").val("$" + result.TaskOrderInformation.TOAmount);
                        }
                        $("#ddlPrime").val(result.TaskOrderInformation.Prime);
                        $("#txtPrimeAmount").val("$" + result.TaskOrderInformation.PrimeAmount);
                        if (result.TaskOrderInformation.SubTaxDetailsDto != null) {
                            $("#ddlSub0").val(result.TaskOrderInformation.SubTaxDetailsDto[0].SubId);
                            $("#txtTaxId0").val(result.TaskOrderInformation.SubTaxDetailsDto[0].TaxId);
                            $("#txtCAmount0").val("$" + result.TaskOrderInformation.SubTaxDetailsDto[0].ContractAmount);
                            if (result.TaskOrderInformation.SubTaxDetailsDto.length > 1) {
                                for (i = 0; i < result.TaskOrderInformation.SubTaxDetailsDto.length; i++) {
                                    AddNewRow();
                                    $("#ddlSub" + (i + 1) + "").val(result.TaskOrderInformation.SubTaxDetailsDto[i].SubId);
                                    $("#txtTaxId" + (i + 1) + "").val(result.TaskOrderInformation.SubTaxDetailsDto[i].TaxId);
                                    $("#txtCAmount" + (i + 1) + "").val(result.TaskOrderInformation.SubTaxDetailsDto[i].ContractAmount);
                                }
                            }
                        }
                    }
                }
            }
        },
        error: function (result) {

        }
    });
}
function ProjectTaskOrder() {

    var ProjectTaskOrder = {};
    ProjectTaskOrder.ProjectGeneralInformationId = ProjectInfoId;
    ProjectTaskOrder.TaskForProject = $("#txtTaskForProject").val();
    ProjectTaskOrder.TaskScope = $("#txtTaskScope").val();
    ProjectTaskOrder.TaxClassification = $("#txtTaxClassification").val();
    ProjectTaskOrder.TaskExpirationDate = $("#txtTaskExpirationDate").val();
    ProjectTaskOrder.ContractExpires = $("#txtContractExpires").val();
    //ProjectTaskOrder.ProjectManagerId = $("#ddlProjectManager").val();
    //ProjectTaskOrder.ManagerPhone = $("#txtManagerPhone").val();

    ProjectTaskOrder.TaskOrderInformation = {};
    if ($("#txtStateProjectNo").val() != "") {
        var information = {};
        // information.ProjectGeneralInformationId = ProjectInfoId;
        information.ContractAmount = $("#txtContractAmount").val().replace("$", "");
        information.TOAmount = $("#txtTOAmount").val().replace("$", "");
        information.Prime = $("#ddlPrime").val();
        information.PrimeAmount = $("#txtPrimeAmount").val().replace("$", "");
        information.SubTaxDetailsDto = [];
        $("#tbodySubTab tr.item").each(function () {
            var TaxDetailsDto = {};
            $(this).find('td :input').each(function () {
                if ($(this).attr('id').indexOf('ddlSub') > -1) {
                    TaxDetailsDto.SubId = $(this).val();
                }
                else if ($(this).attr('id').indexOf('txtTaxId') > -1) {
                    TaxDetailsDto.TaxId = $(this).val();
                }
                else if ($(this).attr('id').indexOf('txtCAmount') > -1) {
                    TaxDetailsDto.ContractAmount = $(this).val().replace("$", "");
                }
            });
            if (TaxDetailsDto.SubId != 0 && TaxDetailsDto.SubId != "" && TaxDetailsDto.SubId != undefined) {
                information.SubTaxDetailsDto.push(TaxDetailsDto);
            }
        });
        ProjectTaskOrder.TaskOrderInformation = information;
    }
    var url = localStorage.getItem("BaseUrl") + "Advertisement/CreateProjectTaskOrderDetails";

    $.ajax({
        type: "POST",
        url: url,
        contentType: "application/json",
        dataType: "json",
        data: JSON.stringify(ProjectTaskOrder),
        success: function (data) {
            if (data.Result.Status == 1 || data.Result.Status == 3) {
                $('#divLogInformationPanel').css('display', 'block');
                alertify.alert('Success!', 'Record has been saved successfully.', function () {
                    window.location.href = localStorage.getItem("BaseUrl") + "Dashboard/AdvertisementDashboard";
                    // $('#btnSaveAdvertisement').prop('disabled', true);
                    // EnableDisable(true);
                    //// $('#btnEditAdvertisement').prop('disabled', false);
                    // $("#btnEditAdvertisement").css("visibility", "visible");


                });
            }
        },
        error: function (err) { }
    });
    return false;
}

function GetAdvertisementDetail(ProjectGeneralInformationId) {
    $.ajax({
        type: "GET",
        url: localStorage.getItem("BaseUrl") + "Advertisement/GetProjectAdvertisementDetails?ProjectInfoId=" + ProjectGeneralInformationId,
        contentType: "application/json",
        dataType: "json",
        success: function (response) {
            var result = response.Result.Result;
            if (result != null) {
                debugger;
                $("#txtStateProjectNo").val(result.Information.StateProjectNo);
                $("#txtFAPNo").val(result.Information.FAPNo);
                $("#txtProjectName").val(result.Information.ProjectName);
                $("#txtRoute").val(result.Information.Route);
                $("#ddlParish").val(result.Information.Parish);
                $("#txtGeneralDescription").val(result.Information.GeneralDescription);
                $("#ddlSelection").val(result.Information.Selection);
                result.Information.FHWAFullOversight == true ? $('#rdoFHWAFullOversight1').prop('checked', true) : $('#rdoFHWAFullOversight1').prop('checked', false);
                $("#ddlContractType").val(result.Information.ContractType);
                $("#ddlFeeType").val(result.Information.FeeType);
                $("#ddlContractAction").val(result.Information.ContractAction);
                $("#ddlProjectManager").val(result.ProjectManagerId);
                $("#txtManagerPhone").val(result.ManagerPhone);

                if (result.Information.AreaDto != null)
                {
                    $("#ddlPrime").val(result.Information.AreaDto.ConsultantProviderId);
                }

                //if (result.Information.SubTaxDetailsDto != null) {
                //    $("#ddlSub0").val(result.Information.SubTaxDetailsDto[0].SubId);
                //    $("#txtTaxId0").val(result.Information.SubTaxDetailsDto[0].TaxId);
                //    if (result.Information.SubTaxDetailsDto[0].ContractAmount != null) {
                //        $("#txtCAmount0").val("$" + result.Information.SubTaxDetailsDto[0].ContractAmount);
                //    }
                //    if (result.Information.SubTaxDetailsDto.length > 1) {
                //        for (i = 0; i < result.Information.SubTaxDetailsDto.length; i++) {
                //            AddNewRow();
                //            $("#ddlSub" + (i + 1) + "").val(result.Information.SubTaxDetailsDto[i].SubId);
                //            $("#txtTaxId" + (i + 1) + "").val(result.Information.SubTaxDetailsDto[i].TaxId);
                //            if (result.Information.SubTaxDetailsDto[i].ContractAmount != null) {
                //                $("#txtCAmount" + (i + 1) + "").val("$" + result.Information.SubTaxDetailsDto[i].ContractAmount);
                //            }
                //        }
                //    }
                //}
            }
        },
        error: function (result) {

        }
    });
}



function EnableDisable(value) {
    $("#txtStateProjectNo").prop('disabled', value);
    $("#txtFAPNo").prop('disabled', value);
    $("#txtProjectName").prop('disabled', value);
    $("#txtRoute").prop('disabled', value);
    $("#ddlParish").prop('disabled', value);
    $("#txtGeneralDescription").prop('disabled', value);
    $("#ddlSelection").prop('disabled', value);
    $("#ddlContractType").prop('disabled', value);
    $("#ddlFeeType").prop('disabled', value);
    $("#txtEstContractAmount").prop('disabled', value);
    $("#ddlProjectManager").prop('disabled', value);
    $("#txtManagerPhone").prop('disabled', value);
    $("#txtProjectDetails").prop('disabled', value);
    $("#txtMinimumManPower").prop('disabled', value);
    $("#txtFundingSource").prop('disabled', value);
    $("#txtContractTime").prop('disabled', value);
    $("#ddlProjectManager").prop('disabled', value);
    $("#txtManagerPhone").prop('disabled', value);
    //Information
    $("#txtContractAmount").prop('disabled', value);
    $("#txtTOAmount").prop('disabled', value);
    $("#ddlPrime").prop('disabled', value);
    $("#txtPrimeAmount").prop('disabled', value);
    //WorkFlow Tracking
    $("#txtSupervisor").prop('disabled', value);
    $("#txtProcessor").prop('disabled', value);
    $("#txtPhone").prop('disabled', value);
    $("#txtComments").prop('disabled', value);
    $("#txtInfoRecAndLogged").prop('disabled', value);

    $("#txtTaskAssignedToStaff").prop('disabled', value);
    $("#txtChiefApprovaltoRetain").prop('disabled', value);
    $("#txtDrafttoSupervisor").prop('disabled', value);
    $("#txtApprovedfromSupervisor").prop('disabled', value);
    $("#txtDrafttoProjectManager").prop('disabled', value);
    $("#txtApprovedFromProjectManager").prop('disabled', value);
    $("#txtDrafttoChiefEngineer").prop('disabled', value);
    $("#txtApprovedfromChiefEngineer").prop('disabled', value);
    $("#txtDrafttoFHWA").prop('disabled', value);
    $("#txtApprovedfromFHWA").prop('disabled', value);
    $("#txtFedAideFHWAAuth").prop('disabled', value);
    $("#txtAdvertisementDate").prop('disabled', value);
    $("#txtClosingDate").prop('disabled', value);
    $("#txtAmendedClosingDate").prop('disabled', value);
    $("#txtSecretaryShortlist").prop('disabled', value);
    $("#txtAwardNotification").prop('disabled', value);
    $("#txtTo3rdFloor").prop('disabled', value);
}


