﻿var ProjectInfoId = localStorage.getItem("ProjectInfoId");
var ProjectAdvId = localStorage.getItem("ProjectContractDetailId");
var subOptionText;

$(document).ready(function () {
    $('#ContractInfoForm').validate({});
    GetAllParishList();
    GetAllSelectionList();
    GetAllConsultantAreaSubTaxList();
    GetAllProjectManagerList();
    GetAllContractTypeList();
    GetAllFeeTypeList();
    GetAllSupervisorList();
    if (ProjectInfoId != null) {
        GetProjectContractDetails();
        GetAdvertisementDetail(ProjectInfoId)
    }
    $('#btnSaveContract').on('click', function (e) {

        e.preventDefault();
        if (!$("#ContractInfoForm").valid()) {
            alert("Please fill all the required fields.");
            return false;
        }
        ProjectContract();
    });

    $('#btnCancel').on('click', function (e) {
        window.location.href = localStorage.getItem("BaseUrl") + "Dashboard/AdvertisementDashboard";
        return false;
    });
    $('#btnAdvertisementDocument').on('click', function (e) {
        window.location.href = localStorage.getItem("BaseUrl") + "Advertisement/AdvertisementDocument";
        return false;
    });

    $('#btnAddendum').on('click', function (e) {
        window.location.href = localStorage.getItem("BaseUrl") + "Advertisement/Addendum";
        return false;
    });
    $('#btnAdd').on('click', function (e) {
        AddNewRow();
    });

    $('#btnEditContract').on('click', function (e) {
        $('#btnSaveContract').prop('disabled', false);
        EnableDisable(false);
        $("#btnEditContract").css("visibility", "hidden");
        // $('#btnEditAdvertisement').prop('disabled', true);
    });


    $('#btnSaveAndAssignAdvertisement').on('click', function (event) {
        event.preventDefault();

        var url = localStorage.getItem("BaseUrl") + "Advertisement/SendEmail?SPN=" + $("#txtStateProjectNo").val() + "&SupId=" + $("#ddlSupervisor").val();
        $.ajax({
            type: "POST",
            url: url,
            contentType: "application/json",
            dataType: "json",
            success: function (data) {

            },
            error: function (err) { }
        });
    });
});

function GetProjectContractDetails() {
    $.ajax({
        type: "GET",
        url: localStorage.getItem("BaseUrl") + "Advertisement/GetProjectContractDetails?ProjectInfoId=" + ProjectInfoId,
        contentType: "application/json",
        dataType: "json",
        success: function (response) {
            // debugger;
            if (response.Result.Status == 1) {
                if (response.Result.Result != null) {
                    var result = response.Result.Result;

                    $("#txtProjectDetails").val(result.ProjectDetails);
                    $("#txtContractAction").val(result.ContractAction);
                    $("#txtExpDate").val(result.ExpDate);

                    //WorkFlow Tracking
                    if (result.ProjectWork != null) {
                        $("#ddlSupervisor").val(result.ProjectWork.Supervisor);
                        $("#txtProcessor").val(result.ProjectWork.Processor);
                        $("#txtPhone").val(result.ProjectWork.Phone);
                        $("#txtComments").val(result.ProjectWork.Comments);
                        result.ProjectWork.Suspended == true ? $('#rdoSuspended').prop('checked', true) : $('#rdoSuspended').prop('checked', false);
                        $("#txtInfoRecAndLogged").val(result.ProjectWork.InfoReceived);
                        if (result.ProjectWork.TaskAssignedToStaff != null) {
                            $("#txtTaskAssignedToStaff").val(result.ProjectWork.TaskAssignedToStaff.split(' ')[0]);
                        }
                        $("#txtDraftandFeePacktoSupervisor").val(result.ProjectWork.DraftandFeePacktoSupervisor);
                        $("#txtApprovedfromSupervisor").val(result.ProjectWork.ApprovedfromSupervisor);
                        $("#txtDraftandFeePacktoProjectManager").val(result.ProjectWork.DraftandFeePacktoProjectManager);
                        $("#txtConcurredFromProjectManager").val(result.ProjectWork.ConcurredFromProjectManager);
                        $("#txtDrafttoConsultant").val(result.ProjectWork.DrafttoConsultant);
                        $("#txtConcurredFromConsultant").val(result.ProjectWork.ConcurredFromConsultant);
                        $("#txtFedAideFHWAAuth").val(result.ProjectWork.FedAideFHWAAuth);
                        $("#txtAuthorized").val(result.ProjectWork.Authorized);
                        $("#txtContractToConsultant").val(result.ProjectWork.ContractToConsultant);
                        $("#txtSigned").val(result.ProjectWork.Signed);
                        $("#txtContractTo3rdFloor").val(result.ProjectWork.ContractTo3rdFloor);
                        $("#txtSignedExecuted").val(result.ProjectWork.SignedExecuted);
                        $("#txtNTPLetter").val(result.ProjectWork.NTPLetter);
                        $("#txtEffectiveDate").val(result.ProjectWork.EffectiveDate);
                        //Contract Information
                    }
                    if (result.ContractInformation != null) {
                        $("#txtContractAmount").val("$" + result.ContractInformation.ContractAmount);
                        $("#txtDescription").val(result.ContractInformation.Description);
                        $("#ddlPrime").val(result.ContractInformation.Prime);
                        $("#txtPrimeAmount").val("$" + result.ContractInformation.PrimeAmount);
                        if (result.ContractInformation.SubTaxDetailsDto != null) {
                            $("#ddlSub0").val(result.ContractInformation.SubTaxDetailsDto[0].SubId);
                            $("#txtTaxId0").val(result.ContractInformation.SubTaxDetailsDto[0].TaxId);
                            $("#txtCAmount0").val("$" + result.ContractInformation.SubTaxDetailsDto[0].ContractAmount);
                            if (result.ContractInformation.SubTaxDetailsDto.length > 1) {
                                for (i = 0; i < result.ContractInformation.SubTaxDetailsDto.length; i++) {
                                    AddNewRow();
                                    $("#ddlSub" + (i + 1) + "").val(result.ContractInformation.SubTaxDetailsDto[i].SubId);
                                    $("#txtTaxId" + (i + 1) + "").val(result.ContractInformation.SubTaxDetailsDto[i].TaxId);
                                    $("#txtCAmount" + (i + 1) + "").val("$" + result.ContractInformation.SubTaxDetailsDto[i].ContractAmount);
                                }
                            }
                        }
                    }
                }
            }
        },
        error: function (result) {

        }
    });
}
function ProjectContract() {

    var ProjectContract = {};
    ProjectContract.ProjectGeneralInformationId = ProjectInfoId;
    ProjectContract.ProjectDetails = $("#txtProjectDetails").val();
    ProjectContract.ExpDate = $("#txtExpDate").val();
    //  ProjectContract.ContractAction = $("#txtContractAction").val();
    ProjectContract.ProjectManagerId = $("#ddlProjectManager").val();
    ProjectContract.ManagerPhone = $("#txtManagerPhone").val();

    //Save ProjectWork
    ProjectContract.ProjectWork = {};

    if ($("#ddlSupervisor").val() != "") {
        var projectwork = {};
        projectwork.Supervisor = $("#ddlSupervisor").val();
        projectwork.Processor = $("#txtProcessor").val();
        projectwork.Phone = $("#txtPhone").val();
        projectwork.Comments = $("#txtComments").val();
        projectwork.Suspended = $("#Suspended1").prop('checked');
        projectwork.InfoRecAndLogged = $("#txtInfoRecAndLogged").val();
        projectwork.TaskAssignedToStaff = $("#txtTaskAssignedToStaff").val();
        // projectwork.DraftandFeePacktoSupervisor = $("#txtDraftandFeePacktoSupervisor").val();
        projectwork.ApprovedfromSupervisor = $("#txtApprovedfromSupervisor").val();
        //  projectwork.DraftandFeePacktoProjectManager = $("#txtDraftandFeePacktoProjectManager").val();
        projectwork.ConcurredFromProjectManager = $("#txtConcurredFromProjectManager").val();
        projectwork.DrafttoConsultant = $("#txtDrafttoConsultant").val();
        projectwork.ConcurredFromConsultant = $("#txtConcurredFromConsultant").val();
        // projectwork.FedAideFHWAAuth = $("#txtFedAideFHWAAuth").val();
        projectwork.Authorized = $("#txtAuthorized").val();
        projectwork.ContractToConsultant = $("#txtContractToConsultant").val();
        projectwork.Signed = $("#txtSigned").val();
        projectwork.ContractTo3rdFloor = $("#txtContractTo3rdFloor").val();
        projectwork.SignedExecuted = $("#txtSignedExecuted").val();
        projectwork.NTPLetter = $("#txtNTPLetter").val();
        projectwork.EffectiveDate = $("#txtEffectiveDate").val();

        ProjectContract.ProjectWork = projectwork;
    }
    ProjectContract.ContractInformation = {};
    if ($("#txtStateProjectNo").val() != "") {
        var information = {};
        // information.ProjectGeneralInformationId = ProjectInfoId;
        information.ContractAmount = $("#txtContractAmount").val().replace("$", "");
        information.Description = $("#txtDescription").val();
        information.Prime = $("#ddlPrime").val();
        information.PrimeAmount = $("#txtPrimeAmount").val().replace("$", "");;
        information.SubTaxDetailsDto = [];
        $("#tbodySubTab tr.item").each(function () {
            var TaxDetailsDto = {};
            $(this).find('td :input').each(function () {
                if ($(this).attr('id').indexOf('ddlSub') > -1) {
                    TaxDetailsDto.SubId = $(this).val();
                }
                else if ($(this).attr('id').indexOf('txtTaxId') > -1) {
                    TaxDetailsDto.TaxId = $(this).val();
                }
                else if ($(this).attr('id').indexOf('txtCAmount') > -1) {
                    TaxDetailsDto.ContractAmount = $(this).val().replace("$", "");
                }
            });
            if (TaxDetailsDto.SubId != 0 && TaxDetailsDto.SubId != "" && TaxDetailsDto.SubId != undefined) {
                information.SubTaxDetailsDto.push(TaxDetailsDto);
            }
        });
        ProjectContract.ContractInformation = information;
    }
    var url = localStorage.getItem("BaseUrl") + "Advertisement/CreateProjectContractDetails";

    $.ajax({
        type: "POST",
        url: url,
        contentType: "application/json",
        dataType: "json",
        data: JSON.stringify(ProjectContract),
        success: function (data) {
            if (data.Result.Status == 1 || data.Result.Status == 3) {
                $('#divLogInformationPanel').css('display', 'block');
                alertify.alert('Success!', 'Record has been saved successfully.', function () {
                    window.location.href = localStorage.getItem("BaseUrl") + "Dashboard/AdvertisementDashboard";



                });
            }
        },
        error: function (err) { }
    });
    return false;
}


function GetAdvertisementDetail(ProjectGeneralInformationId) {
    $.ajax({
        type: "GET",
        url: localStorage.getItem("BaseUrl") + "Advertisement/GetProjectAdvertisementDetails?ProjectInfoId=" + ProjectGeneralInformationId,
        contentType: "application/json",
        dataType: "json",
        success: function (response) {
            var result = response.Result.Result;
            if (result != null) {
                debugger;
                $("#txtStateProjectNo").val(result.Information.StateProjectNo);
                $("#txtFAPNo").val(result.Information.FAPNo);
                $("#txtProjectName").val(result.Information.ProjectName);
                $("#txtRoute").val(result.Information.Route);
                $("#ddlParish").val(result.Information.Parish);
                $("#txtGeneralDescription").val(result.Information.GeneralDescription);
                $("#ddlSelection").val(result.Information.Selection);
                result.Information.FHWAFullOversight == true ? $('#rdoFHWAFullOversight1').prop('checked', true) : $('#rdoFHWAFullOversight1').prop('checked', false);
                $("#ddlContractType").val(result.Information.ContractType);
                $("#ddlFeeType").val(result.Information.FeeType);
                $("#ddlContractAction").val(result.Information.ContractAction);
                $("#ddlProjectManager").val(result.ProjectManagerId);
                $("#ddlPrime").val(result.Information.AreaDto.ConsultantProviderId);
                $("#txtManagerPhone").val(result.ManagerPhone);
                if (result.ProjectWork != null) {
                    $("#ddlSupervisor").val(result.ProjectWork.Supervisor);
                    $("#txtProcessor").val(result.ProjectWork.Processor);
                    $("#txtPhone").val(result.ProjectWork.Phone);
                    $("#txtComments").val(result.ProjectWork.Comments);
                    result.ProjectWork.Suspended == true ? $('#rdoSuspended').prop('checked', true) : $('#rdoSuspended').prop('checked', false);
                    $("#txtInfoRecAndLogged").val(result.ProjectWork.InfoReceived);
                    if (result.ProjectWork.TaskAssignedToStaff != null) {
                        $("#txtTaskAssignedToStaff").val(result.ProjectWork.TaskAssignedToStaff.split(' ')[0]);
                    }
                }
                if (result.Information.SubTaxDetailsDto != null) {
                    $("#ddlSub0").val(result.Information.SubTaxDetailsDto[0].SubId);
                    $("#txtTaxId0").val(result.Information.SubTaxDetailsDto[0].TaxId);
                    if (result.Information.SubTaxDetailsDto[0].ContractAmount != null) {
                        $("#txtCAmount0").val("$" + result.Information.SubTaxDetailsDto[0].ContractAmount);
                    }
                    if (result.Information.SubTaxDetailsDto.length > 1) {
                        for (i = 0; i < result.Information.SubTaxDetailsDto.length; i++) {
                            AddNewRow();
                            $("#ddlSub" + (i + 1) + "").val(result.Information.SubTaxDetailsDto[i].SubId);
                            $("#txtTaxId" + (i + 1) + "").val(result.Information.SubTaxDetailsDto[i].TaxId);
                            if (result.Information.SubTaxDetailsDto[i].ContractAmount != null) {
                                $("#txtCAmount" + (i + 1) + "").val("$" + result.Information.SubTaxDetailsDto[i].ContractAmount);
                            }
                        }
                    }
                }
            }
        },
        error: function (result) {

        }
    });
}