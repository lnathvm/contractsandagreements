﻿var ProjectInfoId = localStorage.getItem("ProjectInfoId");
var ProjectAdvId = localStorage.getItem("ProjectSupplementalAgreementId");
var subOptionText;

$(document).ready(function () {
    $('#SupplementalAgreementForIDIQForm').validate({});
    GetAllSupervisorList();
    GetAllParishList();
    GetAllSelectionList();
    GetAllProjectManagerList();
    GetAllConsultantAreaSubTaxList();
    if (ProjectInfoId != null) {
        GetProjectSupplementalAgreementForIDIQ();
        GetAdvertisementDetail(ProjectInfoId);
    }
    $('#btnSaveSupplementalAgreement').on('click', function (e) {

        e.preventDefault();
        if (!$("#SupplementalAgreementForIDIQForm").valid()) {
            alert("Please fill all the required fields.");
            return false;
        }
        ProjectSupplementalAgreement();
    });

    $('#btnCancel').on('click', function (e) {
        window.location.href = localStorage.getItem("BaseUrl") + "Dashboard/AdvertisementDashboard";
        return false;
    });
    $('#btnAdvertisementDocument').on('click', function (e) {
        window.location.href = localStorage.getItem("BaseUrl") + "Advertisement/AdvertisementDocument";
        return false;
    });

    $('#btnAddendum').on('click', function (e) {
        window.location.href = localStorage.getItem("BaseUrl") + "Advertisement/Addendum";
        return false;
    });
    $('#btnAdd').on('click', function (e) {
        //AddNewRowForIDIQ();
        AddNewRow
    });

    $('#btnEditContract').on('click', function (e) {
        $('#btnSaveContract').prop('disabled', false);
        $("#btnEditContract").css("visibility", "hidden");
        // $('#btnEditAdvertisement').prop('disabled', true);
    });


    $('#btnSaveAndAssignAdvertisement').on('click', function (event) {
        event.preventDefault();

        var url = localStorage.getItem("BaseUrl") + "Advertisement/SendEmail?SPN=" + $("#txtStateProjectNo").val() + "&SupId=" + $("#ddlSupervisor").val();
        $.ajax({
            type: "POST",
            url: url,
            contentType: "application/json",
            dataType: "json",
            success: function (data) {

            },
            error: function (err) { }
        });
    });
});


function GetProjectSupplementalAgreementForIDIQ() {
    $.ajax({
        type: "GET",
        url: localStorage.getItem("BaseUrl") + "Advertisement/GetProjectSupplementalAgreementForIDIQ?ProjectInfoId=" + ProjectInfoId,
        contentType: "application/json",
        dataType: "json",
        success: function (response) {

            if (response.Result.Status == 1) {
                if (response.Result.Result != null) {
                    var result = response.Result.Result;

                    $("#txtSAForProject").val(result.SAForProject);
                    $("#txtSAScope").val(result.SAScope);
                    $("#txtTaxClassification").val(result.TaxClassification);
                    $("#txtSAExpirationDate").val(result.SAExpirationDate);
                    $("#txtContractExpires").val(result.ContractExpires);

                    if (result.TaskOrderInformation != null) {
                        $("#txtContractAmount").val("$" + result.TaskOrderInformation.ContractAmount);
                        if (result.TaskOrderInformation.TOAmount != null) {
                            $("#txtTOAmount").val("$" + result.TaskOrderInformation.TOAmount);
                        }
                        $("#ddlPrime").val(result.TaskOrderInformation.Prime);
                        $("#txtPrimeAmount").val("$" + result.TaskOrderInformation.PrimeAmount);
                        if (result.TaskOrderInformation.SubTaxDetailsDto != null) {
                            $("#ddlSub0").val(result.TaskOrderInformation.SubTaxDetailsDto[0].SubId);
                            $("#txtTaxId0").val(result.TaskOrderInformation.SubTaxDetailsDto[0].TaxId);
                            $("#txtCAmount0").val("$" + result.TaskOrderInformation.SubTaxDetailsDto[0].ContractAmount);
                            if (result.TaskOrderInformation.SubTaxDetailsDto.length > 1) {
                                for (i = 0; i < result.TaskOrderInformation.SubTaxDetailsDto.length; i++) {
                                    AddNewRow();
                                    $("#ddlSub" + (i + 1) + "").val(result.TaskOrderInformation.SubTaxDetailsDto[i].SubId);
                                    $("#txtTaxId" + (i + 1) + "").val(result.TaskOrderInformation.SubTaxDetailsDto[i].TaxId);
                                    $("#txtCAmount" + (i + 1) + "").val("$" + result.TaskOrderInformation.SubTaxDetailsDto[i].ContractAmount);
                                }
                            }
                        }
                    }
                }
            }
        },
        error: function (result) {

        }
    });
}
function ProjectSupplementalAgreement() {

    var ProjectSupplementalAgreement = {};
    ProjectSupplementalAgreement.ProjectGeneralInformationId = ProjectInfoId;
    ProjectSupplementalAgreement.SAForProject = $("#txtSAForProject").val();
    ProjectSupplementalAgreement.SAScope = $("#txtSAScope").val();
    ProjectSupplementalAgreement.TaxClassification = $("#txtTaxClassification").val();
    ProjectSupplementalAgreement.SAExpirationDate = $("#txtSAExpirationDate").val();
    ProjectSupplementalAgreement.ContractExpires = $("#txtContractExpires").val();
    ProjectSupplementalAgreement.ProjectManagerId = $("#ddlProjectManager").val();
    ProjectSupplementalAgreement.ManagerPhone = $("#txtManagerPhone").val();

    //Save ProjectWork
    ProjectSupplementalAgreement.ProjectWork = {};

    if ($("#ddlSupervisor").val() != "") {
        var projectwork = {};
        projectwork.Supervisor = $("#ddlSupervisor").val();
        projectwork.Processor = $("#txtProcessor").val();
        projectwork.Phone = $("#txtPhone").val();
        projectwork.Comments = $("#txtComments").val();
        projectwork.Suspended = $("#Suspended1").prop('checked');
        projectwork.InfoRecAndLogged = $("#txtInfoRecAndLogged").val();
        projectwork.TaskAssignedToStaff = $("#txtTaskAssignedToStaff").val();
        projectwork.DraftandFeePacktoSupervisor = $("#txtDraftandFeePacktoSupervisor").val();
        projectwork.ApprovedfromSupervisor = $("#txtApprovedfromSupervisor").val();
        projectwork.DraftandFeePacktoProjectManager = $("#txtDraftandFeePacktoProjectManager").val();
        projectwork.ConcurredFromProjectManager = $("#txtConcurredFromProjectManager").val();
        projectwork.DrafttoConsultant = $("#txtDrafttoConsultant").val();
        projectwork.ConcurredFromConsultant = $("#txtConcurredFromConsultant").val();
        //  projectwork.FedAideFHWAAuth = $("#txtFedAideFHWAAuth").val();
        projectwork.Authorized = $("#txtAuthorized").val();
        projectwork.ContractToConsultant = $("#txtContractToConsultant").val();
        projectwork.Signed = $("#txtSigned").val();
        projectwork.ContractTo3rdFloor = $("#txtContractTo3rdFloor").val();
        projectwork.SignedExecuted = $("#txtSignedExecuted").val();
        projectwork.NTPLetter = $("#txtNTPLetter").val();
        projectwork.EffectiveDate = $("#txtEffectiveDate").val();

        ProjectSupplementalAgreement.ProjectWork = projectwork;
    }
    ProjectSupplementalAgreement.TaskOrderInformation = {};
    if ($("#txtStateProjectNo").val() != "") {
        var information = {};
        // information.ProjectGeneralInformationId = ProjectInfoId;
        information.ContractAmount = $("#txtContractAmount").val().replace("$", "");
        information.TOAmount = $("#txtTOAmount").val().replace("$", "");
        information.Prime = $("#ddlPrime").val();
        //information.PrimeAmount = $("#txtPrimeAmount").val().replace("$", "");
        information.SubTaxDetailsDto = [];
        $("#tbodySubTab tr.item").each(function () {
            var TaxDetailsDto = {};
            $(this).find('td :input').each(function () {
                if ($(this).attr('id').indexOf('ddlSub') > -1) {
                    TaxDetailsDto.SubId = $(this).val();
                }
                else if ($(this).attr('id').indexOf('txtTaxId') > -1) {
                    TaxDetailsDto.TaxId = $(this).val();
                }
                else if ($(this).attr('id').indexOf('txtCAmount') > -1) {
                    TaxDetailsDto.ContractAmount = $(this).val().replace("$", "");;
                }
            });
            if (TaxDetailsDto.SubId != 0 && TaxDetailsDto.SubId != "" && TaxDetailsDto.SubId != undefined) {
                information.SubTaxDetailsDto.push(TaxDetailsDto);
            }
        });
        ProjectSupplementalAgreement.TaskOrderInformation = information;
    }
    var url = localStorage.getItem("BaseUrl") + "Advertisement/CreateProjectSupplementalAgreementForIDIQ";

    $.ajax({
        type: "POST",
        url: url,
        contentType: "application/json",
        dataType: "json",
        data: JSON.stringify(ProjectSupplementalAgreement),
        success: function (data) {
            if (data.Result.Status == 1 || data.Result.Status == 3) {
                $('#divLogInformationPanel').css('display', 'block');
                alertify.alert('Success!', 'Record has been saved successfully.', function () {
                    window.location.href = localStorage.getItem("BaseUrl") + "Dashboard/AdvertisementDashboard";
                });
            }
        },
        error: function (err) { }
    });
    return false;
}


function GetAdvertisementDetail(ProjectGeneralInformationId) {
    $.ajax({
        type: "GET",
        url: localStorage.getItem("BaseUrl") + "Advertisement/GetProjectAdvertisementDetails?ProjectInfoId=" + ProjectGeneralInformationId,
        contentType: "application/json",
        dataType: "json",
        success: function (response) {
            var result = response.Result.Result;
            if (result != null) {
                debugger;
                $("#txtStateProjectNo").val(result.Information.StateProjectNo);
                $("#txtFAPNo").val(result.Information.FAPNo);
                $("#txtProjectName").val(result.Information.ProjectName);
                $("#txtRoute").val(result.Information.Route);
                $("#ddlParish").val(result.Information.Parish);
                $("#txtGeneralDescription").val(result.Information.GeneralDescription);
                $("#ddlSelection").val(result.Information.Selection);
                result.Information.FHWAFullOversight == true ? $('#rdoFHWAFullOversight1').prop('checked', true) : $('#rdoFHWAFullOversight1').prop('checked', false);
                $("#ddlContractType").val(result.Information.ContractType);
                $("#ddlFeeType").val(result.Information.FeeType);
                $("#ddlContractAction").val(result.Information.ContractAction);
                $("#ddlProjectManager").val(result.ProjectManagerId);
                $("#txtManagerPhone").val(result.ManagerPhone);
                if (result.ProjectWork != null) {
                    $("#ddlSupervisor").val(result.ProjectWork.Supervisor);
                    $("#txtProcessor").val(result.ProjectWork.Processor);
                    $("#txtPhone").val(result.ProjectWork.Phone);
                    $("#txtComments").val(result.ProjectWork.Comments);
                    result.ProjectWork.Suspended == true ? $('#rdoSuspended').prop('checked', true) : $('#rdoSuspended').prop('checked', false);
                    $("#txtInfoRecAndLogged").val(result.ProjectWork.InfoReceived);
                    if (result.ProjectWork.TaskAssignedToStaff != null) {
                        $("#txtTaskAssignedToStaff").val(result.ProjectWork.TaskAssignedToStaff.split(' ')[0]);
                    }
                }
                if (result.Information.AreaDto != null) {

                   $("#ddlPrime").val(result.Information.AreaDto.ConsultantProviderId);
                }
            }
        },
        error: function (result) {

        }
    });
}