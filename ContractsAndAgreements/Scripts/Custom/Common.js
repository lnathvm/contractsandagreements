﻿var subOptionText;
$(document).ready(function () {
    //GetAllConsultantProviderList();
    //GetAllContractTypeList();
    //GetAllFeeTypeList();
    //GetAllParishList();
    //GetAllSelectionList();    
    //GetAllProjectManagerList();
    //GetAllSupervisorList();
    $("input[data-type='currency']").on({
        keyup: function () {
            formatCurrency($(this), "", "$");
        },
        blur: function () {
            formatCurrency($(this), "blur", "$");
        }
    });
    $("input[data-type='percent']").on({
        keyup: function () {
            formatCurrency($(this), "", "%");
        },
        blur: function () {
            formatCurrency($(this), "blur", "%");
        }
    });
});
function GetAllConsultantProviderList() {
    $.ajax({
        type: "GET",
        url: localStorage.getItem("BaseUrl") + "Common/GetAllConsultantProviderList",
        contentType: "application/json",
        dataType: "json",
        async : false,
        success: function (response) {
            var options = "<option value='' selected='selected'>--Select Consultant/Provider--</option>";
            if (response.data != undefined && response.data.length > 0) {
                $.each(response.data, function (key, value) {
                    options += '<option value="' + value.ConsultantProviderId + '">' + value.ConsultantProviderName + '</option>';
                });
            }
            $('#ddlConsultantProvider').html(options);
        },
        error: function (result) {
            var options = "<option value='' selected='selected'>--Select Consultant/Provider--</option>";
            $('#ddlConsultantProvider').html(options);
        }
    });
}

function GetAllContractTypeList() {
    $.ajax({
        type: "GET",
        url: localStorage.getItem("BaseUrl") + "Common/GetAllContractTypeList",
        contentType: "application/json",
        dataType: "json",
        async: false,
        success: function (response) {
            var options = "<option value='' selected='selected'>--Select ContractType--</option>";
            if (response.Result.Result != undefined && response.Result.Result.length > 0) {
                $.each(response.Result.Result, function (key, value) {
                    options += '<option value="' + value.Id + '">' + value.Name + '</option>';
                });
            }
            $('#ddlContractType').html(options);
        },
        error: function (result) {
            var options = "<option value='' selected='selected'>--Select ContractType--</option>";
            $('#ddlContractType').html(options);
        }
    });
}

function GetAllFeeTypeList() {
    $.ajax({
        type: "GET",
        url: localStorage.getItem("BaseUrl") + "Common/GetAllFeeTypeList",
        contentType: "application/json",
        dataType: "json",
        async: false,
        success: function (response) {

            var options = "<option value='' selected='selected'>--Select FeeType--</option>";
            if (response.Result.Result != undefined && response.Result.Result.length > 0) {
                $.each(response.Result.Result, function (key, value) {
                    options += '<option value="' + value.Id + '">' + value.Name + '</option>';
                });
            }
            $('#ddlFeeType').html(options);
        },
        error: function (result) {
            var options = "<option value='' selected='selected'>--Select FeeType--</option>";
            $('#ddlFeeType').html(options);
        }
    });
}

function GetAllCompensationTypeList() {
    $.ajax({
        type: "GET",
        url: "../Common/GetAllCompensationTypeList",
        url: localStorage.getItem("BaseUrl") + "Common/GetAllCompensationTypeList",
        contentType: "application/json",
        dataType: "json",
        success: function (response) {

            var options = "<option value='' selected='selected'>--Select Compensation Type--</option>";
            if (response.Result.Result != undefined && response.Result.Result.length > 0) {
                $.each(response.Result.Result, function (key, value) {
                    options += '<option value="' + value.Id + '">' + value.Name + '</option>';
                });
            }
            $('#ddlFeeTypee').html(options);
        },
        error: function (result) {
            var options = "<option value='' selected='selected'>--Select FeeType--</option>";
            $('#ddlFeeTypee').html(options);
        }
    });
}

function GetAllParishList() {
    $.ajax({
        type: "GET",
        url: localStorage.getItem("BaseUrl") + "Common/GetAllParishList",
        contentType: "application/json",
        dataType: "json",
        async: false,
        success: function (response) {
            var options = "<option value='' selected='selected'>--Select Parish--</option>";
            if (response.Result.Result != undefined && response.Result.Result.length > 0) {
                $.each(response.Result.Result, function (key, value) {
                    options += '<option value="' + value.Id + '">' + value.Name + '</option>';
                });
            }
            $('#ddlParish').html(options);
        },
        error: function (result) {
            var options = "<option value='' selected='selected'>--Select Parish--</option>";
            $('#ddlParish').html(options);
        }
    });
}

function GetAllSelectionList() {
    $.ajax({
        type: "GET",
        url: localStorage.getItem("BaseUrl") + "Common/GetAllSelectionList",
        contentType: "application/json",
        dataType: "json",
        async: false,
        success: function (response) {
            var options = "<option value='' selected='selected'>--Select Selection--</option>";
            if (response.Result.Result != undefined && response.Result.Result.length > 0) {
                $.each(response.Result.Result, function (key, value) {
                    options += '<option value="' + value.Id + '">' + value.Name + '</option>';
                });
            }
            $('#ddlSelection').html(options);
        },
        error: function (result) {
            var options = "<option value='' selected='selected'>--Select Selection--</option>";
            $('#ddlSelection').html(options);
        }
    });
}

function GetAllProjectManagerList() {
    $.ajax({
        type: "GET",
        url: localStorage.getItem("BaseUrl") + "Common/GetAllProjectManagerList",
        contentType: "application/json",
        dataType: "json",
        async: false,
        success: function (response) {

            var options = "<option value='' selected='selected'>--Select ProjectManager--</option>";
            if (response.Result.Result != undefined && response.Result.Result.length > 0) {
                $.each(response.Result.Result, function (key, value) {
                    options += '<option value="' + value.Id + '">' + value.Name + '</option>';
                });
            }
            $('#ddlProjectManager').html(options);
        },
        error: function (result) {
            var options = "<option value='' selected='selected'>--Select ProjectManager--</option>";
            $('#ddlProjectManager').html(options);
        }
    });
}


function GetAllSupervisorList() {
    $.ajax({
        type: "GET",
        url: localStorage.getItem("BaseUrl") + "Common/GetAllSupervisorList",
        contentType: "application/json",
        dataType: "json",
        async: false,
        success: function (response) {
            var options = "<option value='' selected='selected'>--Select Supervisor--</option>";
            if (response.Result.Result != undefined && response.Result.Result.length > 0) {
                $.each(response.Result.Result, function (key, value) {
                    options += '<option value="' + value.Id + '">' + value.Name + '</option>';
                });
            }
            $('#ddlSupervisor').html(options);
        },
        error: function (result) {
            var options = "<option value='' selected='selected'>--Select Supervisor--</option>";
            $('#ddlSupervisor').html(options);
        }
    });
}

function GetCompensationTypeList() {
    $.ajax({
        type: "GET",
        url: localStorage.getItem("BaseUrl") + "Common/GetCompensationTypeList",
        contentType: "application/json",
        dataType: "json",
        async: false,
        success: function (response) {
            // var options = "<option value='' selected='selected'>--Select CompensationType--</option>";
            var options = "";
            if (response.Result != undefined && response.Result.length > 0) {
                $.each(response.Result, function (key, value) {
                    options += '<option value="' + value.Id + '">' + value.Name + '</option>';
                });
            }
            $('#ddlCompensationType').html(options);
        },
        error: function (result) {
            var options = "<option value='' selected='selected'>--Select CompensationType--</option>";
            $('#ddlCompensationType').html(options);
        }
    });
}

function formatNumber(n) {
    // format number 1000000 to 1,234,567
    return n.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ",")
}


function formatCurrency(input, blur, symbolName) {

    // appends $ to value, validates decimal side
    // and puts cursor back in right position.

    // get input value
    var input_val = input.val();

    // don't validate empty input
    if (input_val === "") { return; }

    // original length
    var original_len = input_val.length;

    // initial caret position 
    var caret_pos = input.prop("selectionStart");

    // check for decimal
    if (input_val.indexOf(".") >= 0) {

        // get position of first decimal
        // this prevents multiple decimals from
        // being entered
        var decimal_pos = input_val.indexOf(".");

        // split number by decimal point
        var left_side = input_val.substring(0, decimal_pos);
        var right_side = input_val.substring(decimal_pos);

        // add commas to left side of number
        left_side = formatNumber(left_side);

        // validate right side
        right_side = formatNumber(right_side);

        // On blur make sure 2 numbers after decimal
        if (blur === "blur") {
            right_side += "00";
        }

        // Limit decimal to only 2 digits
        right_side = right_side.substring(0, 2);

        // join number by .
        if (symbolName == "%") {
            input_val = left_side + "." + right_side + symbolName;
        }
        else {
            input_val = symbolName + left_side + "." + right_side;
        }

    } else {
        // no decimal entered
        // add commas to number
        // remove all non-digits
        input_val = formatNumber(input_val);
        if (symbolName == "%") {
            input_val = input_val + symbolName;
        }
        else {
            input_val = symbolName + input_val;
        }

        // final formatting
        if (blur === "blur") {
            input_val += ".00";
        }
    }

    // send updated string to input
    input.val(input_val);

    // put caret back in the right position
    var updated_len = input_val.length;
    caret_pos = updated_len - original_len + caret_pos;
    input[0].setSelectionRange(caret_pos, caret_pos);
}

function currenyFormetKeyup(id) {

    formatCurrency($(id), "", "$");
}
function currenyFormetblur(id) {

    formatCurrency($(id), "blur", "$");
}
function AddNewRow() {
    //var rows = (document.getElementById("tableSubTab").getElementsByTagName("tbody")[0].getElementsByTagName("tr").length) + 1;
    var rows = (document.getElementById("tableSubTab").getElementsByTagName("tbody")[0].getElementsByTagName("tr").length);
    var html = '';
    html += '<tr class="odd gradeX item" id="tr' + rows + '">';
    html += '<td> <div class="col-lg-12">';
    html += ' <div class="form-group">';
    html += '     <label>Sub #' + rows + '</label>';
    html += '       <select id="ddlSub' + rows + '" name="Sub' + rows + '" required class="form-control" onchange="SetTaxId(this); return false;">';
    html += '           ' + subOptionText + '';
    html += '       </select>';
    html += '       </div >';
    html += '    </div></td>';
    html += '<td><div class="col-lg-12">';
    html += '   <div class="form-group">';
    html += '      <label>TaxID #' + rows + '</label>';
    html += '     <input id="txtTaxId' + rows + '" name="TaxId' + rows + '" type="text" class="form-control">';
    html += '      </div>';
    html += '  </div></td>';
    //Add Contract Amount
    html += '<td><div class="col-lg-12">';
    html += '   <div class="form-group">';
    html += '      <label>Contract Amount #' + rows + '</label>';
    html += '<input id="txtCAmount' + rows + '" pattern="^\\$\\d{1,3}(,\\d{3})*(\\.\\d+)?$" value="" data-type="currency" onkeyup="currenyFormetKeyup(this);" onblur="currenyFormetblur(this);"  placeholder="$1,000,000.00" type="text" name="CAmount' + rows + '" class="form-control">';
    html += '      </div>';
    html += '  </div></td>';
    //End Contract Amount
    html += '<td style="width: 9%"><div class="col-lg-12"><input id="btnremove' + rows + '" type="buttom" class="btn btn-danger" value="Remove" style="width: 80px;" onclick="remove(tr' + rows + '); return false;"></div> </td>';
    html += '</tr>';
    $('#tbodySubTab').append(html);
}
function remove(id) {
    var dfdf = '#' + id.id;
    alertify.confirm('Alert!', 'Do you want to delete this?', function () { id.remove(); }
        , function () { });
}
function SetTaxId(ConsultantInfo) {
    var seletedTaxId = $("#" + ConsultantInfo.id).find('option:selected').attr("taxId");
    var id = ConsultantInfo.id.split('ddlSub')[1];
    $("#txtTaxId" + id).val(seletedTaxId);
}
function GetAllConsultantAreaSubTaxList() {

    $.ajax({
        type: "GET",
        url: localStorage.getItem("BaseUrl") + "Common/GetAllConsultantProviderList",
        contentType: "application/json",
        dataType: "json",
        async: false,
        success: function (response) {

            var options = "<option value='' selected='selected'>--Select Sub--</option>";
            if (response.data != undefined && response.data.length > 0) {
                $.each(response.data, function (key, value) {
                    options += '<option value="' + value.ConsultantProviderId + '" taxId="' + value.TaxId + '">' + value.ConsultantProviderName + '</option>';
                });
            }
            subOptionText = options;
            $('#ddlSub0').html(options);
            $('#ddlPrime').html(options);

        },
        error: function (result) {
            subOptionText = options;
            var options = "<option value='' selected='selected'>--Select Sub--</option>";
            $('#ddlSub0').html(options);
            $('#ddlPrime').html(options);
        }
    });
}


function GetAllContractAgencyList() {
    $.ajax({
        type: "GET",
        url: localStorage.getItem("BaseUrl") + "Common/GetAllContractAgencyList",
        contentType: "application/json",
        dataType: "json",
        success: function (response) {
            var options = "<option value='' disabled selected>--Contract Agency--</option>";
            if (response.Result.Result != undefined && response.Result.Result.length > 0) {
                $.each(response.Result.Result, function (key, value) {
                    options += '<option value="' + value.Id + '">' + value.Name + '</option>';
                });
            }
            $('#ddlContractAgency').html(options);
        },
        error: function (result) {
            var options = "<option value='' selected='selected'>--Select Parish--</option>";
            $('#ddlContractAgency').html(options);
        }
    });
}
