﻿var ProjectInfoId = localStorage.getItem("ProjectInfoId");
var selected = [];
var AdvId = localStorage.getItem("ProjectAdvId");
//var subOptionText;
$(document).ready(function () {
    GetAllConsultantProviderList();
    GetAllContractAgencyList();
    GetAllContractTypeList();
    GetAllFeeTypeList();
    GetAllCompensationTypeList();
    GetCompensationTypeList();
    GetAllParishList();
    GetAllSelectionList();
    GetAllProjectManagerList();
    GetAllSupervisorList();
    $('.mdb-select').materialSelect();
    $('#ProjectInfoForm').validate({});

    GetAllConsultantAreaSubTaxList();
    if (ProjectInfoId != null) {
        GetProjectGeneralInformation();

    }

    $('#ddlContractType').on('change', function (e) {
        BindType();
        SelectAllComposionType();
    });
    $('#ddlType').on('change', function (e) {
        var url = $('#ddlType').val();
        Redirect(url);
    });
    $('#ddlType1').on('change', function (e) {
        var url = $('#ddlType1').val();
        Redirect(url);
    });




    $("#ProjectInfoForm :input").change(function () {
        if ($("#myform").data("changed")) {
            // submit the form
        }
    });

    $('#btnAdd').on('click', function (e) {
        AddNewRow();
    });
    $('#btnSave').on('click', function (e) {
        e.preventDefault();
        if (!$("#ProjectInfoForm").valid()) {
            // alert('Please select all the mandatory details.');
            alert("Please fill all the required fields.");
            return false;
        }
        ProjectGeneralInformation()
    });
    $('#ddlConsultantProvider').on('change', function (e) {
        GetConsultantProviderDetail();
    });
    $('#btnGetProjectInfo').on('click', function (e) {
        GetProjectInfoFromLagov()
    });


    //$('#btnAdvertisement').on('click', function (e) {
    //    Redirect();
    //});
    $('#btnEditProjectInfo').on('click', function (e) {
        //$('#btnSaveAdvertisement').prop('disabled', false);
        //DisableControls(true);
        //$("#btnEditProjectInfo").css("visibility", "hidden");
        // $('#btnEditAdvertisement').prop('disabled', true);
    });
    $('#btnReadMode').on('click', function (e) {
        DisableControls(true);

    });
    $('#btnEditMode').on('click', function (e) {
        DisableControls(false);
        $('#btnSave').prop('disabled', false);
    });

    var now = new Date();
    var month = (now.getMonth() + 1);
    var day = now.getDate();
    if (month < 10)
        month = "0" + month;
    if (day < 10)
        day = "0" + day;
    var today = now.getFullYear() + '-' + month + '-' + day;
    $('#txtReceived').val(today);

    $('#ddlCompensationType').multiselect({
        //templates: { // Use the Awesome Bootstrap Checkbox structure
        //    li: '<li class="checkList"><a tabindex="0"><div class="aweCheckbox aweCheckbox-danger"><label for=""></label></div></a></li>'
        //},
        onChange: function (option, checked) {
            if (checked) {
                selected.push(option[0].value);
            }
            else {
                // selected.pop(option[0].value);
                selected = selected.filter(function (ele) {
                    return ele != option[0].value;
                });
            }
            // var brands = $('#ddlCompensationType option:selected');
            //selected = []
            //$(brands).each(function (index, brand) {
            //    selected.push([$(this).val()]);
            //});
        },
    });
    $('.multiselect-container div.aweCheckbox').each(function (index) {

        var id = 'multiselect-' + index,
            $input = $(this).find('input');

        // Associate the label and the input
        $(this).find('label').attr('for', id);
        $input.attr('id', id);

        // Remove the input from the label wrapper
        $input.detach();

        // Place the input back in before the label
        $input.prependTo($(this));

        $(this).click(function (e) {
            // Prevents the click from bubbling up and hiding the dropdown
            e.stopPropagation();
        });

    });

});



function AddEditButton() {

    //var para = document.createElement("button");
    //var node = document.createTextNode("editt");
    //para.appendChild(node);
    //para.id = "lok";
    ////para.setAttribute("id", "kk");
    //var element = document.getElementById("new");
    //element.appendChild(para);
    ////element.setAttribute("id","nedit")

    var x = document.createElement("INPUT");
    x.setAttribute("type", "button");
    x.id = "kol"
    var element = document.getElementById("new");
    element.appendChild(x);



}


//function AddNewRow() {
//    var rows = (document.getElementById("tableSubTab").getElementsByTagName("tbody")[0].getElementsByTagName("tr").length) + 1;
//    var html = '';
//    html += '<tr class="odd gradeX item" id="tr' + rows + '">';
//    html += '<td> <div class="col-lg-12">';
//    html += ' <div class="form-group">';
//    html += '     <label>Sub #' + rows + '</label>';
//    html += '       <select id="ddlSub' + rows + '" name="Sub' + rows + '" required class="form-control" onchange="SetTaxId(this); return false;">';
//    html += '           ' + subOptionText + '';
//    html += '       </select>';
//    html += '       </div >';
//    html += '    </div></td>';
//    html += '<td><div class="col-lg-12">';
//    html += '   <div class="form-group">';
//    html += '      <label>TaxID #' + rows + '</label>';
//    html += '     <input id="txtTaxId' + rows + '" name="TaxId' + rows + '" type="text" class="form-control">';
//    html += '      </div>';
//    html += '  </div></td>';
//    html += '<td style="width: 9%"><div class="col-lg-12"><input id="btnremove' + rows + '" type="buttom" class="btn btn-danger" value="Remove" style="width: 80px;" onclick="remove(tr' + rows + '); return false;"></div> </td>';
//    html += '</tr>';
//    $('#tbodySubTab').append(html);
//}

//function remove(id) {

//    var dfdf = '#' + id.id;
//    alertify.confirm('Alert!', 'Do you want to delete this?', function () { id.remove(); }
//        , function () { });

//    //if (confirm('Do you want to delete this?')) {
//    //    id.remove();       
//    //}

//}



function ProjectGeneralInformation() {

    var informationDto = {};
    informationDto.ProjectGeneralInformationId = ProjectInfoId;
    informationDto.StateProjectNo = $("#txtStateProjectNo").val();
    informationDto.ContractNo = $("#txtContractNo").val();
    informationDto.FAPNo = $("#txtFAPNo").val();
    informationDto.ProjectName = $("#txtProjectName").val();
    informationDto.Route = $("#txtRoute").val();
    informationDto.Parish = $("#ddlParish").val();
    informationDto.GeneralDescription = $("#txtGeneralDescription").val();
    informationDto.Selection = $("#ddlSelection").val();
    informationDto.FHWAFullOversight = $("#rdoFHWAFullOversight1").prop('checked');
    informationDto.ContractType = $("#ddlContractType").val();
    informationDto.FeeType = $("#ddlFeeType").val();
    informationDto.EStContractAmount = $("#txtEstContractAmount").val().replace("$", "");
    informationDto.ContractAgency = $("#ddlContractAgency").val();
    informationDto.Comment = $("#txtComment").val();
    informationDto.DBEGoel = $("#txtDBEGoel").val().replace("%", "");
    //informationDto.CompensationType = selected;
    var type = "";
    selected.forEach(function (item) {
        type = type + "," + item;
    });
    informationDto.CompensationType = type;
    //Save Consultant Area
    informationDto.AreaDto = {};
    informationDto.SubTaxDetailsDto = [];
   // if ($("#ddlProjectManager").val() != "") {
        var area = {};
        area.ProjectManagerId = $("#ddlProjectManager").val();
        area.ManagerPhone = $("#txtManagerPhone").val();
        area.ConsultantProviderId = $("#ddlConsultantProvider").val();
        area.ContactPerson = $("#txtContactPerson").val();
        area.Title = $("#txtTitle").val();
        area.JobTitle = $("#txtJobTitle").val();
        area.OfficeAddress = $("#txtOfficeAddress").val();
        area.Fax = $("#txtFax").val();
        area.TaxId = $("#txtTaxId").val();
        area.Email = $("#txtTaxId").val();
        area.Phone = $("#txtPhone").val();
        informationDto.AreaDto = area;
        $("#tbodySubTab tr.item").each(function () {
            var TaxDetailsDto = {};
            $(this).find('td :input').each(function () {
                if ($(this).attr('id').indexOf('ddlSub') > -1) {
                    TaxDetailsDto.SubId = $(this).val();
                }
                else if ($(this).attr('id').indexOf('txtTaxId') > -1) {
                    TaxDetailsDto.TaxId = $(this).val();
                }
            });
            if (TaxDetailsDto.SubId != 0 && TaxDetailsDto.SubId != "" && TaxDetailsDto.SubId != undefined) {
                informationDto.SubTaxDetailsDto.push(TaxDetailsDto);
            }
        });
   // }
    informationDto.TrackingDto = {};
    if ($("#txtReceived").val() != "") {
        var tracking = {};
        tracking.Received = $("#txtReceived").val();
        tracking.AwardNotification = $("#txtAwardNotification").val();
        tracking.Closed = $("#txtClosed").val();
        tracking.FinalInvoicedRecvd = $("#txtFinalInvoicedRecvd").val();
        tracking.FinalToAcct = $("#txtFinaltoAcct").val();
        tracking.AuditReport = $("#txtAuditReport").val();
        tracking.FinalCost = $("#txtFinalCost").val();
        informationDto.TrackingDto = tracking;
    }
    var url;
    if (ProjectInfoId != null) {
        url = localStorage.getItem("BaseUrl") + "Project/UpdateProjectGeneralInformation";
    }
    else {
        url = localStorage.getItem("BaseUrl") + "Project/CreateProjectGeneralInformation";
    }
    $.ajax({
        type: "POST",
        url: url,
        contentType: "application/json",
        dataType: "json",
        data: JSON.stringify(informationDto),
        success: function (data) {
            if (data.Result.Status == 1 || data.Result.Status == 3) {
                $('#divLogInformationPanel').css('display', 'block');
                alertify.alert('Success!', 'Record has been saved successfully.', function () {
                    window.location.href = localStorage.getItem("BaseUrl") + "/DashBoard/Home";
                    //BindType();
                    //localStorage.setItem("ProjectInfoId", data.Result.Result);
                    //DisableControls(true);
                    //$("#btnEditProjectInfo").css("visibility", "visible");
                });
            }
        },
        error: function (err) { }
    });
    return false;
}
function GetConsultantProviderDetail() {
    $.ajax({
        type: "GET",
        url: localStorage.getItem("BaseUrl") + "Common/GetConsultantProviderDetail?ConsultantProviderId=" + $("#ddlConsultantProvider").val(),
        contentType: "application/json",
        dataType: "json",
        success: function (response) {

            if (response.Result.Status == 1) {
                if (response.Result.Result != null) {
                    var resp = response.Result.Result;
                    $("#txtContactPerson").val(resp.ContactName);
                    $("#txtTitle").val(resp.Title);
                    $("#txtJobTitle").val(resp.JobTitle);
                    $("#txtFax").val(resp.Fax);
                    $("#txtTaxId").val(resp.TaxId);
                    $("#txtEmail").val(resp.Email);
                    $("#txtPhone").val(resp.Phone);
                    var address = resp.OfficeAddress + "," + resp.City + "," + resp.State + "-" + resp.Zip;
                    $("#txtOfficeAddress").val(address);


                }
            }
        }
    });
}
function GetProjectGeneralInformation() {
    $.ajax({
        type: "GET",
        url: localStorage.getItem("BaseUrl") + "Project/GetProjectGeneralInformation?ProjectInfoId=" + ProjectInfoId,
        contentType: "application/json",
        dataType: "json",
        success: function (response) {
            if (response.data != null) {
                $('#divLogInformationPanel').css('display', 'block');
                $("#txtStateProjectNo").val(response.data.StateProjectNo);
                $("#txtContractNo").val(response.data.ContractNo);
                $("#txtFAPNo").val(response.data.FAPNo);
                $("#txtProjectName").val(response.data.ProjectName);
                $("#txtRoute").val(response.data.Route);
                $("#ddlParish").val(response.data.Parish);
                $("#txtGeneralDescription").val(response.data.GeneralDescription);
                $("#ddlSelection").val(response.data.Selection);
                response.data.FHWAFullOversight == true ? $('#rdoFHWAFullOversight1').prop('checked', true) : $('#rdoFHWAFullOversight1').prop('checked', false);
                $("#ddlContractType").val(response.data.ContractType);
                $("#ddlFeeType").val(response.data.FeeType);
                $("#txtEstContractAmount").val("$" + response.data.EStContractAmount);
                $("#ddlContractAgency").val(response.data.ContractAgency);
                $("#txtComment").val(response.data.Comment);
                $("#txtDBEGoel").val(response.data.DBEGoel + "%");
                if (response.data.CompensationType != null && response.data.CompensationType.length != 0) {
                    $.each(response.data.CompensationType.split(","), function (i, e) {
                        if (e != "") {
                            $("#ddlCompensationType option[value='" + e + "']").attr("selected", 1);
                            $("#ddlCompensationType").multiselect("refresh");
                            selected.push(e);
                        }
                    });
                }
                if (response.data.AreaDto != null) {

                    $("#ddlProjectManager").val(response.data.AreaDto.ProjectManagerId);
                    $("#txtManagerPhone").val(response.data.AreaDto.ManagerPhone);
                    $("#ddlConsultantProvider").val(response.data.AreaDto.ConsultantProviderId);
                    $("#txtContactPerson").val(response.data.AreaDto.ContactPerson);
                    $("#txtTitle").val(response.data.AreaDto.Title);
                    $("#txtJobTitle").val(response.data.AreaDto.JobTitle);
                    $("#txtFax").val(response.data.AreaDto.Fax);
                    $("#txtTaxId").val(response.data.AreaDto.TaxId);
                    $("#txtEmail").val(response.data.AreaDto.Email);
                    $("#txtPhone").val(response.data.AreaDto.Phone);
                    $("#txtOfficeAddress").val(response.data.AreaDto.OfficeAddress);
                    if (response.data.SubTaxDetailsDto != null) {
                        $("#ddlSub0").val(response.data.SubTaxDetailsDto[0].SubId);
                        $("#txtTaxId0").val(response.data.SubTaxDetailsDto[0].TaxId);
                        $("#txtCAmount0").val("$" + response.data.SubTaxDetailsDto[0].ContractAmount);
                        if (response.data.SubTaxDetailsDto.length > 1) {
                            for (i = 0; i < response.data.SubTaxDetailsDto.length; i++) {
                                AddNewRow();
                                $("#ddlSub" + (i + 1) + "").val(response.data.SubTaxDetailsDto[i].SubId);
                                $("#txtTaxId" + (i + 1) + "").val(response.data.SubTaxDetailsDto[i].TaxId);
                                $("#txtCAmount" + (i + 1) + "").val(response.data.SubTaxDetailsDto[i].ContractAmount);
                            }
                        }
                    }
                }
                if (response.data.TrackingDto != null) {
                    $("#txtReceived").val(response.data.TrackingDto.Received);
                    $("#txtAwardNotification").val(response.data.TrackingDto.AwardNotification);
                    $("#txtClosed").val(response.data.TrackingDto.Closed);
                    $("#txtFinalInvoicedRecvd").val(response.data.TrackingDto.FinalInvoicedRecvd);
                    $("#txtFinaltoAcct").val(response.data.TrackingDto.FinalToAcct);
                    $("#txtAuditReport").val(response.data.TrackingDto.AuditReport);
                    $("#txtFinalCost").val(response.data.TrackingDto.FinalCost);
                }
                BindType();
                DisableControls(true);
            }
        },
        error: function (result) {

        }
    });
}

function DisableData() {
    //$("#txtStateProjectNo").prop("disabled", true);
    $("#txtLegacySPNo").prop("disabled", true);
    $("#txtFAPNo").prop("disabled", true);
    $("#txtProjectName").prop("disabled", true);
    $("#txtRoute").prop("disabled", true);
    $("#ddlParish").prop("disabled", true);
    $("#txtGeneralDescription").prop("disabled", true);
    $("#txtEstimatedConstructionCost").prop("disabled", true);
    $("#ddlSelection").prop("disabled", true);
    response.data.FHWAFullOversight == true ? $('#rdoFHWAFullOversight1').prop('checked', true) : $('#rdoFHWAFullOversight1').prop('checked', false).prop("disabled", true);


}
//function GetAllConsultantAreaSubTaxList() {
//    $.ajax({
//        type: "GET",
//        url: "../Common/GetAllConsultantProviderList",
//        contentType: "application/json",
//        dataType: "json",
//        async: false,
//        success: function (response) {
//            var options = "<option value='' selected='selected'>--Select Sub--</option>";
//            if (response.data != undefined && response.data.length > 0) {
//                $.each(response.data, function (key, value) {
//                    options += '<option value="' + value.ConsultantProviderId + '" taxId="' + value.TaxId + '">' + value.ConsultantProviderName + '</option>';
//                });
//            }
//            subOptionText = options;
//            $('#ddlSub1').html(options);
//        },
//        error: function (result) {
//            subOptionText = options;
//            var options = "<option value='' selected='selected'>--Select Sub--</option>";
//            $('#ddlSub1').html(options);
//        }
//    });
//}
function GetProjectInfoFromLagov() {
    if ($("#txtContractNo").val() != "") {
        $.ajax({
            type: "GET",
            url: localStorage.getItem("BaseUrl") + "Project/GetProjectInfoFromLaGov?ContractNo=" + $("#txtContractNo").val(),
            contentType: "application/json",
            dataType: "json",
            success: function (response) {

                if (response != null && response.data != null /*&& response.Result.Status === 1*/) {
                    $("#txtStateProjectNo").val(response.data.StateProjectNumber).prop("disabled", true);
                    $("#txtProjectName").val(response.data.ProjectName).prop("disabled", true);
                    $("#txtRoute").val(response.data.RouteNumber).prop("disabled", true);
                    $("#txtFAPNo").val(response.data.FederalAidProjectNumber).prop("disabled", true);
                    $("#txtClosed").val(response.data.FinalAcceptanceDate).prop("disabled", true);

                    //$("#ddlParish option").each(function () {
                    //    if ($(this).text() == response.data.ParishName){
                    //        $(this).attr('selected', 'selected');
                    //    }
                    //    });

                    $("#ddlParish option:contains(" + response.data.ParishName + ") ").attr('selected', 'selected');
                    $("#ddlContractType option: contains(" + response.data.ContractName + ") ").attr('selected', 'selected');
                    $("#ddlProjectManager option:contains(" + response.data.ProjectManager + ") ").attr('selected', 'selected');

                    $("#ddlConsultantProvider option:contains(" + response.data.ContractorConsultant + ") ").attr('selected', 'selected');
                    //$("#ddlConsultantProvider option").each(function () {
                    //    if ($(this).text() == response.data.ContractorConsultant) {
                    //        $(this).attr('selected', 'selected');
                    //    }
                    //});
                    GetConsultantProviderDetail();
                }
                else {
                    alertify.alert('Alert!', 'Record not found.', function () {
                    });
                }
            },
            error: function (result) {

            }
        });
    }
    else {
        alertify.alert('Alert!', 'Please enter state project first..', function () {
        });
    }
    return false;
}

function Redirect(url) {
    window.location.href = url;
    return false;
}
function DisableControls(value) {
    $("#txtStateProjectNo").prop('disabled', value);
    $('#divLogInformationPanel').prop('disabled', value);
    $("#txtStateProjectNo").prop('disabled', value);
    $("#txtContractNo").prop('disabled', value);
    $("#txtFAPNo").prop('disabled', value);
    $("#txtProjectName").prop('disabled', value);
    $("#txtRoute").prop('disabled', value);
    $("#ddlParish").prop('disabled', value);
    $("#txtGeneralDescription").prop('disabled', value);
    //$("#txtEstimatedConstructionCost").prop('disabled', value);
    $("#ddlSelection").prop('disabled', value);

    $("#ddlContractType").prop('disabled', value);
    $("#ddlFeeType").prop('disabled', value);
    $("#ddlFeeTypee").prop('disabled', value);
    $("#txtEstContractAmount").prop('disabled', value);
    $("#txtContractAgency").prop('disabled', value);
    $("#txtComment").prop('disabled', value);
    $("#ddlProjectManager").prop('disabled', value);
    $("#txtManagerPhone").prop('disabled', value);
    $("#ddlConsultantProvider").prop('disabled', value);
    $("#txtContactPerson").prop('disabled', value);
    $("#txtTitle").prop('disabled', value);
    $("#txtJobTitle").prop('disabled', value);
    $("#txtFax").prop('disabled', value);
    $("#txtTaxId").prop('disabled', value);
    $("#txtEmail").prop('disabled', value);
    $("#txtPhone").prop('disabled', value);
    $("#txtOfficeAddress").prop('disabled', value);

    //$("#ddlSub1").prop('disabled', value);
    //$("#txtTaxId1").prop('disabled', value);

    $("#tbodySubTab tr.item").each(function () {
        $(this).find('td :input').each(function () {
            if ($(this).attr('id').indexOf('ddlSub') > -1) {
                $(this).prop('disabled', value);
            }
            else if ($(this).attr('id').indexOf('txtTaxId') > -1) {
                $(this).prop('disabled', value);
            }
        });
    });

    $("#txtReceived").prop('disabled', value);
    $("#txtAwardNotification").prop('disabled', value);
    $("#txtClosed").prop('disabled', value);
    $("#txtFinalInvoicedRecvd").prop('disabled', value);
    $("#txtFinaltoAcct").prop('disabled', value);
    $("#txtAuditReport").prop('disabled', value);
    $("#txtFinalCost").prop('disabled', value);
}
function SetTaxId(ConsultantInfo) {
    var seletedTaxId = $("#" + ConsultantInfo.id).find('option:selected').attr("taxId");
    var id = ConsultantInfo.id.split('ddlSub')[1];
    $("#txtTaxId" + id).val(seletedTaxId);
}
function BindType() {
    $('#ddlType').html("");
    $('#ddlType1').html("");
    var options = "<option value='' selected='selected'>--Select Type--</option>";
    var options1 = "<option value='' selected='selected'>--Select Type--</option>";
    var seletedText = $("#ddlContractType").find('option:selected')[0].text;
    if (seletedText == "IDIQ") {
        options += '<option value="' + localStorage.getItem("BaseUrl") + 'Advertisement/Advertisement">Advertisement</option>';
        options1 += '<option value="' + localStorage.getItem("BaseUrl") + 'Advertisement/Contract">Contract</option>';
        options1 += '<option value="' + localStorage.getItem("BaseUrl") + 'Advertisement/TaskOrder">Task Order</option>';
        options1 += '<option value="' + localStorage.getItem("BaseUrl") + 'Advertisement/SupplementalAgreementForIDIQ">Supplemental Agreement</option>';
        options += '<option value="' + localStorage.getItem("BaseUrl") + 'Advertisement/ProjectAdvertisementShortlist">Short List</option>';
        options += '<option value="' + localStorage.getItem("BaseUrl") + 'Advertisement/ProjectAdvertisementApparentSelection">Apparent Selection</option>';
        options += '<option value="' + localStorage.getItem("BaseUrl") + 'Advertisement/ProjectAdvertisementSelection">Selection</option>';

    }
    else if (seletedText == "GENERAL") {
        options += '<option value="' + localStorage.getItem("BaseUrl") + 'Advertisement/Advertisement">Advertisement</option>';
        options1 += '<option value="' + localStorage.getItem("BaseUrl") + 'Advertisement/OriginalContract">Original Contract</option>';
        options1 += '<option value="' + localStorage.getItem("BaseUrl") + 'Advertisement/SupplementalAgreement">Supplemental Agreement</option>';
        options1 += '<option value="' + localStorage.getItem("BaseUrl") + 'Advertisement/ExtraWorkLetter">Extra Work Letter</option>';
        options += '<option value="' + localStorage.getItem("BaseUrl") + 'Advertisement/ProjectAdvertisementShortlist">Short List</option>';
        options += '<option value="' + localStorage.getItem("BaseUrl") + 'Advertisement/ProjectAdvertisementApparentSelection">Apparent Selection</option>';
        options += '<option value="' + localStorage.getItem("BaseUrl") + 'Advertisement/ProjectAdvertisementSelection">Selection</option>';
    }
    $('#ddlType').html(options);
    $('#ddlType1').html(options1);
}

function SelectAllComposionType() {
    var seletedText = $("#ddlContractType").find('option:selected')[0].text;
    if (seletedText == "IDIQ") {
        $.each($("#ddlCompensationType").find('option'), function (i, e) {

            if (e != "" && e.innerText == "All Compensation Type") {
                $("#ddlCompensationType option[value='" + e.value + "']").attr("selected", 1);
                $("#ddlCompensationType").multiselect("refresh");
                if (selected.indexOf(e.value) == -1) {
                    selected.push(e.value);
                }
            }
        });
        $("#ddlFeeType option:contains('Negotiated/Non-Negotiated')").attr('selected', 'selected');
    }
    return false;
}