﻿/* Formatting function for row details - modify as you need */
function format(d) {
    var html = '';
    var html1 = '';
    var html2 = '';

    // `d` is the original data object for the row
    var url = localStorage.getItem("BaseUrl") + "Project/GetProjectContractTaskOrderMoneyBalanceList?ProjectGeneralInformationId=" + d.ProjectGeneralInformationId;
    $.ajax({
        type: 'GET',
        url: url,
        async: false,
        success: function (obj) {
            if (obj.data != null) {
                var bal;
                $.each(obj.data, function (i, r) {
                    if (i == 0) {
                        bal = (parseFloat(r.ContractAmount) - parseFloat(r.TaskOrderAmount));
                        html += '<tr>';
                        html += '<td class="w10">' + r.ContractDate + '</td>';
                        html += '<td class="w10">' + r.ContractAction + '</td>';
                        html += '<td class="w10"></td>';
                        html += '<td class="w10" style="color: green">$' + r.ContractAmount + '</td>';
                        html += '<td class="w10"></td>';
                        html += '<td class="w10">$' + r.ContractAmount + '</td>';
                        html += '<td class="w10">$' + r.ContractAmount + '</td>';
                        html += '<td class="w10"></td>';
                        html += '<td class="w10"></td>';
                        html += '</tr>';
                        html2 += '<tr>';
                        html2 += '<td class="w10">' + r.TODate + '</td>';
                        html2 += '<td class="w10">' + r.TaskForProject + '</td>';
                        html2 += '<td class="w10" style="color: red">$' + r.TaskOrderAmount + '</td>';
                        html2 += '<td class="w10"></td>';
                        html2 += '<td class="w10">$' + r.TaskOrderAmount + '</td>';
                        html2 += '<td class="w10">$' + bal + '</td>';
                        html2 += '<td class="w10"></td>';
                        html2 += '<td class="w10"></td>';
                        html2 += '<td class="w10"><a href="" class="editor_edit" onclick="Redirect(' + d.ProjectGeneralInformationId + ',\'SupplementalAgreementForIDIQ\'); return false;">Edit</a></td>';
                        html2 += '</tr>';

                    }
                    else {
                        bal = bal + parseFloat(r.ContractAmount);
                        html1 += '<tr>';
                        html1 += '<td>' + r.TODate + '</td>';
                        html1 += '<td>' + r.TaskForProject + '</td>';
                        html1 += '<td >' + r.TaskOrderAmount + '</td>';
                        html1 += '<td style="color: green">$' + r.ContractAmount + '</td>';
                        html1 += '<td></td>';
                        html1 += '<td>$' + bal + '</td>';
                        html1 += '<td></td>';
                        html1 += '<td></td>';
                        html1 += '<td></td>';
                        html1 += '</tr>';
                    }

                });
            }

        }
    })
    return '<table cellpadding="5" cellspacing="0" class="dataTableSubTableMoneyView" border="0" ">'
        +
        html + html2 + html1
    '</table>';
}



function OpenUrl(type) {
    var url;
    switch (type) {
        case "Project":
            localStorage.removeItem("ProjectInfoId");
            url = localStorage.getItem("BaseUrl") + "Project/ProjectInfo";
    }
    window.location.href = url;
}
function Redirect(id, type) {
    var url;
    if (id == null) {
        localStorage.removeItem("ProjectInfoId");

    }
    else {
        localStorage.setItem("ProjectInfoId", id);
    }
    switch (type) {
        case "SupplementalAgreementForIDIQ":
            url =   localStorage.getItem("BaseUrl") + "Advertisement/SupplementalAgreementForIDIQ"
    }
    window.location.href = url;
}



$(document).ready(function () {
    var table = $('#tblPjrojectInfo').DataTable({
        "ajax": localStorage.getItem("BaseUrl") + "Project/GetAllProjectMoneyBalanceList",
        "columns": [
            {
                "className": 'details-control',
                "orderable": false,
                "data": null,
                "defaultContent": ''
            },
            { "data": "ProjectGeneralInformationId" },
            { "data": "StateProjectNo" },
            {
                "data": "ProjectGeneralInformationId",
                "render": function () {
                    return "Original Contract";
                }
            },
            {
                "data": "ProjectGeneralInformationId",
                "render": function () {
                    return "";
                }
            },
            {
                "data": "ProjectGeneralInformationId",
                "render": function () {
                    return "";
                }
            },
            {
                "data": "TaskOrderAmount",
                "render": function (data, type, row, meta) {
                    return "$" + data
                }
            },
            {
                "data": "ProjectGeneralInformationId",
                "render": function (data, type, row, meta) {
                    var x = parseFloat(row.ContractAmount) - parseFloat(row.TaskOrderAmount);
                    return "$" + x
                }
            },
            {
                "data": "ContractAmount",
                "render": function (data, type, row, meta) {
                    return "$" + data
                },
            },
            {
                "data": null,
                defaultContent: ''
            },
            {
                "data": null,
                defaultContent: ''
            },

        ],
        "columnDefs": [
            {
                "targets": [1],
                "visible": false,
                "searchable": false
            }
        ],
        "order": [[1, 'asc']]
    });

    // Add event listener for opening and closing details
    //$('#tblPjrojectInfo tbody').on('click', 'td.editor_edit', function () {
    //    var tr = $(this).closest('tr');
    //    var row = table.row(tr);
    //    var data = row.data();
    //    localStorage.setItem("ProjectInfoId", data.ProjectGeneralInformationId);
    //    window.location.href = "Project/ProjectInfo";
    //    return false;
    //});

    $('#tblPjrojectInfo tbody').on('click', 'td.details-control', function () {
        var tr = $(this).closest('tr');
        var row = table.row(tr);
        if (row.child.isShown()) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        }
        else {
            // Open this row
            row.child(format(row.data())).show();
            tr.addClass('shown');
        }
    });
});




