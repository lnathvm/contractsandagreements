﻿var ProjectInfoId = localStorage.getItem("ProjectInfoId");
var ProjectAdvSelectionId = localStorage.getItem("ProjectAdvSelectionId");
var subOptionText;

$(document).ready(function () {
    GetShortlistsList();
    $('#SelectionForm').validate({});
    if (ProjectInfoId != null) {
        GetProjectSelectionInformation(ProjectInfoId);
        GetAdvertisementDetail(ProjectInfoId);
    }
    $('#btnSaveSelection').on('click', function (e) {
        e.preventDefault();
        if (!$("#SelectionForm").valid()) {
            alert("Please fill all the required fields.");
            return false;
        }
        ProjectAdvertisementSelection(false);
    });


    $('#btnPTWSelection').on('click', function (e) {
        e.preventDefault();
        if (!$("#SelectionForm").valid()) {
            alert("Please fill all the required fields.");
            return false;
        }
        ProjectAdvertisementSelection(true);
    });
    $('#btnUPTWSelection').on('click', function (e) {
        e.preventDefault();
        if (!$("#SelectionForm").valid()) {
            alert("Please fill all the required fields.");
            return false;
        }
        ProjectAdvertisementSelection(false);
    });



    $('#btnCancel').on('click', function (e) {
        window.location.href = localStorage.getItem("BaseUrl") + "Dashboard/AdvertisementDashboard";
        return false;
    });
    $('#btnAdvertisementDocument').on('click', function (e) {
        window.location.href = localStorage.getItem("BaseUrl") + "Advertisement/AdvertisementDocument";
        return false;
    });

    $('#btnAddendum').on('click', function (e) {
        window.location.href = localStorage.getItem("BaseUrl") + "Advertisement/Addendum";
        return false;
    });
    $('#btnAdd').on('click', function (e) {
        AddNewRow();
    });

    $('#btnEditSelection').on('click', function (e) {
        $('#btnSaveSelection').prop('disabled', false);
        EnableDisable(false);
        $("#btnEditSelection").css("visibility", "hidden");
        // $('#btnEditAdvertisement').prop('disabled', true);
    });


});

//function GetAdvertisementDetail(ProjectGeneralInformationId) {
//    $.ajax({
//        type: "GET",
//        url: localStorage.getItem("BaseUrl") + "Advertisement/GetAdvertisementDetail?ProjectGeneralInformationId=" + ProjectGeneralInformationId,
//        contentType: "application/json",
//        dataType: "json",
//        success: function (response) {

//            if (response.data != null) {
//                var result = response.data;
//                $("#txtAdvertisementDate").val(result.AdvertisementDate);
//                $("#txtClosingDate").val(result.ClosingDate);
//                $("#txtContractNo").val(result.ProjectGeneralInformationId);
//                $("#txtStateProjectno").val(result.StateProjectno);
//            }
//        },
//        error: function (result) {

//        }
//    });
//}


function GetShortlistsList() {

    $.ajax({
        type: "GET",
        url: localStorage.getItem("BaseUrl") + "Advertisement/GetShortListConsultantProviderList?ProjectGeneralInformationId=" + ProjectInfoId,
        contentType: "application/json",
        dataType: "json",
        success: function (response) {

            var options = "<option value='' selected='selected'>--Select --</option>";
            if (response.Result.Result != undefined && response.Result.Result.length > 0) {
                $.each(response.Result.Result, function (key, value) {
                    options += '<option value="' + value.Id + '" rank="' + value.Rank + '">' + value.Name + '</option>';
                });
            }
            $('#ddlSelection1').html(options);
        },
        error: function (result) {
            var options = "<option value='' selected='selected'>--Select --</option>";
            $('#ddlSelection1').html(options);
        }
    });
}

function GetProjectSelectionInformation(ProjectGeneralInformationId) {
    $.ajax({
        type: "GET",
        url: localStorage.getItem("BaseUrl") + "Advertisement/GetAdvertisementSelection?ProjectGeneralInformationId=" + ProjectGeneralInformationId,
        contentType: "application/json",
        dataType: "json",
        success: function (response) {
            if (response.data != null) {
                var result = response.data;
                if (result.IsPostToWeb) {
                    $('#btnUPTWSelection').show();
                }
                else {
                    $('#btnPTWSelection').show();
                }

                $("#txtAmmendedClosingDate").val(result.AmmendedClosingDate);
                $("#txtSelectionDate").val(result.SelectionDate);
                $("#txtSubject").val(result.Subject);
                $("#ddlSelection1").val(result.Selection);
                if (result.Comment1 != null) {
                    $("#txtComment1").val(result.Comment1);
                }
                if (result.Comment2 != null) {
                    $("#txtComment2").val(result.Comment2);
                }
            }


        },
        error: function (result) {

        }
    });
}
function ProjectAdvertisementSelection(postToWeb) {

    var ProjectAdvSelection = {};
    ProjectAdvSelection.ProjectGeneralInformationId = ProjectInfoId;
    // ProjectAdvSelection.Title39 = $("#txtTitle39").val();
    ProjectAdvSelection.AdvertisementDate = $("#txtAdvertisementDate").val();
    ProjectAdvSelection.ClosingDate = $("#txtClosingDate").val();
    ProjectAdvSelection.AmmendedClosingDate = $("#txtAmmendedClosingDate").val();
    ProjectAdvSelection.SelectionDate = $("#txtSelectionDate").val();
    ProjectAdvSelection.ContractNo = $("#txtContractNo").val();
    ProjectAdvSelection.StateProjectno = $("#txtStateProjectno").val();
    ProjectAdvSelection.Subject = $("#txtSubject").val();
    ProjectAdvSelection.Selection = $("#ddlSelection1").val();
    ProjectAdvSelection.Comment1 = $("#txtComment1").val();
    ProjectAdvSelection.Comment2 = $("#txtComment2").val();
    ProjectAdvSelection.IsPostToWeb = postToWeb;
    ProjectAdvSelection.Rank = $("#ddlSelection1 option:selected").attr("rank");
    var url = localStorage.getItem("BaseUrl") + "Advertisement/CreateProjectAdvertisementSelectionDetails";

    $.ajax({
        type: "POST",
        url: url,
        contentType: "application/json",
        dataType: "json",
        data: JSON.stringify(ProjectAdvSelection),
        success: function (data) {
            if (data.Result.Status == 1 || data.Result.Status == 3) {
                $('#divLogInformationPanel').css('display', 'block');
                alertify.alert('Success!', 'Record has been saved successfully.', function () {
                    window.location.href = localStorage.getItem("BaseUrl") + "Dashboard/AdvertisementDashboard";
                    // $('#btnSaveAdvertisement').prop('disabled', true);
                    // EnableDisable(true);
                    //// $('#btnEditAdvertisement').prop('disabled', false);
                    // $("#btnEditAdvertisement").css("visibility", "visible");


                });
            }
        },
        error: function (err) { }
    });
    return false;
}
function GetAdvertisementDetail(ProjectGeneralInformationId) {
    $.ajax({
        type: "GET",
        url: localStorage.getItem("BaseUrl") + "Advertisement/GetProjectAdvertisementDetails?ProjectInfoId=" + ProjectGeneralInformationId,
        contentType: "application/json",
        dataType: "json",
        success: function (response) {
            var result = response.Result.Result;
            if (result != null) {
                $("#txtAdvertisementDate").val(result.ProjectWork.AdvertisementDate);
                $("#txtClosingDate").val(result.ProjectWork.ClosingDate);
                $("#txtContractNo").val(result.Information.ContractNo);
                $("#txtStateProjectno").val(result.Information.StateProjectNo);
            }
        },
        error: function (result) {

        }
    });
}