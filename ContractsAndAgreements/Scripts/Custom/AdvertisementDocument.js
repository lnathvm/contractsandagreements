﻿var ProjectAdvId = localStorage.getItem("ProjectAdvId");
var ProjectInfoId = localStorage.getItem("ProjectInfoId");
$(document).ready(function () {
    $('#AdvertisementDocumentForm').validate({});
    GetAllParishList();
    if (ProjectInfoId != null) {
        //GetProjectGeneralInformation();
        GetProjectAdvertisementDocument();
    }
    $('#btnSaveAdvertisement').on('click', function (e) {

        e.preventDefault();
        if (!$("#AdvertisementDocumentForm").valid()) {
            alert("Please fill all the required fields.");
            return false;
        }
        ProjectAdvertisementDocument(false);
    });
    $('#btnPublish').on('click', function (e) {

        e.preventDefault();
        if (!$("#AdvertisementDocumentForm").valid()) {
            alert("Please fill all the required fields.");
            return false;
        }
        ProjectAdvertisementDocument(true);
    });
    $('#btnUnPublish').on('click', function (e) {

        e.preventDefault();
        if (!$("#AdvertisementDocumentForm").valid()) {
            alert("Please fill all the required fields.");
            return false;
        }
        ProjectAdvertisementDocument(false);
    });

});


function ProjectAdvertisementDocument(IsPostToWeb) {

    var advDocument = {};
    advDocument.ProjectAdvertisementDetailsId = ProjectAdvId;
    advDocument.AdvertisementDate = $("#txtAdvertisementDate").val();
    advDocument.ClosingDate = $("#txtClosingDate").val();
    advDocument.AdvertDateGoesHere = $("#txtAdvertDateGoes").val();
    advDocument.TypeOfAdvertGoesHere = $("#ddlTypeOfAdvert").val();
    advDocument.Details = $("#txtDetails").val();
    advDocument.Document = $("#txtDocument").val();
    advDocument.DBEGoal = $("#txtDBEGoel").val().replace("%", "");
    advDocument.ContractNo = $("#txtContractNo").val();
    advDocument.ProjectName = $("#txtProjectName").val();
    advDocument.StateProjectNo = $("#txtStateProjectNo").val();
    advDocument.FAPNo = $("#txtFAPNo").val();
    advDocument.Parish = $("#ddlParish").val();
    advDocument.IsPostToWeb = IsPostToWeb;
    //advDocument.Comment1 = $("#txtComment1").val();
    //advDocument.Comment2 = $("#txtComment2").val();
    //advDocument.Comment3 = $("#txtComment3").val();


    var url = localStorage.getItem("BaseUrl") + "Advertisement/CreateProjectAdvertisementDocument";
    //var url = localStorage.getItem("BaseUrll") + "Home/CreateProjectAdvertisementDocument";
    $.ajax({
        
        type: "POST",
        url: url,
        contentType: "application/json",
        dataType: "json",
        data: JSON.stringify(advDocument),
        success: function (data) {
            debugger;
            if (data.Result.Status == 1 || data.Result.Status == 3) {
                alertify.alert('Success!', 'Record has been saved successfully.', function () {
                    //window.location.href = localStorage.getItem("BaseUrl") + "Dashboard/AdvertisementDashboard";
                    window.location.reload();
                });
            }
            else if (data.Result.Status == 5) {
                alertify.alert('Alert!', 'Record already exist.', function () {

                });
            }
        },
        error: function (err) { }
    });
    return false;
}


//function ProjectAdvertisementDocument(postToWeb) {

//    var advDocument = {};
//    advDocument.ProjectAdvertisementDetailsId = ProjectAdvId;
//    advDocument.AdvertisementDate = $("#txtAdvertisementDate").val();
//    advDocument.ClosingDate = $("#txtClosingDate").val();
//    advDocument.AdvertDateGoesHere = $("#txtAdvertDateGoes").val();
//    advDocument.TypeOfAdvertGoesHere = $("#ddlTypeOfAdvert").val();
//    advDocument.Details = $("#txtDetails").val();
//    advDocument.Document = $("#txtDocument").val();
//    advDocument.DBEGoal = $("#txtDBEGoel").val().replace("%", "");
//    advDocument.ContractNo = $("#txtContractNo").val();
//    advDocument.ProjectName = $("#txtProjectName").val();
//    advDocument.StateProjectNo = $("#txtStateProjectNo").val();
//    advDocument.FAPNo = $("#txtFAPNo").val();
//    advDocument.Parish = $("#ddlParish").val();
//    advDocument.IsPostToWeb = postToWeb;
//    //advDocument.Comment1 = $("#txtComment1").val();
//    //advDocument.Comment2 = $("#txtComment2").val();
//    //advDocument.Comment3 = $("#txtComment3").val();


//    var url = localStorage.getItem("BaseUrl") + "Advertisement/CreateUpdateProjectAdvertisementDocument";
//    //var url = localStorage.getItem("BaseUrll") + "Home/CreateProjectAdvertisementDocument";
//    $.ajax({

//        type: "POST",
//        url: url,
//        contentType: "application/json",
//        dataType: "json",
//        data: JSON.stringify(advDocument),
//        success: function (data) {
//            debugger;
//            if (data.Result.Status == 1 || data.Result.Status == 3) {
//                alertify.alert('Success!', 'Record has been saved successfully.', function () {
//                    window.location.href = localStorage.getItem("BaseUrl") + "Dashboard/AdvertisementDashboard";

//                });
//            }
//            else if (data.Result.Status == 5) {
//                alertify.alert('Alert!', 'Record already exist.', function () {

//                });
//            }
//        },
//        error: function (err) { }
//    });
//    return false;
//}





//function GetProjectGeneralInformation() {
//    $.ajax({
//        type: "GET",
//        url: localStorage.getItem("BaseUrl") + "Advertisement/GetProjectAdvertisementDetails?ProjectInfoId=" + ProjectInfoId,
//        contentType: "application/json",
//        dataType: "json",
//        success: function (response) {

//            if (response.Result.Status == 1) {
//                if (response.Result.Result != null) {
//                    localStorage.setItem("ProjectAdvId", response.Result.Result.ProjectAdvertisementDetailsId);
//                    var result = response.Result.Result;

//                    $("#txtDBEGoel").val(result.Information.DBEGoel + "%");
//                    $("#txtStateProjectNo").val(result.Information.StateProjectNo);
//                    $("#txtContractNo").val(result.Information.ContractNo);
//                    $("#txtFAPNo").val(result.Information.FAPNo);
//                    $("#txtProjectName").val(result.Information.ProjectName);
//                    $("#txtRoute").val(result.Information.Route);
//                    $("#ddlParish").val(result.Information.Parish);
//                    if (result.ProjectWork != null) {
//                        $("#txtAdvertisementDate").val(result.ProjectWork.AdvertisementDate);
//                        $("#txtClosingDate").val(result.ProjectWork.ClosingDate);
//                    }
//                }
//            }
//        },
//        error: function (result) {

//        }
//    });
//}
function GetProjectAdvertisementDocument() {
    $.ajax({
        type: "GET",
        //url: localStorage.getItem("BaseUrl") + "Advertisement/GetProjectAdvertisementDetails?ProjectInfoId=" + ProjectInfoId,
        url: localStorage.getItem("BaseUrl") + "Advertisement/GetAdvertisementDocumentDetails?ProjectGeneralInformationId=" + ProjectInfoId,
        contentType: "application/json",
        dataType: "json",
        success: function (response) {
            //if (response.Result.Status == 1) {
            //if (response.Result.Result != null) {
            if (response.data != null) {
                var result = response.data;
                //localStorage.setItem("ProjectAdvId", response.Result.Result.ProjectAdvertisementDetailsId);
                localStorage.setItem("ProjectAdvId", result.ProjectAdvertisementDetailsId);
                if (!result.IsPostToWeb) {
                    $("#btnPublish").show();
                }
                else {
                    $("#btnUnPublish").show();
                }
                //var result = response.Result.Result;


                //$("#txtDBEGoel").val(result.Information.DBEGoel+"%");
                //$("#txtStateProjectNo").val(result.Information.StateProjectNo);
                //$("#txtContractNo").val(result.Information.ContractNo);
                //$("#txtFAPNo").val(result.Information.FAPNo);
                //$("#txtProjectName").val(result.Information.ProjectName);
                //$("#txtRoute").val(result.Information.Route);
                //$("#ddlParish").val(result.Information.Parish);
                //$("#ddlTypeOfAdvert").val()
                //$("#txtAdvertDateGoes").val(result.AdvertDateGoesHere);
                //if (result.ProjectWork != null) {
                //    $("#txtAdvertisementDate").val(result.ProjectWork.AdvertisementDate);
                //    $("#txtClosingDate").val(result.ProjectWork.ClosingDate);
                //}

                $("#txtDBEGoel").val(result.DBEGoal + "%");
                $("#txtStateProjectNo").val(result.StateProjectNo).prop('disabled', true);
                $("#txtContractNo").val(result.ContractNo);
                $("#txtFAPNo").val(result.FAPNo);
                $("#txtProjectName").val(result.ProjectName);
                $("#txtRoute").val(result.Route);
                $("#ddlParish").val(result.Parish);
                $("#ddlTypeOfAdvert").val(result.TypeOfAdvertGoesHere)
                $("#txtAdvertDateGoes").val(formatDate(result.AdvertDateGoesHere));
                $("#txtAdvertisementDate").val(formatDate(result.AdvertisementDate));
                $("#txtClosingDate").val(formatDate(result.ClosingDate));
                $("#txtDetails").val(result.Details);

            }
            //}
        },
        error: function (result) {

        }
    });
}
function formatDate(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2)
        month = '0' + month;
    if (day.length < 2)
        day = '0' + day;

    return [year, month, day].join('-');
}
