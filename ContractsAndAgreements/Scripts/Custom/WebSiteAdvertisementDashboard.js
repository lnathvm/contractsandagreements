﻿$(document).ready(function () {
    var table = $('#tblPjrojectInfo').DataTable({
        "ajax": localStorage.getItem("BaseUrl") + "Advertisement/GetAllAdvertisementDetailsList",
        "columns": [
            {
                "data": "Date",
                "render": function (data) {
                    
                    var d = new Date(data.split("/").reverse().join("-"));
                    var dd = d.getDate() + 1 ;
                    var mm = d.getMonth() + 1;
                    var yy = d.getFullYear();
                    var newdate = mm + "-" + dd + "-" + yy;
                    return newdate;
                }
            },
            { "data": "ProjectGeneralInformationId" },
            { "data": "ContractNo" },
            {
                "data": "Type",
                "className": 'RedirectToAdertisement',
                "render": function (data, type, row, meta) {
                    return "<a href='#'>" + data + "</a>"
                }
            },
            { "data": "ProjectName" },
            { "data": "Parish" },
            {
                "data": "ClosingDate",
                "render": function (data) {
                    
                    var d = new Date(data.split("/").reverse().join("-"));
                    var dd = d.getDate() + 1;
                    var mm = d.getMonth() + 1;
                    var yy = d.getFullYear();
                    var newdate = mm + "-" + dd + "-" + yy;
                    return newdate;
                }
            },
            //   { "data": "Type", "className":"Hide" },

        ],
        "paging": true,
        "ordering": false,
        "info": false,
        "filter": true,
        "columnDefs": [
            {
                "targets": [1],
                "visible": false,
                "searchable": false
            }
        ],
        //"order": [[1, 'asc']]
    });

    // Add event listener for opening and closing details
    $('#tblPjrojectInfo tbody').on('click', 'td.RedirectToAdertisement', function () {
        
        var tr = $(this).closest('tr');
        var row = table.row(tr);
        var data = row.data();
        // alert(data);
        //localStorage.setItem("ProjectInfoId", data.ProjectGeneralInformationId);

        if (data.Type != "Advertisement") {
            window.location.href = localStorage.getItem("BaseUrl") + "DashBoard/AddendumDocument?ProjectAddendumId=" + data.ProjectAdDetailsId;
        }
        else {
            window.location.href = localStorage.getItem("BaseUrl") + "DashBoard/ContractAdvertisementDocument?projectadvertisementdetails=" + data.ProjectAdDetailsId;
        }
        return false;
    });


});

