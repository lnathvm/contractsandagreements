﻿var ProjectInfoId = localStorage.getItem("ProjectInfoId");
var ProjectAdvShortlistId = localStorage.getItem("ProjectAdvShortlistId");
var subOptionText;
var shortList;
$(document).ready(function () {
    GetAllConsultantProviderList();
    GetAllConsultantAreaSubTaxList();

    $('#ShortlistForm').validate({});
    if (ProjectInfoId != null) {
        //GetAdvertisementDetail(ProjectInfoId);
        GetProjectShortlistInformation();
        GetAdvertisementDetail(ProjectInfoId);
    }
    $('#btnPostToWebShortlist').on('click', function (e) {

        //e.preventDefault();
        //if (!$("#ShortlistForm").valid()) {
        //    return false;
        //}
        debugger;
        //ProjectAdvertisementShortlist(true);
        PostToWebShorlist();
    });

    $('#btnSave').on('click', function (e) {

        //e.preventDefault();
        //if (!$("#ShortlistForm").valid()) {
        //    return false;
        //}
        ProjectAdvertisementShortlist(false);
    });

    $('#btnCancel').on('click', function (e) {
        window.location.href = localStorage.getItem("BaseUrl") + "Dashboard/AdvertisementDashboard";
        return false;
    });
    $('#btnAdvertisementDocument').on('click', function (e) {
        window.location.href = localStorage.getItem("BaseUrl") + "Advertisement/AdvertisementDocument";
        return false;
    });

    $('#btnAddendum').on('click', function (e) {
        window.location.href = localStorage.getItem("BaseUrl") + "Advertisement/Addendum";
        return false;
    });
    $('#btnAddSortList').on('click', function (e) {
        AddSortList();
        return false;
    });
    $('#btnDeleteShortList').hide();
    $('#btnGetShortlist').on('click', function (e) {
        debugger;
        //GetProjectShortlistInformation();
        CheckShotlistExist();
        return false;
    });
    $('#btnDeleteShortList').on('click', function (e) {
        DeleteShorlist();
        return false;
    });


});
function CheckShotlistExist() {
    if (localStorage.getItem("ProjectAdvShortlistId") == null) {
        alertify.alert("Shortlist not exist.");
    }
    else {
        alertify.alert("Shortlist is already exist.");
        $('#btnDeleteShortList').show();
    }
}
function DeleteShorlist() {
    debugger;
    alertify.confirm('Alert!', 'Do you want to delete this?', function () {
        var url = localStorage.getItem("BaseUrl") + "Advertisement/DeleteProjectShorlist?ShorlistId=" + localStorage.getItem("ProjectAdvShortlistId");
        $.ajax({
            type: 'GET',
            url: url,
            async: false,
            success: function (obj) {
                debugger;
                if (obj.data == 1 || obj.data == 3) {
                    alertify.alert('Success!', 'Record has been deleted successfully.', function () {
                        localStorage.removeItem("ProjectAdvShortlistId");
                        window.location.reload();
                    });
                }
            },
            error: function (err) { }
        });
    }
        , function () { });


}
function PostToWebShorlist() {
    debugger;
    alertify.confirm('Alert!', 'Do you want to post this?', function () {
        var url = localStorage.getItem("BaseUrl") + "Advertisement/PostToWebShorlist?ShorlistId=" + localStorage.getItem("ProjectAdvShortlistId");
        $.ajax({
            type: 'GET',
            url: url,
            async: false,
            success: function (obj) {
                if (obj.data == 1 || obj.data == 3) {
                    alertify.alert('Success!', 'Record has been post successfully.', function () {
                        window.location.reload();
                    });
                }
            },
            error: function (err) { }
        });
    }
        , function () { });


}



function GetAdvertisementDetail(ProjectGeneralInformationId) {
    $.ajax({
        type: "GET",
        //url: localStorage.getItem("BaseUrl") + "Advertisement/GetAdvertisementDetail?ProjectGeneralInformationId=" + ProjectGeneralInformationId,
        url: localStorage.getItem("BaseUrl") + "Advertisement/GetProjectAdvertisementDetails?ProjectInfoId=" + ProjectGeneralInformationId,
        contentType: "application/json",
        dataType: "json",
        success: function (response) {
            //if (response.data != null) {
            //    var result = response.data;
            //    $("#txtAdvertisementDate").val(result.AdvertisementDate);
            //    $("#txtClosingDate").val(result.ClosingDate);
            //    $("#txtContractNo").val(result.ContractNo);
            //    $("#txtStateProjectno").val(result.StateProjectno);
            $("#txtAdvertisementDate").val(result.ProjectWork.AdvertisementDate);
            $("#txtClosingDate").val(result.ProjectWork.ClosingDate);
            $("#txtContractNo").val(result.Information.ContractNo);
            $("#txtStateProjectno").val(result.Information.StateProjectNo);

            }
        },
        error: function (result) {

        }
    });
}

function GetProjectShortlistInformation() {
    localStorage.removeItem("ProjectAdvShortlistId");
    $.ajax({
        type: "GET",
        url: localStorage.getItem("BaseUrl") + "Advertisement/GetProjectAdvertisementShortlistDetailsByProjectInfoId?ProjectInfoId=" + ProjectInfoId,
        contentType: "application/json",
        dataType: "json",
        success: function (response) {
            debugger;

            if (response.data != null) {
                //alertify.alert("Shortlist is already exist.");
                var result = response.data;
                localStorage.setItem("ProjectAdvShortlistId", result.ProjectAdvertisementShortlistId);
                //$('#btnDeleteShortList').show();

                //var result = response.data;
                //$("#txtAdvertisementDate").val(result.AdvertisementDate);
                $("#txtAmmendedClosingDate").val(result.AmmendedClosingDate);
                $("#txtShortListDate").val(result.ShortListDate);
                $("#txtComment1").val(result.Comment1);
                $("#txtComment2").val(result.Comment2);
                $("#txtSubject").val(result.Subject);

                //$("#txtClosingDate").val(result.ClosingDate);
                //$("#txtContractNo").val(result.ContractNo);
                //$("#txtStateProjectno").val(result.StateProjectno);
               
                var shortlist = result.ShortList;
                for (var i = 0; i < shortlist.length; i++) {
                    if (i > 2) {
                        AddSortList();
                    }
                    var num = i + 1;
                    $("#ddlShortList" + num + "").val(shortlist[i].ShortList);
                    for (var j = 0; j < shortlist[i].SubTaxDetailsDto.length - 1; j++) {
                        AddNewRow(num);
                    }
                    var k = 0;
                    //var dsds = $("#ddlSub1");
                    //$("#ddlSub" + num + "").each(function () {
                    //    $(this).val(shortlist[i].SubTaxDetailsDto[k].SubId);
                    //    k += 1;
                    //});
                    $("#tbodySubTab" + num + "").each(function () {
                        var k = 0;
                        $(this).find("tr.item").each(function () {
                            var TaxDetailsDto = {};
                            $(this).find('td :input').each(function () {
                                if ($(this).attr('id').indexOf('ddlSub') > -1) {
                                    $(this).val(shortlist[i].SubTaxDetailsDto[k].SubId);
                                    k += 1;
                                }
                            });
                        });
                    });
                    //for (var k = 0; k < $("#ddlSub" + num + "").length; k++) {
                    //    //var sub = j + 1;                        
                    //    $("#ddlSub" + num + "")[k].val(shortlist[i].SubTaxDetailsDto[k].SubId);
                    //}
                };
            }
            else {
                // alertify.alert("Shortlist not exist.");
                //GetAdvertisementDetail(ProjectInfoId);
            }


        },
        error: function (result) {

        }
    });
}
function ProjectAdvertisementShortlist(PostToWeb) {
    var ProjectAdvShortlist = {};
    ProjectAdvShortlist.ProjectGeneralInformationId = ProjectInfoId;
    // ProjectAdvShortlist.Title39 = $("#txtTitle39").val();
    ProjectAdvShortlist.AdvertisementDate = $("#txtAdvertisementDate").val();
    ProjectAdvShortlist.ClosingDate = $("#txtClosingDate").val();
    ProjectAdvShortlist.AmmendedClosingDate = $("#txtAmmendedClosingDate").val();
    ProjectAdvShortlist.ShortListDate = $("#txtShortListDate").val();
    ProjectAdvShortlist.ContractNo = $("#txtContractNo").val();
    ProjectAdvShortlist.StateProjectno = $("#txtStateProjectno").val();
    ProjectAdvShortlist.Subject = $("#txtSubject").val();
    //ProjectAdvShortlist.ShortList = $("#ddlShortList1").val();
    //ProjectAdvShortlist.ShortList2 = $("#ddlShortList2").val();
    //ProjectAdvShortlist.ShortList3 = $("#ddlShortList3").val();
    ProjectAdvShortlist.Comment1 = $("#txtComment1").val();
    ProjectAdvShortlist.Comment2 = $("#txtComment2").val();
    ProjectAdvShortlist.IsPostToWeb = PostToWeb;
    ProjectAdvShortlist.ShortList = [];

    var k = 1;
    $(".listtbodySubTab").each(function () {
        var ShortListdto = {};
        ShortListdto.SubTaxDetailsDto = [];
        $(this).find("tr.item").each(function () {
            var TaxDetailsDto = {};
            $(this).find('td :input').each(function () {
                if ($(this).attr('id').indexOf('ddlSub') > -1) {
                    
                    var sub = {};
                    sub.SubId = $(this).val();
                    ShortListdto.SubTaxDetailsDto.push(sub);
                }
                if ($(this).attr('id').indexOf('ddlShortList') > -1) {
                    ShortListdto.ShortList = $(this).val();
                    ShortListdto.Rank = k;
                }
            });
        });
        k += 1;
        ProjectAdvShortlist.ShortList.push(ShortListdto);
    });

    var url = localStorage.getItem("BaseUrl") + "Advertisement/CreateUpdateProjectAdvertisementShortlistDetails";

    $.ajax({
        type: "POST",
        url: url,
        contentType: "application/json",
        dataType: "json",
        data: JSON.stringify(ProjectAdvShortlist),
        success: function (data) {
            debugger;
            if (data.Result.Status == 1 || data.Result.Status == 3) {
                $('#divLogInformationPanel').css('display', 'block');
                localStorage.setItem("ProjectAdvShortlistId", data.Result.Result.ProjectAdvertisementShortlistId);
                alertify.alert('Success!', 'Record has been saved successfully.', function () {
                     window.location.href = localStorage.getItem("BaseUrl") + "Dashboard/AdvertisementDashboard";
                });
            }
            else if (data.Result.Status == 5) {
                localStorage.setItem("ProjectAdvShortlistId", data.Result.Result.ProjectAdvertisementShortlistId);
                alertify.alert('Alert!', 'Record already exist.', function () {

                });
            }
        },
        error: function (err) { }
    });
    return false;
}



function GetAllConsultantProviderList() {
    $.ajax({
        type: "GET",
        url: localStorage.getItem("BaseUrl") + "Common/GetAllConsultantProviderList",
        contentType: "application/json",
        dataType: "json",
        success: function (response) {
            var options = "<option value='' selected='selected'>--Select Consultant/Provider--</option>";
            if (response.data != undefined && response.data.length > 0) {
                $.each(response.data, function (key, value) {
                    options += '<option value="' + value.ConsultantProviderId + '">' + value.ConsultantProviderName + '</option>';
                });
            }
            shortList = options;
            $('#ddlShortList1').html(options);
            $('#ddlShortList2').html(options);
            $('#ddlShortList3').html(options);
        },
        error: function (result) {
            var options = "<option value='' selected='selected'>--Select Consultant/Provider--</option>";
            $('#ddlShortList1').html(options);
            $('#ddlShortList2').html(options);
            $('#ddlShortList3').html(options);
        }
    });
}

//$('#btnAdd').on('click', function (e) {
//    AddNewRow("tableSubTab", "ddlSub", "Sub","tbodySubTab");
//});
//$('#btnAdd2').on('click', function (e) {
//    AddNewRow("tableSubTab2", "ddl2Sub", "2Sub","tbodySubTab2");
//});
//$('#btnAdd3').on('click', function (e) {
//    AddNewRow("tableSubTab3", "ddl3Sub", "3Sub", "tbodySubTab3");
//});
function AddNewRow(id) {
    var tableid = 'tableSubTab' + id;
    var ddlid = 'ddlSub' + id;
    var ddlname = 'Sub' + id;
    var tbodyid = 'tbodySubTab' + id;
    var rows = (document.getElementById(tableid).getElementsByTagName("tbody")[0].getElementsByTagName("tr").length);
    var html = '';
    html += '<tr class="odd gradeX item" id="tr' + rows + '">';
    html += '<td> <div class="col-lg-12">';
    html += ' <div class="form-group">';
    html += '     <label>Sub #' + rows + '</label>';
    html += '       <select id="' + ddlid + '" name="' + ddlname + '" required class="form-control"  return false;">';
    html += '           ' + subOptionText + '';
    html += '       </select>';
    html += '       </div >';
    html += '    </div></td>';
    html += '<td style="width: 9%"><div class="col-lg-12"><input id="btnremove' + rows + '" type="buttom" class="btn btn-danger" value="Remove" style="width: 80px;" onclick="remove(tr' + rows + ',' + tbodyid + '); return false;"></div> </td>';
    html += '</tr>';
    $('#' + tbodyid).append(html);
}


function AddSortList() {
    //
    var lenshortlist = $(".listtbodySubTab").length + 1;
    var htmlShorlist = "";
    htmlShorlist += '<div class="col-lg-4 itemlist" id="divShortlist' + lenshortlist + '">';

    htmlShorlist += '    <div class="col-lg-12">';
    htmlShorlist += '        <table class="table table-striped table-bordered table-hover SubTab" id="tableSubTab' + lenshortlist + '">';
    htmlShorlist += '            <tbody id="tbodySubTab' + lenshortlist + '" class="listtbodySubTab">';
    htmlShorlist += '  <tr class="odd gradeX item">';
    htmlShorlist += '      <td colspan="2">';
    htmlShorlist += '          <div class="form-group">';
    htmlShorlist += '              <label>Shortlist ' + lenshortlist + ':</label>';
    htmlShorlist += '<input id="btnremove' + lenshortlist + '" type="buttom" class="btn btn-danger" value="Remove" style="width: 80px; float:right;" onclick="removedv(divShortlist' + lenshortlist + '); return false;">';
    htmlShorlist += '              <select id="ddlShortList' + lenshortlist + '" name="ddlShortList' + lenshortlist + '" class="form-control ShortList" required>';
    htmlShorlist += shortList;
    htmlShorlist += ' </select > ';
    htmlShorlist += '          </div>';
    htmlShorlist += '      </td>';
    htmlShorlist += '  </tr>';
    htmlShorlist += '                <tr class="odd gradeX item">';
    htmlShorlist += '                    <td>';
    htmlShorlist += '                        <div class="col-lg-12">';
    htmlShorlist += '                            <div class="form-group">';
    htmlShorlist += '                                <label>Sub #1</label>';
    htmlShorlist += '                                <select id="ddlSub' + lenshortlist + '" name="Sub' + lenshortlist + '" class="form-control">';
    htmlShorlist += subOptionText;
    htmlShorlist += '</select > ';
    htmlShorlist += '                            </div>';
    htmlShorlist += '                        </div>';
    htmlShorlist += '                    </td>';
    htmlShorlist += '                    <td style="width: 9%">';
    htmlShorlist += '                        <div class="col-lg-12" style="margin-top:20px">';
    htmlShorlist += '                            <button type="button" id="btnAdd' + lenshortlist + '" class="btn btn-primary" onclick="AddNewRow(' + lenshortlist + '); return false;">Get Tax IDs</button>';
    htmlShorlist += '                        </div>';
    htmlShorlist += '                    </td>';
    htmlShorlist += '                </tr>';
    htmlShorlist += '            </tbody>';
    htmlShorlist += '        </table>';
    htmlShorlist += '    </div>';
    htmlShorlist += '  </div>';

    $("#dvShortList").append(htmlShorlist);
}
function removedv(dv) {
    var dsd = $(dv);
    alertify.confirm('Alert!', 'Do you want to delete this?', function () { dsd.remove(); }
        , function () { });
}
function remove(id, tboddy) {
    debugger;
    var jh = $(tboddy).find(id);
    var dfdf = '#' + id.id;
    alertify.confirm('Alert!', 'Do you want to delete this?', function () { jh.remove(); }
        , function () { });
}
function SetTaxId(ConsultantInfo) {
    var seletedTaxId = $("#" + ConsultantInfo.id).find('option:selected').attr("taxId");
    var id = ConsultantInfo.id.split('ddlSub')[1];
    $("#txtTaxId" + id).val(seletedTaxId);
}
function GetAllConsultantAreaSubTaxList() {
    $.ajax({
        type: "GET",
        url: localStorage.getItem("BaseUrl") + "Common/GetAllConsultantProviderList",
        contentType: "application/json",
        dataType: "json",
        async: false,
        success: function (response) {
            var options = "<option value='' selected='selected'>--Select Sub--</option>";
            if (response.data != undefined && response.data.length > 0) {
                $.each(response.data, function (key, value) {
                    options += '<option value="' + value.ConsultantProviderId + '" taxId="' + value.TaxId + '">' + value.ConsultantProviderName + '</option>';
                });
            }
            subOptionText = options;
            $('#ddlSub1').html(options);
            $('#ddlSub2').html(options);
            $('#ddlSub3').html(options);
        },
        error: function (result) {
            subOptionText = options;
            var options = "<option value='' selected='selected'>--Select Sub--</option>";
            $('#ddlSub1').html(options);
            $('#ddlSub2').html(options);
            $('#ddlSub3').html(options);
        }
    });
}

//function EnableDisable(value) {
//    $("#txtTitle39").prop('disabled', value);
//    $("#txtAdvertisementDate").prop('disabled', value);
//    $("#txtClosingDate").prop('disabled', value);
//    $("#txtAmmendedClosingDate").prop('disabled', value);
//    $("#txtShortListDate").prop('disabled', value);
//    $("#txtContractNo").prop('disabled', value);
//    $("#txtStateProjectno").prop('disabled', value);
//    $("#txtSubject").prop('disabled', value);
//    $("#ddlShortList1").prop('disabled', value);
//    $("#ddlShortList2").prop('disabled', value);
//    $("#ddlShortList3").prop('disabled', value);
//  }

