﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContractsAndAgreements_DataAccess.StoredProcedures
{
    public class StoredProcedures
    {
        public const string GetAllProjectGeneralInformationList = @"usp_GetAllProjectGeneralInformationList";
        public const string GetAllProjectContractAndTaskOrderList = @"usp_GetAllProjectContractAndTaskOrderList @ProjectGeneralInformationId";
        public const string GetAllProjectMoneyBalanceList = @"usp_GetAllProjectMoneyBalanceList";
        public const string GetProjectContractTaskOrderMoneyBalanceList = @"usp_GetProjectContractTaskOrderMoneyBalanceList @ProjectGeneralInformationId";
        public const string GetAllAdvertisementDetailsList = @"usp_GetAllAdvertisementList";
        public const string GetAddendunDocumentDetail = @"usp_GetAddendunDocumentDetail @ProjectAddendumId";
        public const string GetAdvertisementDocumentDetail = @"usp_GetAdvertisementDocumentDetail @projectadvertisementdetails";

        public const string GetAllAdvertisementSelectionDetailsList = @"usp_GetAllAdvertisementSelectionDetailsList";
        public const string GetAllAdvertisementShortlistDetailsList = @"usp_GetAllAdvertisementShortlistDetailsList";
        public const string GetAllAdvertisementApparentSelectionDetailsList = @"usp_GetAllAdvertisementApparentSelectionDetailsList";
        public const string GetShortListConsultantProviderList = @"usp_GetShortListConsultantProviderList @ProjectGeneralInformationId";

        public const string GetAdvertisementShortListDocDetail = @"usp_GetAdvertisementShortListDocDetail @ProjectGeneralInformationId";
        public const string GetAdvertisementApparentSelectionDocDetail = @"usp_GetAdvertisementApparentSelectionDocDetail @ProjectAdvertisementApparentSelectionId";
        public const string GetAdvertisementSelectionDocDetail = @"usp_GetAdvertisementSelectionDocDetail @ProjectAdvertisementSelectionId";
        public const string GetSubByShortListId = @"usp_GetSubByShortListId @ProjectAdvertisementShortlistConsultantProviderId";
        public const string GetAdvertisementDetail = @"usp_GetAdvertisementDetail @ProjectGeneralInformationId";
        public const string GetSelectedShortListConsultantProviderDetail = @"usp_GetSelectedShortListConsultantProviderDetail @ProjectAdvertisementShortlistId";




    }
}
