﻿using ContractsAndAgreements_DataAccess.Context;
using ContractsAndAgreements_DataAccess.Repository;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContractsAndAgreements_DataAccess.UnitofWorks
{
    public class UnitOfWork : IUnitOfWork
    {
        private bool _isDisposed = false;
        private readonly DbContext _dbContext;

        /// <summary>
        /// Because repository patterns are limiting.
        /// </summary>
        public DbContext DbContext => _dbContext;

        /// <summary>
        /// Initializes a new instance of the <see cref="UnitOfWorks" /> class.
        /// </summary>
     
        public UnitOfWork()
        {
            _dbContext = new ContractsAndAgreementsDbContext();
        }
        public IRepository<T> CreateRepository<T>() where T : class
        {
            return new Repository<T>(_dbContext);
        }

        public void Save()
        {
            _dbContext.SaveChanges();
        }

        public async Task<int> SaveAsync()
        {
            return await _dbContext.SaveChangesAsync();
        }
        protected virtual void Dispose(bool disposing)
        {
            if (!_isDisposed)
            {
                if (disposing)
                {
                    _dbContext.Dispose();
                }
            }

            _isDisposed = true;
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        //private string GetConnectionString()
        //{
        //    var connectionStrings = ConfigurationManager.ConnectionStrings["ContractsAndAgreementsDBContext"];

        //    return connectionStrings.ConnectionString;
        //}
    }
}
