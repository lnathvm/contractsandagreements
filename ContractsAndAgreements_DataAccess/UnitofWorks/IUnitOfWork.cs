﻿using ContractsAndAgreements_DataAccess.Repository;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContractsAndAgreements_DataAccess.UnitofWorks
{
    public interface IUnitOfWork
    {
        /// <summary>
        /// Creates the generic repository.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        IRepository<T> CreateRepository<T>() where T : class;

        /// <summary>
        /// Saves current state.
        /// </summary>
        void Save();

        /// <summary>
        /// Saves current state asynchronous.
        /// </summary>
        Task<int> SaveAsync();

        /// <summary>
        /// For those special cases when the repository pattern can't cope.
        /// </summary>
        DbContext DbContext { get; }

    }
}
