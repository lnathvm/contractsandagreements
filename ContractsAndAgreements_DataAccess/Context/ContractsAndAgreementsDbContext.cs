﻿using ContractsAndAgreements_Domain.Entity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContractsAndAgreements_DataAccess.Context
{
    public class ContractsAndAgreementsDbContext : DbContext
    {
        public ContractsAndAgreementsDbContext() : base("ContractsTemp")
        {

        }
        DbSet<ProjectGeneralInformation> ProjectGeneralInformation { get; set; }
        DbSet<ProjectConsultantArea> ProjectConsultantArea { get; set; }
        DbSet<ConsultantAreaSubTaxDetails> ConsultantAreaSubTaxDetails { get; set; }
        DbSet<ProjectTracking> ProjectTracking { get; set; }
        DbSet<ConsultantProviderMaster> ConsultantProviderMaster { get; set; }
        DbSet<ContractTypeMaster> ContractTypeMaster { get; set; }
        DbSet<FeeTypeMaster> FeeTypeMaster { get; set; }
        DbSet<FeeTypeMasterr> FeeTypeMasterr { get; set; }
        DbSet<ParishMaster> ParishMaster { get; set; }
        DbSet<SelectionMaster> SelectionMaster { get; set; }
        DbSet<ProjectManagerMaster> ProjectManagerMaster { get; set; }

        //DbSet<ConsultantAreaSubTaxMaster> ConsultantAreaSubTaxMaster { get; set; }

        DbSet<ProjectAdvertisementDetails> ProjectAdvertisementDetails { get; set; }
        DbSet<ProjectWorkFlowTracking> ProjectWorkFlowTracking { get; set; }

        DbSet<CommunicationSettingMaster> CommunicationSettingMaster { get; set; }
        DbSet<SuperVisorMaster> SupervisorMaster { get; set; }

        DbSet<ProcessorMaster> ProcessorMaster { get; set; }
        DbSet<ProjectAddendumDetails> ProjectAddendumDetails { get; set; }
        DbSet<ProjectAddendumWorkFlowTracking> ProjectAddendumWorkFlowTracking { get; set; }
        DbSet<ProjectPublishedDetails> ProjectPublishedDetails { get; set; }

        DbSet<ProjectContractDetails> ProjectContractDetails { get; set; }
        DbSet<ProjectContractInformation> ProjectContractInformation { get; set; }
        DbSet<ProjectContractWorkFlowTracking> ProjectContractWorkFlowTracking { get; set; }
        DbSet<ContractInformationSubTaxDetails> ContractInformationSubTaxDetails { get; set; }
        DbSet<ProjectTaskOrderDetails> ProjectTaskOrderDetails { get; set; }
        DbSet<ProjectTaskOrderInformation> ProjectTaskOrderInformation { get; set; }

        DbSet<TaskOrderInformationSubTaxDetails> TaskOrderInformationSubTaxDetails { get; set; }

        DbSet<ProjectAdvertisementApparentSelection> ProjectAdvertisementApparentSelection { get; set; }
        DbSet<ProjectAdvertisementSelection> ProjectAdvertisementSelection { get; set; }
        DbSet<ProjectAdvertisementShortlist> ProjectAdvertisementShortlist { get; set; }

        DbSet<ShortlistSubDetails> ShortlistSubDetails { get; set; }
        DbSet<AdvertisementDocumentMaster> AdvertisementDocumentMaster { get; set; }

        DbSet<ProjectAdvertisementShortlistConsultantProvider> ProjectAdvertisementShortlistConsultantProvider { get; set; }

        DbSet<AddendumDocumentMaster> AddendumDocumentMaster { get; set; }


        //comments
        DbSet<ProjectExtraWorkLetterDetails> ProjectExtraWorkLetterDetails { get; set; }

        DbSet<ProjectOriginalContractDetails> ProjectOriginalContractDetails { get; set; }

        DbSet<ProjectSupplementalAgreementDetails> ProjectSupplementalAgreementDetails { get; set; }

        DbSet<ProjectSupplementalAgreementForIDIQ> ProjectSupplementalAgreementForIDIQ { get; set; }
        DbSet<ProjectSupplementalAgreementInformationForIDIQ> ProjectSupplementalAgreementInformationForIDIQ { get; set; }
        DbSet<ProjectSupplementalAgreementInformationForIDIQSubTaxDetails> ProjectSupplementalAgreementInformationForIDIQSubTaxDetails { get; set; }

        DbSet<ProjectExtraWorkLetterInformationDetails> ProjectExtraWorkLetterInformationDetails { get; set; }
        DbSet<ProjectExtraWorkLetterSubTaxDetails> ProjectExtraWorkLetterSubTaxDetails { get; set; }
        DbSet<ProjectOriginalContractInformationDetails> ProjectOriginalContractInformationDetails { get; set; }
        DbSet<ProjectOriginalContractSubTaxDetails> ProjectOriginalContractSubTaxDetails { get; set; }
        DbSet<ProjectSupplementalAgreementInformationDetails> ProjectSupplementalAgreementInformationDetails { get; set; }
        DbSet<ProjectSupplementalAgreementSubTaxDetails> ProjectSupplementalAgreementSubTaxDetails { get; set; }

        DbSet<CompensationTypeMaster> CompensationTypeMaster { get; set; }

        DbSet<ContractAgencyMaster> ContractAgencyMaster { get; set; }

        public ContractsAndAgreementsDbContext(string connectionstring)
            : base(connectionstring)
        {

        }

    }
}
