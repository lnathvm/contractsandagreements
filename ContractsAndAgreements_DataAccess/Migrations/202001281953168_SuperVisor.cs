namespace ContractsAndAgreements_DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SuperVisor : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.SuperVisorMasters",
                c => new
                    {
                        SupervisorId = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        SupervisorEmail = c.String(),
                        IsActive = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.SupervisorId);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.SuperVisorMasters");
        }
    }
}
