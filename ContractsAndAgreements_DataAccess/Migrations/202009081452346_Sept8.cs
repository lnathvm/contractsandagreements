namespace ContractsAndAgreements_DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Sept8 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ProjectAdvertisementDetails", "AdvertisedContractAmount", c => c.String());
            AlterColumn("dbo.ProjectAdvertisementDetails", "ContractTime", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.ProjectAdvertisementDetails", "ContractTime", c => c.Time(nullable: false, precision: 7));
            DropColumn("dbo.ProjectAdvertisementDetails", "AdvertisedContractAmount");
        }
    }
}
