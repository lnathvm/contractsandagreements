namespace ContractsAndAgreements_DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Ra : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ProjectGeneralInformations", "FeeTypee", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.ProjectGeneralInformations", "FeeTypee");
        }
    }
}
