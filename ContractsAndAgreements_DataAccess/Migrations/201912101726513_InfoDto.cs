namespace ContractsAndAgreements_DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InfoDto : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.ProjectWorkFlowTrackings", "InfoReceived", c => c.String());
            AlterColumn("dbo.ProjectWorkFlowTrackings", "TaskAssignedToStaff", c => c.String());
            AlterColumn("dbo.ProjectWorkFlowTrackings", "ChiefApprovalToRetain", c => c.String());
            AlterColumn("dbo.ProjectWorkFlowTrackings", "DraftToSupervisor", c => c.String());
            AlterColumn("dbo.ProjectWorkFlowTrackings", "ApprovedFromSupervisor", c => c.String());
            AlterColumn("dbo.ProjectWorkFlowTrackings", "DraftToProjectManager", c => c.String());
            AlterColumn("dbo.ProjectWorkFlowTrackings", "ApprovedFromProjectManager", c => c.String());
            AlterColumn("dbo.ProjectWorkFlowTrackings", "DraftToChiefEngineer", c => c.String());
            AlterColumn("dbo.ProjectWorkFlowTrackings", "ApprovedFromChiefEngineer", c => c.String());
            AlterColumn("dbo.ProjectWorkFlowTrackings", "DraftToFHWA", c => c.String());
            AlterColumn("dbo.ProjectWorkFlowTrackings", "ApprovedFromFHWA", c => c.String());
            AlterColumn("dbo.ProjectWorkFlowTrackings", "FedAideFHWA", c => c.String());
            AlterColumn("dbo.ProjectWorkFlowTrackings", "AdvertisementDate", c => c.String());
            AlterColumn("dbo.ProjectWorkFlowTrackings", "ClosingDate", c => c.String());
            AlterColumn("dbo.ProjectWorkFlowTrackings", "AmendedClosingDate", c => c.String());
            AlterColumn("dbo.ProjectWorkFlowTrackings", "SecretaryShortlist", c => c.String());
            AlterColumn("dbo.ProjectWorkFlowTrackings", "AwardNotification", c => c.String());
            AlterColumn("dbo.ProjectWorkFlowTrackings", "To3Floor", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.ProjectWorkFlowTrackings", "To3Floor", c => c.DateTime());
            AlterColumn("dbo.ProjectWorkFlowTrackings", "AwardNotification", c => c.DateTime());
            AlterColumn("dbo.ProjectWorkFlowTrackings", "SecretaryShortlist", c => c.DateTime());
            AlterColumn("dbo.ProjectWorkFlowTrackings", "AmendedClosingDate", c => c.DateTime());
            AlterColumn("dbo.ProjectWorkFlowTrackings", "ClosingDate", c => c.DateTime());
            AlterColumn("dbo.ProjectWorkFlowTrackings", "AdvertisementDate", c => c.DateTime());
            AlterColumn("dbo.ProjectWorkFlowTrackings", "FedAideFHWA", c => c.DateTime());
            AlterColumn("dbo.ProjectWorkFlowTrackings", "ApprovedFromFHWA", c => c.DateTime());
            AlterColumn("dbo.ProjectWorkFlowTrackings", "DraftToFHWA", c => c.DateTime());
            AlterColumn("dbo.ProjectWorkFlowTrackings", "ApprovedFromChiefEngineer", c => c.DateTime());
            AlterColumn("dbo.ProjectWorkFlowTrackings", "DraftToChiefEngineer", c => c.DateTime());
            AlterColumn("dbo.ProjectWorkFlowTrackings", "ApprovedFromProjectManager", c => c.DateTime());
            AlterColumn("dbo.ProjectWorkFlowTrackings", "DraftToProjectManager", c => c.DateTime());
            AlterColumn("dbo.ProjectWorkFlowTrackings", "ApprovedFromSupervisor", c => c.DateTime());
            AlterColumn("dbo.ProjectWorkFlowTrackings", "DraftToSupervisor", c => c.DateTime());
            AlterColumn("dbo.ProjectWorkFlowTrackings", "ChiefApprovalToRetain", c => c.DateTime());
            AlterColumn("dbo.ProjectWorkFlowTrackings", "TaskAssignedToStaff", c => c.DateTime());
            AlterColumn("dbo.ProjectWorkFlowTrackings", "InfoReceived", c => c.DateTime());
        }
    }
}
