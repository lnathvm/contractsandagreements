// <auto-generated />
namespace ContractsAndAgreements_DataAccess.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.2.0-61023")]
    public sealed partial class Raa : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(Raa));
        
        string IMigrationMetadata.Id
        {
            get { return "201911051657209_Raa"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
