namespace ContractsAndAgreements_DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Aug17 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ProjectAddendumDetails", "ContractAmountAdjustment", c => c.String());
            AddColumn("dbo.ProjectGeneralInformations", "AdvertisedContractAmount", c => c.String());
            AddColumn("dbo.ProjectAdvertisementDetails", "FundingSource", c => c.String());
            AddColumn("dbo.ProjectWorkFlowTrackings", "Shortlist", c => c.String());
            AddColumn("dbo.ProjectWorkFlowTrackings", "PackettoSecretary", c => c.String());
            DropColumn("dbo.ProjectAddendumDetails", "ProjectDetails");
            DropColumn("dbo.ProjectAddendumDetails", "ProjectManagerId");
            DropColumn("dbo.ProjectAddendumDetails", "AddendumComments");
            DropColumn("dbo.ProjectAddendumDetails", "ContractType");
            DropColumn("dbo.ProjectAddendumDetails", "FeeType");
            DropColumn("dbo.ProjectAddendumDetails", "ContractAmount");
            DropColumn("dbo.ProjectAddendumDetails", "ManagerPhone");
            DropColumn("dbo.ProjectGeneralInformations", "EstContractAmount");
            DropColumn("dbo.ProjectWorkFlowTrackings", "DraftToChiefEngineer");
            DropColumn("dbo.ProjectWorkFlowTrackings", "ApprovedFromChiefEngineer");
            DropColumn("dbo.ProjectWorkFlowTrackings", "FedAideFHWA");
            DropColumn("dbo.ProjectWorkFlowTrackings", "SecretaryShortlist");
            DropColumn("dbo.ProjectWorkFlowTrackings", "To3Floor");
        }
        
        public override void Down()
        {
            AddColumn("dbo.ProjectWorkFlowTrackings", "To3Floor", c => c.String());
            AddColumn("dbo.ProjectWorkFlowTrackings", "SecretaryShortlist", c => c.String());
            AddColumn("dbo.ProjectWorkFlowTrackings", "FedAideFHWA", c => c.String());
            AddColumn("dbo.ProjectWorkFlowTrackings", "ApprovedFromChiefEngineer", c => c.String());
            AddColumn("dbo.ProjectWorkFlowTrackings", "DraftToChiefEngineer", c => c.String());
            AddColumn("dbo.ProjectGeneralInformations", "EstContractAmount", c => c.String());
            AddColumn("dbo.ProjectAddendumDetails", "ManagerPhone", c => c.String());
            AddColumn("dbo.ProjectAddendumDetails", "ContractAmount", c => c.String());
            AddColumn("dbo.ProjectAddendumDetails", "FeeType", c => c.Int(nullable: false));
            AddColumn("dbo.ProjectAddendumDetails", "ContractType", c => c.Int(nullable: false));
            AddColumn("dbo.ProjectAddendumDetails", "AddendumComments", c => c.String());
            AddColumn("dbo.ProjectAddendumDetails", "ProjectManagerId", c => c.Int(nullable: false));
            AddColumn("dbo.ProjectAddendumDetails", "ProjectDetails", c => c.String());
            DropColumn("dbo.ProjectWorkFlowTrackings", "PackettoSecretary");
            DropColumn("dbo.ProjectWorkFlowTrackings", "Shortlist");
            DropColumn("dbo.ProjectAdvertisementDetails", "FundingSource");
            DropColumn("dbo.ProjectGeneralInformations", "AdvertisedContractAmount");
            DropColumn("dbo.ProjectAddendumDetails", "ContractAmountAdjustment");
        }
    }
}
