namespace ContractsAndAgreements_DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class clean : DbMigration
    {
        public override void Up()
        {
            DropTable("dbo.ProcessorMasters");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.ProcessorMasters",
                c => new
                    {
                        SupervisorId = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        SupervisorEmail = c.String(),
                        IsActive = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.SupervisorId);
            
        }
    }
}
