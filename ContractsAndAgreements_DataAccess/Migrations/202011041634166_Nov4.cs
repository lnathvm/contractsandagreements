namespace ContractsAndAgreements_DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Nov4 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ConsultantProviderMasters", "ProofofInsurance", c => c.Boolean(nullable: false));
            AddColumn("dbo.ConsultantProviderMasters", "TypeofInsurance", c => c.Boolean(nullable: false));
            AddColumn("dbo.ConsultantProviderMasters", "InsuranceCompany", c => c.String());
            AddColumn("dbo.ConsultantProviderMasters", "EffectiveDateofCoverage", c => c.String());
            AddColumn("dbo.ConsultantProviderMasters", "ExpirationDateofCoverage", c => c.String());
            AddColumn("dbo.ConsultantProviderMasters", "DisclosureofOwnership", c => c.Boolean(nullable: false));
            AddColumn("dbo.ConsultantProviderMasters", "CertificateofAuthority", c => c.Boolean(nullable: false));
            AddColumn("dbo.ConsultantProviderMasters", "ResolutionCertificate", c => c.Boolean(nullable: false));
            AddColumn("dbo.ConsultantProviderMasters", "EffectiveDate", c => c.String());
            AddColumn("dbo.ConsultantProviderMasters", "PersonAuthorizedToSign", c => c.String());
            AddColumn("dbo.ConsultantProviderMasters", "Type", c => c.String());
            AddColumn("dbo.ConsultantProviderMasters", "Comments", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.ConsultantProviderMasters", "Comments");
            DropColumn("dbo.ConsultantProviderMasters", "Type");
            DropColumn("dbo.ConsultantProviderMasters", "PersonAuthorizedToSign");
            DropColumn("dbo.ConsultantProviderMasters", "EffectiveDate");
            DropColumn("dbo.ConsultantProviderMasters", "ResolutionCertificate");
            DropColumn("dbo.ConsultantProviderMasters", "CertificateofAuthority");
            DropColumn("dbo.ConsultantProviderMasters", "DisclosureofOwnership");
            DropColumn("dbo.ConsultantProviderMasters", "ExpirationDateofCoverage");
            DropColumn("dbo.ConsultantProviderMasters", "EffectiveDateofCoverage");
            DropColumn("dbo.ConsultantProviderMasters", "InsuranceCompany");
            DropColumn("dbo.ConsultantProviderMasters", "TypeofInsurance");
            DropColumn("dbo.ConsultantProviderMasters", "ProofofInsurance");
        }
    }
}
