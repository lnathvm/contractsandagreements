namespace ContractsAndAgreements_DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CreateProcessorTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ProcessorMasters",
                c => new
                    {
                        ProcessorId = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        ProcessorEmail = c.String(),
                        IsActive = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ProcessorId);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.ProcessorMasters");
        }
    }
}
