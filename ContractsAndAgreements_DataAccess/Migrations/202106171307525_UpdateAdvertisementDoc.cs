namespace ContractsAndAgreements_DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateAdvertisementDoc : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AdvertisementDocumentMasters", "IsPostToWeb", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.AdvertisementDocumentMasters", "IsPostToWeb");
        }
    }
}
