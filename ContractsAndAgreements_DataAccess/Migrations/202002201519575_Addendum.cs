namespace ContractsAndAgreements_DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Addendum : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ProjectAddendumDetails",
                c => new
                    {
                        ProjectAddendumId = c.Int(nullable: false, identity: true),
                        ProjectGeneralInformationId = c.Int(nullable: false),
                        ProjectAddendumWorkFlowId = c.Int(nullable: false),
                        ProjectDetails = c.String(),
                        MinimumManPower = c.String(),
                        FundingSource = c.String(),
                        ContractTime = c.Time(nullable: false, precision: 7),
                        ProjectManagerId = c.Int(nullable: false),
                        AddendumComments = c.String(),
                        FAPNo = c.String(),
                        Route = c.String(),
                        GeneralDescription = c.String(),
                        AddenDescription = c.String(),
                        ContractType = c.Int(nullable: false),
                        FeeType = c.Int(nullable: false),
                        ContractAmount = c.String(),
                        ManagerPhone = c.String(),
                        CreatedAt = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.ProjectAddendumId)
                .ForeignKey("dbo.ProjectAddendumWorkFlowTrackings", t => t.ProjectAddendumWorkFlowId, cascadeDelete: true)
                .ForeignKey("dbo.ProjectGeneralInformations", t => t.ProjectGeneralInformationId, cascadeDelete: true)
                .Index(t => t.ProjectGeneralInformationId)
                .Index(t => t.ProjectAddendumWorkFlowId);
            
            CreateTable(
                "dbo.ProjectAddendumWorkFlowTrackings",
                c => new
                    {
                        ProjectAddendumWorkFlowId = c.Int(nullable: false, identity: true),
                        SupervisorId = c.Int(nullable: false),
                        Processor = c.String(),
                        Phone = c.String(),
                        Comments = c.String(),
                        InfoReceived = c.String(),
                        TaskAssignedToStaff = c.String(),
                        AddendumDate = c.String(),
                        ClosingDate = c.String(),
                        CreatedAt = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.ProjectAddendumWorkFlowId)
                .ForeignKey("dbo.SuperVisorMasters", t => t.SupervisorId, cascadeDelete: true)
                .Index(t => t.SupervisorId);
            
            CreateTable(
                "dbo.ProjectPublishedDetails",
                c => new
                    {
                        ProjectPublishedId = c.Int(nullable: false, identity: true),
                        ProjectGeneralInformationId = c.Int(nullable: false),
                        ProjectDocumentId = c.Int(nullable: false),
                        AdvertismentId = c.Int(nullable: false),
                        AdvertismentType = c.String(),
                        IsAdvertisment = c.Boolean(nullable: false),
                        CreatedAt = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.ProjectPublishedId)
                .ForeignKey("dbo.ProjectGeneralInformations", t => t.ProjectGeneralInformationId, cascadeDelete: true)
                .Index(t => t.ProjectGeneralInformationId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ProjectPublishedDetails", "ProjectGeneralInformationId", "dbo.ProjectGeneralInformations");
            DropForeignKey("dbo.ProjectAddendumDetails", "ProjectGeneralInformationId", "dbo.ProjectGeneralInformations");
            DropForeignKey("dbo.ProjectAddendumDetails", "ProjectAddendumWorkFlowId", "dbo.ProjectAddendumWorkFlowTrackings");
            DropForeignKey("dbo.ProjectAddendumWorkFlowTrackings", "SupervisorId", "dbo.SuperVisorMasters");
            DropIndex("dbo.ProjectPublishedDetails", new[] { "ProjectGeneralInformationId" });
            DropIndex("dbo.ProjectAddendumWorkFlowTrackings", new[] { "SupervisorId" });
            DropIndex("dbo.ProjectAddendumDetails", new[] { "ProjectAddendumWorkFlowId" });
            DropIndex("dbo.ProjectAddendumDetails", new[] { "ProjectGeneralInformationId" });
            DropTable("dbo.ProjectPublishedDetails");
            DropTable("dbo.ProjectAddendumWorkFlowTrackings");
            DropTable("dbo.ProjectAddendumDetails");
        }
    }
}
