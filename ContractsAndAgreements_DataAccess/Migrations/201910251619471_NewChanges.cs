namespace ContractsAndAgreements_DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class NewChanges : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ProjectConsultantAreas", "ManagerPhone", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.ProjectConsultantAreas", "ManagerPhone");
        }
    }
}
