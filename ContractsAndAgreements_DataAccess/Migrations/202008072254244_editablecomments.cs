namespace ContractsAndAgreements_DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class editablecomments : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AdvertisementDocumentMasters", "DBEGoal", c => c.String());
            AddColumn("dbo.ProjectAdvertisementApparentSelections", "Comment1", c => c.String());
            AddColumn("dbo.ProjectAdvertisementApparentSelections", "Comment2", c => c.String());
            AddColumn("dbo.ProjectAdvertisementSelections", "Comment1", c => c.String());
            AddColumn("dbo.ProjectAdvertisementSelections", "Comment2", c => c.String());
            AddColumn("dbo.ProjectAdvertisementShortlists", "Comment1", c => c.String());
            AddColumn("dbo.ProjectAdvertisementShortlists", "Comment2", c => c.String());
            DropColumn("dbo.ProjectAdvertisementDetails", "DBEGoal");
        }
        
        public override void Down()
        {
            AddColumn("dbo.ProjectAdvertisementDetails", "DBEGoal", c => c.String());
            DropColumn("dbo.ProjectAdvertisementShortlists", "Comment2");
            DropColumn("dbo.ProjectAdvertisementShortlists", "Comment1");
            DropColumn("dbo.ProjectAdvertisementSelections", "Comment2");
            DropColumn("dbo.ProjectAdvertisementSelections", "Comment1");
            DropColumn("dbo.ProjectAdvertisementApparentSelections", "Comment2");
            DropColumn("dbo.ProjectAdvertisementApparentSelections", "Comment1");
            DropColumn("dbo.AdvertisementDocumentMasters", "DBEGoal");
        }
    }
}
