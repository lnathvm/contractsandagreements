namespace ContractsAndAgreements_DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SubContractor : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ProjectAdvertisementShortlistConsultantProviders",
                c => new
                    {
                        ProjectAdvertisementShortlistConsultantProviderId = c.Int(nullable: false, identity: true),
                        ProjectAdvertisementShortListId = c.Int(nullable: false),
                        SelectedShortlistConsultantProviderId = c.Int(nullable: false),
                        CreatedAt = c.DateTime(nullable: false),
                        UpdatedAt = c.DateTime(),
                    })
                .PrimaryKey(t => t.ProjectAdvertisementShortlistConsultantProviderId);
            
            AddColumn("dbo.ShortlistSubDetails", "ProjectAdvertisementShortlistConsultantProviderId", c => c.Int(nullable: false));
            DropColumn("dbo.ShortlistSubDetails", "ProjectAdvertisementShortlistId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.ShortlistSubDetails", "ProjectAdvertisementShortlistId", c => c.Int(nullable: false));
            DropColumn("dbo.ShortlistSubDetails", "ProjectAdvertisementShortlistConsultantProviderId");
            DropTable("dbo.ProjectAdvertisementShortlistConsultantProviders");
        }
    }
}
