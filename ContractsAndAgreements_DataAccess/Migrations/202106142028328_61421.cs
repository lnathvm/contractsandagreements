namespace ContractsAndAgreements_DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _61421 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.AdvertisementDocumentMasters", "AdvertisementDate", c => c.DateTime(nullable: false));
            AlterColumn("dbo.AdvertisementDocumentMasters", "ClosingDate", c => c.DateTime(nullable: false));
            AlterColumn("dbo.AdvertisementDocumentMasters", "AdvertDateGoesHere", c => c.DateTime(nullable: false));
            DropColumn("dbo.ProjectAdvertisementApparentSelections", "IsPostToWeb");
            DropColumn("dbo.ProjectAdvertisementSelections", "IsPostToWeb");
        }
        
        public override void Down()
        {
            AddColumn("dbo.ProjectAdvertisementSelections", "IsPostToWeb", c => c.Boolean(nullable: false));
            AddColumn("dbo.ProjectAdvertisementApparentSelections", "IsPostToWeb", c => c.Boolean(nullable: false));
            AlterColumn("dbo.AdvertisementDocumentMasters", "AdvertDateGoesHere", c => c.DateTime(nullable: false, storeType: "smalldatetime"));
            AlterColumn("dbo.AdvertisementDocumentMasters", "ClosingDate", c => c.DateTime(nullable: false, storeType: "smalldatetime"));
            AlterColumn("dbo.AdvertisementDocumentMasters", "AdvertisementDate", c => c.DateTime(nullable: false, storeType: "smalldatetime"));
        }
    }
}
