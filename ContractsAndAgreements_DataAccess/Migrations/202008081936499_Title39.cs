namespace ContractsAndAgreements_DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Title39 : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.ProjectAdvertisementApparentSelections", "Title39");
            DropColumn("dbo.ProjectAdvertisementShortlists", "Title39");
        }
        
        public override void Down()
        {
            AddColumn("dbo.ProjectAdvertisementShortlists", "Title39", c => c.String());
            AddColumn("dbo.ProjectAdvertisementApparentSelections", "Title39", c => c.String());
        }
    }
}
