namespace ContractsAndAgreements_DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _111820 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ContractInformationSubTaxDetails", "ContractAmount", c => c.String());
            AddColumn("dbo.ProjectExtraWorkLetterSubTaxDetails", "ContractAmount", c => c.String());
            AddColumn("dbo.ProjectOriginalContractSubTaxDetails", "ContractAmount", c => c.String());
            AddColumn("dbo.ProjectSupplementalAgreementSubTaxDetails", "ContractAmount", c => c.String());
            AddColumn("dbo.TaskOrderInformationSubTaxDetails", "ContractAmount", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.TaskOrderInformationSubTaxDetails", "ContractAmount");
            DropColumn("dbo.ProjectSupplementalAgreementSubTaxDetails", "ContractAmount");
            DropColumn("dbo.ProjectOriginalContractSubTaxDetails", "ContractAmount");
            DropColumn("dbo.ProjectExtraWorkLetterSubTaxDetails", "ContractAmount");
            DropColumn("dbo.ContractInformationSubTaxDetails", "ContractAmount");
        }
    }
}
