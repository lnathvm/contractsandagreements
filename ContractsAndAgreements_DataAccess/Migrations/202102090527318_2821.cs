namespace ContractsAndAgreements_DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _2821 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ProjectAdvertisementShortlists", "IsPostToWeb", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.ProjectAdvertisementShortlists", "IsPostToWeb");
        }
    }
}
