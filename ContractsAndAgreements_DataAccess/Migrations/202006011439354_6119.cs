namespace ContractsAndAgreements_DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _6119 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ContractInformationSubTaxDetails",
                c => new
                    {
                        ContractInformationSubTaxDetailsId = c.Int(nullable: false, identity: true),
                        ProjectContractInformationId = c.Int(nullable: false),
                        SubId = c.Int(nullable: false),
                        TaxId = c.String(),
                        CreatedAt = c.DateTime(nullable: false, storeType: "smalldatetime"),
                        UpdatedAt = c.DateTime(storeType: "smalldatetime"),
                    })
                .PrimaryKey(t => t.ContractInformationSubTaxDetailsId);
            
            CreateTable(
                "dbo.ProjectContractDetails",
                c => new
                    {
                        ProjectContractDetailsId = c.Int(nullable: false, identity: true),
                        ProjectGeneralInformationId = c.Int(nullable: false),
                        ProjectContractInformationId = c.Int(nullable: false),
                        ProjectContractWorkFlowTrackingId = c.Int(nullable: false),
                        ProjectDetails = c.String(),
                        ContractAction = c.String(),
                        ExpDate = c.String(),
                        ProjectManagerId = c.Int(nullable: false),
                        ManagerPhone = c.String(),
                        CreatedAt = c.DateTime(nullable: false),
                        UpdatedAt = c.DateTime(),
                    })
                .PrimaryKey(t => t.ProjectContractDetailsId)
                .ForeignKey("dbo.ProjectContractInformations", t => t.ProjectContractInformationId, cascadeDelete: true)
                .ForeignKey("dbo.ProjectContractWorkFlowTrackings", t => t.ProjectContractWorkFlowTrackingId, cascadeDelete: true)
                .ForeignKey("dbo.ProjectGeneralInformations", t => t.ProjectGeneralInformationId, cascadeDelete: true)
                .Index(t => t.ProjectGeneralInformationId)
                .Index(t => t.ProjectContractInformationId)
                .Index(t => t.ProjectContractWorkFlowTrackingId);
            
            CreateTable(
                "dbo.ProjectContractInformations",
                c => new
                    {
                        ProjectContractInformationId = c.Int(nullable: false, identity: true),
                        ContractAmount = c.String(),
                        Description = c.String(),
                        Prime = c.String(),
                        PrimeAmount = c.String(),
                        CreatedAt = c.DateTime(nullable: false),
                        UpdatedAt = c.DateTime(),
                    })
                .PrimaryKey(t => t.ProjectContractInformationId);
            
            CreateTable(
                "dbo.ProjectContractWorkFlowTrackings",
                c => new
                    {
                        ProjectContractWorkFlowTrackingId = c.Int(nullable: false, identity: true),
                        Supervisor = c.String(),
                        Processor = c.String(),
                        Phone = c.String(),
                        Comments = c.String(),
                        Suspended = c.Boolean(nullable: false),
                        InfoReceived = c.String(),
                        TaskAssignedToStaff = c.String(),
                        DraftandFeePacktoSupervisor = c.String(),
                        ApprovedfromSupervisor = c.String(),
                        DraftandFeePacktoProjectManager = c.String(),
                        ConcurredFromProjectManager = c.String(),
                        DrafttoConsultant = c.String(),
                        ConcurredFromConsultant = c.String(),
                        FedAideFHWAAuth = c.String(),
                        Authorized = c.String(),
                        ContractToConsultant = c.String(),
                        Signed = c.String(),
                        ContractTo3rdFloor = c.String(),
                        SignedExecuted = c.String(),
                        NTPLetter = c.String(),
                        EffectiveDate = c.String(),
                        CreatedAt = c.DateTime(nullable: false),
                        UpdatedAt = c.DateTime(),
                    })
                .PrimaryKey(t => t.ProjectContractWorkFlowTrackingId);
            
            CreateTable(
                "dbo.ProjectTaskOrderDetails",
                c => new
                    {
                        ProjectTaskOrderDetailsId = c.Int(nullable: false, identity: true),
                        ProjectGeneralInformationId = c.Int(nullable: false),
                        ProjectTaskOrderInformationId = c.Int(nullable: false),
                        TaskForProject = c.String(),
                        TaskScope = c.String(),
                        TaxClassification = c.String(),
                        TaskExpirationDate = c.String(),
                        ContractExpires = c.String(),
                        ProjectManagerId = c.Int(nullable: false),
                        ManagerPhone = c.String(),
                        CreatedAt = c.DateTime(nullable: false),
                        UpdatedAt = c.DateTime(),
                    })
                .PrimaryKey(t => t.ProjectTaskOrderDetailsId)
                .ForeignKey("dbo.ProjectGeneralInformations", t => t.ProjectGeneralInformationId, cascadeDelete: true)
                .ForeignKey("dbo.ProjectTaskOrderInformations", t => t.ProjectTaskOrderInformationId, cascadeDelete: true)
                .Index(t => t.ProjectGeneralInformationId)
                .Index(t => t.ProjectTaskOrderInformationId);
            
            CreateTable(
                "dbo.ProjectTaskOrderInformations",
                c => new
                    {
                        ProjectTaskOrderInformationId = c.Int(nullable: false, identity: true),
                        ContractAmount = c.String(),
                        TOAmount = c.String(),
                        Prime = c.String(),
                        PrimeAmount = c.String(),
                        CreatedAt = c.DateTime(nullable: false),
                        UpdatedAt = c.DateTime(),
                    })
                .PrimaryKey(t => t.ProjectTaskOrderInformationId);
            
            CreateTable(
                "dbo.TaskOrderInformationSubTaxDetails",
                c => new
                    {
                        TaskOrderInformationSubTaxDetailsId = c.Int(nullable: false, identity: true),
                        ProjectTaskOrderInformationId = c.Int(nullable: false),
                        SubId = c.Int(nullable: false),
                        TaxId = c.String(),
                        CreatedAt = c.DateTime(nullable: false, storeType: "smalldatetime"),
                        UpdatedAt = c.DateTime(storeType: "smalldatetime"),
                    })
                .PrimaryKey(t => t.TaskOrderInformationSubTaxDetailsId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ProjectTaskOrderDetails", "ProjectTaskOrderInformationId", "dbo.ProjectTaskOrderInformations");
            DropForeignKey("dbo.ProjectTaskOrderDetails", "ProjectGeneralInformationId", "dbo.ProjectGeneralInformations");
            DropForeignKey("dbo.ProjectContractDetails", "ProjectGeneralInformationId", "dbo.ProjectGeneralInformations");
            DropForeignKey("dbo.ProjectContractDetails", "ProjectContractWorkFlowTrackingId", "dbo.ProjectContractWorkFlowTrackings");
            DropForeignKey("dbo.ProjectContractDetails", "ProjectContractInformationId", "dbo.ProjectContractInformations");
            DropIndex("dbo.ProjectTaskOrderDetails", new[] { "ProjectTaskOrderInformationId" });
            DropIndex("dbo.ProjectTaskOrderDetails", new[] { "ProjectGeneralInformationId" });
            DropIndex("dbo.ProjectContractDetails", new[] { "ProjectContractWorkFlowTrackingId" });
            DropIndex("dbo.ProjectContractDetails", new[] { "ProjectContractInformationId" });
            DropIndex("dbo.ProjectContractDetails", new[] { "ProjectGeneralInformationId" });
            DropTable("dbo.TaskOrderInformationSubTaxDetails");
            DropTable("dbo.ProjectTaskOrderInformations");
            DropTable("dbo.ProjectTaskOrderDetails");
            DropTable("dbo.ProjectContractWorkFlowTrackings");
            DropTable("dbo.ProjectContractInformations");
            DropTable("dbo.ProjectContractDetails");
            DropTable("dbo.ContractInformationSubTaxDetails");
        }
    }
}
