namespace ContractsAndAgreements_DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Oct20 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ProjectExtraWorkLetterInformationDetails",
                c => new
                    {
                        ProjectExtraWorkLetterInformationId = c.Int(nullable: false, identity: true),
                        ProjectExtraWorkLetterDetailId = c.Int(nullable: false),
                        ContractAmount = c.String(),
                        EWLAmount = c.String(),
                        CreatedAt = c.DateTime(nullable: false),
                        UpdatedAt = c.DateTime(),
                    })
                .PrimaryKey(t => t.ProjectExtraWorkLetterInformationId)
                .ForeignKey("dbo.ProjectExtraWorkLetterDetails", t => t.ProjectExtraWorkLetterDetailId, cascadeDelete: true)
                .Index(t => t.ProjectExtraWorkLetterDetailId);
            
            CreateTable(
                "dbo.ProjectExtraWorkLetterSubTaxDetails",
                c => new
                    {
                        ProjectExtraWorkLetterSubTaxDetailId = c.Int(nullable: false, identity: true),
                        ProjectExtraWorkLetterInformationId = c.Int(nullable: false),
                        SubId = c.Int(nullable: false),
                        TaxId = c.String(),
                        CreatedAt = c.DateTime(nullable: false, storeType: "smalldatetime"),
                        UpdatedAt = c.DateTime(storeType: "smalldatetime"),
                    })
                .PrimaryKey(t => t.ProjectExtraWorkLetterSubTaxDetailId)
                .ForeignKey("dbo.ProjectExtraWorkLetterInformationDetails", t => t.ProjectExtraWorkLetterInformationId, cascadeDelete: true)
                .Index(t => t.ProjectExtraWorkLetterInformationId);
            
            CreateTable(
                "dbo.ProjectOriginalContractInformationDetails",
                c => new
                    {
                        ProjectOriginalContractInformationId = c.Int(nullable: false, identity: true),
                        ProjectOriginalContractDetailId = c.Int(nullable: false),
                        ContractAmount = c.String(),
                        CreatedAt = c.DateTime(nullable: false),
                        UpdatedAt = c.DateTime(),
                    })
                .PrimaryKey(t => t.ProjectOriginalContractInformationId)
                .ForeignKey("dbo.ProjectOriginalContractDetails", t => t.ProjectOriginalContractDetailId, cascadeDelete: true)
                .Index(t => t.ProjectOriginalContractDetailId);
            
            CreateTable(
                "dbo.ProjectOriginalContractSubTaxDetails",
                c => new
                    {
                        ProjectOriginalContractSubTaxDetailId = c.Int(nullable: false, identity: true),
                        ProjectOriginalContractInformationId = c.Int(nullable: false),
                        SubId = c.Int(nullable: false),
                        TaxId = c.String(),
                        CreatedAt = c.DateTime(nullable: false, storeType: "smalldatetime"),
                        UpdatedAt = c.DateTime(storeType: "smalldatetime"),
                    })
                .PrimaryKey(t => t.ProjectOriginalContractSubTaxDetailId)
                .ForeignKey("dbo.ProjectOriginalContractInformationDetails", t => t.ProjectOriginalContractInformationId, cascadeDelete: true)
                .Index(t => t.ProjectOriginalContractInformationId);
            
            CreateTable(
                "dbo.ProjectSupplementalAgreementInformationDetails",
                c => new
                    {
                        ProjectSupplementalAgreementInformationId = c.Int(nullable: false, identity: true),
                        ProjectSupplementalAgreementDetailId = c.Int(nullable: false),
                        ContractAmount = c.String(),
                        SAAmount = c.String(),
                        CreatedAt = c.DateTime(nullable: false),
                        UpdatedAt = c.DateTime(),
                    })
                .PrimaryKey(t => t.ProjectSupplementalAgreementInformationId)
                .ForeignKey("dbo.ProjectSupplementalAgreementDetails", t => t.ProjectSupplementalAgreementDetailId, cascadeDelete: true)
                .Index(t => t.ProjectSupplementalAgreementDetailId);
            
            CreateTable(
                "dbo.ProjectSupplementalAgreementSubTaxDetails",
                c => new
                    {
                        ProjectSupplementalAgreementSubTaxDetailId = c.Int(nullable: false, identity: true),
                        ProjectSupplementalAgreementInformationId = c.Int(nullable: false),
                        SubId = c.Int(nullable: false),
                        TaxId = c.String(),
                        CreatedAt = c.DateTime(nullable: false, storeType: "smalldatetime"),
                        UpdatedAt = c.DateTime(storeType: "smalldatetime"),
                    })
                .PrimaryKey(t => t.ProjectSupplementalAgreementSubTaxDetailId)
                .ForeignKey("dbo.ProjectSupplementalAgreementInformationDetails", t => t.ProjectSupplementalAgreementInformationId, cascadeDelete: true)
                .Index(t => t.ProjectSupplementalAgreementInformationId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ProjectSupplementalAgreementSubTaxDetails", "ProjectSupplementalAgreementInformationId", "dbo.ProjectSupplementalAgreementInformationDetails");
            DropForeignKey("dbo.ProjectSupplementalAgreementInformationDetails", "ProjectSupplementalAgreementDetailId", "dbo.ProjectSupplementalAgreementDetails");
            DropForeignKey("dbo.ProjectOriginalContractSubTaxDetails", "ProjectOriginalContractInformationId", "dbo.ProjectOriginalContractInformationDetails");
            DropForeignKey("dbo.ProjectOriginalContractInformationDetails", "ProjectOriginalContractDetailId", "dbo.ProjectOriginalContractDetails");
            DropForeignKey("dbo.ProjectExtraWorkLetterSubTaxDetails", "ProjectExtraWorkLetterInformationId", "dbo.ProjectExtraWorkLetterInformationDetails");
            DropForeignKey("dbo.ProjectExtraWorkLetterInformationDetails", "ProjectExtraWorkLetterDetailId", "dbo.ProjectExtraWorkLetterDetails");
            DropIndex("dbo.ProjectSupplementalAgreementSubTaxDetails", new[] { "ProjectSupplementalAgreementInformationId" });
            DropIndex("dbo.ProjectSupplementalAgreementInformationDetails", new[] { "ProjectSupplementalAgreementDetailId" });
            DropIndex("dbo.ProjectOriginalContractSubTaxDetails", new[] { "ProjectOriginalContractInformationId" });
            DropIndex("dbo.ProjectOriginalContractInformationDetails", new[] { "ProjectOriginalContractDetailId" });
            DropIndex("dbo.ProjectExtraWorkLetterSubTaxDetails", new[] { "ProjectExtraWorkLetterInformationId" });
            DropIndex("dbo.ProjectExtraWorkLetterInformationDetails", new[] { "ProjectExtraWorkLetterDetailId" });
            DropTable("dbo.ProjectSupplementalAgreementSubTaxDetails");
            DropTable("dbo.ProjectSupplementalAgreementInformationDetails");
            DropTable("dbo.ProjectOriginalContractSubTaxDetails");
            DropTable("dbo.ProjectOriginalContractInformationDetails");
            DropTable("dbo.ProjectExtraWorkLetterSubTaxDetails");
            DropTable("dbo.ProjectExtraWorkLetterInformationDetails");
        }
    }
}
