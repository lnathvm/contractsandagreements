namespace ContractsAndAgreements_DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Aug21 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AddendumDocumentMasters", "ContractNo", c => c.String());
            AddColumn("dbo.AddendumDocumentMasters", "ProjectName", c => c.String());
            AddColumn("dbo.AddendumDocumentMasters", "FAPNo", c => c.String());
            AddColumn("dbo.AddendumDocumentMasters", "Parish", c => c.String());
            DropColumn("dbo.AddendumDocumentMasters", "FederalAidProjectNo");
            DropColumn("dbo.AddendumDocumentMasters", "Idiqcpm");
            DropColumn("dbo.AddendumDocumentMasters", "Route");
            DropColumn("dbo.AddendumDocumentMasters", "StateWideParish");
        }
        
        public override void Down()
        {
            AddColumn("dbo.AddendumDocumentMasters", "StateWideParish", c => c.String());
            AddColumn("dbo.AddendumDocumentMasters", "Route", c => c.String());
            AddColumn("dbo.AddendumDocumentMasters", "Idiqcpm", c => c.String());
            AddColumn("dbo.AddendumDocumentMasters", "FederalAidProjectNo", c => c.String());
            DropColumn("dbo.AddendumDocumentMasters", "Parish");
            DropColumn("dbo.AddendumDocumentMasters", "FAPNo");
            DropColumn("dbo.AddendumDocumentMasters", "ProjectName");
            DropColumn("dbo.AddendumDocumentMasters", "ContractNo");
        }
    }
}
