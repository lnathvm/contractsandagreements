namespace ContractsAndAgreements_DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class newd : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.ProjectSupplementalAgreementForIDIQs", "ProjectGeneralInformationId", "dbo.ProjectGeneralInformations");
            DropIndex("dbo.ProjectSupplementalAgreementForIDIQs", new[] { "ProjectGeneralInformationId" });
            AddColumn("dbo.ProjectAdvertisementApparentSelections", "Rank", c => c.Int(nullable: false));
            AddColumn("dbo.ProjectAdvertisementSelections", "Rank", c => c.Int(nullable: false));
            AddColumn("dbo.ProjectAdvertisementShortlistConsultantProviders", "Rank", c => c.Int(nullable: false));
           
        }
        
        public override void Down()
        {
            AddColumn("dbo.ProjectSupplementalAgreementForIDIQs", "ManagerPhone", c => c.String());
            AddColumn("dbo.ProjectSupplementalAgreementForIDIQs", "ProjectManagerId", c => c.Int(nullable: false));
            AddColumn("dbo.ProjectAdvertisementShortlists", "StateProjectno", c => c.String());
            AddColumn("dbo.ProjectAdvertisementShortlists", "ContractNo", c => c.String());
            AddColumn("dbo.ProjectAdvertisementSelections", "StateProjectno", c => c.String());
            AddColumn("dbo.ProjectAdvertisementSelections", "ContractNo", c => c.String());
            AddColumn("dbo.AdvertisementDocumentMasters", "DBEGoal", c => c.String());
            AddColumn("dbo.AdvertisementDocumentMasters", "Parish", c => c.String());
            AddColumn("dbo.AdvertisementDocumentMasters", "FAPNo", c => c.String());
            AddColumn("dbo.AdvertisementDocumentMasters", "StateProjectNo", c => c.String());
            AddColumn("dbo.AdvertisementDocumentMasters", "ProjectName", c => c.String());
            AddColumn("dbo.AdvertisementDocumentMasters", "ContractNo", c => c.String());
            DropColumn("dbo.ProjectAdvertisementShortlistConsultantProviders", "Rank");
            DropColumn("dbo.ProjectAdvertisementSelections", "Rank");
            DropColumn("dbo.ProjectAdvertisementApparentSelections", "Rank");
            CreateIndex("dbo.ProjectSupplementalAgreementForIDIQs", "ProjectGeneralInformationId");
            AddForeignKey("dbo.ProjectSupplementalAgreementForIDIQs", "ProjectGeneralInformationId", "dbo.ProjectGeneralInformations", "ProjectGeneralInformationId", cascadeDelete: true);
        }
    }
}
