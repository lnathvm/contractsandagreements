// <auto-generated />
namespace ContractsAndAgreements_DataAccess.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.2.0-61023")]
    public sealed partial class Title39 : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(Title39));
        
        string IMigrationMetadata.Id
        {
            get { return "202008081936499_Title39"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
