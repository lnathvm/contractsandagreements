namespace ContractsAndAgreements_DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Aug25 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AddendumDocumentMasters", "DBEGoel", c => c.String());
            AddColumn("dbo.ProjectAddendumDetails", "DBEGoel", c => c.String());
            AddColumn("dbo.ProjectGeneralInformations", "DBEGoel", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.ProjectGeneralInformations", "DBEGoel");
            DropColumn("dbo.ProjectAddendumDetails", "DBEGoel");
            DropColumn("dbo.AddendumDocumentMasters", "DBEGoel");
        }
    }
}
