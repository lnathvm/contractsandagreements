namespace ContractsAndAgreements_DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class NewDBC : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ConsultantAreaSubTaxDetails",
                c => new
                    {
                        ConsultantAreaSubTaxDetailsId = c.Int(nullable: false, identity: true),
                        ProjectConsultantAreaId = c.Int(nullable: false),
                        SubId = c.Int(nullable: false),
                        TaxId = c.String(),
                        CreatedAt = c.DateTime(nullable: false, storeType: "smalldatetime"),
                        UpdatedAt = c.DateTime(storeType: "smalldatetime"),
                    })
                .PrimaryKey(t => t.ConsultantAreaSubTaxDetailsId)
                .ForeignKey("dbo.ProjectConsultantAreas", t => t.ProjectConsultantAreaId, cascadeDelete: true)
                .Index(t => t.ProjectConsultantAreaId);
            
            DropTable("dbo.ConsultantAreaSubTaxMasters");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.ConsultantAreaSubTaxMasters",
                c => new
                    {
                        ConsultantAreaSubTaxMasterId = c.Int(nullable: false, identity: true),
                        SubName = c.String(),
                        IsActive = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ConsultantAreaSubTaxMasterId);
            
            DropForeignKey("dbo.ConsultantAreaSubTaxDetails", "ProjectConsultantAreaId", "dbo.ProjectConsultantAreas");
            DropIndex("dbo.ConsultantAreaSubTaxDetails", new[] { "ProjectConsultantAreaId" });
            DropTable("dbo.ConsultantAreaSubTaxDetails");
        }
    }
}
