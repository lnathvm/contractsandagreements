namespace ContractsAndAgreements_DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _111620 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.CompensationTypeMasters",
                c => new
                    {
                        CompensationTypeId = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        IsActive = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.CompensationTypeId);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.CompensationTypeMasters");
        }
    }
}
