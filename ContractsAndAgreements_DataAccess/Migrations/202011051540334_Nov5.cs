namespace ContractsAndAgreements_DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Nov5 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ProjectSupplementalAgreementInformationForIDIQSubTaxDetails", "ContractAmount", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.ProjectSupplementalAgreementInformationForIDIQSubTaxDetails", "ContractAmount");
        }
    }
}
