namespace ContractsAndAgreements_DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class MB : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.ProjectAdvertisementShortlists", "ShortList");
        }
        
        public override void Down()
        {
            AddColumn("dbo.ProjectAdvertisementShortlists", "ShortList", c => c.String());
        }
    }
}
