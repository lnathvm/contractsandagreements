namespace ContractsAndAgreements_DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Clientreqchanges : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ProjectGeneralInformations", "ContractNo", c => c.String());
            DropColumn("dbo.ProjectGeneralInformations", "LegacySPNo");
            DropColumn("dbo.ProjectGeneralInformations", "EstimatedConstructionCost");
            DropColumn("dbo.ProjectGeneralInformations", "TimedProject");
        }
        
        public override void Down()
        {
            AddColumn("dbo.ProjectGeneralInformations", "TimedProject", c => c.Boolean(nullable: false));
            AddColumn("dbo.ProjectGeneralInformations", "EstimatedConstructionCost", c => c.String());
            AddColumn("dbo.ProjectGeneralInformations", "LegacySPNo", c => c.String());
            DropColumn("dbo.ProjectGeneralInformations", "ContractNo");
        }
    }
}
