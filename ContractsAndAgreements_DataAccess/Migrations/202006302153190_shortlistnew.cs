namespace ContractsAndAgreements_DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class shortlistnew : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.AdvertisementDocumentMasters",
                c => new
                    {
                        AdvertisementDocumentMasterId = c.Int(nullable: false, identity: true),
                        ProjectAdvertisementDetailsId = c.Int(nullable: false),
                        AdvertisementDate = c.DateTime(nullable: false),
                        ClosingDate = c.DateTime(nullable: false),
                        AdvertDateGoesHere = c.DateTime(nullable: false),
                        TypeOfAdvertGoesHere = c.String(),
                        Details = c.String(),
                        Document = c.String(),
                        CreatedAt = c.DateTime(nullable: false),
                        UpdatedAt = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.AdvertisementDocumentMasterId)
                .ForeignKey("dbo.ProjectAdvertisementDetails", t => t.ProjectAdvertisementDetailsId, cascadeDelete: true)
                .Index(t => t.ProjectAdvertisementDetailsId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AdvertisementDocumentMasters", "ProjectAdvertisementDetailsId", "dbo.ProjectAdvertisementDetails");
            DropIndex("dbo.AdvertisementDocumentMasters", new[] { "ProjectAdvertisementDetailsId" });
            DropTable("dbo.AdvertisementDocumentMasters");
        }
    }
}
