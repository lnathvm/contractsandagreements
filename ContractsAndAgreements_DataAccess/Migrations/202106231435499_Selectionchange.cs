namespace ContractsAndAgreements_DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Selectionchange : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ProjectAdvertisementSelections", "IsPostToWeb", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.ProjectAdvertisementSelections", "IsPostToWeb");
        }
    }
}
