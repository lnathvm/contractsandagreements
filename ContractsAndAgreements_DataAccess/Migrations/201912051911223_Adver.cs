namespace ContractsAndAgreements_DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Adver : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ProjectAdvertisementDetails",
                c => new
                    {
                        ProjectAdvertisementDetailsId = c.Int(nullable: false, identity: true),
                        ProjectGeneralInformationId = c.Int(nullable: false),
                        ProjectWorkFlowTrackingId = c.Int(nullable: false),
                        ProjectDetails = c.String(),
                        MinimumManPower = c.String(),
                        FundingSource = c.String(),
                        ContractTime = c.Time(nullable: false, precision: 7),
                        CreatedAt = c.DateTime(nullable: false),
                        UpdatedAt = c.DateTime(),
                    })
                .PrimaryKey(t => t.ProjectAdvertisementDetailsId)
                .ForeignKey("dbo.ProjectGeneralInformations", t => t.ProjectGeneralInformationId, cascadeDelete: true)
                .ForeignKey("dbo.ProjectWorkFlowTrackings", t => t.ProjectWorkFlowTrackingId, cascadeDelete: true)
                .Index(t => t.ProjectGeneralInformationId)
                .Index(t => t.ProjectWorkFlowTrackingId);
            
            CreateTable(
                "dbo.ProjectWorkFlowTrackings",
                c => new
                    {
                        ProjectWorkFlowTrackingId = c.Int(nullable: false, identity: true),
                        Supervisor = c.String(),
                        Processor = c.String(),
                        Phone = c.String(),
                        Comments = c.String(),
                        Suspended = c.Boolean(nullable: false),
                        InfoReceived = c.DateTime(),
                        TaskAssignedToStaff = c.DateTime(),
                        ChiefApprovalToRetain = c.DateTime(),
                        DraftToSupervisor = c.DateTime(),
                        ApprovedFromSupervisor = c.DateTime(),
                        DraftToProjectManager = c.DateTime(),
                        ApprovedFromProjectManager = c.DateTime(),
                        DraftToChiefEngineer = c.DateTime(),
                        ApprovedFromChiefEngineer = c.DateTime(),
                        DraftToFHWA = c.DateTime(),
                        ApprovedFromFHWA = c.DateTime(),
                        FedAideFHWA = c.DateTime(),
                        AdvertisementDate = c.DateTime(),
                        ClosingDate = c.DateTime(),
                        AmendedClosingDate = c.DateTime(),
                        SecretaryShortlist = c.DateTime(),
                        AwardNotification = c.DateTime(),
                        To3Floor = c.DateTime(),
                        CreatedAt = c.DateTime(nullable: false),
                        UpdatedAt = c.DateTime(),
                    })
                .PrimaryKey(t => t.ProjectWorkFlowTrackingId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ProjectAdvertisementDetails", "ProjectWorkFlowTrackingId", "dbo.ProjectWorkFlowTrackings");
            DropForeignKey("dbo.ProjectAdvertisementDetails", "ProjectGeneralInformationId", "dbo.ProjectGeneralInformations");
            DropIndex("dbo.ProjectAdvertisementDetails", new[] { "ProjectWorkFlowTrackingId" });
            DropIndex("dbo.ProjectAdvertisementDetails", new[] { "ProjectGeneralInformationId" });
            DropTable("dbo.ProjectWorkFlowTrackings");
            DropTable("dbo.ProjectAdvertisementDetails");
        }
    }
}
