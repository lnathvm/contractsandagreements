namespace ContractsAndAgreements_DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ContractChangess : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ProjectSupplementalAgreementForIDIQs", "SAForProject", c => c.String());
            AddColumn("dbo.ProjectSupplementalAgreementForIDIQs", "SAScope", c => c.String());
            AddColumn("dbo.ProjectSupplementalAgreementForIDIQs", "SAExpirationDate", c => c.String());
            AlterColumn("dbo.ContractInformationSubTaxDetails", "ContractAmount", c => c.Decimal(precision: 18, scale: 2));
            AlterColumn("dbo.ProjectContractInformations", "ContractAmount", c => c.Decimal(precision: 18, scale: 2));
            AlterColumn("dbo.ProjectContractInformations", "PrimeAmount", c => c.Decimal(precision: 18, scale: 2));
            AlterColumn("dbo.ProjectExtraWorkLetterInformationDetails", "ContractAmount", c => c.Decimal(precision: 18, scale: 2));
            AlterColumn("dbo.ProjectExtraWorkLetterInformationDetails", "EWLAmount", c => c.Decimal(precision: 18, scale: 2));
            AlterColumn("dbo.ProjectExtraWorkLetterSubTaxDetails", "ContractAmount", c => c.Decimal(precision: 18, scale: 2));
            AlterColumn("dbo.ProjectOriginalContractInformationDetails", "ContractAmount", c => c.Decimal(precision: 18, scale: 2));
            AlterColumn("dbo.ProjectOriginalContractSubTaxDetails", "ContractAmount", c => c.Decimal(precision: 18, scale: 2));
            AlterColumn("dbo.ProjectSupplementalAgreementInformationForIDIQs", "ContractAmount", c => c.Decimal(precision: 18, scale: 2));
            AlterColumn("dbo.ProjectSupplementalAgreementInformationForIDIQs", "TOAmount", c => c.Decimal(precision: 18, scale: 2));
            AlterColumn("dbo.ProjectSupplementalAgreementInformationForIDIQs", "PrimeAmount", c => c.Decimal(precision: 18, scale: 2));
            AlterColumn("dbo.ProjectSupplementalAgreementInformationDetails", "ContractAmount", c => c.Decimal(precision: 18, scale: 2));
            AlterColumn("dbo.ProjectSupplementalAgreementInformationDetails", "SAAmount", c => c.Decimal(precision: 18, scale: 2));
            AlterColumn("dbo.ProjectSupplementalAgreementInformationForIDIQSubTaxDetails", "ContractAmount", c => c.Decimal(precision: 18, scale: 2));
            AlterColumn("dbo.ProjectSupplementalAgreementSubTaxDetails", "ContractAmount", c => c.Decimal(precision: 18, scale: 2));
            AlterColumn("dbo.ProjectTaskOrderInformations", "ContractAmount", c => c.Decimal(precision: 18, scale: 2));
            AlterColumn("dbo.ProjectTaskOrderInformations", "TOAmount", c => c.Decimal(precision: 18, scale: 2));
            AlterColumn("dbo.ProjectTaskOrderInformations", "PrimeAmount", c => c.Decimal(precision: 18, scale: 2));
            DropColumn("dbo.ProjectSupplementalAgreementForIDIQs", "TaskForProject");
            DropColumn("dbo.ProjectSupplementalAgreementForIDIQs", "TaskScope");
            DropColumn("dbo.ProjectSupplementalAgreementForIDIQs", "TaskExpirationDate");
        }
        
        public override void Down()
        {
            AddColumn("dbo.ProjectSupplementalAgreementForIDIQs", "TaskExpirationDate", c => c.String());
            AddColumn("dbo.ProjectSupplementalAgreementForIDIQs", "TaskScope", c => c.String());
            AddColumn("dbo.ProjectSupplementalAgreementForIDIQs", "TaskForProject", c => c.String());
            AlterColumn("dbo.ProjectTaskOrderInformations", "PrimeAmount", c => c.String());
            AlterColumn("dbo.ProjectTaskOrderInformations", "TOAmount", c => c.String());
            AlterColumn("dbo.ProjectTaskOrderInformations", "ContractAmount", c => c.String());
            AlterColumn("dbo.ProjectSupplementalAgreementSubTaxDetails", "ContractAmount", c => c.String());
            AlterColumn("dbo.ProjectSupplementalAgreementInformationForIDIQSubTaxDetails", "ContractAmount", c => c.String());
            AlterColumn("dbo.ProjectSupplementalAgreementInformationDetails", "SAAmount", c => c.String());
            AlterColumn("dbo.ProjectSupplementalAgreementInformationDetails", "ContractAmount", c => c.String());
            AlterColumn("dbo.ProjectSupplementalAgreementInformationForIDIQs", "PrimeAmount", c => c.String());
            AlterColumn("dbo.ProjectSupplementalAgreementInformationForIDIQs", "TOAmount", c => c.String());
            AlterColumn("dbo.ProjectSupplementalAgreementInformationForIDIQs", "ContractAmount", c => c.String());
            AlterColumn("dbo.ProjectOriginalContractSubTaxDetails", "ContractAmount", c => c.String());
            AlterColumn("dbo.ProjectOriginalContractInformationDetails", "ContractAmount", c => c.String());
            AlterColumn("dbo.ProjectExtraWorkLetterSubTaxDetails", "ContractAmount", c => c.String());
            AlterColumn("dbo.ProjectExtraWorkLetterInformationDetails", "EWLAmount", c => c.String());
            AlterColumn("dbo.ProjectExtraWorkLetterInformationDetails", "ContractAmount", c => c.String());
            AlterColumn("dbo.ProjectContractInformations", "PrimeAmount", c => c.String());
            AlterColumn("dbo.ProjectContractInformations", "ContractAmount", c => c.String());
            AlterColumn("dbo.ContractInformationSubTaxDetails", "ContractAmount", c => c.String());
            DropColumn("dbo.ProjectSupplementalAgreementForIDIQs", "SAExpirationDate");
            DropColumn("dbo.ProjectSupplementalAgreementForIDIQs", "SAScope");
            DropColumn("dbo.ProjectSupplementalAgreementForIDIQs", "SAForProject");
        }
    }
}
