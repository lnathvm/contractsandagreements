namespace ContractsAndAgreements_DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _10620 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ProjectSupplementalAgreementForIDIQs",
                c => new
                    {
                        ProjectSupplementalAgreementForIDIQId = c.Int(nullable: false, identity: true),
                        ProjectGeneralInformationId = c.Int(nullable: false),
                        ProjectSupplementalAgreementInformationForIDIQId = c.Int(nullable: false),
                        TaskForProject = c.String(),
                        TaskScope = c.String(),
                        TaxClassification = c.String(),
                        TaskExpirationDate = c.String(),
                        ContractExpires = c.String(),
                        ProjectManagerId = c.Int(nullable: false),
                        ManagerPhone = c.String(),
                        CreatedAt = c.DateTime(nullable: false),
                        UpdatedAt = c.DateTime(),
                    })
                .PrimaryKey(t => t.ProjectSupplementalAgreementForIDIQId)
                .ForeignKey("dbo.ProjectGeneralInformations", t => t.ProjectGeneralInformationId, cascadeDelete: true)
                .ForeignKey("dbo.ProjectSupplementalAgreementInformationForIDIQs", t => t.ProjectSupplementalAgreementInformationForIDIQId, cascadeDelete: true)
                .Index(t => t.ProjectGeneralInformationId)
                .Index(t => t.ProjectSupplementalAgreementInformationForIDIQId);
            
            CreateTable(
                "dbo.ProjectSupplementalAgreementInformationForIDIQs",
                c => new
                    {
                        ProjectSupplementalAgreementInformationForIDIQId = c.Int(nullable: false, identity: true),
                        ContractAmount = c.String(),
                        TOAmount = c.String(),
                        Prime = c.String(),
                        PrimeAmount = c.String(),
                        CreatedAt = c.DateTime(nullable: false),
                        UpdatedAt = c.DateTime(),
                    })
                .PrimaryKey(t => t.ProjectSupplementalAgreementInformationForIDIQId);
            
            CreateTable(
                "dbo.ProjectSupplementalAgreementInformationForIDIQSubTaxDetails",
                c => new
                    {
                        ProjectSupplementalAgreementInformationForIDIQSubTaxDetailsId = c.Int(nullable: false, identity: true),
                        ProjectSupplementalAgreementInformationForIDIQId = c.Int(nullable: false),
                        SubId = c.Int(nullable: false),
                        TaxId = c.String(),
                        CreatedAt = c.DateTime(nullable: false, storeType: "smalldatetime"),
                        UpdatedAt = c.DateTime(storeType: "smalldatetime"),
                    })
                .PrimaryKey(t => t.ProjectSupplementalAgreementInformationForIDIQSubTaxDetailsId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ProjectSupplementalAgreementForIDIQs", "ProjectSupplementalAgreementInformationForIDIQId", "dbo.ProjectSupplementalAgreementInformationForIDIQs");
            DropForeignKey("dbo.ProjectSupplementalAgreementForIDIQs", "ProjectGeneralInformationId", "dbo.ProjectGeneralInformations");
            DropIndex("dbo.ProjectSupplementalAgreementForIDIQs", new[] { "ProjectSupplementalAgreementInformationForIDIQId" });
            DropIndex("dbo.ProjectSupplementalAgreementForIDIQs", new[] { "ProjectGeneralInformationId" });
            DropTable("dbo.ProjectSupplementalAgreementInformationForIDIQSubTaxDetails");
            DropTable("dbo.ProjectSupplementalAgreementInformationForIDIQs");
            DropTable("dbo.ProjectSupplementalAgreementForIDIQs");
        }
    }
}
