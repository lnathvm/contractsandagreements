namespace ContractsAndAgreements_DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _2321 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ContractAgencyMasters", "ContractAgency", c => c.String());
            DropColumn("dbo.ContractAgencyMasters", "ContractAgencyy");
        }
        
        public override void Down()
        {
            AddColumn("dbo.ContractAgencyMasters", "ContractAgencyy", c => c.String());
            DropColumn("dbo.ContractAgencyMasters", "ContractAgency");
        }
    }
}
