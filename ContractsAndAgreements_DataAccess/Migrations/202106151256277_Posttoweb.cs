namespace ContractsAndAgreements_DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Posttoweb : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ProjectAdvertisementApparentSelections", "IsPostToWeb", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.ProjectAdvertisementApparentSelections", "IsPostToWeb");
        }
    }
}
