namespace ContractsAndAgreements_DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class sep28 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ProjectExtraWorkLetterDetails",
                c => new
                    {
                        ProjectExtraWorkLetterDetailId = c.Int(nullable: false, identity: true),
                        ProjectGeneralInformationId = c.Int(nullable: false),
                        ReasonforEWL = c.String(),
                        ExtraWorkAction = c.String(),
                        ContractExpries = c.String(),
                        Commnets = c.String(),
                        InformationAmount = c.String(),
                        InfoRecAndLogged = c.String(),
                        TaskAssignedToStaff = c.String(),
                        NTPLetter = c.String(),
                        NTPDate = c.String(),
                        CreatedAt = c.DateTime(nullable: false),
                        UpdatedAt = c.DateTime(),
                    })
                .PrimaryKey(t => t.ProjectExtraWorkLetterDetailId)
                .ForeignKey("dbo.ProjectGeneralInformations", t => t.ProjectGeneralInformationId, cascadeDelete: true)
                .Index(t => t.ProjectGeneralInformationId);
            
            CreateTable(
                "dbo.ProjectOriginalContractDetails",
                c => new
                    {
                        ProjectOriginalContractDetailId = c.Int(nullable: false, identity: true),
                        ProjectGeneralInformationId = c.Int(nullable: false),
                        ContractAction = c.String(),
                        ContractExpries = c.String(),
                        Commnets = c.String(),
                        InfoRecAndLogged = c.String(),
                        TaskAssignedToStaff = c.String(),
                        DrafttoSupervisor = c.String(),
                        ApprovedfromSupervisor = c.String(),
                        FeePackagetoChiefEngineer = c.String(),
                        ApprovedFromFeePackagetoChiefEngineer = c.String(),
                        FeeNegotiationsTOProjMgr = c.String(),
                        Complete = c.String(),
                        FedAideFHWAANDAuth = c.String(),
                        Authorized = c.String(),
                        DraftToManagerForReview = c.String(),
                        DraftToManagerForReviewConcurred = c.String(),
                        DraftToConsultantForReview = c.String(),
                        DraftToConsultantForReviewConcurred = c.String(),
                        ContractToConsultant = c.String(),
                        ContractTo3rdFloor = c.String(),
                        SignedExecuted = c.String(),
                        NTPLetter = c.String(),
                        NTPDate = c.String(),
                        CreatedAt = c.DateTime(nullable: false),
                        UpdatedAt = c.DateTime(),
                    })
                .PrimaryKey(t => t.ProjectOriginalContractDetailId)
                .ForeignKey("dbo.ProjectGeneralInformations", t => t.ProjectGeneralInformationId, cascadeDelete: true)
                .Index(t => t.ProjectGeneralInformationId);
            
            CreateTable(
                "dbo.ProjectSupplementalAgreementDetails",
                c => new
                    {
                        ProjectSupplementalAgreementDetailId = c.Int(nullable: false, identity: true),
                        ProjectGeneralInformationId = c.Int(nullable: false),
                        ReasonForSA = c.String(),
                        SADescription = c.String(),
                        SAAction = c.String(),
                        SAFeeType = c.String(),
                        SAExpries = c.String(),
                        ContractExpries = c.String(),
                        Commnets = c.String(),
                        InfoRecAndLogged = c.String(),
                        TaskAssignedToStaff = c.String(),
                        DrafttoSupervisor = c.String(),
                        ApprovedfromSupervisor = c.String(),
                        FeePackagetoChiefEngineer = c.String(),
                        ApprovedFromFeePackagetoChiefEngineer = c.String(),
                        FeeNegotiationsTOProjMgr = c.String(),
                        Complete = c.String(),
                        FedAideFHWA = c.String(),
                        Authorized = c.String(),
                        DraftToManagerForReview = c.String(),
                        DraftToManagerForReviewConcurred = c.String(),
                        DraftToConsultantForReview = c.String(),
                        DraftToConsultantForReviewConcurred = c.String(),
                        ContractToConsultant = c.String(),
                        Signed = c.String(),
                        ContractTo3rdFloor = c.String(),
                        SignedExecuted = c.String(),
                        NTPLetter = c.String(),
                        NTPDate = c.String(),
                        CreatedAt = c.DateTime(nullable: false),
                        UpdatedAt = c.DateTime(),
                    })
                .PrimaryKey(t => t.ProjectSupplementalAgreementDetailId)
                .ForeignKey("dbo.ProjectGeneralInformations", t => t.ProjectGeneralInformationId, cascadeDelete: true)
                .Index(t => t.ProjectGeneralInformationId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ProjectSupplementalAgreementDetails", "ProjectGeneralInformationId", "dbo.ProjectGeneralInformations");
            DropForeignKey("dbo.ProjectOriginalContractDetails", "ProjectGeneralInformationId", "dbo.ProjectGeneralInformations");
            DropForeignKey("dbo.ProjectExtraWorkLetterDetails", "ProjectGeneralInformationId", "dbo.ProjectGeneralInformations");
            DropIndex("dbo.ProjectSupplementalAgreementDetails", new[] { "ProjectGeneralInformationId" });
            DropIndex("dbo.ProjectOriginalContractDetails", new[] { "ProjectGeneralInformationId" });
            DropIndex("dbo.ProjectExtraWorkLetterDetails", new[] { "ProjectGeneralInformationId" });
            DropTable("dbo.ProjectSupplementalAgreementDetails");
            DropTable("dbo.ProjectOriginalContractDetails");
            DropTable("dbo.ProjectExtraWorkLetterDetails");
        }
    }
}
