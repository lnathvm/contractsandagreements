namespace ContractsAndAgreements_DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addendumdocument : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.AddendumDocumentMasters",
                c => new
                    {
                        AddendumDocumentMasterId = c.Int(nullable: false, identity: true),
                        ProjectAddendumId = c.Int(nullable: false),
                        AdvertisementDate = c.DateTime(nullable: false),
                        AddendumDate = c.DateTime(nullable: false),
                        ClosingDate = c.DateTime(nullable: false),
                        Addendumno = c.String(),
                        StateProjectNo = c.String(),
                        FederalAidProjectNo = c.String(),
                        Idiqcpm = c.String(),
                        Route = c.String(),
                        StateWideParish = c.String(),
                        Comment1 = c.String(),
                        Comment2 = c.String(),
                        Comment3 = c.String(),
                        CreatedAt = c.DateTime(nullable: false),
                        UpdatedAt = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.AddendumDocumentMasterId)
                .ForeignKey("dbo.ProjectAddendumDetails", t => t.ProjectAddendumId, cascadeDelete: true)
                .Index(t => t.ProjectAddendumId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AddendumDocumentMasters", "ProjectAddendumId", "dbo.ProjectAddendumDetails");
            DropIndex("dbo.AddendumDocumentMasters", new[] { "ProjectAddendumId" });
            DropTable("dbo.AddendumDocumentMasters");
        }
    }
}
