namespace ContractsAndAgreements_DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Consul : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ConsultantProviderMasters", "ContactName", c => c.String());
            AddColumn("dbo.ConsultantProviderMasters", "City", c => c.String());
            AddColumn("dbo.ConsultantProviderMasters", "State", c => c.String());
            AddColumn("dbo.ConsultantProviderMasters", "Zip", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.ConsultantProviderMasters", "Zip");
            DropColumn("dbo.ConsultantProviderMasters", "State");
            DropColumn("dbo.ConsultantProviderMasters", "City");
            DropColumn("dbo.ConsultantProviderMasters", "ContactName");
        }
    }
}
