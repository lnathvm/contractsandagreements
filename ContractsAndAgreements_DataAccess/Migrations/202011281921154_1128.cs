namespace ContractsAndAgreements_DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _1128 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ProjectGeneralInformations", "CompensationType", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.ProjectGeneralInformations", "CompensationType");
        }
    }
}
