namespace ContractsAndAgreements_DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class shortli : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ProjectAdvertisementApparentSelections",
                c => new
                    {
                        ProjectAdvertisementApparentSelectionId = c.Int(nullable: false, identity: true),
                        ProjectGeneralInformationId = c.Int(nullable: false),
                        Title39 = c.String(),
                        AdvertisementDate = c.String(),
                        ClosingDate = c.String(),
                        AmmendedClosingDate = c.String(),
                        ApparentSelectionDate = c.String(),
                        ContractNo = c.String(),
                        StateProjectno = c.String(),
                        Subject = c.String(),
                        ApparentSelection = c.String(),
                        CreatedAt = c.DateTime(nullable: false),
                        UpdatedAt = c.DateTime(),
                    })
                .PrimaryKey(t => t.ProjectAdvertisementApparentSelectionId);
            
            CreateTable(
                "dbo.ProjectAdvertisementSelections",
                c => new
                    {
                        ProjectAdvertisementSelectionId = c.Int(nullable: false, identity: true),
                        ProjectGeneralInformationId = c.Int(nullable: false),
                        Title39 = c.String(),
                        AdvertisementDate = c.String(),
                        ClosingDate = c.String(),
                        AmmendedClosingDate = c.String(),
                        SelectionDate = c.String(),
                        ContractNo = c.String(),
                        StateProjectno = c.String(),
                        Subject = c.String(),
                        Selection = c.String(),
                        CreatedAt = c.DateTime(nullable: false),
                        UpdatedAt = c.DateTime(),
                    })
                .PrimaryKey(t => t.ProjectAdvertisementSelectionId);
            
            CreateTable(
                "dbo.ProjectAdvertisementShortlists",
                c => new
                    {
                        ProjectAdvertisementShortlistId = c.Int(nullable: false, identity: true),
                        ProjectGeneralInformationId = c.Int(nullable: false),
                        Title39 = c.String(),
                        AdvertisementDate = c.String(),
                        ClosingDate = c.String(),
                        AmmendedClosingDate = c.String(),
                        ShortListDate = c.String(),
                        ContractNo = c.String(),
                        StateProjectno = c.String(),
                        Subject = c.String(),
                        ShortList = c.String(),
                        CreatedAt = c.DateTime(nullable: false),
                        UpdatedAt = c.DateTime(),
                    })
                .PrimaryKey(t => t.ProjectAdvertisementShortlistId);
            
            CreateTable(
                "dbo.ShortlistSubDetails",
                c => new
                    {
                        ShortlistSubDetailsId = c.Int(nullable: false, identity: true),
                        ProjectAdvertisementShortlistId = c.Int(nullable: false),
                        SubId = c.Int(nullable: false),
                        CreatedAt = c.DateTime(nullable: false),
                        UpdatedAt = c.DateTime(storeType: "smalldatetime"),
                    })
                .PrimaryKey(t => t.ShortlistSubDetailsId);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.ShortlistSubDetails");
            DropTable("dbo.ProjectAdvertisementShortlists");
            DropTable("dbo.ProjectAdvertisementSelections");
            DropTable("dbo.ProjectAdvertisementApparentSelections");
        }
    }
}
