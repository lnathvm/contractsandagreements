namespace ContractsAndAgreements_DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Email : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.CommunicationSettingMasters",
                c => new
                    {
                        CommunicationSettingId = c.Int(nullable: false, identity: true),
                        CommunicationType = c.String(),
                        ToEmail = c.String(),
                        BccEmail = c.String(),
                        Subject = c.String(),
                        Message = c.String(),
                    })
                .PrimaryKey(t => t.CommunicationSettingId);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.CommunicationSettingMasters");
        }
    }
}
