namespace ContractsAndAgreements_DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class MBB : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AdvertisementDocumentMasters", "ContractNo", c => c.String());
            AddColumn("dbo.AdvertisementDocumentMasters", "ProjectName", c => c.String());
            AddColumn("dbo.AdvertisementDocumentMasters", "StateProjectNo", c => c.String());
            AddColumn("dbo.AdvertisementDocumentMasters", "FAPNo", c => c.String());
            AddColumn("dbo.AdvertisementDocumentMasters", "Parish", c => c.String());
            AddColumn("dbo.AdvertisementDocumentMasters", "Comment1", c => c.String());
            AddColumn("dbo.AdvertisementDocumentMasters", "Comment2", c => c.String());
            AddColumn("dbo.AdvertisementDocumentMasters", "Comment3", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.AdvertisementDocumentMasters", "Comment3");
            DropColumn("dbo.AdvertisementDocumentMasters", "Comment2");
            DropColumn("dbo.AdvertisementDocumentMasters", "Comment1");
            DropColumn("dbo.AdvertisementDocumentMasters", "Parish");
            DropColumn("dbo.AdvertisementDocumentMasters", "FAPNo");
            DropColumn("dbo.AdvertisementDocumentMasters", "StateProjectNo");
            DropColumn("dbo.AdvertisementDocumentMasters", "ProjectName");
            DropColumn("dbo.AdvertisementDocumentMasters", "ContractNo");
        }
    }
}
