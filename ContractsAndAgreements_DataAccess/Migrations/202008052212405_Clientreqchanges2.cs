namespace ContractsAndAgreements_DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Clientreqchanges2 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ProjectAdvertisementDetails", "DBEGoal", c => c.String());
            DropColumn("dbo.ProjectAdvertisementDetails", "MinimumManPower");
            DropColumn("dbo.ProjectAdvertisementDetails", "FundingSource");
            DropColumn("dbo.ProjectAddendumDetails", "MinimumManPower");
            DropColumn("dbo.ProjectAddendumDetails", "FundingSource");
        }
        
        public override void Down()
        {
            AddColumn("dbo.ProjectAddendumDetails", "FundingSource", c => c.String());
            AddColumn("dbo.ProjectAddendumDetails", "MinimumManPower", c => c.String());
            AddColumn("dbo.ProjectAdvertisementDetails", "FundingSource", c => c.String());
            AddColumn("dbo.ProjectAdvertisementDetails", "MinimumManPower", c => c.String());
            DropColumn("dbo.ProjectAdvertisementDetails", "DBEGoal");
        }
    }
}
