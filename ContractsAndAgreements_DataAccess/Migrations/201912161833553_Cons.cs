namespace ContractsAndAgreements_DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Cons : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.ConsultantAreaSubTaxDetails", "ProjectConsultantAreaId", "dbo.ProjectConsultantAreas");
            DropIndex("dbo.ConsultantAreaSubTaxDetails", new[] { "ProjectConsultantAreaId" });
            DropTable("dbo.ConsultantAreaSubTaxDetails");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.ConsultantAreaSubTaxDetails",
                c => new
                    {
                        ConsultantAreaSubTaxDetailsId = c.Int(nullable: false, identity: true),
                        ProjectConsultantAreaId = c.Int(nullable: false),
                        SubId = c.Int(nullable: false),
                        TaxId = c.String(),
                        CreatedAt = c.DateTime(nullable: false, storeType: "smalldatetime"),
                        UpdatedAt = c.DateTime(storeType: "smalldatetime"),
                    })
                .PrimaryKey(t => t.ConsultantAreaSubTaxDetailsId);
            
            CreateIndex("dbo.ConsultantAreaSubTaxDetails", "ProjectConsultantAreaId");
            AddForeignKey("dbo.ConsultantAreaSubTaxDetails", "ProjectConsultantAreaId", "dbo.ProjectConsultantAreas", "ProjectConsultantAreaId", cascadeDelete: true);
        }
    }
}
