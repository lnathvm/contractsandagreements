﻿Alter proc [dbo].[usp_GetAddendunDocumentDetail]  
@ProjectAddendumId int =null  
As  
BEGIN  
select pgi.ProjectGeneralInformationId,pm.Name as Parish,adm.ProjectName,pad.ProjectAddendumId ,      
Convert(varchar,adm.AddendumDate,107 ) as AddendumDate      
,Convert(varchar,adm.ClosingDate,107 ) as AmendedClosingDate,pgi.Route,adm.StateProjectNo ,adm.FAPNo,      
adm.Comment1,adm.Comment2,adm.Comment3,adm.Addendumno,adm.DBEGoel,adm.ContractNo  
from ProjectAddendumDetails pad      
inner join AddendumDocumentMasters adm on  adm.ProjectAddendumId=pad.ProjectAddendumId      
inner join ProjectGeneralInformations pgi on pgi.ProjectGeneralInformationId=pad.ProjectGeneralInformationId     
inner join ParishMasters pm on pm.ParishId=pgi.Parish      
inner join ProjectAddendumWorkFlowTrackings pwt on pwt.ProjectAddendumWorkFlowId=pad.ProjectAddendumWorkFlowId      
where adm.AddendumDocumentMasterId=@ProjectAddendumId  and pgi.IsDelete=0 and pad.IsDeleted=0
order by pad.ProjectAddendumId desc
END
GO
/****** Object:  StoredProcedure [dbo].[usp_GetAdvertisementApparentSelectionDocDetail]    Script Date: 7/28/2020 11:18:50 PM ******/

  
Alter proc [dbo].[usp_GetAdvertisementApparentSelectionDocDetail]  
@ProjectAdvertisementApparentSelectionId int =null  
As  
BEGIN  
select pas.ApparentSelection,pas.ProjectGeneralInformationId,pm.Name as Parish,pgi.ProjectName, 
Convert(varchar,pas.ApparentSelectionDate,107) as ApparentSelectionDate,
Convert(varchar,pas.AdvertisementDate,107 ) as AdvertisementDate  
,Convert(varchar,pas.ClosingDate,107 ) as ClosingDate,pgi.[Route] as Route,pgi.StateProjectNo ,pgi.FAPNo,  
cpm.ConsultantProviderName,pas.ApparentSelection,pas.Comment1,pas.Comment2,pgi.ContractNo,pas.Rank
from ProjectAdvertisementApparentSelections pas  
inner join ProjectGeneralInformations pgi on pgi.ProjectGeneralInformationId=pas.ProjectGeneralInformationId  
inner join ParishMasters pm on pm.ParishId=pgi.Parish  
inner join ProjectAdvertisementShortlistConsultantProviders pascp
on pascp.ProjectAdvertisementShortlistConsultantProviderId=pas.ApparentSelection
inner join ConsultantProviderMasters cpm on cpm.ConsultantProviderId=pascp.SelectedShortlistConsultantProviderId
where pas.ProjectAdvertisementApparentSelectionId=@ProjectAdvertisementApparentSelectionId  
and pgi.IsDelete=0
END  
GO
/****** Object:  StoredProcedure [dbo].[usp_GetAdvertisementDetail]    Script Date: 7/28/2020 11:18:50 PM ******/

Alter proc [dbo].[usp_GetAdvertisementDetail]
@ProjectGeneralInformationId int =null
As
BEGIN
select pgi.StateProjectNo,pgi.ProjectGeneralInformationId ,
Convert(varchar,adm.AdvertisementDate,23 ) as AdvertisementDate,
Convert(varchar,adm.ClosingDate,23 ) as ClosingDate,pgi.ContractNo
from ProjectGeneralInformations pgi 
inner join ProjectAdvertisementDetails pad
on pad.ProjectGeneralInformationId=pgi.ProjectGeneralInformationId  
inner join AdvertisementDocumentMasters adm
on adm.ProjectAdvertisementDetailsId=pad.ProjectAdvertisementDetailsId   
where pgi.ProjectGeneralInformationId=@ProjectGeneralInformationId and pgi.IsDelete=0
END
GO
/****** Object:  StoredProcedure [dbo].[usp_GetAdvertisementDocumentDetail]    Script Date: 7/28/2020 11:18:50 PM ******/

Alter proc [dbo].[usp_GetAdvertisementDocumentDetail]  
@projectadvertisementdetails int =null  
As  
BEGIN  
select pgi.ProjectGeneralInformationId,pm.Name as Parish,  
adm.ProjectName,pad.ProjectAdvertisementDetailsId ,    
Convert(varchar,adm.AdvertisementDate,107 ) as AdvertisementDate    
,Convert(varchar,adm.ClosingDate,107 ) as ClosingDate,pgi.[Route] as Route,adm.StateProjectNo ,    
adm.FAPNo,adm.DBEGoal,adm.ContractNo,adm.Comment1,adm.Comment2,adm.Comment3,adm.TypeOfAdvertGoesHere,adm.Details 
from projectadvertisementdetails pad    
inner join AdvertisementDocumentMasters adm on adm.ProjectAdvertisementDetailsId=pad.ProjectAdvertisementDetailsId    
inner join ProjectGeneralInformations pgi on pgi.ProjectGeneralInformationId=pad.ProjectGeneralInformationId 
inner join ParishMasters pm on pm.ParishId=adm.Parish    
where adm.AdvertisementDocumentMasterId=@projectadvertisementdetails and pgi.IsDelete=0
order by adm.AdvertisementDocumentMasterId desc
END
GO
/****** Object:  StoredProcedure [dbo].[usp_GetAdvertisementSelectionDocDetail]    Script Date: 7/28/2020 11:18:50 PM ******/
Alter proc [dbo].[usp_GetAdvertisementSelectionDocDetail]
@ProjectAdvertisementSelectionId int =null
As
BEGIN
select pas.Selection,pas.ProjectGeneralInformationId,pm.Name as Parish,pgi.ProjectName,
Convert(varchar,pas.SelectionDate,107) as SelectionDate,
Convert(varchar,pas.AdvertisementDate,107 ) as AdvertisementDate
,Convert(varchar,pas.ClosingDate,107 ) as ClosingDate,pgi.[Route] as Route,pgi.StateProjectNo ,pgi.FAPNo,
cpm.ConsultantProviderName,pas.Comment1,pas.Comment2,pgi.ContractNo,pas.Rank
from ProjectAdvertisementSelections pas
inner join ProjectGeneralInformations pgi on pgi.ProjectGeneralInformationId=pas.ProjectGeneralInformationId 
inner join ParishMasters pm on pm.ParishId=pgi.Parish
inner join ProjectAdvertisementShortlistConsultantProviders pascp
on pascp.ProjectAdvertisementShortlistConsultantProviderId=pas.Selection
inner join ConsultantProviderMasters cpm on cpm.ConsultantProviderId=pascp.SelectedShortlistConsultantProviderId
where pas.ProjectAdvertisementSelectionId=@ProjectAdvertisementSelectionId and pgi.IsDelete=0 
order by pas.Selection desc
END
GO
/****** Object:  StoredProcedure [dbo].[usp_GetAdvertisementShortListDocDetail]    Script Date: 7/28/2020 11:18:50 PM ******/

Alter proc [dbo].[usp_GetAdvertisementShortListDocDetail]
@ProjectGeneralInformationId int =null
As
BEGIN
select distinct pas.ProjectAdvertisementShortlistId, pgi.ProjectGeneralInformationId,pm.Name as Parish,pgi.ProjectName,
Convert(varchar,pas.ShortListDate,107 ) as ShortListDate,
Convert(varchar,pas.AdvertisementDate,107 ) as AdvertisementDate  
,Convert(varchar,pas.ClosingDate,107 ) as ClosingDate,pgi.[Route] as Route,pgi.StateProjectNo ,
pgi.FAPNo,--cpm.ConsultantProviderName,
pas.Comment1,pas.Comment2,pgi.ContractNo
from ProjectAdvertisementShortlists pas  
inner join ProjectGeneralInformations pgi on pgi.ProjectGeneralInformationId=pas.ProjectGeneralInformationId 
inner join ParishMasters pm on pm.ParishId=pgi.Parish  
--inner join ConsultantProviderMasters cpm on cpm.ConsultantProviderId=pas.ShortList  
where pas.ProjectAdvertisementShortlistId=@ProjectGeneralInformationId and pgi.IsDelete=0  
and pas.IsDeleted=0
order by pas.ProjectAdvertisementShortlistId desc
END
GO


/****** Object:  StoredProcedure [dbo].[usp_GetAllAdvertisementApparentSelectionDetailsList]    Script Date: 7/28/2020 11:18:50 PM ******/
Alter proc [dbo].[usp_GetAllAdvertisementApparentSelectionDetailsList]
As
BEGIN
select pgi.ProjectGeneralInformationId,pgi.StateProjectNo,pm.Name as Parish,pgi.ProjectName,pasd.ProjectAdvertisementApparentSelectionId as ProjectAdDetailsId,
Convert(varchar,pasd.ApparentSelectionDate,103 ) as Date,pgi.Route,pgi.ContractNo
from ProjectAdvertisementApparentSelections pasd
inner join ProjectGeneralInformations pgi on pgi.ProjectGeneralInformationId=pasd.ProjectGeneralInformationId 
inner join ParishMasters pm on pm.ParishId=pgi.Parish
where pasd.IsPostToWeb=1
order by pasd.ProjectAdvertisementApparentSelectionId desc
END
GO


/****** Object:  StoredProcedure [dbo].[usp_GetAllAdvertisementList]    Script Date: 7/28/2020 11:18:50 PM ******/
Alter proc [dbo].[usp_GetAllAdvertisementList]          
As          
BEGIN          
select pgi.ProjectGeneralInformationId,pgi.ContractNo,  
pm.Name as Parish,pgi.ProjectName,adm.AdvertisementDocumentMasterId as ProjectAdDetailsId,          
Convert(varchar,adm.AdvertisementDate,103 ) as Date ,  
Convert(varchar,adm.ClosingDate,103 ) as ClosingDate,'Advertisement' as Type          
from projectadvertisementdetails pad          
inner join ProjectGeneralInformations pgi   
on pgi.ProjectGeneralInformationId=pad.ProjectGeneralInformationId         
inner join ParishMasters pm on pm.ParishId=pgi.Parish          
inner join AdvertisementDocumentMasters adm   
on adm.ProjectAdvertisementDetailsId=pad.ProjectAdvertisementDetailsId    
where pgi.IsDelete=0 and adm.IsPostToWeb=1
union          
select pgi.ProjectGeneralInformationId,pgi.StateProjectNo,  
pm.Name as Parish,pgi.ProjectName,adm.AddendumDocumentMasterId  as ProjectAdDetailsId,          
Convert(varchar,ADM.AddendumDate,103 ) as Date          
,Convert(varchar,ADM.ClosingDate,103 ) as ClosingDate,  
'Adden. No. '+ Isnull(ADM.Addendumno,'') as Type          
from ProjectAddendumDetails pad          
inner join AddendumDocumentMasters ADM on pad.ProjectAddendumId=ADM.ProjectAddendumId        
inner join ProjectGeneralInformations pgi   
on pgi.ProjectGeneralInformationId=pad.ProjectGeneralInformationId           
inner join ParishMasters pm on pm.ParishId=pgi.Parish          
left join ProjectAddendumWorkFlowTrackings pwt   
on pwt.ProjectAddendumWorkFlowId=pad.ProjectAddendumWorkFlowId     
where pgi.IsDelete=0 and pad.IsDeleted=0
END 
GO





/****** Object:  StoredProcedure [dbo].[usp_GetAllAdvertisementSelectionDetailsList]    Script Date: 7/28/2020 11:18:50 PM ******/

Alter proc [dbo].[usp_GetAllAdvertisementSelectionDetailsList]
As
BEGIN
select pgi.ProjectGeneralInformationId,pgi.StateProjectNo,pm.Name as Parish,pgi.ProjectName,pasd.ProjectAdvertisementSelectionId as ProjectAdDetailsId,
Convert(varchar,pasd.SelectionDate,103 ) as Date,pgi.Route,pgi.ContractNo
from ProjectAdvertisementSelections pasd
inner join ProjectGeneralInformations pgi on pgi.ProjectGeneralInformationId=pasd.ProjectGeneralInformationId 
inner join ParishMasters pm on pm.ParishId=pgi.Parish
where pasd.IsPostToWeb=1
order by pasd.ProjectAdvertisementSelectionId desc
END
GO
/****** Object:  StoredProcedure [dbo].[usp_GetAllAdvertisementShortlistDetailsList]    Script Date: 7/28/2020 11:18:50 PM ******/
Alter proc [dbo].[usp_GetAllAdvertisementShortlistDetailsList]
As
BEGIN
select  pasd.ProjectAdvertisementShortlistId,pgi.ProjectGeneralInformationId,pgi.StateProjectNo,pm.Name as Parish,pgi.ProjectName,Convert(varchar,pasd.ShortListDate,103 ) as Date,pgi.Route 
from ProjectAdvertisementShortlists pasd
inner join ProjectGeneralInformations pgi on pgi.ProjectGeneralInformationId=pasd.ProjectGeneralInformationId 
inner join ParishMasters pm on pm.ParishId=pgi.Parish
where pgi.IsDelete=0 and pasd.IsDeleted=0 and pasd.IsPostToWeb=1
order by pasd.ProjectAdvertisementShortlistId desc
END
GO
/****** Object:  StoredProcedure [dbo].[usp_GetAllProjectContractAndTaskOrderList]    Script Date: 7/28/2020 11:18:50 PM ******/

Alter proc [dbo].[usp_GetAllProjectContractAndTaskOrderList]
@ProjectGeneralInformationId int =null
As
BEGIN
Select PGI.ProjectGeneralInformationId,PC.ProjectContractDetailsId,POC.ProjectOriginalContractDetailId,PTD.ProjectTaskOrderDetailsId,
PSD.ProjectSupplementalAgreementDetailId,PSDIDIQ.ProjectSupplementalAgreementForIDIQId
from ProjectGeneralInformations PGI
Left JOin
ProjectContractDetails PC  
on PGI.ProjectGeneralInformationId=PC.ProjectContractInformationId
Left JOin
ProjectOriginalContractDetails POC  
on PGI.ProjectGeneralInformationId=POC.ProjectGeneralInformationId
Left JOin ProjectTaskOrderDetails PTD  
on PGI.ProjectGeneralInformationId=PTD.ProjectGeneralInformationId 
LEFT JOin ProjectSupplementalAgreementDetails PSD
on PGI.ProjectGeneralInformationId=PSD.ProjectGeneralInformationId
LEFT JOin ProjectSupplementalAgreementForIDIQs PSDIDIQ
on PGI.ProjectGeneralInformationId=PSDIDIQ.ProjectGeneralInformationId
where PGI.ProjectGeneralInformationId=@ProjectGeneralInformationId and pgi.IsDelete=0 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_GetAllProjectGeneralInformationList]    Script Date: 7/28/2020 11:18:50 PM ******/

alter proc [dbo].[usp_GetAllProjectGeneralInformationList]    
As    
BEGIN    
Select P.ProjectGeneralInformationId,P.StateProjectNo,P.ContractNo,P.FAPNo,P.ContractAgency,    
P.ProjectName,convert(varchar, P.CreatedAt, 23) ProjectDate,PA.ProjectAdvertisementDetailsId,    
Isnull(convert(varchar, PA.CreatedAt, 23),'') AdvertisementDate,
Isnull(convert(varchar, PT.AwardNotification, 23),'') AwardNotification,
P.DBEGoel,ISNULL(P.EStContractAmount,'0') EStContractAmount  
from ProjectGeneralInformations P left join     
ProjectAdvertisementDetails PA    
on PA.ProjectGeneralInformationId=p.ProjectGeneralInformationId  
Left Join ProjectTrackings PT 
on PT.ProjectGeneralInformationId=p.ProjectGeneralInformationId
where p.IsDelete=0     
END 
GO
/****** Object:  StoredProcedure [dbo].[usp_GetAllProjectMoneyBalanceList]    Script Date: 7/28/2020 11:18:50 PM ******/

Alter proc [dbo].[usp_GetAllProjectMoneyBalanceList]          
As          
BEGIN          
select distinct pgi.ProjectGeneralInformationId,pgi.StateProjectNo,        
(IsNULL(sum(convert(decimal,pci.ContractAmount)),0) + IsNULL(sum(convert(decimal,PSI.TOAmount)),0)) as ContractAmount,          
(ISNULL(sum(convert(decimal,ptoi.TOAmount)),0))  as TaskOrderAmount           
 from ProjectGeneralInformations pgi          
 left join ProjectTaskOrderDetails ptod on ptod.ProjectGeneralInformationId=pgi.ProjectGeneralInformationId           
left join projectTaskOrderInformations ptoi on ptoi.ProjectTaskOrderInformationId=ptod.ProjectTaskOrderInformationId          
 left join  ProjectContractDetails pcd on pcd.ProjectGeneralInformationId=pgi.ProjectGeneralInformationId          
left join projectContractInformations pci on pcd.ProjectContractInformationId=pci.ProjectContractInformationId  
left JOin ProjectSupplementalAgreementForIDIQs PS on PS.ProjectGeneralInformationId=pgi.ProjectGeneralInformationId    
left join ProjectSupplementalAgreementInformationForIDIQs PSI on PS.ProjectSupplementalAgreementInformationForIDIQId=PSI.ProjectSupplementalAgreementInformationForIDIQId    
  where pgi.IsDelete=0  
 group by pgi.ProjectGeneralInformationId,pgi.StateProjectNo         
END 
GO
/****** Object:  StoredProcedure [dbo].[usp_GetProjectContractTaskOrderMoneyBalanceList]    Script Date: 7/28/2020 11:18:50 PM ******/      
alter proc [dbo].[usp_GetProjectContractTaskOrderMoneyBalanceList]        
@ProjectGeneralInformationId int =null        
As        
BEGIN        
select distinct pgi.ProjectGeneralInformationId,        
  (Cast(ISNULL(pci.ContractAmount,0) as decimal))  ContractAmount,
 Isnull(pcd.ContractAction,'') ContractAction,Convert(varchar,pcd.CreatedAt,103 ) as ContractDate,        
 ptoi.TOAmount as TaskOrderAmount,'Task Order' TaskForProject,Convert(varchar,ptod.CreatedAt ,103) as TODate        
 from ProjectGeneralInformations pgi        
 inner join ProjectTaskOrderDetails ptod on ptod.ProjectGeneralInformationId=pgi.ProjectGeneralInformationId        
inner join projectTaskOrderInformations ptoi on ptoi.ProjectTaskOrderInformationId=ptod.ProjectTaskOrderInformationId        
 inner join  ProjectContractDetails pcd on pcd.ProjectGeneralInformationId=pgi.ProjectGeneralInformationId        
inner join projectContractInformations pci on pcd.ProjectContractInformationId=pci.ProjectContractInformationId     
--left JOin ProjectSupplementalAgreementForIDIQs PS on PS.ProjectGeneralInformationId=pgi.ProjectGeneralInformationId    
--left join ProjectSupplementalAgreementInformationForIDIQs PSI on PS.ProjectSupplementalAgreementInformationForIDIQId=PSI.ProjectSupplementalAgreementInformationForIDIQId    
  where pgi.ProjectGeneralInformationId=@ProjectGeneralInformationId  and pgi.IsDelete=0 
    
  union all  
  select distinct PS.ProjectGeneralInformationId,        
  Cast(ISNULL(PSI.TOAmount,0) as decimal)  ContractAmount,NULL,Convert(varchar,PS.CreatedAt,103 ) as ContractDate,        
  '0.0' as TaskOrderAmount,'Supplemental Agreement' TaskForProject,Convert(varchar,PS.CreatedAt ,103) as TODate        
 from ProjectSupplementalAgreementForIDIQs PS    
left join ProjectSupplementalAgreementInformationForIDIQs PSI on PS.ProjectSupplementalAgreementInformationForIDIQId=PSI.ProjectSupplementalAgreementInformationForIDIQId    
  where PS.ProjectGeneralInformationId=@ProjectGeneralInformationId  
END
GO
/****** Object:  StoredProcedure [dbo].[usp_GetSelectedShortListConsultantProviderDetail]    Script Date: 7/28/2020 11:18:50 PM ******/

Alter proc [dbo].[usp_GetSelectedShortListConsultantProviderDetail]    
@ProjectAdvertisementShortlistId int    
As    
BEGIN    
Select Convert(int,pascp.ProjectAdvertisementShortlistConsultantProviderId) as Id,cpm.ConsultantProviderName as Name  
from ProjectAdvertisementShortlistConsultantProviders pascp     
inner join ConsultantProviderMasters cpm  
on cpm.ConsultantProviderId=pascp.SelectedShortlistConsultantProviderId       
where pascp.ProjectAdvertisementShortlistId=@ProjectAdvertisementShortlistId  


END 
GO
/****** Object:  StoredProcedure [dbo].[usp_GetShortListConsultantProviderList]    Script Date: 7/28/2020 11:18:50 PM ******/
 ALTER proc [dbo].[usp_GetShortListConsultantProviderList]    
@ProjectGeneralInformationId int    
As    
BEGIN    
Select Convert(int,pascp.ProjectAdvertisementShortlistConsultantProviderId) as Id,cpm.ConsultantProviderName as Name,pascp.Rank as Rank
from ProjectAdvertisementShortlists pas  
inner join ProjectAdvertisementShortlistConsultantProviders  pascp 
on pascp.ProjectAdvertisementShortListId=pas.ProjectAdvertisementShortlistId
inner join ConsultantProviderMasters cpm on cpm.ConsultantProviderId=pascp.SelectedShortlistConsultantProviderId     
where pas.ProjectGeneralInformationId=@ProjectGeneralInformationId and pas.IsDeleted=0
END 
GO
/****** Object:  StoredProcedure [dbo].[usp_GetSubByShortListId]    Script Date: 7/28/2020 11:18:50 PM ******/

Alter proc [dbo].[usp_GetSubByShortListId]
@ProjectAdvertisementShortlistConsultantProviderId int =null
As
BEGIN

  select ssd.SubId,cpm.ConsultantProviderName from ShortlistSubDetails ssd
  inner join ConsultantProviderMasters cpm on cpm.ConsultantProviderId=ssd.SubId 
  where ssd.ProjectAdvertisementShortlistConsultantProviderId=@ProjectAdvertisementShortlistConsultantProviderId

END
GO
