﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContractsAndAgreements_Domain.Dtos
{
    public class GetAddendumDetailsForDashboardDto
    {
        public int ProjectAddendumId { get; set; }
        public int ProjectGeneralInformationId { get; set; }
        public string AddendumComments { get; set; }
        public string AddendumDate { get; set; }

    }
}
