﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContractsAndAgreements_Domain.Dtos
{
    public class AdvertisementShortListDocDto
    {
        public int ProjectAdDetailsId { get; set; }
        public int ProjectAdvertisementShortlistId { get; set; }
        public int ProjectGeneralInformationId { get; set; }
        public string Type { get; set; }
        //public string ProjectName { get; set; }
        //public string Parish { get; set; }
        public string Date { get; set; }
        public string AdvertisementDate { get; set; }
        public string ClosingDate { get; set; }
        //public string Route { get; set; }
        //public string StateProjectNo { get; set; }
        //public string ContractNo { get; set; }
        public string FAPNo { get; set; }

        public string ShortListDate { get; set; }
        //public string ShortList1 { get; set; }
        //public string ShortList2 { get; set; }
        //public string ShortList3 { get; set; }
        public string Comment1 { get; set; }
        public string Comment2 { get; set; }
        //public IList<ShortlistSubDetailsDto> SubTaxDetailsDto1 { get; set; }
        //public IList<ShortlistSubDetailsDto> SubTaxDetailsDto2 { get; set; }
        //public IList<ShortlistSubDetailsDto> SubTaxDetailsDto3 { get; set; }

        public IList<ShortListDto> ShortList { get; set; }
    }
}
