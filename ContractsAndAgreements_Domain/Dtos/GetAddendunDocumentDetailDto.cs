﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContractsAndAgreements_Domain.Dtos
{
    public class GetAddendunDocumentDetailDto
    {
        public int ProjectAddendumId { get; set; }
        public int ProjectGeneralInformationId { get; set; }

        public string ProjectName { get; set; }

        public string Parish { get; set; }
        public string AddendumDate { get; set; }

        public string AmendedClosingDate { get; set; }
        public string Route { get; set; }
        public string StateProjectNo { get; set; }
        public string FAPNo { get; set; }
        public string DBEGoel { get; set; }
        public string Comment1 { get; set; }
        public string Comment2 { get; set; }
        public string Comment3 { get; set; }
        public string Addendumno { get; set; }

        public string ContractNo { get; set; }
    }
}
