﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContractsAndAgreements_Domain.Dtos
{
    public class AddendumDetailsDto
    {
        public int ProjectAddendumId { get; set; }
        public int ProjectGeneralInformationId { get; set; }
        public int ProjectWorkFlowTrackingId { get; set; }

        public string AddendumDescription { get; set; }
        public string ProjectDetails { get; set; }
        public string MinimumManPower { get; set; }
        //public string FundingSource { get; set; }
        public string ContractTime { get; set; }
        public int ProjectManagerId { get; set; }
        public string ManagerPhone { get; set; }

        public string AddenComments { get; set; }
        public ProjectGeneralInformationDto Information { get; set; }
        public ProjectAddendumWorkFlowTrackingDto ProjectAddendWork { get; set; }

    }
}
