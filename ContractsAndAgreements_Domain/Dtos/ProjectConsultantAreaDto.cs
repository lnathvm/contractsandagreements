﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContractsAndAgreements_Domain.Dtos
{
    public class ProjectConsultantAreaDto
    {
        public int ProjectConsultantAreaId { get; set; }
        public int ProjectGeneralInformationId { get; set; }
        public int ProjectManagerId { get; set; }
        public string ManagerPhone { get; set; }
        public string Phone { get; set; }
        public int ConsultantProviderId { get; set; }
        public string ContactPerson { get; set; }
        public string Title { get; set; }
        public string JobTitle { get; set; }
        public string OfficeAddress { get; set; }
        public string Fax { get; set; }
        public string TaxId { get; set; }
        public string Email { get; set; }
    }
}
