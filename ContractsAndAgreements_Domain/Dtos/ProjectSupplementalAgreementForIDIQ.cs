﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContractsAndAgreements_Domain.Dtos
{
    public class ProjectSupplementalAgreementForIDIQDto
    {
        public int ProjectSupplementalAgreementForIDIQId { get; set; }
        public int ProjectGeneralInformationId { get; set; }
        public int ProjectSupplementalAgreementInformationForIDIQId { get; set; }
        public string SAForProject { get; set; }
        public string SAScope { get; set; }
        public string TaxClassification { get; set; }
        public string SAExpirationDate { get; set; }
        public string ContractExpires { get; set; }

        public int ProjectManagerId { get; set; }
        public string ManagerPhone { get; set; }
        public ProjectGeneralInformationDto Information { get; set; }
        public ProjectSupplementalAgreementInformationForIDIQDto TaskOrderInformation { get; set; }
    }

    public class ProjectSupplementalAgreementInformationForIDIQDto
    {
        public int ProjectSupplementalAgreementInformationForIDIQId { get; set; }
        public string ContractAmount { get; set; }
        public string TOAmount { get; set; }
        public string Prime { get; set; }
        public string PrimeAmount { get; set; }
        public IList<ProjectSupplementalAgreementInformationForIDIQSubTaxDetailsDto> SubTaxDetailsDto { get; set; }
    }

    public class ProjectSupplementalAgreementInformationForIDIQSubTaxDetailsDto
    {
        public int ProjectSupplementalAgreementInformationForIDIQSubTaxDetailsId { get; set; }
        public int ProjectSupplementalAgreementInformationForIDIQId { get; set; }
        public int SubId { get; set; }
        public string TaxId { get; set; }

        public string ContractAmount { get; set; }
    }
}

