﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContractsAndAgreements_Domain.Dtos
{
    public class ProjectTrackingDto
    {
        public int ProjectTrackingId { get; set; }
        public int ProjectTrackingGeneralInformationId { get; set; }

        public string Received { get; set; }
        public string AwardNotification { get; set; }
        public string Closed { get; set; }
        public string FinalInvoicedRecvd { get; set; }
        public string FinalToAcct { get; set; }
        public string AuditReport { get; set; }
        public string FinalCost { get; set; }



    }
}
