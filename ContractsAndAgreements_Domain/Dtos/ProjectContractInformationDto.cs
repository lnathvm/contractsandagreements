﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContractsAndAgreements_Domain.Dtos
{
    public class ProjectContractInformationDto
    {
        public int ProjectContractInformationId { get; set; }
        public string ContractAmount { get; set; }
        public string Description { get; set; }
        public string Prime { get; set; }
        public string PrimeAmount { get; set; }

        public IList<ContractInformationSubTaxDetailsDto> SubTaxDetailsDto { get; set; }
    }
}
