﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContractsAndAgreements_Domain.Dtos
{
    public class ConsultantProviderDto
    {
        public int ConsultantProviderId { get; set; }
        public string ConsultantProviderName { get; set; }

        public string ContactName { get; set; }
        public string Title { get; set; }
        public string JobTitle { get; set; }
        public string Fax { get; set; }
        public string TaxId { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string OfficeAddress { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }

        public bool IsActive { get; set; }
        public bool ProofofInsurance { get; set; }
        public bool TypeofInsurance { get; set; }
        public string InsuranceCompany { get; set; }
        public string EffectiveDateofCoverage { get; set; }
        public string ExpirationDateofCoverage { get; set; }
        public bool DisclosureofOwnership { get; set; }
        public bool CertificateofAuthority { get; set; }
        public bool ResolutionCertificate { get; set; }
        public string EffectiveDate { get; set; }
        public string PersonAuthorizedToSign { get; set; }
        public string Type { get; set; }
        public string Comments { get; set; }

    }
}
