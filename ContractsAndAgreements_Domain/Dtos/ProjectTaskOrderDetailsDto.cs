﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContractsAndAgreements_Domain.Dtos
{
    public class ProjectTaskOrderDetailsDto
    {
        public int ProjectTaskOrderDetailsId { get; set; }
        public int ProjectGeneralInformationId { get; set; }
        public int ProjectTaskOrderInformationId { get; set; }
        public int ProjectTaskOrderWorkFlowTrackingId { get; set; }
        public string TaskForProject { get; set; }
        public string TaskScope { get; set; }
        public string TaxClassification { get; set; }
        public string TaskExpirationDate { get; set; }
        public string ContractExpires { get; set; }

        //public int ProjectManagerId { get; set; }
        //public string ManagerPhone { get; set; }
        //public ProjectGeneralInformationDto Information { get; set; }
        public ProjectInformationDto TaskOrderInformation { get; set; }
        //public ProjectTaskOrderWorkFlowTrackingDto ProjectWork { get; set; }

    }
}
