﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContractsAndAgreements_Domain.Dtos
{
    public class ContractAndTaskOrderListDto
    {
        public int ProjectGeneralInformationId { get; set; }
        public int? ProjectContractDetailsId { get; set; }
        public int? ProjectOriginalContractDetailId { get; set; }
        public int? ProjectTaskOrderDetailsId { get; set; }
        public int? ProjectSupplementalAgreementDetailId { get; set; }
        public int? ProjectSupplementalAgreementForIDIQId { get; set; }

    }
}
