﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContractsAndAgreements_Domain.Dtos
{
    public class ProjectExtraWorkLetterDetailsDto
    {
        public int ProjectExtraWorkLetterDetailId { get; set; }
        public int ProjectGeneralInformationId { get; set; }
        public ProjectGeneralInformationDto Information { get; set; }
        public string ReasonforEWL { get; set; }
        public string ExtraWorkAction { get; set; }
        public string ContractExpries { get; set; }
        public string Commnets { get; set; }
        public string InformationAmount { get; set; }
        public string InfoRecAndLogged { get; set; }
        public string TaskAssignedToStaff { get; set; }
        public string NTPLetter { get; set; }
        public string NTPDate { get; set; }
        public ProjectInformationDto ProjectInformation { get; set; }

    }
}
