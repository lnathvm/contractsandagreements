﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContractsAndAgreements_Domain.Dtos
{
    public class DashboardInfoDto
    {
        public int ProjectGeneralInformationId { get; set; }
        public string StateProjectNo { get; set; }
        public string ContractNo { get; set; }
        public string FAPNo { get; set; }
        public string ContractAgency { get; set; }
        public string ProjectName { get; set; }
        public string ProjectDate { get; set; }
        public int? ProjectAdvertisementDetailsId { get; set; }
        public string AdvertisementDate { get; set; }
        public string DBEGoel { get; set; }

        public string EStContractAmount { get; set; }

        public string AwardNotification { get; set; }


    }
}
