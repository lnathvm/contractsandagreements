﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContractsAndAgreements_Domain.Dtos
{
    public class ProjectInformationDto
    {
        public int ProjectTaskOrderInformationId { get; set; }
        public string ContractAmount { get; set; }
        public string TOAmount { get; set; }
        public string Prime { get; set; }
        public string PrimeAmount { get; set; }

        public IList<ProjectInformationSubTaxDetailsDto> SubTaxDetailsDto { get; set; }
    }
}
