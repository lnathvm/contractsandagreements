﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContractsAndAgreements_Domain.Dtos
{
    public class AdvertisementCommonDetailDto
    {
        public int ProjectGeneralInformationId { get; set; }
        public string AdvertisementDate { get; set; }
        public string ClosingDate { get; set; }
        public string StateProjectno { get; set; }

        public string ContractNo { get; set; }
        public string FAPNo { get; set; }
        public string Parish { get; set; }
        public string ProjectName { get; set; }

    }
}
