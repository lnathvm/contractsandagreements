﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContractsAndAgreements_Domain.Dtos
{
    public class ProcessorMasterDto
    {
        public int ProcessorId { get; set; }
        public string Name { get; set; }
        public string ProcessorEmail { get; set; }
        //public bool IsActive { get; set; }
    }
}
