﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContractsAndAgreements_Domain.Dtos
{
    public class ProjectInformationSubTaxDetailsDto
    {
        public int ProjectInformationSubTaxDetailsId { get; set; }
        public int ProjectInformationId { get; set; }
        public int SubId { get; set; }
        public string TaxId { get; set; }

        public string ContractAmount { get; set; }

    }
}
