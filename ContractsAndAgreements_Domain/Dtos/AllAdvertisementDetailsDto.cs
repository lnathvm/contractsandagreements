﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContractsAndAgreements_Domain.Dtos
{
    public class AllAdvertisementDetailsDto
    {
        public int ProjectAdDetailsId { get; set; }
        public int ProjectGeneralInformationId { get; set; }
        public string Type { get; set; }
        public string ProjectName { get; set; }
        public string Parish { get; set; }
        public string Date { get; set; }
        public int Rank { get; set; }

        public string Details { get; set; }
        public string AdvertisementDate { get; set; }
        public string ApparentSelectionDate { get; set; }
        public string SelectionDate { get; set; }
        public string TypeOfAdvertGoesHere { get; set; }
        public string ClosingDate { get; set; }
        public string Route { get; set; }
        public string StateProjectNo { get; set; }
        public string FAPNo { get; set; }
        public string ConsultantProviderName { get; set; }
        public string ApparentSelection { get; set; }
        public string Selection { get; set; }
        public string Comment1 { get; set; }
        public string Comment2 { get; set; }
        public string DBEGoal { get; set; }
        public string Comment3 { get; set; }
        public string ContractNo { get; set; }

        public bool IsPostToWeb { get; set; }
        public int ProjectAdvertisementShortlistId { get; set; }
        public int ProjectAdvertisementSelectionId { get; set; }
        public int ProjectAdvertisementApparentSelectionId { get; set; }
        public IList<ShortlistSubDetailsDto> SubTaxDetailsDto
        {
            get; set;

        }
    }
}
