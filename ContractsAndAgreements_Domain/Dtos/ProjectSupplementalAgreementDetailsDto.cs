﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContractsAndAgreements_Domain.Dtos
{
    public class ProjectSupplementalAgreementDetailsDto
    {
        public int ProjectSupplementalAgreementDetailId { get; set; }
        public int ProjectGeneralInformationId { get; set; }
        //public ProjectGeneralInformationDto Information { get; set; }
        public string ReasonForSA { get; set; }
        public string SADescription { get; set; }
        public string SAAction { get; set; }
        public string SAFeeType { get; set; }
        public string SAExpries { get; set; }
        public string ContractExpries { get; set; }
        public string Commnets { get; set; }
        public string InfoRecAndLogged { get; set; }
        public string TaskAssignedToStaff { get; set; }
        public string DrafttoSupervisor { get; set; }
        public string ApprovedfromSupervisor { get; set; }
        public string FeePackagetoChiefEngineer { get; set; }
        public string ApprovedFromFeePackagetoChiefEngineer { get; set; }
        public string FeeNegotiationsTOProjMgr { get; set; }
        public string Complete { get; set; }
        public string FedAideFHWA { get; set; }
        public string Authorized { get; set; }
        public string DraftToManagerForReview { get; set; }
        public string DraftToManagerForReviewConcurred { get; set; }
        public string DraftToConsultantForReview { get; set; }
        public string DraftToConsultantForReviewConcurred { get; set; }
        public string ContractToConsultant { get; set; }
        public string Signed { get; set; }
        public string ContractTo3rdFloor { get; set; }
        public string SignedExecuted { get; set; }
        public string NTPLetter { get; set; }
        public string NTPDate { get; set; }
        public ProjectInformationDto ProjectInformation { get; set; }
    }
}
