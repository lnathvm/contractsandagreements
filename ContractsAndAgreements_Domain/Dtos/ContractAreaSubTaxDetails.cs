﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContractsAndAgreements_Domain.Dtos
{
    public class ContractAreaSubTaxDetails
    {
        public int ContractInformationSubTaxDetailsId { get; set; }
        public int ProjectContractInformationId { get; set; }
        public int SubId { get; set; }
        public string TaxId { get; set; }
    }
}
