﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContractsAndAgreements_Domain.Dtos
{
    public class ProjectAddendumWorkFlowTrackingDto
    {
        public int ProjectAddendumWorkFlowTrackingId { get; set; }
        public int SupervisorId { get; set; }
        public string Processor { get; set; }
        public string Phone { get; set; }
        public string Comments { get; set; }
        //public bool Suspended { get; set; }
        public string InfoReceived { get; set; }
        public string TaskAssignedToStaff { get; set; }
        public string AddendumDate { get; set; }
        public string ClosingDate { get; set; }


    }
}
