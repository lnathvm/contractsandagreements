﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContractsAndAgreements_Domain.Dtos
{
    public class ProjectAdvertisementShortlistDto
    {
        public int ProjectGeneralInformationId { get; set; }
        public int ProjectAdvertisementShortlistId { get; set; }
        //   public string Title39 { get; set; }
        public string AdvertisementDate { get; set; }
        public string ClosingDate { get; set; }

        public string AmmendedClosingDate { get; set; }
        public string ShortListDate { get; set; }
        //public string ContractNo { get; set; }
        //public string StateProjectno { get; set; }
        public string Subject { get; set; }

        //    public string MinimumManPower { get; set; }
        //public string ShortList { get; set; }
        //public string ShortList2 { get; set; }
        //public string ShortList3 { get; set; }
        public string ConsultantProviderName { get; set; }

        public string Comment1 { get; set; }
        public string Comment2 { get; set; }
        public bool IsPostToWeb { get; set; }
        //public IList<ShortlistSubDetailsDto> SubTaxDetailsDto1 { get; set; }
        //public IList<ShortlistSubDetailsDto> SubTaxDetailsDto2 { get; set; }
        //public IList<ShortlistSubDetailsDto> SubTaxDetailsDto3 { get; set; }

        public IList<ShortListDto> ShortList { get; set; }

    }

    public class ShortListDto
    {
        public string ShortList { get; set; }
        public int Rank { get; set; }
        public IList<ShortlistSubDetailsDto> SubTaxDetailsDto { get; set; }
    }
}
