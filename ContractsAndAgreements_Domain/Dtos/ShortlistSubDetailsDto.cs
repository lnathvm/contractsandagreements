﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContractsAndAgreements_Domain.Dtos
{
    public class ShortlistSubDetailsDto
    {
        public int ShortlistSubDetailsId { get; set; }
        public int ProjectAdvertisementShortlistConsultantProviderId { get; set; }
        public int SubId { get; set; }

        public string ConsultantProviderName { get; set; }

    }
}
