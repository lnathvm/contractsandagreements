﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContractsAndAgreements_Domain.Dtos
{
    public class ProjectContractDetailsDto
    {
        public int ProjectContractDetailsId { get; set; }
        public int ProjectGeneralInformationId { get; set; }
        public int ProjectContractInformationId { get; set; }
        public int ProjectContractWorkFlowTrackingId { get; set; }
        public string ProjectDetails { get; set; }
        public string ContractAction { get; set; }
        public string ExpDate { get; set; }
        public int ProjectManagerId { get; set; }
        public string ManagerPhone { get; set; }
        //public ProjectGeneralInformationDto Information { get; set; }
        public ProjectContractInformationDto ContractInformation { get; set; }
        public ProjectContractWorkFlowTrackingDto ProjectWork { get; set; }
    }
}
