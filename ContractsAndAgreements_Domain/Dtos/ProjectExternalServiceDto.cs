﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContractsAndAgreements_Domain.Dtos
{
    public class ProjectExternalServiceDto
    {
        public string ContractNumber { get; set; }
        public string ContractName { get; set; }
        public string ContractorConsultant { get; set; }
        public string ContractorId { get; set; }
        public string FederalAidProjectNumber { get; set; }
        public string FinalAcceptanceDate { get; set; }
        public string ParishNumber { get; set; }
        public string ParishName { get; set; }
        public string ProjectManager { get; set; }
        public string ProjectName { get; set; }
        public string RouteNumber { get; set; }
        public string StateProjectNumber { get; set; }

    }
}
