﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContractsAndAgreements_Domain.Dtos
{
    public class CommonDropdownDto
    {
        public int Id { get; set; }

        public string Name { get; set; }
        public int Rank { get; set; }

    }
}
