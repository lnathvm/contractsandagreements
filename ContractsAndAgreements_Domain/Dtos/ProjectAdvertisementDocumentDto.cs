﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContractsAndAgreements_Domain.Dtos
{
    public class ProjectAdvertisementDocumentDto
    {
        public int AdvertisementDocumentMasterId { get; set; }
        public int ProjectAdvertisementDetailsId { get; set; }
        public string AdvertisementDate { get; set; }
        public string ClosingDate { get; set; }
        public string AdvertDateGoesHere { get; set; }
        public string TypeOfAdvertGoesHere { get; set; }
        public string Details { get; set; }
        public string Document { get; set; }
        public string DBEGoal { get; set; }
        public string ContractNo { get; set; }
        public string ProjectName { get; set; }
        public string StateProjectNo { get; set; }
        public string FAPNo { get; set; }
        public string Parish { get; set; }
        public string Comment1 { get; set; }
        public string Comment2 { get; set; }
        public string Comment3 { get; set; }
        public bool IsPostToWeb { get; set; }
    }
}
