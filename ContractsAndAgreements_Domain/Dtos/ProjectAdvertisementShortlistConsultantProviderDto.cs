﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContractsAndAgreements_Domain.Dtos
{
    public class ProjectAdvertisementShortlistConsultantProviderDto
    {
        public int ProjectAdvertisementShortlistConsultantProviderId { get; set; }
        public int ProjectAdvertisementShortListId { get; set; }
        public int SelectedShortlistConsultantProviderId { get; set; }
    }
}
