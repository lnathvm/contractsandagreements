﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContractsAndAgreements_Domain.Dtos
{
    public class ProjectAdvertisementSelectionDto
    {
        public int ProjectGeneralInformationId { get; set; }
        public int ProjectAdvertisementSelectionId { get; set; }
        //public string Title39 { get; set; }
        public string AdvertisementDate { get; set; }
        public string ClosingDate { get; set; }
        public string AmmendedClosingDate { get; set; }
        public string SelectionDate { get; set; }
        //public string ContractNo { get; set; }
        //public string StateProjectno { get; set; }
        public string Subject { get; set; }

        //    public string MinimumManPower { get; set; }
        public string Selection { get; set; }
        public string Comment1 { get; set; }
        public string Comment2 { get; set; }
        public bool IsPostToWeb { get; set; }
        public int Rank { get; set; }

    }
}
