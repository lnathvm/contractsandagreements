﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContractsAndAgreements_Domain.Dtos
{
    public class ConsultantAreaSubTaxDetailsDto
    {

        public int ConsultantAreaSubTaxDetailsId { get; set; }

        public int ProjectConsultantAreaId { get; set; }

        public int SubId { get; set; }

        public string TaxId { get; set; }

        public string ContractAmount { get; set; }

    }
}
