﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContractsAndAgreements_Domain.Dtos
{
    public class ProjectAdvertisementApparentSelectionDto
    {
        public int ProjectGeneralInformationId { get; set; }
        public int ProjectAdvertisementApparentSelectionId { get; set; }
        //public string Title39 { get; set; }

        public string AdvertisementDate { get; set; }
        public string ClosingDate { get; set; }

        public string AmmendedClosingDate { get; set; }
        public string ApparentSelectionDate { get; set; }


        //public string ContractNo { get; set; }

        //public string StateProjectno { get; set; }
        public string Subject { get; set; }

        public string ApparentSelection { get; set; }

        public string Comment1 { get; set; }
        public string Comment2 { get; set; }
        public bool IsPostToWeb { get; set; }
        public int Rank { get; set; }



    }
}
