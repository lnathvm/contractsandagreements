﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContractsAndAgreements_Domain.Dtos
{
    public class ProjectContractTaskOrderMoneyBalanceDto
    {
        public int ProjectGeneralInformationId { get; set; }
        public string StateProjectNo { get; set; }
        public decimal? ContractAmount { get; set; }
        public string ContractAction { get; set; }
        public string ContractDate { get; set; }
        public decimal? TaskOrderAmount { get; set; }
        public string TaskForProject { get; set; }
        public string TODate { get; set; }
    }
}
