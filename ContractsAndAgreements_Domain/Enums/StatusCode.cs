﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContractsAndAgreements_Domain.Enums
{
    public enum StatusCode
    {
        Success = 1,  
        Deleted=2,
        Updated = 3,
        NotFound = 4,
        RecordAlreadyExist = 5,
        EmailDoesNotExist = 6,
        PasswordNotMatch = 7,
        UserNameExist=8,
        UserNotVerify=9,
        BadRequest=10,


    }
}
