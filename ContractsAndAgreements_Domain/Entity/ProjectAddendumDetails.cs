﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContractsAndAgreements_Domain.Entity
{
    public class ProjectAddendumDetails
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ProjectAddendumId { get; set; }
        public int ProjectGeneralInformationId { get; set; }
        public ProjectGeneralInformation ProjectGeneralInformation { get; set; }
        public int ProjectAddendumWorkFlowId { get; set; }
        public ProjectAddendumWorkFlowTracking ProjectAddendumWorkFlowTracking { get; set; }
        //public string ProjectDetails { get; set; }
        //public string MinimumManPower { get; set; }
        //public string FundingSource { get; set; }
        public TimeSpan ContractTime { get; set; }
        // public int ProjectManagerId { get; set; }
        //public string AddendumComments { get; set; }
        public string FAPNo { get; set; }
        public string Route { get; set; }
        public string GeneralDescription { get; set; }
        public string AddenDescription { get; set; }
        //public int ContractType { get; set; }
        //public int FeeType { get; set; }
        public string ContractAmountAdjustment { get; set; }
        public string DBEGoel { get; set; }
        //public string ManagerPhone { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }

    }
}
