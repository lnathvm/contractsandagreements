﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContractsAndAgreements_Domain.Entity
{
    public class ProjectAdvertisementShortlist
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ProjectAdvertisementShortlistId { get; set; }

        public int ProjectGeneralInformationId { get; set; }
        

        public string AdvertisementDate { get; set; }
        public string ClosingDate { get; set; }

        public string AmmendedClosingDate { get; set; }
        public string ShortListDate { get; set; }


        //public string ContractNo { get; set; }

        //public string StateProjectno { get; set; }
        public string Subject { get; set; }
        //public string ShortList { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }

        public string Comment1 { get; set; }
        public string Comment2 { get; set; }

        public bool IsPostToWeb { get; set; }

        public bool IsDeleted { get; set; }
    }
}
