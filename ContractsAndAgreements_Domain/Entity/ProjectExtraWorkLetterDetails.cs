﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContractsAndAgreements_Domain.Entity
{
    public class ProjectExtraWorkLetterDetails
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ProjectExtraWorkLetterDetailId { get; set; }
        public int ProjectGeneralInformationId { get; set; }
        public ProjectGeneralInformation ProjectGeneralInformation { get; set; }
        public string ReasonforEWL { get; set; }
        public string ExtraWorkAction { get; set; }
        public string ContractExpries { get; set; }
        public string Commnets { get; set; }
        public string InformationAmount { get; set; }
        public string InfoRecAndLogged { get; set; }
        public string TaskAssignedToStaff { get; set; }
        public string NTPLetter { get; set; }
        public string NTPDate { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }
    }
}
