﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContractsAndAgreements_Domain.Entity
{
    public class ProjectConsultantArea
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ProjectConsultantAreaId { get; set; }
        public int ProjectGeneralInformationId { get; set; }
        public ProjectGeneralInformation GeneralProjectInformation { get; set; }
        public int ProjectManagerId { get; set; }
        public string ManagerPhone { get; set; }
        public int ConsultantProviderId { get; set; }
        public string ContactPerson { get; set; }
        public string Title { get; set; }
        public string JobTitle { get; set; }
        public string OfficeAddress { get; set; }
        public string Fax { get; set; }
        public string TaxId { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        [Column(TypeName = "smalldatetime")]
        public DateTime CreatedAt { get; set; }
        [Column(TypeName = "smalldatetime")]
        public DateTime? UpdatedAt { get; set; }
    }
}
