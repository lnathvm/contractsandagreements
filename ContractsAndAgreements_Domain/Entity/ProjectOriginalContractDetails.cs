﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContractsAndAgreements_Domain.Entity
{
    public class ProjectOriginalContractDetails
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ProjectOriginalContractDetailId { get; set; }
        public int ProjectGeneralInformationId { get; set; }
        public ProjectGeneralInformation ProjectGeneralInformation { get; set; }
        public string ContractAction { get; set; }
        public string ContractExpries { get; set; }
        public string Commnets { get; set; }
        public string InfoRecAndLogged { get; set; }
        public string TaskAssignedToStaff { get; set; }
        public string DrafttoSupervisor { get; set; }
        public string ApprovedfromSupervisor { get; set; }
        public string FeePackagetoChiefEngineer { get; set; }
        public string ApprovedFromFeePackagetoChiefEngineer { get; set; }
        public string FeeNegotiationsTOProjMgr { get; set; }
        public string Complete { get; set; }
        public string FedAideFHWAANDAuth { get; set; }
        public string Authorized { get; set; }
        public string DraftToManagerForReview { get; set; }
        public string DraftToManagerForReviewConcurred { get; set; }
        public string DraftToConsultantForReview { get; set; }
        public string DraftToConsultantForReviewConcurred { get; set; }
        public string ContractToConsultant { get; set; }
        public string ContractTo3rdFloor { get; set; }
        public string SignedExecuted { get; set; }
        public string NTPLetter { get; set; }
        public string NTPDate { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }
    }
}
