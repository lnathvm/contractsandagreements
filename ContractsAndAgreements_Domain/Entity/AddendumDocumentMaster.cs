﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContractsAndAgreements_Domain.Entity
{
    public class AddendumDocumentMaster
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int AddendumDocumentMasterId { get; set; }
        public int ProjectAddendumId { get; set; }
        public ProjectAddendumDetails ProjectAddendumDetails { get; set; }
        public DateTime AdvertisementDate { get; set; }
        public DateTime AddendumDate { get; set; }
        public DateTime ClosingDate { get; set; }
        public string Addendumno { get; set; }
        //public string StateProjectNo { get; set; }
        //public string FederalAidProjectNo { get; set; }

        //public string Idiqcpm { get; set; }
        //public string Route { get; set; }
        //public string StateWideParish { get; set; }
        public string ContractNo { get; set; }
        public string ProjectName { get; set; }
        public string StateProjectNo { get; set; }
        public string FAPNo { get; set; }
        public string Parish { get; set; }

        public string DBEGoel { get; set; }
        public string Comment1 { get; set; }

        public string Comment2 { get; set; }
        public string Comment3 { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
    }
}
