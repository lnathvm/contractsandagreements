﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContractsAndAgreements_Domain.Entity
{
    public class ProjectExtraWorkLetterInformationDetails
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]

        public int ProjectExtraWorkLetterInformationId { get; set; }
        public int ProjectExtraWorkLetterDetailId { get; set; }
        public ProjectExtraWorkLetterDetails ProjectExtraWorkLetterDetails { get; set; }
        public decimal? ContractAmount { get; set; }
        public decimal? EWLAmount { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }
    }
}
