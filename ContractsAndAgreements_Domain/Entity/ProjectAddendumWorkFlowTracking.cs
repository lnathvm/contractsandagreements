﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContractsAndAgreements_Domain.Entity
{
    public class ProjectAddendumWorkFlowTracking
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ProjectAddendumWorkFlowId { get; set; }
        public int SupervisorId { get; set; }
        public SuperVisorMaster SupervisorMaster { get; set; }
        public string Processor { get; set; }
        public string Phone { get; set; }
        public string Comments { get; set; }
        public string InfoReceived { get; set; }

        public string TaskAssignedToStaff { get; set; }

        public string AddendumDate { get; set; }

        public string ClosingDate { get; set; }
        public DateTime CreatedAt { get; set; }

    }
}
