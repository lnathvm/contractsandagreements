﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContractsAndAgreements_Domain.Entity
{
    public class ProjectPublishedDetails
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ProjectPublishedId { get; set; }
        public int ProjectGeneralInformationId { get; set; }
        public ProjectGeneralInformation ProjectGeneralInformation { get; set; }
        public int ProjectDocumentId { get; set; }
        public int AdvertismentId { get; set; }
        public string AdvertismentType { get; set; }
        public bool IsAdvertisment { get; set; }
        public DateTime CreatedAt { get; set; }

    }
}
