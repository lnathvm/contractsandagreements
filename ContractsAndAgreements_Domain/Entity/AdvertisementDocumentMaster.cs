﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContractsAndAgreements_Domain.Entity
{
    public class AdvertisementDocumentMaster
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int AdvertisementDocumentMasterId { get; set; }
        public int ProjectAdvertisementDetailsId { get; set; }
        public ProjectAdvertisementDetails ProjectAdvertisementDetails { get; set; }
        public DateTime AdvertisementDate { get; set; }
        public DateTime ClosingDate { get; set; }
        public DateTime AdvertDateGoesHere { get; set; }
        public string TypeOfAdvertGoesHere { get; set; }
        public string Details { get; set; }
        public string Document { get; set; }

        //public string ContractNo { get; set; }
        //public string ProjectName { get; set; }
        //public string StateProjectNo { get; set; }
        //public string FAPNo { get; set; }
        //public string Parish { get; set; }
        //public string DBEGoal { get; set; }
        public string Comment1 { get; set; }
        public string Comment2 { get; set; }
        public string Comment3 { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
        public bool IsPostToWeb { get; set; }
    }
}
