﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContractsAndAgreements_Domain.Entity
{
    public class ProjectSupplementalAgreementSubTaxDetails
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ProjectSupplementalAgreementSubTaxDetailId { get; set; }
        public int ProjectSupplementalAgreementInformationId { get; set; }
        public ProjectSupplementalAgreementInformationDetails ProjectSupplementalAgreementInformationDetails { get; set; }
        public int SubId { get; set; }
        public string TaxId { get; set; }
        public decimal? ContractAmount { get; set; }
        [Column(TypeName = "smalldatetime")]
        public DateTime CreatedAt { get; set; }
        [Column(TypeName = "smalldatetime")]
        public DateTime? UpdatedAt { get; set; }
    }
}
