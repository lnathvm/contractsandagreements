﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContractsAndAgreements_Domain.Entity
{
    public class ProjectSupplementalAgreementForIDIQ
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ProjectSupplementalAgreementForIDIQId { get; set; }
        public int ProjectGeneralInformationId { get; set; }
        public int ProjectSupplementalAgreementInformationForIDIQId { get; set; }
        public ProjectSupplementalAgreementInformationForIDIQ ProjectSupplementalAgreementInformationForIDIQ { get; set; }
        //public ProjectGeneralInformation ProjectGeneralInformation { get; set; }
        public string SAForProject { get; set; }
        public string SAScope { get; set; }
        public string TaxClassification { get; set; }
        public string SAExpirationDate { get; set; }
        public string ContractExpires { get; set; }

        //public int ProjectManagerId { get; set; }
        //public string ManagerPhone { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }
    }
}
