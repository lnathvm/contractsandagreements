﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContractsAndAgreements_Domain.Entity
{
    public class ConsultantAreaSubTaxMaster
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ConsultantAreaSubTaxMasterId { get; set; }
        public string SubName { get; set; }
        public bool IsActive { get; set; }

    }
}
