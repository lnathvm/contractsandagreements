﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContractsAndAgreements_Domain.Entity
{
    public class CommunicationSettingMaster
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int CommunicationSettingId { get; set; }
        public string CommunicationType { get; set; }
        public string ToEmail { get; set; }
        public string BccEmail { get; set; }
        public string Subject { get; set; }
        public string Message { get; set; }
    }
}
