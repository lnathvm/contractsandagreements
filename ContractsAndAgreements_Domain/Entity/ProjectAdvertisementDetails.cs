﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContractsAndAgreements_Domain.Entity
{
    public class ProjectAdvertisementDetails
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ProjectAdvertisementDetailsId { get; set; }
        public int ProjectGeneralInformationId { get; set; }
        public ProjectGeneralInformation ProjectGeneralInformation { get; set; }
        public int ProjectWorkFlowTrackingId { get; set; }
        public ProjectWorkFlowTracking ProjectWorkFlowTracking { get; set; }
        public string ProjectDetails { get; set; }
        //public string MinimumManPower { get; set; }
        public string FundingSource { get; set; }
        //public string DBEGoal { get; set; }
        public string AdvertisedContractAmount { get; set; }
        public string ContractTime { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }
    }
}
