﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContractsAndAgreements_Domain.Entity
{
    public class ProjectSupplementalAgreementInformationForIDIQ
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ProjectSupplementalAgreementInformationForIDIQId { get; set; }
        public decimal? ContractAmount { get; set; }
        public decimal? TOAmount { get; set; }
        public string Prime { get; set; }
        public decimal? PrimeAmount { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }
    }
}
