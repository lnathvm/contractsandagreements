﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContractsAndAgreements_Domain.Entity
{
    public class ProjectContractWorkFlowTracking
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ProjectContractWorkFlowTrackingId { get; set; }
        public string Supervisor { get; set; }
        public string Processor { get; set; }
        public string Phone { get; set; }
        public string Comments { get; set; }
        public bool Suspended { get; set; }
        public string InfoReceived { get; set; }
        public string TaskAssignedToStaff { get; set; }
        public string DraftandFeePacktoSupervisor { get; set; }
        public string ApprovedfromSupervisor { get; set; }
        public string DraftandFeePacktoProjectManager { get; set; }
        public string ConcurredFromProjectManager { get; set; }
        public string DrafttoConsultant { get; set; }
        public string ConcurredFromConsultant { get; set; }
        public string FedAideFHWAAuth { get; set; }
        public string Authorized { get; set; }
        public string ContractToConsultant { get; set; }
        public string Signed { get; set; }
        public string ContractTo3rdFloor { get; set; }
        public string SignedExecuted { get; set; }
        public string NTPLetter { get; set; }
        public string EffectiveDate { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }

    }
}
