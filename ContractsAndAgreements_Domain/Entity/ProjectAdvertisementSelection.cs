﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContractsAndAgreements_Domain.Entity
{
    public class ProjectAdvertisementSelection
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ProjectAdvertisementSelectionId { get; set; }

        public int ProjectGeneralInformationId { get; set; }
        public string Title39 { get; set; }

        public string AdvertisementDate { get; set; }
        public string ClosingDate { get; set; }

        public string AmmendedClosingDate { get; set; }
        public string SelectionDate { get; set; }


       // public string ContractNo { get; set; }

       // public string StateProjectno { get; set; }
        public string Subject { get; set; }
        public string Selection { get; set; }
        public int Rank { get; set; }
        public string Comment1 { get; set; }
        public string Comment2 { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }

        public bool IsPostToWeb { get; set; }

    }
}
