﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContractsAndAgreements_Domain.Entity
{
    public class ProjectTracking
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ProjectTrackingId { get; set; }
        public int ProjectGeneralInformationId { get; set; }
        public ProjectGeneralInformation GeneralProjectInformation { get; set; }
        public string Received { get; set; }
        public string AwardNotification { get; set; }
        public string Closed { get; set; }
        public string FinalInvoicedRecvd { get; set; }
        public string FinalToAcct { get; set; }
        public string AuditReport { get; set; }
        public string FinalCost { get; set; }
        [Column(TypeName = "smalldatetime")]
        public DateTime CreatedAt { get; set; }
        [Column(TypeName = "smalldatetime")]
        public DateTime? UpdatedAt { get; set; }

    }
}
