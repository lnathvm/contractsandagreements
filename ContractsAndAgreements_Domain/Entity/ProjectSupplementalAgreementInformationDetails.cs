﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContractsAndAgreements_Domain.Entity
{
    public class ProjectSupplementalAgreementInformationDetails
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ProjectSupplementalAgreementInformationId { get; set; }
        public int ProjectSupplementalAgreementDetailId { get; set; }
        public ProjectSupplementalAgreementDetails ProjectSupplementalAgreementDetails { get; set; }
        public decimal? ContractAmount { get; set; }
        public decimal? SAAmount { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }
    }
}
