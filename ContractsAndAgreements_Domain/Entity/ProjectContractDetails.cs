﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContractsAndAgreements_Domain.Entity
{
    public class ProjectContractDetails
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ProjectContractDetailsId { get; set; }
        public int ProjectGeneralInformationId { get; set; }
        public ProjectGeneralInformation ProjectGeneralInformation { get; set; }
        public int ProjectContractInformationId { get; set; }
        public ProjectContractInformation ProjectContractInformation { get; set; }
        public int ProjectContractWorkFlowTrackingId { get; set; }
        public ProjectContractWorkFlowTracking ProjectContractWorkFlowTracking { get; set; }
        public string ProjectDetails { get; set; }
        public string ContractAction { get; set; }

        public string ExpDate { get; set; }
        public int ProjectManagerId { get; set; }
        public string ManagerPhone { get; set; }

        public DateTime CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }

    }
}
