﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ContractsAndAgreements_Domain.Entity
{
    public class ProjectWorkFlowTracking
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ProjectWorkFlowTrackingId { get; set; }
        public string Supervisor { get; set; }
        public string Processor { get; set; }
        public string Phone { get; set; }
        public string Comments { get; set; }
        public bool Suspended { get; set; }
        public string InfoReceived { get; set; }
        public string TaskAssignedToStaff { get; set; }
        public string ChiefApprovalToRetain { get; set; }
        public string DraftToSupervisor { get; set; }
        public string ApprovedFromSupervisor { get; set; }
        public string DraftToProjectManager { get; set; }
        public string ApprovedFromProjectManager { get; set; }
        //public string DraftToChiefEngineer { get; set; }
        //public string ApprovedFromChiefEngineer { get; set; }
        public string DraftToFHWA { get; set; }
        public string ApprovedFromFHWA { get; set; }
        //public string FedAideFHWA { get; set; }
        public string AdvertisementDate { get; set; }
        public string ClosingDate { get; set; }
        public string AmendedClosingDate { get; set; }
        public string Shortlist { get; set; }
        public string AwardNotification { get; set; }
        public string PackettoSecretary { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }

    }
}