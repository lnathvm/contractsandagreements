﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContractsAndAgreements_Domain.Entity
{
    public class ProjectTaskOrderDetails
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ProjectTaskOrderDetailsId { get; set; }
        public int ProjectGeneralInformationId { get; set; }
        public int ProjectTaskOrderInformationId { get; set; }
        public ProjectTaskOrderInformation ProjectTaskOrderInformation { get; set; }
        public ProjectGeneralInformation ProjectGeneralInformation { get; set; }
        //  public int ProjectTaskOrderWorkFlowTrackingId { get; set; }
        //   public ProjectTaskOrderWorkFlowTracking ProjectTaskOrderWorkFlowTracking { get; set; }
        public string TaskForProject { get; set; }
        public string TaskScope { get; set; }
        public string TaxClassification { get; set; }
        public string TaskExpirationDate { get; set; }
        public string ContractExpires { get; set; }

        public int ProjectManagerId { get; set; }
        public string ManagerPhone { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }
    }
}
