﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContractsAndAgreements_Domain.Entity
{
    public class ProjectSupplementalAgreementInformationForIDIQSubTaxDetails
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ProjectSupplementalAgreementInformationForIDIQSubTaxDetailsId { get; set; }
        public int ProjectSupplementalAgreementInformationForIDIQId { get; set; }
        public int SubId { get; set; }
        public string TaxId { get; set; }
        public decimal? ContractAmount { get; set; }
        [Column(TypeName = "smalldatetime")]
        public DateTime CreatedAt { get; set; }
        [Column(TypeName = "smalldatetime")]
        public DateTime? UpdatedAt { get; set; }
    }
}
