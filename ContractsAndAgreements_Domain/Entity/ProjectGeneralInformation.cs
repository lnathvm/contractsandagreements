﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContractsAndAgreements_Domain.Entity
{
    public class ProjectGeneralInformation
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ProjectGeneralInformationId { get; set; }
        public string StateProjectNo { get; set; }
        public string ContractNo { get; set; }
        public string FAPNo { get; set; }
        public string ProjectName { get; set; }
        public string Route { get; set; }
        public int Parish { get; set; }
        public string GeneralDescription { get; set; }
        //public string EstimatedConstructionCost { get; set; }
        public int Selection { get; set; }
        public bool FHWAFullOversight { get; set; }
        //public bool TimedProject { get; set; }
        public int ContractType { get; set; }
        public int FeeType { get; set; }

        public int FeeTypee { get; set; }

        public string CompensationType { get; set; }
        public string EStContractAmount { get; set; }
        public string ContractAgency { get; set; }
        public string Comment { get; set; }
        public string DBEGoel { get; set; }

        public bool IsDelete { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime CreatedAt { get; set; }
        [Column(TypeName = "smalldatetime")]
        public DateTime? UpdatedAt { get; set; }



    }
}
