﻿using ContractsAndAgreements_DomainLogic.Services.Implementation;
using ContractsAndAgreements_DomainLogic.Services.Interfaces;
using LightInject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContractsAndAgreements_DomainLogic
{
    public class DomainLogicCompositionRoot : ICompositionRoot
    {
        public void Compose(IServiceRegistry serviceRegistry)
        {
            serviceRegistry.Register<IProjectService, ProjectService>();
            serviceRegistry.Register<ICommonService, CommonService>();
            serviceRegistry.Register<IAdvertisementService, AdvertisementService>();
            serviceRegistry.Register<ICommunicationService, CommunicationService>();

        }
    }
}
