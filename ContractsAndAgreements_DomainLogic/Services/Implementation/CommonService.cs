﻿using ContractsAndAgreements_DataAccess.Repository;
using ContractsAndAgreements_DataAccess.UnitofWorks;
using ContractsAndAgreements_Domain.Entity;
using ContractsAndAgreements_Domain.Enums;
using ContractsAndAgreements_DomainLogic.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContractsAndAgreements_DomainLogic.Services.Implementation
{
    public class CommonService : ICommonService
    {
        private readonly IUnitOfWork _unitOfWork;

        private readonly IRepository<ContractTypeMaster> _ContractTypeMaster;
        private readonly IRepository<FeeTypeMaster> _FeeTypeMaster;
        private readonly IRepository<ParishMaster> _ParishMaster;
        private readonly IRepository<SelectionMaster> _SelectionMaster;
        private readonly IRepository<ProjectManagerMaster> _ProjectManagerMaster;
        private readonly IRepository<FeeTypeMasterr> _FeeTypeMasterr;
        private readonly IRepository<ConsultantAreaSubTaxMaster> _ConsultantAreaSubTaxMaster;
        private readonly IRepository<ConsultantProviderMaster> _ConsultantProviderMaster;
        private readonly IRepository<CommunicationSettingMaster> _CommunicationSettingMaster;
        private readonly IRepository<SuperVisorMaster> _SuperVisorMaster;
        private readonly IRepository<ProcessorMaster> _ProcessorMaster;
        private readonly IRepository<CompensationTypeMaster> _CompensationTypeMaster;
        private readonly IRepository<ContractAgencyMaster> _ContractAgencyMaster;

        public CommonService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
            _ContractTypeMaster = _unitOfWork.CreateRepository<ContractTypeMaster>();
            _FeeTypeMaster = _unitOfWork.CreateRepository<FeeTypeMaster>();
            _FeeTypeMasterr = _unitOfWork.CreateRepository<FeeTypeMasterr>();
            _ParishMaster = _unitOfWork.CreateRepository<ParishMaster>();
            _SelectionMaster = _unitOfWork.CreateRepository<SelectionMaster>();
            _ProjectManagerMaster = _unitOfWork.CreateRepository<ProjectManagerMaster>();
            _ConsultantProviderMaster = _unitOfWork.CreateRepository<ConsultantProviderMaster>();
            _CommunicationSettingMaster = _unitOfWork.CreateRepository<CommunicationSettingMaster>();
            _SuperVisorMaster = _unitOfWork.CreateRepository<SuperVisorMaster>();
            _ProcessorMaster = _unitOfWork.CreateRepository<ProcessorMaster>();
            _CompensationTypeMaster = _unitOfWork.CreateRepository<CompensationTypeMaster>();
            _ContractAgencyMaster = _unitOfWork.CreateRepository<ContractAgencyMaster>();
        }
        public async Task<IList<ContractTypeMaster>> GetAllContractTypeList()
        {
            try
            {
                return (await _ContractTypeMaster.FindAsync(c => c.IsActive == true)).ToList();
            }
            catch (DbEntityValidationException dbEx)
            {
                var errorMessages = dbEx.EntityValidationErrors
                    .SelectMany(x => x.ValidationErrors)
                    .Select(x => x.ErrorMessage);

                var fullErrorMessage = string.Join("; ", errorMessages);

                var exceptionMessage = string.Concat(dbEx.Message, " The validation errors are: ", fullErrorMessage);

                throw new DbEntityValidationException(exceptionMessage, dbEx.EntityValidationErrors);
            }
        }

        public async Task<IList<FeeTypeMaster>> GetAllFeeTypeList()
        {
            try
            {
                return (await _FeeTypeMaster.FindAsync(c => c.IsActive == true)).ToList();
            }
            catch (DbEntityValidationException dbEx)
            {
                var errorMessages = dbEx.EntityValidationErrors
                    .SelectMany(x => x.ValidationErrors)
                    .Select(x => x.ErrorMessage);

                var fullErrorMessage = string.Join("; ", errorMessages);

                var exceptionMessage = string.Concat(dbEx.Message, " The validation errors are: ", fullErrorMessage);

                throw new DbEntityValidationException(exceptionMessage, dbEx.EntityValidationErrors);
            }
        }

        public async Task<IList<FeeTypeMasterr>> GetAllCompensationTypeList()
        {
            try
            {
                return (await _FeeTypeMasterr.FindAsync(c => c.IsActive == true)).ToList();
            }
            catch (DbEntityValidationException dbEx)
            {
                var errorMessages = dbEx.EntityValidationErrors
                    .SelectMany(x => x.ValidationErrors)
                    .Select(x => x.ErrorMessage);

                var fullErrorMessage = string.Join("; ", errorMessages);

                var exceptionMessage = string.Concat(dbEx.Message, " The validation errors are: ", fullErrorMessage);

                throw new DbEntityValidationException(exceptionMessage, dbEx.EntityValidationErrors);
            }
        }

        public async Task<IList<ParishMaster>> GetAllParishList()
        {
            try
            {
                return (await _ParishMaster.FindAsync(c => c.IsActive == true)).ToList();
            }
            catch (DbEntityValidationException dbEx)
            {
                var errorMessages = dbEx.EntityValidationErrors
                    .SelectMany(x => x.ValidationErrors)
                    .Select(x => x.ErrorMessage);

                var fullErrorMessage = string.Join("; ", errorMessages);

                var exceptionMessage = string.Concat(dbEx.Message, " The validation errors are: ", fullErrorMessage);

                throw new DbEntityValidationException(exceptionMessage, dbEx.EntityValidationErrors);
            }
        }

        public async Task<IList<SelectionMaster>> GetAllSelectionList()
        {
            try
            {
                return (await _SelectionMaster.FindAsync(c => c.IsActive == true)).ToList();
            }
            catch (DbEntityValidationException dbEx)
            {
                var errorMessages = dbEx.EntityValidationErrors
                    .SelectMany(x => x.ValidationErrors)
                    .Select(x => x.ErrorMessage);

                var fullErrorMessage = string.Join("; ", errorMessages);

                var exceptionMessage = string.Concat(dbEx.Message, " The validation errors are: ", fullErrorMessage);

                throw new DbEntityValidationException(exceptionMessage, dbEx.EntityValidationErrors);
            }
        }

        public async Task<IList<ProjectManagerMaster>> GetAllProjectManagerList()
        {
            try
            {
                return (await _ProjectManagerMaster.FindAsync(c => c.IsActive == true)).ToList();
            }
            catch (DbEntityValidationException dbEx)
            {
                var errorMessages = dbEx.EntityValidationErrors
                    .SelectMany(x => x.ValidationErrors)
                    .Select(x => x.ErrorMessage);

                var fullErrorMessage = string.Join("; ", errorMessages);

                var exceptionMessage = string.Concat(dbEx.Message, " The validation errors are: ", fullErrorMessage);

                throw new DbEntityValidationException(exceptionMessage, dbEx.EntityValidationErrors);
            }
        }

        //public async Task<IList<ConsultantAreaSubTaxMaster>> GetAllConsultantAreaSubTaxList()
        //{
        //    try
        //    {
        //        return (await _ConsultantAreaSubTaxMaster.FindAsync(c => c.IsActive == true)).ToList();
        //    }
        //    catch (DbEntityValidationException dbEx)
        //    {
        //        var errorMessages = dbEx.EntityValidationErrors
        //            .SelectMany(x => x.ValidationErrors)
        //            .Select(x => x.ErrorMessage);

        //        var fullErrorMessage = string.Join("; ", errorMessages);

        //        var exceptionMessage = string.Concat(dbEx.Message, " The validation errors are: ", fullErrorMessage);

        //        throw new DbEntityValidationException(exceptionMessage, dbEx.EntityValidationErrors);
        //    }
        //}
        public async Task<IList<ConsultantProviderMaster>> GetAllConsultantProviderList()
        {
            try
            {
                return (await _ConsultantProviderMaster.FindAsync(c => c.IsActive == true)).ToList();
            }
            catch (DbEntityValidationException dbEx)
            {
                var errorMessages = dbEx.EntityValidationErrors
                    .SelectMany(x => x.ValidationErrors)
                    .Select(x => x.ErrorMessage);

                var fullErrorMessage = string.Join("; ", errorMessages);

                var exceptionMessage = string.Concat(dbEx.Message, " The validation errors are: ", fullErrorMessage);

                throw new DbEntityValidationException(exceptionMessage, dbEx.EntityValidationErrors);
            }
        }

        public async Task<ConsultantProviderMaster> GetConsultantProviderDetail(int ConsultantProviderId)
        {
            try
            {
                return (await _ConsultantProviderMaster.FindAsync(c => c.IsActive == true && c.ConsultantProviderId == ConsultantProviderId)).FirstOrDefault();
            }
            catch (DbEntityValidationException dbEx)
            {
                var errorMessages = dbEx.EntityValidationErrors
                    .SelectMany(x => x.ValidationErrors)
                    .Select(x => x.ErrorMessage);

                var fullErrorMessage = string.Join("; ", errorMessages);

                var exceptionMessage = string.Concat(dbEx.Message, " The validation errors are: ", fullErrorMessage);

                throw new DbEntityValidationException(exceptionMessage, dbEx.EntityValidationErrors);
            }
        }

        public async Task<CommunicationSettingMaster> GetCommunicationSettingMaster(string type)
        {
            try
            {
                return (await _CommunicationSettingMaster.FindAsync(c => c.CommunicationType == type)).FirstOrDefault();
            }
            catch (DbEntityValidationException dbEx)
            {
                var errorMessages = dbEx.EntityValidationErrors
                    .SelectMany(x => x.ValidationErrors)
                    .Select(x => x.ErrorMessage);

                var fullErrorMessage = string.Join("; ", errorMessages);

                var exceptionMessage = string.Concat(dbEx.Message, " The validation errors are: ", fullErrorMessage);

                throw new DbEntityValidationException(exceptionMessage, dbEx.EntityValidationErrors);
            }
        }

        public async Task<IList<SuperVisorMaster>> GetAllSupervisorList()
        {
            try
            {
                return (await _SuperVisorMaster.FindAsync(c => c.IsActive == true)).ToList();
            }
            catch (DbEntityValidationException dbEx)
            {
                var errorMessages = dbEx.EntityValidationErrors
                    .SelectMany(x => x.ValidationErrors)
                    .Select(x => x.ErrorMessage);

                var fullErrorMessage = string.Join("; ", errorMessages);

                var exceptionMessage = string.Concat(dbEx.Message, " The validation errors are: ", fullErrorMessage);

                throw new DbEntityValidationException(exceptionMessage, dbEx.EntityValidationErrors);
            }
        }
        public async Task<IList<ProcessorMaster>> GetAllProcessorList()
        {
            try
            {
                return (await _ProcessorMaster.FindAsync(c => c.IsActive == true)).ToList();
            }
            catch (DbEntityValidationException dbEx)
            {
                var errorMessages = dbEx.EntityValidationErrors
                    .SelectMany(x => x.ValidationErrors)
                    .Select(x => x.ErrorMessage);

                var fullErrorMessage = string.Join("; ", errorMessages);

                var exceptionMessage = string.Concat(dbEx.Message, " The validation errors are: ", fullErrorMessage);

                throw new DbEntityValidationException(exceptionMessage, dbEx.EntityValidationErrors);
            }
        }


        public async Task<SuperVisorMaster> GetSupervisorDetails(int SupervisorId)
        {
            try
            {
                return (await _SuperVisorMaster.FindAsync(c => c.IsActive == true)).FirstOrDefault();
            }
            catch (DbEntityValidationException dbEx)
            {
                var errorMessages = dbEx.EntityValidationErrors
                    .SelectMany(x => x.ValidationErrors)
                    .Select(x => x.ErrorMessage);

                var fullErrorMessage = string.Join("; ", errorMessages);

                var exceptionMessage = string.Concat(dbEx.Message, " The validation errors are: ", fullErrorMessage);

                throw new DbEntityValidationException(exceptionMessage, dbEx.EntityValidationErrors);
            }
        }

        public async Task<StatusCode> SaveConsultantProvider(ConsultantProviderMaster consultant)
        {
            try
            {
                consultant.CreatedAt = DateTime.Now;
                _ConsultantProviderMaster.Insert(consultant);
                await _unitOfWork.SaveAsync();
                return StatusCode.Success;
            }
            catch (DbEntityValidationException dbEx)
            {
                var errorMessages = dbEx.EntityValidationErrors
                    .SelectMany(x => x.ValidationErrors)
                    .Select(x => x.ErrorMessage);

                var fullErrorMessage = string.Join("; ", errorMessages);

                var exceptionMessage = string.Concat(dbEx.Message, " The validation errors are: ", fullErrorMessage);

                throw new DbEntityValidationException(exceptionMessage, dbEx.EntityValidationErrors);
            }
        }
        public async Task<StatusCode> UpdateConsultantProvider(ConsultantProviderMaster consultant)
        {
            try
            {
                _ConsultantProviderMaster.Update(consultant);
                await _unitOfWork.SaveAsync();
                return StatusCode.Success;
            }
            catch (DbEntityValidationException dbEx)
            {
                var errorMessages = dbEx.EntityValidationErrors
                    .SelectMany(x => x.ValidationErrors)
                    .Select(x => x.ErrorMessage);

                var fullErrorMessage = string.Join("; ", errorMessages);

                var exceptionMessage = string.Concat(dbEx.Message, " The validation errors are: ", fullErrorMessage);

                throw new DbEntityValidationException(exceptionMessage, dbEx.EntityValidationErrors);
            }
        }

        public async Task<StatusCode> DeleteConsultantProvider(ConsultantProviderMaster consultant)
        {
            try
            {
                _ConsultantProviderMaster.Delete(consultant);
                await _unitOfWork.SaveAsync();
                return StatusCode.Success;
            }
            catch (DbEntityValidationException dbEx)
            {
                var errorMessages = dbEx.EntityValidationErrors
                    .SelectMany(x => x.ValidationErrors)
                    .Select(x => x.ErrorMessage);

                var fullErrorMessage = string.Join("; ", errorMessages);

                var exceptionMessage = string.Concat(dbEx.Message, " The validation errors are: ", fullErrorMessage);

                throw new DbEntityValidationException(exceptionMessage, dbEx.EntityValidationErrors);
            }
        }
        public async Task<IList<CompensationTypeMaster>> GetCompensationTypeList()
        {
            try
            {
                return (await _CompensationTypeMaster.FindAsync(c => c.IsActive == true)).ToList();
            }
            catch (DbEntityValidationException dbEx)
            {
                var errorMessages = dbEx.EntityValidationErrors
                    .SelectMany(x => x.ValidationErrors)
                    .Select(x => x.ErrorMessage);

                var fullErrorMessage = string.Join("; ", errorMessages);

                var exceptionMessage = string.Concat(dbEx.Message, " The validation errors are: ", fullErrorMessage);

                throw new DbEntityValidationException(exceptionMessage, dbEx.EntityValidationErrors);
            }
        }

        public async Task<IList<ContractAgencyMaster>> GetAllContractAgencyList()
        {
            try
            {
                return (await _ContractAgencyMaster.FindAsync(c => c.IsActive == true)).ToList();
            }
            catch (DbEntityValidationException dbEx)
            {
                var errorMessages = dbEx.EntityValidationErrors
                    .SelectMany(x => x.ValidationErrors)
                    .Select(x => x.ErrorMessage);

                var fullErrorMessage = string.Join("; ", errorMessages);

                var exceptionMessage = string.Concat(dbEx.Message, " The validation errors are: ", fullErrorMessage);

                throw new DbEntityValidationException(exceptionMessage, dbEx.EntityValidationErrors);
            }
        }
    }
}
