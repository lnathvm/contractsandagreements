﻿using ContractsAndAgreements_DataAccess.Repository;
using ContractsAndAgreements_DataAccess.StoredProcedures;
using ContractsAndAgreements_DataAccess.UnitofWorks;
using ContractsAndAgreements_Domain.Dtos;
using ContractsAndAgreements_Domain.Entity;
using ContractsAndAgreements_Domain.Enums;
using ContractsAndAgreements_DomainLogic.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContractsAndAgreements_DomainLogic.Services.Implementation
{

    public class ProjectService : IProjectService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IRepository<ProjectGeneralInformation> _ProjectGeneralInformation;
        private readonly IRepository<ProjectConsultantArea> _ProjectConsultantArea;
        private readonly IRepository<ConsultantAreaSubTaxDetails> _ConsultantAreaSubTaxDetails;
        private readonly IRepository<ProjectTracking> _ProjectTracking;
        private readonly IRepository<ContractTypeMaster> _ContractTypeMaster;
        private readonly IRepository<FeeTypeMaster> _FeeTypeMaster;
        private readonly IRepository<ParishMaster> _ParishMaster;
        private readonly IRepository<SelectionMaster> _SelectionMaster;

        public ProjectService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
            _ProjectGeneralInformation = _unitOfWork.CreateRepository<ProjectGeneralInformation>();
            _ProjectConsultantArea = _unitOfWork.CreateRepository<ProjectConsultantArea>();
            _ConsultantAreaSubTaxDetails = _unitOfWork.CreateRepository<ConsultantAreaSubTaxDetails>();
            _ProjectTracking = _unitOfWork.CreateRepository<ProjectTracking>();

            _ContractTypeMaster = _unitOfWork.CreateRepository<ContractTypeMaster>();
            _FeeTypeMaster = _unitOfWork.CreateRepository<FeeTypeMaster>();
            _ParishMaster = _unitOfWork.CreateRepository<ParishMaster>();
            _SelectionMaster = _unitOfWork.CreateRepository<SelectionMaster>();
        }

        public async Task<ProjectGeneralInformation> CreateProjectGeneralInformation(ProjectGeneralInformation projectGeneral)
        {
            try
            {
                projectGeneral.CreatedAt = DateTime.Now;
                _ProjectGeneralInformation.Insert(projectGeneral);
                await _unitOfWork.SaveAsync();
                return projectGeneral;
            }
            catch (DbEntityValidationException dbEx)
            {
                var errorMessages = dbEx.EntityValidationErrors
                    .SelectMany(x => x.ValidationErrors)
                    .Select(x => x.ErrorMessage);

                var fullErrorMessage = string.Join("; ", errorMessages);

                var exceptionMessage = string.Concat(dbEx.Message, " The validation errors are: ", fullErrorMessage);

                throw new DbEntityValidationException(exceptionMessage, dbEx.EntityValidationErrors);
            }
        }

        public async Task<ProjectConsultantArea> CreateProjectConsultantArea(ProjectConsultantArea consultantArea)
        {
            try
            {
                consultantArea.CreatedAt = DateTime.Now;
                _ProjectConsultantArea.Insert(consultantArea);
                await _unitOfWork.SaveAsync();
                return consultantArea;
            }
            catch (DbEntityValidationException dbEx)
            {
                var errorMessages = dbEx.EntityValidationErrors
                    .SelectMany(x => x.ValidationErrors)
                    .Select(x => x.ErrorMessage);

                var fullErrorMessage = string.Join("; ", errorMessages);

                var exceptionMessage = string.Concat(dbEx.Message, " The validation errors are: ", fullErrorMessage);

                throw new DbEntityValidationException(exceptionMessage, dbEx.EntityValidationErrors);
            }
        }

        public async Task<StatusCode> CreateConsultantAreaSubTaxDetails(IList<ConsultantAreaSubTaxDetails> subTaxDetails)
        {
            try
            {
                _ConsultantAreaSubTaxDetails.InsertRange(subTaxDetails);
                await _unitOfWork.SaveAsync();
                return StatusCode.Success;
            }
            catch (DbEntityValidationException dbEx)
            {
                var errorMessages = dbEx.EntityValidationErrors
                    .SelectMany(x => x.ValidationErrors)
                    .Select(x => x.ErrorMessage);

                var fullErrorMessage = string.Join("; ", errorMessages);

                var exceptionMessage = string.Concat(dbEx.Message, " The validation errors are: ", fullErrorMessage);

                throw new DbEntityValidationException(exceptionMessage, dbEx.EntityValidationErrors);
            }
        }

        public async Task<ProjectTracking> CreateProjectTracking(ProjectTracking projectTracking)
        {
            try
            {
                projectTracking.CreatedAt = DateTime.Now;
                _ProjectTracking.Insert(projectTracking);
                await _unitOfWork.SaveAsync();
                return projectTracking;
            }
            catch (DbEntityValidationException dbEx)
            {
                var errorMessages = dbEx.EntityValidationErrors
                    .SelectMany(x => x.ValidationErrors)
                    .Select(x => x.ErrorMessage);

                var fullErrorMessage = string.Join("; ", errorMessages);

                var exceptionMessage = string.Concat(dbEx.Message, " The validation errors are: ", fullErrorMessage);

                throw new DbEntityValidationException(exceptionMessage, dbEx.EntityValidationErrors);
            }
        }

        public async Task<ProjectGeneralInformation> GetProjectGeneralInformation(int ProjectGeneralInformationId)
        {
            try
            {
                return (await _ProjectGeneralInformation.FindAsync(p => p.ProjectGeneralInformationId == ProjectGeneralInformationId && p.IsDelete == false)).FirstOrDefault();
            }
            catch (DbEntityValidationException dbEx)
            {
                var errorMessages = dbEx.EntityValidationErrors
                    .SelectMany(x => x.ValidationErrors)
                    .Select(x => x.ErrorMessage);

                var fullErrorMessage = string.Join("; ", errorMessages);

                var exceptionMessage = string.Concat(dbEx.Message, " The validation errors are: ", fullErrorMessage);

                throw new DbEntityValidationException(exceptionMessage, dbEx.EntityValidationErrors);
            }
        }

        public async Task<ProjectConsultantArea> GetProjectConsultantArea(int ProjectConsultantAreaId)
        {
            try
            {
                return (await _ProjectConsultantArea.FindAsync(p => p.ProjectConsultantAreaId == ProjectConsultantAreaId)).FirstOrDefault();
            }
            catch (DbEntityValidationException dbEx)
            {
                var errorMessages = dbEx.EntityValidationErrors
                    .SelectMany(x => x.ValidationErrors)
                    .Select(x => x.ErrorMessage);

                var fullErrorMessage = string.Join("; ", errorMessages);

                var exceptionMessage = string.Concat(dbEx.Message, " The validation errors are: ", fullErrorMessage);

                throw new DbEntityValidationException(exceptionMessage, dbEx.EntityValidationErrors);
            }
        }

        public async Task<IList<ConsultantAreaSubTaxDetails>> GetConsultantAreaSubTaxDetails(int ProjectConsultantAreaId)
        {
            try
            {
                return (await _ConsultantAreaSubTaxDetails.FindAsync(p => p.ProjectConsultantAreaId == ProjectConsultantAreaId)).ToList();
            }
            catch (DbEntityValidationException dbEx)
            {
                var errorMessages = dbEx.EntityValidationErrors
                    .SelectMany(x => x.ValidationErrors)
                    .Select(x => x.ErrorMessage);

                var fullErrorMessage = string.Join("; ", errorMessages);

                var exceptionMessage = string.Concat(dbEx.Message, " The validation errors are: ", fullErrorMessage);

                throw new DbEntityValidationException(exceptionMessage, dbEx.EntityValidationErrors);
            }
        }

        public async Task<ProjectTracking> GetProjectTracking(int ProjectTrackingId)
        {
            try
            {
                return (await _ProjectTracking.FindAsync(p => p.ProjectTrackingId == ProjectTrackingId)).FirstOrDefault();
            }
            catch (DbEntityValidationException dbEx)
            {
                var errorMessages = dbEx.EntityValidationErrors
                    .SelectMany(x => x.ValidationErrors)
                    .Select(x => x.ErrorMessage);

                var fullErrorMessage = string.Join("; ", errorMessages);

                var exceptionMessage = string.Concat(dbEx.Message, " The validation errors are: ", fullErrorMessage);

                throw new DbEntityValidationException(exceptionMessage, dbEx.EntityValidationErrors);
            }
        }

        public async Task<IList<DashboardInfoDto>> GetAllProjectGeneralInformationList()
        {
            try
            {
                return _unitOfWork.DbContext.Database.SqlQuery<DashboardInfoDto>(StoredProcedures.GetAllProjectGeneralInformationList).ToList();
            }
            catch (DbEntityValidationException dbEx)
            {
                var errorMessages = dbEx.EntityValidationErrors
                    .SelectMany(x => x.ValidationErrors)
                    .Select(x => x.ErrorMessage);

                var fullErrorMessage = string.Join("; ", errorMessages);

                var exceptionMessage = string.Concat(dbEx.Message, " The validation errors are: ", fullErrorMessage);

                throw new DbEntityValidationException(exceptionMessage, dbEx.EntityValidationErrors);
            }
        }
        public async Task<ProjectConsultantArea> GetProjectConsultantAreaByProjectInfoId(int ProjectGeneralInformationId)
        {
            try
            {
                return (await _ProjectConsultantArea.FindAsync(p => p.ProjectGeneralInformationId == ProjectGeneralInformationId)).FirstOrDefault();
            }
            catch (DbEntityValidationException dbEx)
            {
                var errorMessages = dbEx.EntityValidationErrors
                    .SelectMany(x => x.ValidationErrors)
                    .Select(x => x.ErrorMessage);

                var fullErrorMessage = string.Join("; ", errorMessages);

                var exceptionMessage = string.Concat(dbEx.Message, " The validation errors are: ", fullErrorMessage);

                throw new DbEntityValidationException(exceptionMessage, dbEx.EntityValidationErrors);
            }
        }
        public async Task<ProjectTracking> GetProjectTrackingByProjectGeneralInformationId(int ProjectGeneralInformationId)
        {
            try
            {
                return (await _ProjectTracking.FindAsync(p => p.ProjectGeneralInformationId == ProjectGeneralInformationId)).FirstOrDefault();
            }
            catch (DbEntityValidationException dbEx)
            {
                var errorMessages = dbEx.EntityValidationErrors
                    .SelectMany(x => x.ValidationErrors)
                    .Select(x => x.ErrorMessage);

                var fullErrorMessage = string.Join("; ", errorMessages);

                var exceptionMessage = string.Concat(dbEx.Message, " The validation errors are: ", fullErrorMessage);

                throw new DbEntityValidationException(exceptionMessage, dbEx.EntityValidationErrors);
            }
        }

        public async Task<ProjectGeneralInformation> UpdateProjectGeneralInformation(ProjectGeneralInformation projectGeneral)
        {
            try
            {
                projectGeneral.UpdatedAt = DateTime.Now;
                _ProjectGeneralInformation.Update(projectGeneral);
                await _unitOfWork.SaveAsync();
                return projectGeneral;
            }
            catch (DbEntityValidationException dbEx)
            {
                var errorMessages = dbEx.EntityValidationErrors
                    .SelectMany(x => x.ValidationErrors)
                    .Select(x => x.ErrorMessage);

                var fullErrorMessage = string.Join("; ", errorMessages);

                var exceptionMessage = string.Concat(dbEx.Message, " The validation errors are: ", fullErrorMessage);

                throw new DbEntityValidationException(exceptionMessage, dbEx.EntityValidationErrors);
            }
        }

        public async Task<ProjectConsultantArea> UpdateProjectConsultantArea(ProjectConsultantArea consultantArea)
        {
            try
            {
                consultantArea.UpdatedAt = DateTime.Now;
                _ProjectConsultantArea.Update(consultantArea);
                await _unitOfWork.SaveAsync();
                return consultantArea;
            }
            catch (DbEntityValidationException dbEx)
            {
                var errorMessages = dbEx.EntityValidationErrors
                    .SelectMany(x => x.ValidationErrors)
                    .Select(x => x.ErrorMessage);

                var fullErrorMessage = string.Join("; ", errorMessages);

                var exceptionMessage = string.Concat(dbEx.Message, " The validation errors are: ", fullErrorMessage);

                throw new DbEntityValidationException(exceptionMessage, dbEx.EntityValidationErrors);
            }
        }

        public async Task<StatusCode> DeleteConsultantAreaSubTaxDetails(int ProjectConsultantAreaId)
        {
            try
            {
                var getsubDetails = await GetConsultantAreaSubTaxDetails(ProjectConsultantAreaId);
                if (getsubDetails.Count > 0)
                {
                    _ConsultantAreaSubTaxDetails.DeleteRange(getsubDetails);
                }
                return StatusCode.Deleted;
            }
            catch (DbEntityValidationException dbEx)
            {
                var errorMessages = dbEx.EntityValidationErrors
                    .SelectMany(x => x.ValidationErrors)
                    .Select(x => x.ErrorMessage);

                var fullErrorMessage = string.Join("; ", errorMessages);

                var exceptionMessage = string.Concat(dbEx.Message, " The validation errors are: ", fullErrorMessage);

                throw new DbEntityValidationException(exceptionMessage, dbEx.EntityValidationErrors);
            }
        }

        public async Task<ProjectTracking> UpdateProjectTracking(ProjectTracking projectTracking)
        {
            try
            {
                projectTracking.UpdatedAt = DateTime.Now;
                _ProjectTracking.Update(projectTracking);
                await _unitOfWork.SaveAsync();
                return projectTracking;
            }
            catch (DbEntityValidationException dbEx)
            {
                var errorMessages = dbEx.EntityValidationErrors
                    .SelectMany(x => x.ValidationErrors)
                    .Select(x => x.ErrorMessage);

                var fullErrorMessage = string.Join("; ", errorMessages);

                var exceptionMessage = string.Concat(dbEx.Message, " The validation errors are: ", fullErrorMessage);

                throw new DbEntityValidationException(exceptionMessage, dbEx.EntityValidationErrors);
            }
        }
        public async Task<IList<ContractAndTaskOrderListDto>> GetAllProjectContractAndTaskOrderList(int ProjectGeneralInformationId)
        {
            try
            {
                //var parameters = new List<SqlParameter> { new SqlParameter("ProjectGeneralInformationId", ProjectGeneralInformationId) };
                // return _unitOfWork.DbContext.Database.SqlQuery<ContractAndTaskOrderListDto>(StoredProcedures.GetAllProjectContractAndTaskOrderList, ProjectGeneralInformationId).ToList();
                var parameters = new List<SqlParameter> { new SqlParameter("ProjectGeneralInformationId", ProjectGeneralInformationId) };
                var response = _unitOfWork.DbContext.Database.SqlQuery<ContractAndTaskOrderListDto>(string.Format(StoredProcedures.GetAllProjectContractAndTaskOrderList, ProjectGeneralInformationId),
                    parameters.ToArray()).ToList();
                return response;
            }
            catch (DbEntityValidationException dbEx)
            {
                var errorMessages = dbEx.EntityValidationErrors
                    .SelectMany(x => x.ValidationErrors)
                    .Select(x => x.ErrorMessage);

                var fullErrorMessage = string.Join("; ", errorMessages);

                var exceptionMessage = string.Concat(dbEx.Message, " The validation errors are: ", fullErrorMessage);

                throw new DbEntityValidationException(exceptionMessage, dbEx.EntityValidationErrors);
            }
        }


        public async Task<IList<ProjectContractTaskOrderMoneyBalanceDto>> GetAllProjectMoneyBalanceList()
        {
            try
            {
                return _unitOfWork.DbContext.Database.SqlQuery<ProjectContractTaskOrderMoneyBalanceDto>(StoredProcedures.GetAllProjectMoneyBalanceList).ToList();
            }
            catch (DbEntityValidationException dbEx)
            {
                var errorMessages = dbEx.EntityValidationErrors
                    .SelectMany(x => x.ValidationErrors)
                    .Select(x => x.ErrorMessage);

                var fullErrorMessage = string.Join("; ", errorMessages);

                var exceptionMessage = string.Concat(dbEx.Message, " The validation errors are: ", fullErrorMessage);

                throw new DbEntityValidationException(exceptionMessage, dbEx.EntityValidationErrors);
            }
        }
        public async Task<IList<ProjectContractTaskOrderMoneyBalanceDto>> GetProjectContractTaskOrderMoneyBalanceList(int ProjectGeneralInformationId)
        {
            try
            {
                //var parameters = new List<SqlParameter> { new SqlParameter("ProjectGeneralInformationId", ProjectGeneralInformationId) };
                //return _unitOfWork.DbContext.Database.SqlQuery<ProjectContractTaskOrderMoneyBalanceDto>(StoredProcedures.GetProjectContractTaskOrderMoneyBalanceList, ProjectGeneralInformationId).ToList();

                var parameters = new List<SqlParameter> { new SqlParameter("ProjectGeneralInformationId", ProjectGeneralInformationId) };
                var response = _unitOfWork.DbContext.Database.SqlQuery<ProjectContractTaskOrderMoneyBalanceDto>(string.Format(StoredProcedures.GetProjectContractTaskOrderMoneyBalanceList, ProjectGeneralInformationId),
                    parameters.ToArray()).ToList();
                return response;
            }
            catch (DbEntityValidationException dbEx)
            {
                var errorMessages = dbEx.EntityValidationErrors
                    .SelectMany(x => x.ValidationErrors)
                    .Select(x => x.ErrorMessage);

                var fullErrorMessage = string.Join("; ", errorMessages);

                var exceptionMessage = string.Concat(dbEx.Message, " The validation errors are: ", fullErrorMessage);

                throw new DbEntityValidationException(exceptionMessage, dbEx.EntityValidationErrors);
            }
        }

       


    }
}
