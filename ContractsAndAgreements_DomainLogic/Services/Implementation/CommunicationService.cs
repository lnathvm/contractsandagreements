﻿using ContractsAndAgreements_Domain.Dtos;
using ContractsAndAgreements_DomainLogic.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Outlook = Microsoft.Office.Interop.Outlook;

namespace ContractsAndAgreements_DomainLogic.Services.Implementation
{
    public class CommunicationService : ICommunicationService
    {
        public async Task<bool> SendEmail(EmailDto email)
        {
            try
            {

                Outlook.Application outlookApp = new Outlook.Application();
                Outlook._MailItem oMailItem = (Outlook._MailItem)outlookApp.CreateItem(Outlook.OlItemType.olMailItem);
                Outlook.Inspector oInspector = oMailItem.GetInspector;
                // Thread.Sleep(10000);
                // Recipient
                Outlook.Recipients oRecips = oMailItem.Recipients;
                if (!string.IsNullOrEmpty(email.ToEmail))
                {
                    string[] emails = email.ToEmail.Split(';');
                    foreach (String recipient in emails)
                    {
                        Outlook.Recipient oRecip = oRecips.Add(recipient);
                        oRecip.Resolve();
                    }
                }

                //Add CC                                 
                if (!string.IsNullOrEmpty(email.CcEmail))
                {
                    string[] bccemails = email.CcEmail.Split(';');
                    foreach (String recipient in bccemails)
                    {
                        Outlook.Recipient oRecip = oRecips.Add(recipient);
                        oRecip.Type = (int)Outlook.OlMailRecipientType.olCC;
                        oRecip.Resolve();
                    }
                }

                //Add Subject
                oMailItem.Subject = email.Subject;

                //Add Body
                oMailItem.Body = email.Body;

                oMailItem.BodyFormat = Outlook.OlBodyFormat.olFormatHTML;

                //Display the mailbox
                oMailItem.Display(true);

                return true;
            }
            catch (Exception objEx)
            {
                return false;
            }

        }
    }
}

