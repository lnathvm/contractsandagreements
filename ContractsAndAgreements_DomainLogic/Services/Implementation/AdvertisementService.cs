﻿using ContractsAndAgreements_DataAccess.Repository;
using ContractsAndAgreements_DataAccess.StoredProcedures;
using ContractsAndAgreements_DataAccess.UnitofWorks;
using ContractsAndAgreements_Domain.Dtos;
using ContractsAndAgreements_Domain.Entity;
using ContractsAndAgreements_Domain.Enums;
using ContractsAndAgreements_DomainLogic.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContractsAndAgreements_DomainLogic.Services.Implementation
{
    public class AdvertisementService : IAdvertisementService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IRepository<ProjectAdvertisementDetails> _ProjectAdvertisementDetails;
        private readonly IRepository<ProjectWorkFlowTracking> _ProjectWorkFlowTracking;
        private readonly IRepository<ProjectAddendumDetails> _ProjectAddendumDetails;
        private readonly IRepository<ProjectAddendumWorkFlowTracking> _ProjectAddendumWorkFlowTracking;
        private readonly IRepository<ProjectPublishedDetails> _ProjectPublishedDetails;
        private readonly IRepository<ProjectContractDetails> _ProjectContractDetails;
        private readonly IRepository<ProjectContractWorkFlowTracking> _ProjectContractWorkFlowTracking;
        private readonly IRepository<ProjectContractInformation> _ProjectContractInformation;
        private readonly IRepository<ContractInformationSubTaxDetails> _ContractInformationSubTaxDetails;
        private readonly IRepository<ProjectTaskOrderDetails> _ProjectTaskOrderDetails;
        // private readonly IRepository<ProjectTaskOrderWorkFlowTracking> _ProjectTaskOrderWorkFlowTracking;
        private readonly IRepository<ProjectTaskOrderInformation> _ProjectTaskOrderInformation;
        private readonly IRepository<TaskOrderInformationSubTaxDetails> _TaskOrderInformationSubTaxDetails;
        private readonly IRepository<ProjectAdvertisementSelection> _ProjectAdvertisementSelection;
        private readonly IRepository<ProjectAdvertisementShortlist> _ProjectAdvertisementShortlist;
        private readonly IRepository<ProjectAdvertisementApparentSelection> _ProjectAdvertisementApparentSelection;
        private readonly IRepository<ShortlistSubDetails> _ShortlistSubDetails;
        private readonly IRepository<AdvertisementDocumentMaster> _AdvertisementDocumentMaster;
        private readonly IRepository<ProjectAdvertisementShortlistConsultantProvider> _ProjectAdvertisementShortlistConsultantProvider;
        private readonly IRepository<AddendumDocumentMaster> _AddendumDocumentMaster;
        private readonly IRepository<ProjectSupplementalAgreementDetails> _ProjectSupplementalAgreementDetails;
        private readonly IRepository<ProjectOriginalContractDetails> _ProjectOriginalContractDetails;
        private readonly IRepository<ProjectExtraWorkLetterDetails> _ProjectExtraWorkLetterDetails;

        private readonly IRepository<ProjectSupplementalAgreementForIDIQ> _ProjectSupplementalAgreementForIDIQ;
        private readonly IRepository<ProjectSupplementalAgreementInformationForIDIQ> _ProjectSupplementalAgreementInformationForIDIQ;
        private readonly IRepository<ProjectSupplementalAgreementInformationForIDIQSubTaxDetails> _ProjectSupplementalAgreementInformationForIDIQSubTaxDetails;
        private readonly IRepository<ProjectSupplementalAgreementSubTaxDetails> _ProjectSupplementalAgreementSubTaxDetails;
        private readonly IRepository<ProjectOriginalContractSubTaxDetails> _ProjectOriginalContractSubTaxDetails;
        private readonly IRepository<ProjectExtraWorkLetterSubTaxDetails> _ProjectExtraWorkLetterSubTaxDetails;
        private readonly IRepository<ProjectExtraWorkLetterInformationDetails> _ProjectExtraWorkLetterInformationDetails;
        private readonly IRepository<ProjectOriginalContractInformationDetails> _ProjectOriginalContractInformationDetails;
        private readonly IRepository<ProjectSupplementalAgreementInformationDetails> _ProjectSupplementalAgreementInformationDetails;
        private readonly IRepository<ProcessorMaster> _ProcessorMaster;
 

        public AdvertisementService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
            _ProjectAdvertisementDetails = _unitOfWork.CreateRepository<ProjectAdvertisementDetails>();
            _ProjectWorkFlowTracking = _unitOfWork.CreateRepository<ProjectWorkFlowTracking>();
            _ProjectAddendumDetails = _unitOfWork.CreateRepository<ProjectAddendumDetails>();
            _ProjectAddendumWorkFlowTracking = _unitOfWork.CreateRepository<ProjectAddendumWorkFlowTracking>();
            _ProjectPublishedDetails = _unitOfWork.CreateRepository<ProjectPublishedDetails>();
            _ProjectContractDetails = _unitOfWork.CreateRepository<ProjectContractDetails>();
            _ProjectContractWorkFlowTracking = _unitOfWork.CreateRepository<ProjectContractWorkFlowTracking>();
            _ProjectContractInformation = _unitOfWork.CreateRepository<ProjectContractInformation>();
            _ContractInformationSubTaxDetails = _unitOfWork.CreateRepository<ContractInformationSubTaxDetails>();
            _ProjectTaskOrderDetails = _unitOfWork.CreateRepository<ProjectTaskOrderDetails>();
            _ProjectTaskOrderInformation = _unitOfWork.CreateRepository<ProjectTaskOrderInformation>();
            _TaskOrderInformationSubTaxDetails = _unitOfWork.CreateRepository<TaskOrderInformationSubTaxDetails>();

            _ProjectAdvertisementSelection = _unitOfWork.CreateRepository<ProjectAdvertisementSelection>();
            _ProjectAdvertisementShortlist = _unitOfWork.CreateRepository<ProjectAdvertisementShortlist>();
            _ProjectAdvertisementApparentSelection = _unitOfWork.CreateRepository<ProjectAdvertisementApparentSelection>();
            _ShortlistSubDetails = _unitOfWork.CreateRepository<ShortlistSubDetails>();
            _AdvertisementDocumentMaster = _unitOfWork.CreateRepository<AdvertisementDocumentMaster>();
            _ProjectAdvertisementShortlistConsultantProvider = _unitOfWork.CreateRepository<ProjectAdvertisementShortlistConsultantProvider>();
            _AddendumDocumentMaster = _unitOfWork.CreateRepository<AddendumDocumentMaster>();

            _ProjectSupplementalAgreementDetails = _unitOfWork.CreateRepository<ProjectSupplementalAgreementDetails>();
            _ProjectOriginalContractDetails = _unitOfWork.CreateRepository<ProjectOriginalContractDetails>();
            _ProjectExtraWorkLetterDetails = _unitOfWork.CreateRepository<ProjectExtraWorkLetterDetails>();
            _ProjectSupplementalAgreementForIDIQ = _unitOfWork.CreateRepository<ProjectSupplementalAgreementForIDIQ>();
            _ProjectSupplementalAgreementInformationForIDIQ = _unitOfWork.CreateRepository<ProjectSupplementalAgreementInformationForIDIQ>();
            _ProjectSupplementalAgreementInformationForIDIQSubTaxDetails = _unitOfWork.CreateRepository<ProjectSupplementalAgreementInformationForIDIQSubTaxDetails>();
            _ProjectSupplementalAgreementSubTaxDetails = _unitOfWork.CreateRepository<ProjectSupplementalAgreementSubTaxDetails>();
            _ProjectOriginalContractSubTaxDetails = _unitOfWork.CreateRepository<ProjectOriginalContractSubTaxDetails>();
            _ProjectExtraWorkLetterSubTaxDetails = _unitOfWork.CreateRepository<ProjectExtraWorkLetterSubTaxDetails>();
            _ProjectExtraWorkLetterInformationDetails = _unitOfWork.CreateRepository<ProjectExtraWorkLetterInformationDetails>();
            _ProjectOriginalContractInformationDetails = _unitOfWork.CreateRepository<ProjectOriginalContractInformationDetails>();
            _ProjectSupplementalAgreementInformationDetails = _unitOfWork.CreateRepository<ProjectSupplementalAgreementInformationDetails>();
            _ProcessorMaster = _unitOfWork.CreateRepository<ProcessorMaster>();

        }

        public async Task<ProjectAdvertisementDetails> CreateProjectAdvertisementDetails(ProjectAdvertisementDetails project)
        {
            try
            {
                project.CreatedAt = DateTime.Now;
                _ProjectAdvertisementDetails.Insert(project);
                await _unitOfWork.SaveAsync();
                return project;
            }
            catch (DbEntityValidationException dbEx)
            {
                var errorMessages = dbEx.EntityValidationErrors
                    .SelectMany(x => x.ValidationErrors)
                    .Select(x => x.ErrorMessage);

                var fullErrorMessage = string.Join("; ", errorMessages);

                var exceptionMessage = string.Concat(dbEx.Message, " The validation errors are: ", fullErrorMessage);

                throw new DbEntityValidationException(exceptionMessage, dbEx.EntityValidationErrors);
            }
        }

        public async Task<ProjectWorkFlowTracking> CreateProjectWorkFlowTracking(ProjectWorkFlowTracking tracking)
        {
            try
            {
                tracking.CreatedAt = DateTime.Now;
                _ProjectWorkFlowTracking.Insert(tracking);
                await _unitOfWork.SaveAsync();
                return tracking;
            }
            catch (DbEntityValidationException dbEx)
            {
                var errorMessages = dbEx.EntityValidationErrors
                    .SelectMany(x => x.ValidationErrors)
                    .Select(x => x.ErrorMessage);

                var fullErrorMessage = string.Join("; ", errorMessages);

                var exceptionMessage = string.Concat(dbEx.Message, " The validation errors are: ", fullErrorMessage);

                throw new DbEntityValidationException(exceptionMessage, dbEx.EntityValidationErrors);
            }
        }
        public async Task<ProjectAdvertisementDetails> UpdateProjectAdvertisementDetails(ProjectAdvertisementDetails project)
        {
            try
            {
                project.UpdatedAt = DateTime.Now;
                _ProjectAdvertisementDetails.Update(project);
                await _unitOfWork.SaveAsync();
                return project;
            }
            catch (DbEntityValidationException dbEx)
            {
                var errorMessages = dbEx.EntityValidationErrors
                    .SelectMany(x => x.ValidationErrors)
                    .Select(x => x.ErrorMessage);

                var fullErrorMessage = string.Join("; ", errorMessages);

                var exceptionMessage = string.Concat(dbEx.Message, " The validation errors are: ", fullErrorMessage);

                throw new DbEntityValidationException(exceptionMessage, dbEx.EntityValidationErrors);
            }
        }

        public async Task<ProjectWorkFlowTracking> UpdateProjectWorkFlowTracking(ProjectWorkFlowTracking tracking)
        {
            try
            {
                tracking.UpdatedAt = DateTime.Now;
                _ProjectWorkFlowTracking.Update(tracking);
                await _unitOfWork.SaveAsync();
                return tracking;
            }
            catch (DbEntityValidationException dbEx)
            {
                var errorMessages = dbEx.EntityValidationErrors
                    .SelectMany(x => x.ValidationErrors)
                    .Select(x => x.ErrorMessage);

                var fullErrorMessage = string.Join("; ", errorMessages);

                var exceptionMessage = string.Concat(dbEx.Message, " The validation errors are: ", fullErrorMessage);

                throw new DbEntityValidationException(exceptionMessage, dbEx.EntityValidationErrors);
            }
        }


        public async Task<ProjectAdvertisementDetails> GetProjectAdvertisementDetailsByProjectInfoId(int ProjectInfoId)
        {
            try
            {
                return (await _ProjectAdvertisementDetails.FindAsync(p => p.ProjectGeneralInformationId == ProjectInfoId)).FirstOrDefault();
            }
            catch (DbEntityValidationException dbEx)
            {
                var errorMessages = dbEx.EntityValidationErrors
                    .SelectMany(x => x.ValidationErrors)
                    .Select(x => x.ErrorMessage);

                var fullErrorMessage = string.Join("; ", errorMessages);

                var exceptionMessage = string.Concat(dbEx.Message, " The validation errors are: ", fullErrorMessage);

                throw new DbEntityValidationException(exceptionMessage, dbEx.EntityValidationErrors);
            }
        }

        public async Task<ProjectWorkFlowTracking> GetProjectWorkFlowTracking(int ProjectWorkFlowTrackingId)
        {
            try
            {
                return (await _ProjectWorkFlowTracking.FindAsync(p => p.ProjectWorkFlowTrackingId == ProjectWorkFlowTrackingId)).FirstOrDefault();
            }
            catch (DbEntityValidationException dbEx)
            {
                var errorMessages = dbEx.EntityValidationErrors
                    .SelectMany(x => x.ValidationErrors)
                    .Select(x => x.ErrorMessage);

                var fullErrorMessage = string.Join("; ", errorMessages);

                var exceptionMessage = string.Concat(dbEx.Message, " The validation errors are: ", fullErrorMessage);

                throw new DbEntityValidationException(exceptionMessage, dbEx.EntityValidationErrors);
            }
        }
        public async Task<ProjectAddendumDetails> CreateProjectAddendumDetails(ProjectAddendumDetails project)
        {
            try
            {
                project.CreatedAt = DateTime.Now;
                _ProjectAddendumDetails.Insert(project);
                await _unitOfWork.SaveAsync();
                return project;
            }
            catch (DbEntityValidationException dbEx)
            {
                var errorMessages = dbEx.EntityValidationErrors
                    .SelectMany(x => x.ValidationErrors)
                    .Select(x => x.ErrorMessage);

                var fullErrorMessage = string.Join("; ", errorMessages);

                var exceptionMessage = string.Concat(dbEx.Message, " The validation errors are: ", fullErrorMessage);

                throw new DbEntityValidationException(exceptionMessage, dbEx.EntityValidationErrors);
            }
        }
        public async Task<ProjectAddendumDetails> UpdateProjectAddendumDetails(ProjectAddendumDetails project)
        {
            try
            {
                project.UpdatedAt = DateTime.Now;
                _ProjectAddendumDetails.Update(project);
                await _unitOfWork.SaveAsync();
                return project;
            }
            catch (DbEntityValidationException dbEx)
            {
                var errorMessages = dbEx.EntityValidationErrors
                    .SelectMany(x => x.ValidationErrors)
                    .Select(x => x.ErrorMessage);

                var fullErrorMessage = string.Join("; ", errorMessages);

                var exceptionMessage = string.Concat(dbEx.Message, " The validation errors are: ", fullErrorMessage);

                throw new DbEntityValidationException(exceptionMessage, dbEx.EntityValidationErrors);
            }
        }
        public async Task<ProjectAddendumWorkFlowTracking> CreateProjectAddendumWorkFlowTracking(ProjectAddendumWorkFlowTracking tracking)
        {
            try
            {
                tracking.CreatedAt = DateTime.Now;
                _ProjectAddendumWorkFlowTracking.Insert(tracking);
                await _unitOfWork.SaveAsync();
                return tracking;
            }
            catch (DbEntityValidationException dbEx)
            {
                var errorMessages = dbEx.EntityValidationErrors
                    .SelectMany(x => x.ValidationErrors)
                    .Select(x => x.ErrorMessage);

                var fullErrorMessage = string.Join("; ", errorMessages);

                var exceptionMessage = string.Concat(dbEx.Message, " The validation errors are: ", fullErrorMessage);

                throw new DbEntityValidationException(exceptionMessage, dbEx.EntityValidationErrors);
            }
        }

        public async Task<ProjectPublishedDetails> CreateProjectPublishedDetails(ProjectPublishedDetails project)
        {
            try
            {
                project.CreatedAt = DateTime.Now;
                _ProjectPublishedDetails.Insert(project);
                await _unitOfWork.SaveAsync();
                return project;
            }
            catch (DbEntityValidationException dbEx)
            {
                var errorMessages = dbEx.EntityValidationErrors
                    .SelectMany(x => x.ValidationErrors)
                    .Select(x => x.ErrorMessage);

                var fullErrorMessage = string.Join("; ", errorMessages);

                var exceptionMessage = string.Concat(dbEx.Message, " The validation errors are: ", fullErrorMessage);

                throw new DbEntityValidationException(exceptionMessage, dbEx.EntityValidationErrors);
            }
        }

        public async Task<IList<ProjectAddendumDetails>> GetProjectAddendumDetails(int ProjectInfoId)
        {
            try
            {
                return (await _ProjectAddendumDetails.FindAsync(x => x.ProjectGeneralInformationId == ProjectInfoId && x.IsDeleted == false)).ToList();
            }
            catch (DbEntityValidationException dbEx)
            {
                var errorMessages = dbEx.EntityValidationErrors
                    .SelectMany(x => x.ValidationErrors)
                    .Select(x => x.ErrorMessage);

                var fullErrorMessage = string.Join("; ", errorMessages);

                var exceptionMessage = string.Concat(dbEx.Message, " The validation errors are: ", fullErrorMessage);

                throw new DbEntityValidationException(exceptionMessage, dbEx.EntityValidationErrors);
            }
        }

        public async Task<ProjectAddendumWorkFlowTracking> GetProjectAddendumWorkFlowDetails(int ProjectAddendumWorkFlowId)
        {
            try
            {
                return (await _ProjectAddendumWorkFlowTracking.FindAsync(x => x.ProjectAddendumWorkFlowId == ProjectAddendumWorkFlowId)).FirstOrDefault();
            }
            catch (DbEntityValidationException dbEx)
            {
                var errorMessages = dbEx.EntityValidationErrors
                    .SelectMany(x => x.ValidationErrors)
                    .Select(x => x.ErrorMessage);

                var fullErrorMessage = string.Join("; ", errorMessages);

                var exceptionMessage = string.Concat(dbEx.Message, " The validation errors are: ", fullErrorMessage);

                throw new DbEntityValidationException(exceptionMessage, dbEx.EntityValidationErrors);
            }

        }
        public async Task<ProjectContractDetails> GetProjectContractDetailsByProjectInfoId(int ProjectInfoId)
        {
            try
            {
                return (await _ProjectContractDetails.FindAsync(p => p.ProjectGeneralInformationId == ProjectInfoId)).FirstOrDefault();
            }
            catch (DbEntityValidationException dbEx)
            {
                var errorMessages = dbEx.EntityValidationErrors
                    .SelectMany(x => x.ValidationErrors)
                    .Select(x => x.ErrorMessage);

                var fullErrorMessage = string.Join("; ", errorMessages);

                var exceptionMessage = string.Concat(dbEx.Message, " The validation errors are: ", fullErrorMessage);

                throw new DbEntityValidationException(exceptionMessage, dbEx.EntityValidationErrors);
            }
        }
        public async Task<ProjectContractDetails> CreateProjectContractDetails(ProjectContractDetails project)
        {
            try
            {
                project.CreatedAt = DateTime.Now;
                _ProjectContractDetails.Insert(project);
                await _unitOfWork.SaveAsync();
                return project;
            }
            catch (DbEntityValidationException dbEx)
            {
                var errorMessages = dbEx.EntityValidationErrors
                    .SelectMany(x => x.ValidationErrors)
                    .Select(x => x.ErrorMessage);

                var fullErrorMessage = string.Join("; ", errorMessages);

                var exceptionMessage = string.Concat(dbEx.Message, " The validation errors are: ", fullErrorMessage);

                throw new DbEntityValidationException(exceptionMessage, dbEx.EntityValidationErrors);
            }
        }
        public async Task<ProjectContractDetails> UpdateProjectContractDetails(ProjectContractDetails project)
        {
            try
            {
                project.UpdatedAt = DateTime.Now;
                _ProjectContractDetails.Update(project);
                await _unitOfWork.SaveAsync();
                return project;
            }
            catch (DbEntityValidationException dbEx)
            {
                var errorMessages = dbEx.EntityValidationErrors
                    .SelectMany(x => x.ValidationErrors)
                    .Select(x => x.ErrorMessage);

                var fullErrorMessage = string.Join("; ", errorMessages);

                var exceptionMessage = string.Concat(dbEx.Message, " The validation errors are: ", fullErrorMessage);

                throw new DbEntityValidationException(exceptionMessage, dbEx.EntityValidationErrors);
            }
        }
        public async Task<ProjectContractInformation> CreateProjectContractInformation(ProjectContractInformation tracking)
        {
            try
            {
                tracking.CreatedAt = DateTime.Now;
                _ProjectContractInformation.Insert(tracking);
                await _unitOfWork.SaveAsync();
                return tracking;
            }
            catch (DbEntityValidationException dbEx)
            {
                var errorMessages = dbEx.EntityValidationErrors
                    .SelectMany(x => x.ValidationErrors)
                    .Select(x => x.ErrorMessage);

                var fullErrorMessage = string.Join("; ", errorMessages);

                var exceptionMessage = string.Concat(dbEx.Message, " The validation errors are: ", fullErrorMessage);

                throw new DbEntityValidationException(exceptionMessage, dbEx.EntityValidationErrors);
            }
        }

        public async Task<ProjectContractInformation> UpdateProjectContractatInformation(ProjectContractInformation info)
        {
            try
            {
                info.UpdatedAt = DateTime.Now;
                _ProjectContractInformation.Update(info);
                await _unitOfWork.SaveAsync();
                return info;
            }
            catch (DbEntityValidationException dbEx)
            {
                var errorMessages = dbEx.EntityValidationErrors
                    .SelectMany(x => x.ValidationErrors)
                    .Select(x => x.ErrorMessage);

                var fullErrorMessage = string.Join("; ", errorMessages);

                var exceptionMessage = string.Concat(dbEx.Message, " The validation errors are: ", fullErrorMessage);

                throw new DbEntityValidationException(exceptionMessage, dbEx.EntityValidationErrors);
            }
        }
        public async Task<ProjectContractInformation> GetProjectContractInformation(int ProjectContractInformationId)
        {
            try
            {
                return (await _ProjectContractInformation.FindAsync(p => p.ProjectContractInformationId == ProjectContractInformationId)).FirstOrDefault();
            }
            catch (DbEntityValidationException dbEx)
            {
                var errorMessages = dbEx.EntityValidationErrors
                    .SelectMany(x => x.ValidationErrors)
                    .Select(x => x.ErrorMessage);

                var fullErrorMessage = string.Join("; ", errorMessages);

                var exceptionMessage = string.Concat(dbEx.Message, " The validation errors are: ", fullErrorMessage);

                throw new DbEntityValidationException(exceptionMessage, dbEx.EntityValidationErrors);
            }
        }
        public async Task<ProjectContractWorkFlowTracking> CreateProjectContractWorkFlowTracking(ProjectContractWorkFlowTracking tracking)
        {
            try
            {
                tracking.CreatedAt = DateTime.Now;
                _ProjectContractWorkFlowTracking.Insert(tracking);
                await _unitOfWork.SaveAsync();
                return tracking;
            }
            catch (DbEntityValidationException dbEx)
            {
                var errorMessages = dbEx.EntityValidationErrors
                    .SelectMany(x => x.ValidationErrors)
                    .Select(x => x.ErrorMessage);

                var fullErrorMessage = string.Join("; ", errorMessages);

                var exceptionMessage = string.Concat(dbEx.Message, " The validation errors are: ", fullErrorMessage);

                throw new DbEntityValidationException(exceptionMessage, dbEx.EntityValidationErrors);
            }
        }

        public async Task<ProjectContractWorkFlowTracking> GetProjectContractWorkFlowTracking(int ProjectWorkFlowTrackingId)
        {
            try
            {
                return (await _ProjectContractWorkFlowTracking.FindAsync(p => p.ProjectContractWorkFlowTrackingId == ProjectWorkFlowTrackingId)).FirstOrDefault();
            }
            catch (DbEntityValidationException dbEx)
            {
                var errorMessages = dbEx.EntityValidationErrors
                    .SelectMany(x => x.ValidationErrors)
                    .Select(x => x.ErrorMessage);

                var fullErrorMessage = string.Join("; ", errorMessages);

                var exceptionMessage = string.Concat(dbEx.Message, " The validation errors are: ", fullErrorMessage);

                throw new DbEntityValidationException(exceptionMessage, dbEx.EntityValidationErrors);
            }
        }
        public async Task<ProjectContractWorkFlowTracking> UpdateProjectContrcatWorkFlowTracking(ProjectContractWorkFlowTracking tracking)
        {
            try
            {
                tracking.UpdatedAt = DateTime.Now;
                _ProjectContractWorkFlowTracking.Update(tracking);
                await _unitOfWork.SaveAsync();
                return tracking;
            }
            catch (DbEntityValidationException dbEx)
            {
                var errorMessages = dbEx.EntityValidationErrors
                    .SelectMany(x => x.ValidationErrors)
                    .Select(x => x.ErrorMessage);

                var fullErrorMessage = string.Join("; ", errorMessages);

                var exceptionMessage = string.Concat(dbEx.Message, " The validation errors are: ", fullErrorMessage);

                throw new DbEntityValidationException(exceptionMessage, dbEx.EntityValidationErrors);
            }
        }

        public async Task<StatusCode> CreateContractInformationSubTaxDetails(IList<ContractInformationSubTaxDetails> subTaxDetails)
        {
            try
            {
                _ContractInformationSubTaxDetails.InsertRange(subTaxDetails);
                await _unitOfWork.SaveAsync();
                return StatusCode.Success;
            }
            catch (DbEntityValidationException dbEx)
            {
                var errorMessages = dbEx.EntityValidationErrors
                    .SelectMany(x => x.ValidationErrors)
                    .Select(x => x.ErrorMessage);

                var fullErrorMessage = string.Join("; ", errorMessages);

                var exceptionMessage = string.Concat(dbEx.Message, " The validation errors are: ", fullErrorMessage);

                throw new DbEntityValidationException(exceptionMessage, dbEx.EntityValidationErrors);
            }
        }
        public async Task<StatusCode> DeleteContractInformationSubTaxDetails(int ProjectContractInformationId)
        {
            try
            {
                var getsubDetails = await GetContractInformationSubTaxDetails(ProjectContractInformationId);
                if (getsubDetails.Count > 0)
                {
                    _ContractInformationSubTaxDetails.DeleteRange(getsubDetails);
                }
                return StatusCode.Deleted;
            }
            catch (DbEntityValidationException dbEx)
            {
                var errorMessages = dbEx.EntityValidationErrors
                    .SelectMany(x => x.ValidationErrors)
                    .Select(x => x.ErrorMessage);

                var fullErrorMessage = string.Join("; ", errorMessages);

                var exceptionMessage = string.Concat(dbEx.Message, " The validation errors are: ", fullErrorMessage);

                throw new DbEntityValidationException(exceptionMessage, dbEx.EntityValidationErrors);
            }
        }
        public async Task<IList<ContractInformationSubTaxDetails>> GetContractInformationSubTaxDetails(int ProjectContractInformationId)
        {
            try
            {
                return (await _ContractInformationSubTaxDetails.FindAsync(p => p.ProjectContractInformationId == ProjectContractInformationId)).ToList();
            }
            catch (DbEntityValidationException dbEx)
            {
                var errorMessages = dbEx.EntityValidationErrors
                    .SelectMany(x => x.ValidationErrors)
                    .Select(x => x.ErrorMessage);

                var fullErrorMessage = string.Join("; ", errorMessages);

                var exceptionMessage = string.Concat(dbEx.Message, " The validation errors are: ", fullErrorMessage);

                throw new DbEntityValidationException(exceptionMessage, dbEx.EntityValidationErrors);
            }
        }

        public async Task<ProjectTaskOrderDetails> GetProjectTaskOrderDetailsByProjectInfoId(int ProjectInfoId)
        {
            try
            {
                return (await _ProjectTaskOrderDetails.FindAsync(p => p.ProjectGeneralInformationId == ProjectInfoId)).FirstOrDefault();
            }
            catch (DbEntityValidationException dbEx)
            {
                var errorMessages = dbEx.EntityValidationErrors
                    .SelectMany(x => x.ValidationErrors)
                    .Select(x => x.ErrorMessage);

                var fullErrorMessage = string.Join("; ", errorMessages);

                var exceptionMessage = string.Concat(dbEx.Message, " The validation errors are: ", fullErrorMessage);

                throw new DbEntityValidationException(exceptionMessage, dbEx.EntityValidationErrors);
            }
        }
        public async Task<ProjectTaskOrderDetails> CreateProjectTaskOrderDetails(ProjectTaskOrderDetails project)
        {
            try
            {
                project.CreatedAt = DateTime.Now;
                _ProjectTaskOrderDetails.Insert(project);
                await _unitOfWork.SaveAsync();
                return project;
            }
            catch (DbEntityValidationException dbEx)
            {
                var errorMessages = dbEx.EntityValidationErrors
                    .SelectMany(x => x.ValidationErrors)
                    .Select(x => x.ErrorMessage);

                var fullErrorMessage = string.Join("; ", errorMessages);

                var exceptionMessage = string.Concat(dbEx.Message, " The validation errors are: ", fullErrorMessage);

                throw new DbEntityValidationException(exceptionMessage, dbEx.EntityValidationErrors);
            }
        }
        public async Task<ProjectTaskOrderDetails> UpdateProjectTaskOrderDetails(ProjectTaskOrderDetails project)
        {
            try
            {
                project.UpdatedAt = DateTime.Now;
                _ProjectTaskOrderDetails.Update(project);
                await _unitOfWork.SaveAsync();
                return project;
            }
            catch (DbEntityValidationException dbEx)
            {
                var errorMessages = dbEx.EntityValidationErrors
                    .SelectMany(x => x.ValidationErrors)
                    .Select(x => x.ErrorMessage);

                var fullErrorMessage = string.Join("; ", errorMessages);

                var exceptionMessage = string.Concat(dbEx.Message, " The validation errors are: ", fullErrorMessage);

                throw new DbEntityValidationException(exceptionMessage, dbEx.EntityValidationErrors);
            }
        }
        public async Task<ProjectTaskOrderInformation> CreateProjectTaskOrderInformation(ProjectTaskOrderInformation info)
        {
            try
            {
                info.CreatedAt = DateTime.Now;
                _ProjectTaskOrderInformation.Insert(info);
                await _unitOfWork.SaveAsync();
                return info;
            }
            catch (DbEntityValidationException dbEx)
            {
                var errorMessages = dbEx.EntityValidationErrors
                    .SelectMany(x => x.ValidationErrors)
                    .Select(x => x.ErrorMessage);

                var fullErrorMessage = string.Join("; ", errorMessages);

                var exceptionMessage = string.Concat(dbEx.Message, " The validation errors are: ", fullErrorMessage);

                throw new DbEntityValidationException(exceptionMessage, dbEx.EntityValidationErrors);
            }
        }

        public async Task<ProjectTaskOrderInformation> UpdateProjectTaskOrderInformation(ProjectTaskOrderInformation info)
        {
            try
            {
                info.UpdatedAt = DateTime.Now;
                _ProjectTaskOrderInformation.Update(info);
                await _unitOfWork.SaveAsync();
                return info;
            }
            catch (DbEntityValidationException dbEx)
            {
                var errorMessages = dbEx.EntityValidationErrors
                    .SelectMany(x => x.ValidationErrors)
                    .Select(x => x.ErrorMessage);

                var fullErrorMessage = string.Join("; ", errorMessages);

                var exceptionMessage = string.Concat(dbEx.Message, " The validation errors are: ", fullErrorMessage);

                throw new DbEntityValidationException(exceptionMessage, dbEx.EntityValidationErrors);
            }
        }
        public async Task<ProjectTaskOrderInformation> GetProjectTaskOrderInformation(int ProjectTaskOrderInformationId)
        {
            try
            {
                return (await _ProjectTaskOrderInformation.FindAsync(p => p.ProjectTaskOrderInformationId == ProjectTaskOrderInformationId)).FirstOrDefault();
            }
            catch (DbEntityValidationException dbEx)
            {
                var errorMessages = dbEx.EntityValidationErrors
                    .SelectMany(x => x.ValidationErrors)
                    .Select(x => x.ErrorMessage);

                var fullErrorMessage = string.Join("; ", errorMessages);

                var exceptionMessage = string.Concat(dbEx.Message, " The validation errors are: ", fullErrorMessage);

                throw new DbEntityValidationException(exceptionMessage, dbEx.EntityValidationErrors);
            }
        }
        public async Task<StatusCode> CreateTaskOrderInformationSubTaxDetails(IList<TaskOrderInformationSubTaxDetails> subTaxDetails)
        {
            try
            {
                _TaskOrderInformationSubTaxDetails.InsertRange(subTaxDetails);
                await _unitOfWork.SaveAsync();
                return StatusCode.Success;
            }
            catch (DbEntityValidationException dbEx)
            {
                var errorMessages = dbEx.EntityValidationErrors
                    .SelectMany(x => x.ValidationErrors)
                    .Select(x => x.ErrorMessage);

                var fullErrorMessage = string.Join("; ", errorMessages);

                var exceptionMessage = string.Concat(dbEx.Message, " The validation errors are: ", fullErrorMessage);

                throw new DbEntityValidationException(exceptionMessage, dbEx.EntityValidationErrors);
            }
        }
        public async Task<StatusCode> DeleteTaskOrderInformationSubTaxDetails(int ProjectTaskOrderInformationId)
        {
            try
            {
                var getsubDetails = await GetTaskOrderInformationSubTaxDetails(ProjectTaskOrderInformationId);
                if (getsubDetails.Count > 0)
                {
                    _TaskOrderInformationSubTaxDetails.DeleteRange(getsubDetails);
                }
                return StatusCode.Deleted;
            }
            catch (DbEntityValidationException dbEx)
            {
                var errorMessages = dbEx.EntityValidationErrors
                    .SelectMany(x => x.ValidationErrors)
                    .Select(x => x.ErrorMessage);

                var fullErrorMessage = string.Join("; ", errorMessages);

                var exceptionMessage = string.Concat(dbEx.Message, " The validation errors are: ", fullErrorMessage);

                throw new DbEntityValidationException(exceptionMessage, dbEx.EntityValidationErrors);
            }
        }
        public async Task<IList<TaskOrderInformationSubTaxDetails>> GetTaskOrderInformationSubTaxDetails(int ProjectTaskOrderInformationId)
        {
            try
            {
                return (await _TaskOrderInformationSubTaxDetails.FindAsync(p => p.ProjectTaskOrderInformationId == ProjectTaskOrderInformationId)).ToList();
            }
            catch (DbEntityValidationException dbEx)
            {
                var errorMessages = dbEx.EntityValidationErrors
                    .SelectMany(x => x.ValidationErrors)
                    .Select(x => x.ErrorMessage);

                var fullErrorMessage = string.Join("; ", errorMessages);

                var exceptionMessage = string.Concat(dbEx.Message, " The validation errors are: ", fullErrorMessage);

                throw new DbEntityValidationException(exceptionMessage, dbEx.EntityValidationErrors);
            }
        }


        public async Task<IList<AllAdvertisementDetailsDto>> GetAllAdvertisementDetailsList()
        {
            try
            {
                return (_unitOfWork.DbContext.Database.SqlQuery<AllAdvertisementDetailsDto>(StoredProcedures.GetAllAdvertisementDetailsList)).OrderByDescending(x => x.ProjectAdDetailsId).ToList();
            }
            catch (DbEntityValidationException dbEx)
            {
                var errorMessages = dbEx.EntityValidationErrors
                    .SelectMany(x => x.ValidationErrors)
                    .Select(x => x.ErrorMessage);

                var fullErrorMessage = string.Join("; ", errorMessages);

                var exceptionMessage = string.Concat(dbEx.Message, " The validation errors are: ", fullErrorMessage);

                throw new DbEntityValidationException(exceptionMessage, dbEx.EntityValidationErrors);
            }
        }


        public async Task<GetAddendunDocumentDetailDto> GetAddendunDocumentDetail(int ProjectAddendumId)
        {
            try
            {
                var parameters = new List<SqlParameter> { new SqlParameter("ProjectAddendumId", ProjectAddendumId) };
                var response = _unitOfWork.DbContext.Database.SqlQuery<GetAddendunDocumentDetailDto>(string.Format(StoredProcedures.GetAddendunDocumentDetail, ProjectAddendumId),
                    parameters.ToArray()).FirstOrDefault();

                return response;
            }
            catch (DbEntityValidationException dbEx)
            {
                var errorMessages = dbEx.EntityValidationErrors
                    .SelectMany(x => x.ValidationErrors)
                    .Select(x => x.ErrorMessage);

                var fullErrorMessage = string.Join("; ", errorMessages);

                var exceptionMessage = string.Concat(dbEx.Message, " The validation errors are: ", fullErrorMessage);

                throw new DbEntityValidationException(exceptionMessage, dbEx.EntityValidationErrors);
            }
        }
        public async Task<AllAdvertisementDetailsDto> GetAdvertisementDocumentDetail(int projectadvertisementdetails)
        {
            try
            {
                var parameters = new List<SqlParameter> { new SqlParameter("projectadvertisementdetails", projectadvertisementdetails) };
                var response = _unitOfWork.DbContext.Database.SqlQuery<AllAdvertisementDetailsDto>(string.Format(StoredProcedures.GetAdvertisementDocumentDetail, projectadvertisementdetails),
                    parameters.ToArray()).FirstOrDefault();

                return response;
            }
            catch (DbEntityValidationException dbEx)
            {
                var errorMessages = dbEx.EntityValidationErrors
                    .SelectMany(x => x.ValidationErrors)
                    .Select(x => x.ErrorMessage);

                var fullErrorMessage = string.Join("; ", errorMessages);

                var exceptionMessage = string.Concat(dbEx.Message, " The validation errors are: ", fullErrorMessage);

                throw new DbEntityValidationException(exceptionMessage, dbEx.EntityValidationErrors);
            }
        }


        public async Task<ProjectAdvertisementSelection> CreateProjectAdvertisementSelectionDetails(ProjectAdvertisementSelection project)
        {
            try
            {
                project.CreatedAt = DateTime.Now;
                _ProjectAdvertisementSelection.Insert(project);
                await _unitOfWork.SaveAsync();
                return project;
            }
            catch (DbEntityValidationException dbEx)
            {
                var errorMessages = dbEx.EntityValidationErrors
                    .SelectMany(x => x.ValidationErrors)
                    .Select(x => x.ErrorMessage);

                var fullErrorMessage = string.Join("; ", errorMessages);

                var exceptionMessage = string.Concat(dbEx.Message, " The validation errors are: ", fullErrorMessage);

                throw new DbEntityValidationException(exceptionMessage, dbEx.EntityValidationErrors);
            }
        }
        public async Task<ProjectAdvertisementSelection> UpdateProjectAdvertisementSelectionDetails(ProjectAdvertisementSelection project)
        {
            try
            {
                project.UpdatedAt = DateTime.Now;
                _ProjectAdvertisementSelection.Update(project);
                await _unitOfWork.SaveAsync();
                return project;
            }
            catch (DbEntityValidationException dbEx)
            {
                var errorMessages = dbEx.EntityValidationErrors
                    .SelectMany(x => x.ValidationErrors)
                    .Select(x => x.ErrorMessage);

                var fullErrorMessage = string.Join("; ", errorMessages);

                var exceptionMessage = string.Concat(dbEx.Message, " The validation errors are: ", fullErrorMessage);

                throw new DbEntityValidationException(exceptionMessage, dbEx.EntityValidationErrors);
            }
        }
        public async Task<ProjectAdvertisementSelection> GetProjectAdvertisementSelectionDetailsByProjectInfoId(int ProjectInfoId)
        {
            try
            {
                return (await _ProjectAdvertisementSelection.FindAsync(p => p.ProjectGeneralInformationId == ProjectInfoId)).FirstOrDefault();
            }
            catch (DbEntityValidationException dbEx)
            {
                var errorMessages = dbEx.EntityValidationErrors
                    .SelectMany(x => x.ValidationErrors)
                    .Select(x => x.ErrorMessage);

                var fullErrorMessage = string.Join("; ", errorMessages);

                var exceptionMessage = string.Concat(dbEx.Message, " The validation errors are: ", fullErrorMessage);

                throw new DbEntityValidationException(exceptionMessage, dbEx.EntityValidationErrors);
            }
        }
        public async Task<IList<AllAdvertisementDetailsDto>> GetAllAdvertisementSelectionDetailsList()
        {
            try
            {
                return _unitOfWork.DbContext.Database.SqlQuery<AllAdvertisementDetailsDto>(StoredProcedures.GetAllAdvertisementSelectionDetailsList).OrderByDescending(x => x.ProjectAdDetailsId).ToList();
            }
            catch (DbEntityValidationException dbEx)
            {
                var errorMessages = dbEx.EntityValidationErrors
                    .SelectMany(x => x.ValidationErrors)
                    .Select(x => x.ErrorMessage);

                var fullErrorMessage = string.Join("; ", errorMessages);

                var exceptionMessage = string.Concat(dbEx.Message, " The validation errors are: ", fullErrorMessage);

                throw new DbEntityValidationException(exceptionMessage, dbEx.EntityValidationErrors);
            }
        }



        public async Task<ProjectAdvertisementShortlist> CreateProjectAdvertisementShortlistDetails(ProjectAdvertisementShortlist project)
        {
            try
            {
                project.CreatedAt = DateTime.Now;
                _ProjectAdvertisementShortlist.Insert(project);
                await _unitOfWork.SaveAsync();
                return project;
            }
            catch (DbEntityValidationException dbEx)
            {
                var errorMessages = dbEx.EntityValidationErrors
                    .SelectMany(x => x.ValidationErrors)
                    .Select(x => x.ErrorMessage);

                var fullErrorMessage = string.Join("; ", errorMessages);

                var exceptionMessage = string.Concat(dbEx.Message, " The validation errors are: ", fullErrorMessage);

                throw new DbEntityValidationException(exceptionMessage, dbEx.EntityValidationErrors);
            }
        }
        public async Task<ProjectAdvertisementShortlistConsultantProvider> CreateProjectAdvertisementShortlistConsultantProviderDetails(ProjectAdvertisementShortlistConsultantProvider project)
        {
            try
            {
                project.CreatedAt = DateTime.Now;
                _ProjectAdvertisementShortlistConsultantProvider.Insert(project);
                await _unitOfWork.SaveAsync();
                return project;
            }
            catch (DbEntityValidationException dbEx)
            {
                var errorMessages = dbEx.EntityValidationErrors
                    .SelectMany(x => x.ValidationErrors)
                    .Select(x => x.ErrorMessage);

                var fullErrorMessage = string.Join("; ", errorMessages);

                var exceptionMessage = string.Concat(dbEx.Message, " The validation errors are: ", fullErrorMessage);

                throw new DbEntityValidationException(exceptionMessage, dbEx.EntityValidationErrors);
            }
        }
        public async Task<ProjectAdvertisementShortlist> UpdateProjectAdvertisementShortlistDetails(ProjectAdvertisementShortlist project)
        {
            try
            {
                project.UpdatedAt = DateTime.Now;
                _ProjectAdvertisementShortlist.Update(project);
                await _unitOfWork.SaveAsync();
                return project;
            }
            catch (DbEntityValidationException dbEx)
            {
                var errorMessages = dbEx.EntityValidationErrors
                    .SelectMany(x => x.ValidationErrors)
                    .Select(x => x.ErrorMessage);

                var fullErrorMessage = string.Join("; ", errorMessages);

                var exceptionMessage = string.Concat(dbEx.Message, " The validation errors are: ", fullErrorMessage);

                throw new DbEntityValidationException(exceptionMessage, dbEx.EntityValidationErrors);
            }
        }
        public async Task<ProjectAdvertisementShortlist> GetProjectAdvertisementShortlistDetailsByProjectInfoId(int ProjectInfoId)
        {
            try
            {
                return (await _ProjectAdvertisementShortlist.FindAsync(p => p.ProjectGeneralInformationId == ProjectInfoId && p.IsDeleted == false)).FirstOrDefault();
            }
            catch (DbEntityValidationException dbEx)
            {
                var errorMessages = dbEx.EntityValidationErrors
                    .SelectMany(x => x.ValidationErrors)
                    .Select(x => x.ErrorMessage);

                var fullErrorMessage = string.Join("; ", errorMessages);

                var exceptionMessage = string.Concat(dbEx.Message, " The validation errors are: ", fullErrorMessage);

                throw new DbEntityValidationException(exceptionMessage, dbEx.EntityValidationErrors);
            }
        }
        public async Task<IList<AllAdvertisementDetailsDto>> GetAllAdvertisementShortlistDetailsList()
        {
            try
            {
                return _unitOfWork.DbContext.Database.SqlQuery<AllAdvertisementDetailsDto>(StoredProcedures.GetAllAdvertisementShortlistDetailsList).OrderByDescending(x => x.ProjectAdvertisementShortlistId).ToList();
            }
            catch (DbEntityValidationException dbEx)
            {
                var errorMessages = dbEx.EntityValidationErrors
                    .SelectMany(x => x.ValidationErrors)
                    .Select(x => x.ErrorMessage);

                var fullErrorMessage = string.Join("; ", errorMessages);

                var exceptionMessage = string.Concat(dbEx.Message, " The validation errors are: ", fullErrorMessage);

                throw new DbEntityValidationException(exceptionMessage, dbEx.EntityValidationErrors);
            }
        }


        public async Task<ProjectAdvertisementApparentSelection> CreateProjectAdvertisementApparentSelectionDetails(ProjectAdvertisementApparentSelection project)
        {
            try
            {
                project.CreatedAt = DateTime.Now;
                _ProjectAdvertisementApparentSelection.Insert(project);
                await _unitOfWork.SaveAsync();
                return project;
            }
            catch (DbEntityValidationException dbEx)
            {
                var errorMessages = dbEx.EntityValidationErrors
                    .SelectMany(x => x.ValidationErrors)
                    .Select(x => x.ErrorMessage);

                var fullErrorMessage = string.Join("; ", errorMessages);

                var exceptionMessage = string.Concat(dbEx.Message, " The validation errors are: ", fullErrorMessage);

                throw new DbEntityValidationException(exceptionMessage, dbEx.EntityValidationErrors);
            }
        }
        public async Task<ProjectAdvertisementApparentSelection> UpdateProjectAdvertisementApparentSelectionDetails(ProjectAdvertisementApparentSelection project)
        {
            try
            {
                project.UpdatedAt = DateTime.Now;
                _ProjectAdvertisementApparentSelection.Update(project);
                await _unitOfWork.SaveAsync();
                return project;
            }
            catch (DbEntityValidationException dbEx)
            {
                var errorMessages = dbEx.EntityValidationErrors
                    .SelectMany(x => x.ValidationErrors)
                    .Select(x => x.ErrorMessage);

                var fullErrorMessage = string.Join("; ", errorMessages);

                var exceptionMessage = string.Concat(dbEx.Message, " The validation errors are: ", fullErrorMessage);

                throw new DbEntityValidationException(exceptionMessage, dbEx.EntityValidationErrors);
            }
        }
        public async Task<ProjectAdvertisementApparentSelection> GetProjectAdvertisementApparentSelectionDetailsByProjectInfoId(int ProjectInfoId)
        {
            try
            {
                return (await _ProjectAdvertisementApparentSelection.FindAsync(p => p.ProjectGeneralInformationId == ProjectInfoId)).FirstOrDefault();
            }
            catch (DbEntityValidationException dbEx)
            {
                var errorMessages = dbEx.EntityValidationErrors
                    .SelectMany(x => x.ValidationErrors)
                    .Select(x => x.ErrorMessage);

                var fullErrorMessage = string.Join("; ", errorMessages);

                var exceptionMessage = string.Concat(dbEx.Message, " The validation errors are: ", fullErrorMessage);

                throw new DbEntityValidationException(exceptionMessage, dbEx.EntityValidationErrors);
            }
        }
        public async Task<IList<AllAdvertisementDetailsDto>> GetAllAdvertisementApparentSelectionDetailsList()
        {
            try
            {
                return _unitOfWork.DbContext.Database.SqlQuery<AllAdvertisementDetailsDto>(StoredProcedures.GetAllAdvertisementApparentSelectionDetailsList).OrderByDescending(x => x.ProjectAdDetailsId).ToList();
            }
            catch (DbEntityValidationException dbEx)
            {
                var errorMessages = dbEx.EntityValidationErrors
                    .SelectMany(x => x.ValidationErrors)
                    .Select(x => x.ErrorMessage);

                var fullErrorMessage = string.Join("; ", errorMessages);

                var exceptionMessage = string.Concat(dbEx.Message, " The validation errors are: ", fullErrorMessage);

                throw new DbEntityValidationException(exceptionMessage, dbEx.EntityValidationErrors);
            }
        }

        public async Task<IList<CommonDropdownDto>> GetShortListConsultantProviderList(int ProjectInfoId)
        {
            try
            {
                var parameters = new List<SqlParameter> { new SqlParameter("ProjectGeneralInformationId", ProjectInfoId) };
                var response = _unitOfWork.DbContext.Database.SqlQuery<CommonDropdownDto>(string.Format(StoredProcedures.GetShortListConsultantProviderList, ProjectInfoId),
                    parameters.ToArray()).ToList();

                return response;
            }
            catch (DbEntityValidationException dbEx)
            {
                var errorMessages = dbEx.EntityValidationErrors
                    .SelectMany(x => x.ValidationErrors)
                    .Select(x => x.ErrorMessage);

                var fullErrorMessage = string.Join("; ", errorMessages);

                var exceptionMessage = string.Concat(dbEx.Message, " The validation errors are: ", fullErrorMessage);

                throw new DbEntityValidationException(exceptionMessage, dbEx.EntityValidationErrors);
            }
        }
        public async Task<IList<CommonDropdownDto>> GetSelectedShortListConsultantProviderDetail(int PjtAdvShortlistId)
        {
            try
            {
                var parameters = new List<SqlParameter> { new SqlParameter("ProjectAdvertisementShortlistId", PjtAdvShortlistId) };
                var response = _unitOfWork.DbContext.Database.SqlQuery<CommonDropdownDto>(string.Format(StoredProcedures.GetSelectedShortListConsultantProviderDetail, PjtAdvShortlistId),
                    parameters.ToArray()).ToList();

                return response;
            }
            catch (DbEntityValidationException dbEx)
            {
                var errorMessages = dbEx.EntityValidationErrors
                    .SelectMany(x => x.ValidationErrors)
                    .Select(x => x.ErrorMessage);

                var fullErrorMessage = string.Join("; ", errorMessages);

                var exceptionMessage = string.Concat(dbEx.Message, " The validation errors are: ", fullErrorMessage);

                throw new DbEntityValidationException(exceptionMessage, dbEx.EntityValidationErrors);
            }
        }
        public async Task<StatusCode> CreateShortlistSubDetails(IList<ShortlistSubDetails> subDetails)
        {
            try
            {
                _ShortlistSubDetails.InsertRange(subDetails);
                await _unitOfWork.SaveAsync();
                return StatusCode.Success;
            }
            catch (DbEntityValidationException dbEx)
            {
                var errorMessages = dbEx.EntityValidationErrors
                    .SelectMany(x => x.ValidationErrors)
                    .Select(x => x.ErrorMessage);

                var fullErrorMessage = string.Join("; ", errorMessages);

                var exceptionMessage = string.Concat(dbEx.Message, " The validation errors are: ", fullErrorMessage);

                throw new DbEntityValidationException(exceptionMessage, dbEx.EntityValidationErrors);
            }
        }

        public async Task<StatusCode> CreateProjectAdvertisementDocument(AdvertisementDocumentMaster documentDto)
        {
            try
            {
                documentDto.CreatedAt = DateTime.UtcNow;
                documentDto.UpdatedAt = DateTime.UtcNow;
                _AdvertisementDocumentMaster.Insert(documentDto);
                await _unitOfWork.SaveAsync();
                return StatusCode.Success;
            }
            catch (DbEntityValidationException dbEx)
            {
                var errorMessages = dbEx.EntityValidationErrors
                    .SelectMany(x => x.ValidationErrors)
                    .Select(x => x.ErrorMessage);

                var fullErrorMessage = string.Join("; ", errorMessages);

                var exceptionMessage = string.Concat(dbEx.Message, " The validation errors are: ", fullErrorMessage);

                throw new DbEntityValidationException(exceptionMessage, dbEx.EntityValidationErrors);
            }
        }

        public async Task<StatusCode> UpdateProjectAdvertisementDocument(AdvertisementDocumentMaster documentDto)
        {
            try
            {
                documentDto.CreatedAt = DateTime.UtcNow;
                documentDto.UpdatedAt = DateTime.UtcNow;
                _AdvertisementDocumentMaster.Update(documentDto);
                await _unitOfWork.SaveAsync();
                return StatusCode.Success;
            }
            catch (DbEntityValidationException dbEx)
            {
                var errorMessages = dbEx.EntityValidationErrors
                    .SelectMany(x => x.ValidationErrors)
                    .Select(x => x.ErrorMessage);

                var fullErrorMessage = string.Join("; ", errorMessages);

                var exceptionMessage = string.Concat(dbEx.Message, " The validation errors are: ", fullErrorMessage);

                throw new DbEntityValidationException(exceptionMessage, dbEx.EntityValidationErrors);
            }
        }

        public async Task<AdvertisementDocumentMaster> GetProjectAdvertisementDocument(int ProjectAdvertisementDetailsId)
        {
            try
            {
                return (await _AdvertisementDocumentMaster.FindAsync(x => x.ProjectAdvertisementDetailsId == ProjectAdvertisementDetailsId)).FirstOrDefault();
            }
            catch (DbEntityValidationException dbEx)
            {
                var errorMessages = dbEx.EntityValidationErrors
                    .SelectMany(x => x.ValidationErrors)
                    .Select(x => x.ErrorMessage);

                var fullErrorMessage = string.Join("; ", errorMessages);

                var exceptionMessage = string.Concat(dbEx.Message, " The validation errors are: ", fullErrorMessage);

                throw new DbEntityValidationException(exceptionMessage, dbEx.EntityValidationErrors);
            }
        }

        public async Task<AllAdvertisementDetailsDto> GetAdvertisementSelectionDocumentDetail(int projectadvSelId)
        {
            try
            {
                var parameters = new List<SqlParameter> { new SqlParameter("projectadvertisementdetails", projectadvSelId) };
                var response = _unitOfWork.DbContext.Database.SqlQuery<AllAdvertisementDetailsDto>(string.Format(StoredProcedures.GetAdvertisementDocumentDetail, projectadvSelId),
                    parameters.ToArray()).FirstOrDefault();

                return response;
            }
            catch (DbEntityValidationException dbEx)
            {
                var errorMessages = dbEx.EntityValidationErrors
                    .SelectMany(x => x.ValidationErrors)
                    .Select(x => x.ErrorMessage);

                var fullErrorMessage = string.Join("; ", errorMessages);

                var exceptionMessage = string.Concat(dbEx.Message, " The validation errors are: ", fullErrorMessage);

                throw new DbEntityValidationException(exceptionMessage, dbEx.EntityValidationErrors);
            }
        }
        public async Task<AdvertisementShortListDocDto> GetAdvertisementShortListDocDetail(int ProjectInfoId)
        {
            try
            {
                var parameters = new List<SqlParameter> { new SqlParameter("ProjectGeneralInformationId", ProjectInfoId) };
                var response = _unitOfWork.DbContext.Database.SqlQuery<AdvertisementShortListDocDto>(string.Format(StoredProcedures.GetAdvertisementShortListDocDetail, ProjectInfoId),
                    parameters.ToArray()).FirstOrDefault();

                return response;
            }
            catch (DbEntityValidationException dbEx)
            {
                var errorMessages = dbEx.EntityValidationErrors
                    .SelectMany(x => x.ValidationErrors)
                    .Select(x => x.ErrorMessage);

                var fullErrorMessage = string.Join("; ", errorMessages);

                var exceptionMessage = string.Concat(dbEx.Message, " The validation errors are: ", fullErrorMessage);

                throw new DbEntityValidationException(exceptionMessage, dbEx.EntityValidationErrors);
            }
        }
        public async Task<AllAdvertisementDetailsDto> GetAdvertisementSelectionDocDetail(int ProjectSelId)
        {
            try
            {
                var parameters = new List<SqlParameter> { new SqlParameter("ProjectAdvertisementSelectionId", ProjectSelId) };
                var response = _unitOfWork.DbContext.Database.SqlQuery<AllAdvertisementDetailsDto>(string.Format(StoredProcedures.GetAdvertisementSelectionDocDetail, ProjectSelId),
                    parameters.ToArray()).FirstOrDefault();

                return response;
            }
            catch (DbEntityValidationException dbEx)
            {
                var errorMessages = dbEx.EntityValidationErrors
                    .SelectMany(x => x.ValidationErrors)
                    .Select(x => x.ErrorMessage);

                var fullErrorMessage = string.Join("; ", errorMessages);

                var exceptionMessage = string.Concat(dbEx.Message, " The validation errors are: ", fullErrorMessage);

                throw new DbEntityValidationException(exceptionMessage, dbEx.EntityValidationErrors);
            }
        }

        public async Task<AllAdvertisementDetailsDto> GetAdvertisementApparentSelectionDocDetail(int ProjectAppSelId)
        {
            try
            {
                var parameters = new List<SqlParameter> { new SqlParameter("ProjectAdvertisementApparentSelectionId", ProjectAppSelId) };
                var response = _unitOfWork.DbContext.Database.SqlQuery<AllAdvertisementDetailsDto>(string.Format(StoredProcedures.GetAdvertisementApparentSelectionDocDetail, ProjectAppSelId),
                    parameters.ToArray()).FirstOrDefault();

                return response;
            }
            catch (DbEntityValidationException dbEx)
            {
                var errorMessages = dbEx.EntityValidationErrors
                    .SelectMany(x => x.ValidationErrors)
                    .Select(x => x.ErrorMessage);

                var fullErrorMessage = string.Join("; ", errorMessages);

                var exceptionMessage = string.Concat(dbEx.Message, " The validation errors are: ", fullErrorMessage);

                throw new DbEntityValidationException(exceptionMessage, dbEx.EntityValidationErrors);
            }
        }

        //public async Task<AllAdvertisementDetailsDto> GetAdvertisementSelectionDocumentDetail(int projectadvSelId)
        //{
        //    try
        //    {
        //        var parameters = new List<SqlParameter> { new SqlParameter("projectadvertisementdetails", projectadvSelId) };
        //        var response = _unitOfWork.DbContext.Database.SqlQuery<AllAdvertisementDetailsDto>(string.Format(StoredProcedures.GetAdvertisementDocumentDetail, projectadvSelId),
        //            parameters.ToArray()).FirstOrDefault();

        //        return response;
        //    }
        //    catch (DbEntityValidationException dbEx)
        //    {
        //        var errorMessages = dbEx.EntityValidationErrors
        //            .SelectMany(x => x.ValidationErrors)
        //            .Select(x => x.ErrorMessage);

        //        var fullErrorMessage = string.Join("; ", errorMessages);

        //        var exceptionMessage = string.Concat(dbEx.Message, " The validation errors are: ", fullErrorMessage);

        //        throw new DbEntityValidationException(exceptionMessage, dbEx.EntityValidationErrors);
        //    }
        //}



        //public async Task<AdvertisementShortListDocDto> GetAdvertisementShortListDocDetail(int ProjectInfoId)
        //{
        //    try
        //    {
        //        var parameters = new List<SqlParameter> { new SqlParameter("ProjectGeneralInformationId", ProjectInfoId) };
        //        var response = _unitOfWork.DbContext.Database.SqlQuery<AdvertisementShortListDocDto>(string.Format(StoredProcedures.GetAdvertisementShortListDocDetail, ProjectInfoId),
        //            parameters.ToArray()).FirstOrDefault();

        //        return response;
        //    }
        //    catch (DbEntityValidationException dbEx)
        //    {
        //        var errorMessages = dbEx.EntityValidationErrors
        //            .SelectMany(x => x.ValidationErrors)
        //            .Select(x => x.ErrorMessage);

        //        var fullErrorMessage = string.Join("; ", errorMessages);

        //        var exceptionMessage = string.Concat(dbEx.Message, " The validation errors are: ", fullErrorMessage);

        //        throw new DbEntityValidationException(exceptionMessage, dbEx.EntityValidationErrors);
        //    }
        //}
        //public async Task<AllAdvertisementDetailsDto> GetAdvertisementSelectionDocDetail(int ProjectSelId)
        //{
        //    try
        //    {
        //        var parameters = new List<SqlParameter> { new SqlParameter("ProjectAdvertisementSelectionId", ProjectSelId) };
        //        var response = _unitOfWork.DbContext.Database.SqlQuery<AllAdvertisementDetailsDto>(string.Format(StoredProcedures.GetAdvertisementSelectionDocDetail, ProjectSelId),
        //            parameters.ToArray()).FirstOrDefault();

        //        return response;
        //    }
        //    catch (DbEntityValidationException dbEx)
        //    {
        //        var errorMessages = dbEx.EntityValidationErrors
        //            .SelectMany(x => x.ValidationErrors)
        //            .Select(x => x.ErrorMessage);

        //        var fullErrorMessage = string.Join("; ", errorMessages);

        //        var exceptionMessage = string.Concat(dbEx.Message, " The validation errors are: ", fullErrorMessage);

        //        throw new DbEntityValidationException(exceptionMessage, dbEx.EntityValidationErrors);
        //    }
        //}

        //public async Task<AllAdvertisementDetailsDto> GetAdvertisementApparentSelectionDocDetail(int ProjectAppSelId)
        //{
        //    try
        //    {
        //        var parameters = new List<SqlParameter> { new SqlParameter("ProjectAdvertisementApparentSelectionId", ProjectAppSelId) };
        //        var response = _unitOfWork.DbContext.Database.SqlQuery<AllAdvertisementDetailsDto>(string.Format(StoredProcedures.GetAdvertisementApparentSelectionDocDetail, ProjectAppSelId),
        //            parameters.ToArray()).FirstOrDefault();

        //        return response;
        //    }
        //    catch (DbEntityValidationException dbEx)
        //    {
        //        var errorMessages = dbEx.EntityValidationErrors
        //            .SelectMany(x => x.ValidationErrors)
        //            .Select(x => x.ErrorMessage);

        //        var fullErrorMessage = string.Join("; ", errorMessages);

        //        var exceptionMessage = string.Concat(dbEx.Message, " The validation errors are: ", fullErrorMessage);

        //        throw new DbEntityValidationException(exceptionMessage, dbEx.EntityValidationErrors);
        //    }
        //}

        public async Task<IList<ShortlistSubDetailsDto>> GetSubByShortListId(int ProjectSortconsultantProviderId)
        {
            try
            {
                var parameters = new List<SqlParameter> { new SqlParameter("ProjectAdvertisementShortlistConsultantProviderId", ProjectSortconsultantProviderId) };
                var response = _unitOfWork.DbContext.Database.SqlQuery<ShortlistSubDetailsDto>(string.Format(StoredProcedures.GetSubByShortListId, ProjectSortconsultantProviderId),
                    parameters.ToArray()).ToList();

                return response;
            }
            catch (DbEntityValidationException dbEx)
            {
                var errorMessages = dbEx.EntityValidationErrors
                    .SelectMany(x => x.ValidationErrors)
                    .Select(x => x.ErrorMessage);

                var fullErrorMessage = string.Join("; ", errorMessages);

                var exceptionMessage = string.Concat(dbEx.Message, " The validation errors are: ", fullErrorMessage);

                throw new DbEntityValidationException(exceptionMessage, dbEx.EntityValidationErrors);
            }
        }

        public async Task<AdvertisementCommonDetailDto> GetAdvertisementDetail(int ProjectGeneralInformationId)
        {
            try
            {
                var parameters = new List<SqlParameter> { new SqlParameter("ProjectGeneralInformationId", ProjectGeneralInformationId) };
                var response = _unitOfWork.DbContext.Database.SqlQuery<AdvertisementCommonDetailDto>(string.Format(StoredProcedures.GetAdvertisementDetail, ProjectGeneralInformationId),
                    parameters.FirstOrDefault()).FirstOrDefault();

                return response;
            }
            catch (DbEntityValidationException dbEx)
            {
                var errorMessages = dbEx.EntityValidationErrors
                    .SelectMany(x => x.ValidationErrors)
                    .Select(x => x.ErrorMessage);

                var fullErrorMessage = string.Join("; ", errorMessages);

                var exceptionMessage = string.Concat(dbEx.Message, " The validation errors are: ", fullErrorMessage);

                throw new DbEntityValidationException(exceptionMessage, dbEx.EntityValidationErrors);
            }
        }

        public async Task<StatusCode> CreateProjectAddendumDocumentDetails(AddendumDocumentMaster documentDto)
        {
            try
            {
                documentDto.CreatedAt = DateTime.UtcNow;
                documentDto.UpdatedAt = DateTime.UtcNow;
                _AddendumDocumentMaster.Insert(documentDto);
                await _unitOfWork.SaveAsync();
                return StatusCode.Success;
            }
            catch (DbEntityValidationException dbEx)
            {
                var errorMessages = dbEx.EntityValidationErrors
                    .SelectMany(x => x.ValidationErrors)
                    .Select(x => x.ErrorMessage);

                var fullErrorMessage = string.Join("; ", errorMessages);

                var exceptionMessage = string.Concat(dbEx.Message, " The validation errors are: ", fullErrorMessage);

                throw new DbEntityValidationException(exceptionMessage, dbEx.EntityValidationErrors);
            }
        }

        public async Task<ProjectSupplementalAgreementDetails> CreateProjectSupplementalAgreementDetails(ProjectSupplementalAgreementDetails project)
        {
            try
            {
                project.CreatedAt = DateTime.Now;
                _ProjectSupplementalAgreementDetails.Insert(project);
                await _unitOfWork.SaveAsync();
                return project;
            }
            catch (DbEntityValidationException dbEx)
            {
                var errorMessages = dbEx.EntityValidationErrors
                    .SelectMany(x => x.ValidationErrors)
                    .Select(x => x.ErrorMessage);

                var fullErrorMessage = string.Join("; ", errorMessages);

                var exceptionMessage = string.Concat(dbEx.Message, " The validation errors are: ", fullErrorMessage);

                throw new DbEntityValidationException(exceptionMessage, dbEx.EntityValidationErrors);
            }
        }
        //public async Task<ProjectSupplementalAgreementDetails> GetProjectSupplementalAgreementDetails(int ProjectAdvertisementDetailsId)
        //{
        //    try
        //    {
        //        return (await _ProjectSupplementalAgreementDetails.FindAsync(x => x.ProjectGeneralInformationId == ProjectAdvertisementDetailsId)).FirstOrDefault();
        //    }
        //    catch (DbEntityValidationException dbEx)
        //    {
        //        var errorMessages = dbEx.EntityValidationErrors
        //            .SelectMany(x => x.ValidationErrors)
        //            .Select(x => x.ErrorMessage);

        //        var fullErrorMessage = string.Join("; ", errorMessages);

        //        var exceptionMessage = string.Concat(dbEx.Message, " The validation errors are: ", fullErrorMessage);

        //        throw new DbEntityValidationException(exceptionMessage, dbEx.EntityValidationErrors);
        //    }
        //}

        public async Task<ProjectSupplementalAgreementDetails> GetProjectSupplementalAgreementDetails(int ProjectAdvertisementDetailsId)
        {
            try
            {
                return (await _ProjectSupplementalAgreementDetails.FindAsync(x => x.ProjectGeneralInformationId == ProjectAdvertisementDetailsId)).FirstOrDefault();
            }
            catch (DbEntityValidationException dbEx)
            {
                var errorMessages = dbEx.EntityValidationErrors
                    .SelectMany(x => x.ValidationErrors)
                    .Select(x => x.ErrorMessage);

                var fullErrorMessage = string.Join("; ", errorMessages);

                var exceptionMessage = string.Concat(dbEx.Message, " The validation errors are: ", fullErrorMessage);

                throw new DbEntityValidationException(exceptionMessage, dbEx.EntityValidationErrors);
            }
        }

        public async Task<ProjectSupplementalAgreementDetails> UpdateProjectSupplementalAgreementDetails(ProjectSupplementalAgreementDetails project)
        {
            try
            {
                project.UpdatedAt = DateTime.Now;
                _ProjectSupplementalAgreementDetails.Update(project);
                await _unitOfWork.SaveAsync();
                return project;
            }
            catch (DbEntityValidationException dbEx)
            {
                var errorMessages = dbEx.EntityValidationErrors
                    .SelectMany(x => x.ValidationErrors)
                    .Select(x => x.ErrorMessage);

                var fullErrorMessage = string.Join("; ", errorMessages);

                var exceptionMessage = string.Concat(dbEx.Message, " The validation errors are: ", fullErrorMessage);

                throw new DbEntityValidationException(exceptionMessage, dbEx.EntityValidationErrors);
            }
        }

        public async Task<ProjectExtraWorkLetterDetails> CreateProjectExtraWorkLetterDetails(ProjectExtraWorkLetterDetails project)
        {
            try
            {
                project.CreatedAt = DateTime.Now;
                _ProjectExtraWorkLetterDetails.Insert(project);
                await _unitOfWork.SaveAsync();
                return project;
            }
            catch (DbEntityValidationException dbEx)
            {
                var errorMessages = dbEx.EntityValidationErrors
                    .SelectMany(x => x.ValidationErrors)
                    .Select(x => x.ErrorMessage);

                var fullErrorMessage = string.Join("; ", errorMessages);

                var exceptionMessage = string.Concat(dbEx.Message, " The validation errors are: ", fullErrorMessage);

                throw new DbEntityValidationException(exceptionMessage, dbEx.EntityValidationErrors);
            }
        }
        public async Task<ProjectExtraWorkLetterDetails> GetProjectExtraWorkLetterDetails(int ProjectExtraWorkLetterDetailId)
        {
            try
            {
                return (await _ProjectExtraWorkLetterDetails.FindAsync(x => x.ProjectGeneralInformationId == ProjectExtraWorkLetterDetailId)).FirstOrDefault();
            }
            catch (DbEntityValidationException dbEx)
            {
                var errorMessages = dbEx.EntityValidationErrors
                    .SelectMany(x => x.ValidationErrors)
                    .Select(x => x.ErrorMessage);

                var fullErrorMessage = string.Join("; ", errorMessages);

                var exceptionMessage = string.Concat(dbEx.Message, " The validation errors are: ", fullErrorMessage);

                throw new DbEntityValidationException(exceptionMessage, dbEx.EntityValidationErrors);
            }
        }

        public async Task<ProjectExtraWorkLetterDetails> UpdateProjectExtraWorkLetterDetails(ProjectExtraWorkLetterDetails project)
        {
            try
            {
                project.UpdatedAt = DateTime.Now;
                _ProjectExtraWorkLetterDetails.Update(project);
                await _unitOfWork.SaveAsync();
                return project;
            }
            catch (DbEntityValidationException dbEx)
            {
                var errorMessages = dbEx.EntityValidationErrors
                    .SelectMany(x => x.ValidationErrors)
                    .Select(x => x.ErrorMessage);

                var fullErrorMessage = string.Join("; ", errorMessages);

                var exceptionMessage = string.Concat(dbEx.Message, " The validation errors are: ", fullErrorMessage);

                throw new DbEntityValidationException(exceptionMessage, dbEx.EntityValidationErrors);
            }
        }

        public async Task<ProjectOriginalContractDetails> CreateProjectOriginalContractDetails(ProjectOriginalContractDetails project)
        {
            try
            {
                project.CreatedAt = DateTime.Now;
                _ProjectOriginalContractDetails.Insert(project);
                await _unitOfWork.SaveAsync();
                return project;
            }
            catch (DbEntityValidationException dbEx)
            {
                var errorMessages = dbEx.EntityValidationErrors
                    .SelectMany(x => x.ValidationErrors)
                    .Select(x => x.ErrorMessage);

                var fullErrorMessage = string.Join("; ", errorMessages);

                var exceptionMessage = string.Concat(dbEx.Message, " The validation errors are: ", fullErrorMessage);

                throw new DbEntityValidationException(exceptionMessage, dbEx.EntityValidationErrors);
            }
        }
        public async Task<ProjectOriginalContractDetails> GetProjectOriginalContractDetails(int ProjectAdvertisementDetailsId)
        {
            try
            {
                return (await _ProjectOriginalContractDetails.FindAsync(x => x.ProjectGeneralInformationId == ProjectAdvertisementDetailsId)).FirstOrDefault();
            }
            catch (DbEntityValidationException dbEx)
            {
                var errorMessages = dbEx.EntityValidationErrors
                    .SelectMany(x => x.ValidationErrors)
                    .Select(x => x.ErrorMessage);

                var fullErrorMessage = string.Join("; ", errorMessages);

                var exceptionMessage = string.Concat(dbEx.Message, " The validation errors are: ", fullErrorMessage);

                throw new DbEntityValidationException(exceptionMessage, dbEx.EntityValidationErrors);
            }
        }

        public async Task<ProjectOriginalContractDetails> UpdateProjectOriginalContractDetails(ProjectOriginalContractDetails project)
        {
            try
            {
                project.UpdatedAt = DateTime.Now;
                _ProjectOriginalContractDetails.Update(project);
                await _unitOfWork.SaveAsync();
                return project;
            }
            catch (DbEntityValidationException dbEx)
            {
                var errorMessages = dbEx.EntityValidationErrors
                    .SelectMany(x => x.ValidationErrors)
                    .Select(x => x.ErrorMessage);

                var fullErrorMessage = string.Join("; ", errorMessages);

                var exceptionMessage = string.Concat(dbEx.Message, " The validation errors are: ", fullErrorMessage);

                throw new DbEntityValidationException(exceptionMessage, dbEx.EntityValidationErrors);
            }
        }
        public async Task<ProjectSupplementalAgreementForIDIQ> GetProjectSupplementalAgreementForIDIQDetailsByProjectInfoId(int ProjectInfoId)
        {
            try
            {
                return (await _ProjectSupplementalAgreementForIDIQ.FindAsync(p => p.ProjectGeneralInformationId == ProjectInfoId)).FirstOrDefault();
            }
            catch (DbEntityValidationException dbEx)
            {
                var errorMessages = dbEx.EntityValidationErrors
                    .SelectMany(x => x.ValidationErrors)
                    .Select(x => x.ErrorMessage);

                var fullErrorMessage = string.Join("; ", errorMessages);

                var exceptionMessage = string.Concat(dbEx.Message, " The validation errors are: ", fullErrorMessage);

                throw new DbEntityValidationException(exceptionMessage, dbEx.EntityValidationErrors);
            }
        }
        public async Task<ProjectSupplementalAgreementForIDIQ> CreateProjectSupplementalAgreementForIDIQDetails(ProjectSupplementalAgreementForIDIQ project)
        {
            try
            {
                project.CreatedAt = DateTime.Now;
                _ProjectSupplementalAgreementForIDIQ.Insert(project);
                await _unitOfWork.SaveAsync();
                return project;
            }
            catch (DbEntityValidationException dbEx)
            {
                var errorMessages = dbEx.EntityValidationErrors
                    .SelectMany(x => x.ValidationErrors)
                    .Select(x => x.ErrorMessage);

                var fullErrorMessage = string.Join("; ", errorMessages);

                var exceptionMessage = string.Concat(dbEx.Message, " The validation errors are: ", fullErrorMessage);

                throw new DbEntityValidationException(exceptionMessage, dbEx.EntityValidationErrors);
            }
        }
        public async Task<ProjectSupplementalAgreementForIDIQ> UpdateProjectSupplementalAgreementForIDIQDetails(ProjectSupplementalAgreementForIDIQ project)
        {
            try
            {
                project.UpdatedAt = DateTime.Now;
                _ProjectSupplementalAgreementForIDIQ.Update(project);
                await _unitOfWork.SaveAsync();
                return project;
            }
            catch (DbEntityValidationException dbEx)
            {
                var errorMessages = dbEx.EntityValidationErrors
                    .SelectMany(x => x.ValidationErrors)
                    .Select(x => x.ErrorMessage);

                var fullErrorMessage = string.Join("; ", errorMessages);

                var exceptionMessage = string.Concat(dbEx.Message, " The validation errors are: ", fullErrorMessage);

                throw new DbEntityValidationException(exceptionMessage, dbEx.EntityValidationErrors);
            }
        }
        public async Task<ProjectSupplementalAgreementInformationForIDIQ> CreateProjectSupplementalAgreementForIDIQInformation(ProjectSupplementalAgreementInformationForIDIQ info)
        {
            try
            {
                info.CreatedAt = DateTime.Now;
                _ProjectSupplementalAgreementInformationForIDIQ.Insert(info);
                await _unitOfWork.SaveAsync();
                return info;
            }
            catch (DbEntityValidationException dbEx)
            {
                var errorMessages = dbEx.EntityValidationErrors
                    .SelectMany(x => x.ValidationErrors)
                    .Select(x => x.ErrorMessage);

                var fullErrorMessage = string.Join("; ", errorMessages);

                var exceptionMessage = string.Concat(dbEx.Message, " The validation errors are: ", fullErrorMessage);

                throw new DbEntityValidationException(exceptionMessage, dbEx.EntityValidationErrors);
            }
        }

        public async Task<ProjectSupplementalAgreementInformationForIDIQ> UpdateProjectSupplementalAgreementForIDIQInformation(ProjectSupplementalAgreementInformationForIDIQ info)
        {
            try
            {
                info.UpdatedAt = DateTime.Now;
                _ProjectSupplementalAgreementInformationForIDIQ.Update(info);
                await _unitOfWork.SaveAsync();
                return info;
            }
            catch (DbEntityValidationException dbEx)
            {
                var errorMessages = dbEx.EntityValidationErrors
                    .SelectMany(x => x.ValidationErrors)
                    .Select(x => x.ErrorMessage);

                var fullErrorMessage = string.Join("; ", errorMessages);

                var exceptionMessage = string.Concat(dbEx.Message, " The validation errors are: ", fullErrorMessage);

                throw new DbEntityValidationException(exceptionMessage, dbEx.EntityValidationErrors);
            }
        }
        public async Task<ProjectSupplementalAgreementInformationForIDIQ> GetProjectSupplementalAgreementForIDIQInformation(int ProjectSupplementalAgreementInformationForIDIQId)
        {
            try
            {
                return (await _ProjectSupplementalAgreementInformationForIDIQ.FindAsync(p => p.ProjectSupplementalAgreementInformationForIDIQId == ProjectSupplementalAgreementInformationForIDIQId)).FirstOrDefault();
            }
            catch (DbEntityValidationException dbEx)
            {
                var errorMessages = dbEx.EntityValidationErrors
                    .SelectMany(x => x.ValidationErrors)
                    .Select(x => x.ErrorMessage);

                var fullErrorMessage = string.Join("; ", errorMessages);

                var exceptionMessage = string.Concat(dbEx.Message, " The validation errors are: ", fullErrorMessage);

                throw new DbEntityValidationException(exceptionMessage, dbEx.EntityValidationErrors);
            }
        }
        public async Task<StatusCode> CreateSupplementalAgreementForIDIQInformationSubTaxDetails(IList<ProjectSupplementalAgreementInformationForIDIQSubTaxDetails> subTaxDetails)
        {
            try
            {
                _ProjectSupplementalAgreementInformationForIDIQSubTaxDetails.InsertRange(subTaxDetails);
                await _unitOfWork.SaveAsync();
                return StatusCode.Success;
            }
            catch (DbEntityValidationException dbEx)
            {
                var errorMessages = dbEx.EntityValidationErrors
                    .SelectMany(x => x.ValidationErrors)
                    .Select(x => x.ErrorMessage);

                var fullErrorMessage = string.Join("; ", errorMessages);

                var exceptionMessage = string.Concat(dbEx.Message, " The validation errors are: ", fullErrorMessage);

                throw new DbEntityValidationException(exceptionMessage, dbEx.EntityValidationErrors);
            }
        }
        public async Task<StatusCode> DeleteSupplementalAgreementForIDIQInformationSubTaxDetails(int ProjectTaskOrderInformationId)
        {
            try
            {
                var getsubDetails = await GetSupplementalAgreementForIDIQInformationSubTaxDetails(ProjectTaskOrderInformationId);
                if (getsubDetails.Count > 0)
                {
                    _ProjectSupplementalAgreementInformationForIDIQSubTaxDetails.DeleteRange(getsubDetails);
                }
                return StatusCode.Deleted;
            }
            catch (DbEntityValidationException dbEx)
            {
                var errorMessages = dbEx.EntityValidationErrors
                    .SelectMany(x => x.ValidationErrors)
                    .Select(x => x.ErrorMessage);

                var fullErrorMessage = string.Join("; ", errorMessages);

                var exceptionMessage = string.Concat(dbEx.Message, " The validation errors are: ", fullErrorMessage);

                throw new DbEntityValidationException(exceptionMessage, dbEx.EntityValidationErrors);
            }
        }
        public async Task<IList<ProjectSupplementalAgreementInformationForIDIQSubTaxDetails>> GetSupplementalAgreementForIDIQInformationSubTaxDetails(int ProjectSupplementalAgreementInformationForIDIQId)
        {
            try
            {
                return (await _ProjectSupplementalAgreementInformationForIDIQSubTaxDetails.FindAsync(p => p.ProjectSupplementalAgreementInformationForIDIQId == ProjectSupplementalAgreementInformationForIDIQId)).ToList();
            }
            catch (DbEntityValidationException dbEx)
            {
                var errorMessages = dbEx.EntityValidationErrors
                    .SelectMany(x => x.ValidationErrors)
                    .Select(x => x.ErrorMessage);

                var fullErrorMessage = string.Join("; ", errorMessages);

                var exceptionMessage = string.Concat(dbEx.Message, " The validation errors are: ", fullErrorMessage);

                throw new DbEntityValidationException(exceptionMessage, dbEx.EntityValidationErrors);
            }
        }
        public async Task<StatusCode> CreateProjectOriginalContractInformationDetails(ProjectOriginalContractInformationDetails Details)
        {
            try
            {
                Details.CreatedAt = DateTime.Now;
                _ProjectOriginalContractInformationDetails.Insert(Details);
                await _unitOfWork.SaveAsync();
                return StatusCode.Success;
            }
            catch (DbEntityValidationException dbEx)
            {
                var errorMessages = dbEx.EntityValidationErrors
                    .SelectMany(x => x.ValidationErrors)
                    .Select(x => x.ErrorMessage);

                var fullErrorMessage = string.Join("; ", errorMessages);

                var exceptionMessage = string.Concat(dbEx.Message, " The validation errors are: ", fullErrorMessage);

                throw new DbEntityValidationException(exceptionMessage, dbEx.EntityValidationErrors);
            }
        }
        public async Task<StatusCode> CreateProjectSupplementalAgreementInformationDetails(ProjectSupplementalAgreementInformationDetails Details)
        {
            try
            {
                Details.CreatedAt = DateTime.Now;
                _ProjectSupplementalAgreementInformationDetails.Insert(Details);
                await _unitOfWork.SaveAsync();
                return StatusCode.Success;
            }
            catch (DbEntityValidationException dbEx)
            {
                var errorMessages = dbEx.EntityValidationErrors
                    .SelectMany(x => x.ValidationErrors)
                    .Select(x => x.ErrorMessage);

                var fullErrorMessage = string.Join("; ", errorMessages);

                var exceptionMessage = string.Concat(dbEx.Message, " The validation errors are: ", fullErrorMessage);

                throw new DbEntityValidationException(exceptionMessage, dbEx.EntityValidationErrors);
            }
        }
        public async Task<StatusCode> CreateProjectExtraWorkLetterInformationDetails(ProjectExtraWorkLetterInformationDetails Details)
        {
            try
            {
                Details.CreatedAt = DateTime.Now;
                _ProjectExtraWorkLetterInformationDetails.Insert(Details);
                await _unitOfWork.SaveAsync();
                return StatusCode.Success;
            }
            catch (DbEntityValidationException dbEx)
            {
                var errorMessages = dbEx.EntityValidationErrors
                    .SelectMany(x => x.ValidationErrors)
                    .Select(x => x.ErrorMessage);

                var fullErrorMessage = string.Join("; ", errorMessages);

                var exceptionMessage = string.Concat(dbEx.Message, " The validation errors are: ", fullErrorMessage);

                throw new DbEntityValidationException(exceptionMessage, dbEx.EntityValidationErrors);
            }
        }

        public async Task<StatusCode> UpdateProjectOriginalContractInformationDetails(ProjectOriginalContractInformationDetails Details)
        {
            try
            {
                _ProjectOriginalContractInformationDetails.Update(Details);
                Details.UpdatedAt = DateTime.UtcNow;
                await _unitOfWork.SaveAsync();
                return StatusCode.Success;
            }
            catch (DbEntityValidationException dbEx)
            {
                var errorMessages = dbEx.EntityValidationErrors
                    .SelectMany(x => x.ValidationErrors)
                    .Select(x => x.ErrorMessage);

                var fullErrorMessage = string.Join("; ", errorMessages);

                var exceptionMessage = string.Concat(dbEx.Message, " The validation errors are: ", fullErrorMessage);

                throw new DbEntityValidationException(exceptionMessage, dbEx.EntityValidationErrors);
            }
        }
        public async Task<StatusCode> UpdateProjectSupplementalAgreementInformationDetails(ProjectSupplementalAgreementInformationDetails Details)
        {
            try
            {
                _ProjectSupplementalAgreementInformationDetails.Update(Details);
                Details.UpdatedAt = DateTime.UtcNow;
                await _unitOfWork.SaveAsync();
                return StatusCode.Success;
            }
            catch (DbEntityValidationException dbEx)
            {
                var errorMessages = dbEx.EntityValidationErrors
                    .SelectMany(x => x.ValidationErrors)
                    .Select(x => x.ErrorMessage);

                var fullErrorMessage = string.Join("; ", errorMessages);

                var exceptionMessage = string.Concat(dbEx.Message, " The validation errors are: ", fullErrorMessage);

                throw new DbEntityValidationException(exceptionMessage, dbEx.EntityValidationErrors);
            }
        }
        public async Task<StatusCode> UpdateProjectExtraWorkLetterInformationDetails(ProjectExtraWorkLetterInformationDetails Details)
        {
            try
            {
                _ProjectExtraWorkLetterInformationDetails.Update(Details);
                Details.UpdatedAt = DateTime.UtcNow;
                await _unitOfWork.SaveAsync();
                return StatusCode.Success;
            }
            catch (DbEntityValidationException dbEx)
            {
                var errorMessages = dbEx.EntityValidationErrors
                    .SelectMany(x => x.ValidationErrors)
                    .Select(x => x.ErrorMessage);

                var fullErrorMessage = string.Join("; ", errorMessages);

                var exceptionMessage = string.Concat(dbEx.Message, " The validation errors are: ", fullErrorMessage);

                throw new DbEntityValidationException(exceptionMessage, dbEx.EntityValidationErrors);
            }
        }

        public async Task<ProjectOriginalContractInformationDetails> GetProjectOriginalContractInformationDetails(int ProjectOriginalContractId)
        {
            try
            {
                return (await _ProjectOriginalContractInformationDetails.FindAsync(x => x.ProjectOriginalContractDetailId == ProjectOriginalContractId)).FirstOrDefault();
            }
            catch (DbEntityValidationException dbEx)
            {
                var errorMessages = dbEx.EntityValidationErrors
                    .SelectMany(x => x.ValidationErrors)
                    .Select(x => x.ErrorMessage);

                var fullErrorMessage = string.Join("; ", errorMessages);

                var exceptionMessage = string.Concat(dbEx.Message, " The validation errors are: ", fullErrorMessage);

                throw new DbEntityValidationException(exceptionMessage, dbEx.EntityValidationErrors);
            }
        }
        public async Task<ProjectSupplementalAgreementInformationDetails> GetProjectSupplementalAgreementInformationDetails(int ProjectSupplementalAgreementId)
        {
            try
            {
                return (await _ProjectSupplementalAgreementInformationDetails.FindAsync(x => x.ProjectSupplementalAgreementDetailId == ProjectSupplementalAgreementId)).FirstOrDefault();
            }
            catch (DbEntityValidationException dbEx)
            {
                var errorMessages = dbEx.EntityValidationErrors
                    .SelectMany(x => x.ValidationErrors)
                    .Select(x => x.ErrorMessage);

                var fullErrorMessage = string.Join("; ", errorMessages);

                var exceptionMessage = string.Concat(dbEx.Message, " The validation errors are: ", fullErrorMessage);

                throw new DbEntityValidationException(exceptionMessage, dbEx.EntityValidationErrors);
            }
        }
        public async Task<ProjectExtraWorkLetterInformationDetails> GetProjectExtraWorkLetterInformationDetails(int ProjectExtraWorkLetterId)
        {
            try
            {
                return (await _ProjectExtraWorkLetterInformationDetails.FindAsync(x => x.ProjectExtraWorkLetterDetailId == ProjectExtraWorkLetterId)).FirstOrDefault();
            }
            catch (DbEntityValidationException dbEx)
            {
                var errorMessages = dbEx.EntityValidationErrors
                    .SelectMany(x => x.ValidationErrors)
                    .Select(x => x.ErrorMessage);

                var fullErrorMessage = string.Join("; ", errorMessages);

                var exceptionMessage = string.Concat(dbEx.Message, " The validation errors are: ", fullErrorMessage);

                throw new DbEntityValidationException(exceptionMessage, dbEx.EntityValidationErrors);
            }
        }

        public async Task<StatusCode> CreateProjectOriginalContractSubTaxDetails(IList<ProjectOriginalContractSubTaxDetails> subTaxDetails)
        {
            try
            {
                _ProjectOriginalContractSubTaxDetails.InsertRange(subTaxDetails);
                await _unitOfWork.SaveAsync();
                return StatusCode.Success;
            }
            catch (DbEntityValidationException dbEx)
            {
                var errorMessages = dbEx.EntityValidationErrors
                    .SelectMany(x => x.ValidationErrors)
                    .Select(x => x.ErrorMessage);

                var fullErrorMessage = string.Join("; ", errorMessages);

                var exceptionMessage = string.Concat(dbEx.Message, " The validation errors are: ", fullErrorMessage);

                throw new DbEntityValidationException(exceptionMessage, dbEx.EntityValidationErrors);
            }
        }
        public async Task<StatusCode> CreateProjectSupplementalAgreementSubTaxDetails(IList<ProjectSupplementalAgreementSubTaxDetails> subTaxDetails)
        {
            try
            {
                _ProjectSupplementalAgreementSubTaxDetails.InsertRange(subTaxDetails);
                await _unitOfWork.SaveAsync();
                return StatusCode.Success;
            }
            catch (DbEntityValidationException dbEx)
            {
                var errorMessages = dbEx.EntityValidationErrors
                    .SelectMany(x => x.ValidationErrors)
                    .Select(x => x.ErrorMessage);

                var fullErrorMessage = string.Join("; ", errorMessages);

                var exceptionMessage = string.Concat(dbEx.Message, " The validation errors are: ", fullErrorMessage);

                throw new DbEntityValidationException(exceptionMessage, dbEx.EntityValidationErrors);
            }
        }
        public async Task<StatusCode> CreateProjectExtraWorkLetterSubTaxDetails(IList<ProjectExtraWorkLetterSubTaxDetails> subTaxDetails)
        {
            try
            {
                _ProjectExtraWorkLetterSubTaxDetails.InsertRange(subTaxDetails);
                await _unitOfWork.SaveAsync();
                return StatusCode.Success;
            }
            catch (DbEntityValidationException dbEx)
            {
                var errorMessages = dbEx.EntityValidationErrors
                    .SelectMany(x => x.ValidationErrors)
                    .Select(x => x.ErrorMessage);

                var fullErrorMessage = string.Join("; ", errorMessages);

                var exceptionMessage = string.Concat(dbEx.Message, " The validation errors are: ", fullErrorMessage);

                throw new DbEntityValidationException(exceptionMessage, dbEx.EntityValidationErrors);
            }
        }

        public async Task<IList<ProjectOriginalContractSubTaxDetails>> GetProjectOriginalContractSubTaxDetails(int ProjectOriginalContractId)
        {
            try
            {
                return (await _ProjectOriginalContractSubTaxDetails.FindAsync(x => x.ProjectOriginalContractInformationId == ProjectOriginalContractId)).ToList();
            }
            catch (DbEntityValidationException dbEx)
            {
                var errorMessages = dbEx.EntityValidationErrors
                    .SelectMany(x => x.ValidationErrors)
                    .Select(x => x.ErrorMessage);

                var fullErrorMessage = string.Join("; ", errorMessages);

                var exceptionMessage = string.Concat(dbEx.Message, " The validation errors are: ", fullErrorMessage);

                throw new DbEntityValidationException(exceptionMessage, dbEx.EntityValidationErrors);
            }
        }
        public async Task<IList<ProjectSupplementalAgreementSubTaxDetails>> GetProjectSupplementalAgreementSubTaxDetails(int ProjectSupplementalAgreementId)
        {
            try
            {
                return (await _ProjectSupplementalAgreementSubTaxDetails.FindAsync(x => x.ProjectSupplementalAgreementInformationId == ProjectSupplementalAgreementId)).ToList();
            }
            catch (DbEntityValidationException dbEx)
            {
                var errorMessages = dbEx.EntityValidationErrors
                    .SelectMany(x => x.ValidationErrors)
                    .Select(x => x.ErrorMessage);

                var fullErrorMessage = string.Join("; ", errorMessages);

                var exceptionMessage = string.Concat(dbEx.Message, " The validation errors are: ", fullErrorMessage);

                throw new DbEntityValidationException(exceptionMessage, dbEx.EntityValidationErrors);
            }
        }
        public async Task<IList<ProjectExtraWorkLetterSubTaxDetails>> GetProjectExtraWorkLetterSubTaxDetails(int ProjectExtraWorkLetterId)
        {
            try
            {
                return (await _ProjectExtraWorkLetterSubTaxDetails.FindAsync(x => x.ProjectExtraWorkLetterInformationId == ProjectExtraWorkLetterId)).ToList();
            }
            catch (DbEntityValidationException dbEx)
            {
                var errorMessages = dbEx.EntityValidationErrors
                    .SelectMany(x => x.ValidationErrors)
                    .Select(x => x.ErrorMessage);

                var fullErrorMessage = string.Join("; ", errorMessages);

                var exceptionMessage = string.Concat(dbEx.Message, " The validation errors are: ", fullErrorMessage);

                throw new DbEntityValidationException(exceptionMessage, dbEx.EntityValidationErrors);
            }
        }

        public async Task<StatusCode> DeleteProjectOriginalContractSubTaxDetails(IList<ProjectOriginalContractSubTaxDetails> subTaxDetails)
        {
            try
            {
                _ProjectOriginalContractSubTaxDetails.DeleteRange(subTaxDetails);
                await _unitOfWork.SaveAsync();
                return StatusCode.Success;
            }
            catch (DbEntityValidationException dbEx)
            {
                var errorMessages = dbEx.EntityValidationErrors
                    .SelectMany(x => x.ValidationErrors)
                    .Select(x => x.ErrorMessage);

                var fullErrorMessage = string.Join("; ", errorMessages);

                var exceptionMessage = string.Concat(dbEx.Message, " The validation errors are: ", fullErrorMessage);

                throw new DbEntityValidationException(exceptionMessage, dbEx.EntityValidationErrors);
            }
        }
        public async Task<StatusCode> DeleteProjectSupplementalAgreementSubTaxDetails(IList<ProjectSupplementalAgreementSubTaxDetails> subTaxDetails)
        {
            try
            {
                _ProjectSupplementalAgreementSubTaxDetails.DeleteRange(subTaxDetails);
                await _unitOfWork.SaveAsync();
                return StatusCode.Success;
            }
            catch (DbEntityValidationException dbEx)
            {
                var errorMessages = dbEx.EntityValidationErrors
                    .SelectMany(x => x.ValidationErrors)
                    .Select(x => x.ErrorMessage);

                var fullErrorMessage = string.Join("; ", errorMessages);

                var exceptionMessage = string.Concat(dbEx.Message, " The validation errors are: ", fullErrorMessage);

                throw new DbEntityValidationException(exceptionMessage, dbEx.EntityValidationErrors);
            }
        }
        public async Task<StatusCode> DeleteProjectExtraWorkLetterSubTaxDetails(IList<ProjectExtraWorkLetterSubTaxDetails> subTaxDetails)
        {
            try
            {
                _ProjectExtraWorkLetterSubTaxDetails.DeleteRange(subTaxDetails);
                await _unitOfWork.SaveAsync();
                return StatusCode.Success;
            }
            catch (DbEntityValidationException dbEx)
            {
                var errorMessages = dbEx.EntityValidationErrors
                    .SelectMany(x => x.ValidationErrors)
                    .Select(x => x.ErrorMessage);

                var fullErrorMessage = string.Join("; ", errorMessages);

                var exceptionMessage = string.Concat(dbEx.Message, " The validation errors are: ", fullErrorMessage);

                throw new DbEntityValidationException(exceptionMessage, dbEx.EntityValidationErrors);
            }
        }

        public async Task<ProjectAddendumDetails> GetProjectAddendumDetailsById(int AddendumId)
        {
            try
            {
                return (await _ProjectAddendumDetails.FindAsync(x => x.ProjectAddendumId == AddendumId && x.IsDeleted == false)).FirstOrDefault();
            }
            catch (DbEntityValidationException dbEx)
            {
                var errorMessages = dbEx.EntityValidationErrors
                    .SelectMany(x => x.ValidationErrors)
                    .Select(x => x.ErrorMessage);

                var fullErrorMessage = string.Join("; ", errorMessages);

                var exceptionMessage = string.Concat(dbEx.Message, " The validation errors are: ", fullErrorMessage);

                throw new DbEntityValidationException(exceptionMessage, dbEx.EntityValidationErrors);
            }
        }
        public async Task<ProjectAdvertisementShortlist> GetProjectAdvertisementShortlistDetailsById(int ShorlistId)
        {
            try
            {
                return (await _ProjectAdvertisementShortlist.FindAsync(p => p.ProjectAdvertisementShortlistId == ShorlistId && p.IsDeleted == false)).FirstOrDefault();
            }
            catch (DbEntityValidationException dbEx)
            {
                var errorMessages = dbEx.EntityValidationErrors
                    .SelectMany(x => x.ValidationErrors)
                    .Select(x => x.ErrorMessage);

                var fullErrorMessage = string.Join("; ", errorMessages);

                var exceptionMessage = string.Concat(dbEx.Message, " The validation errors are: ", fullErrorMessage);

                throw new DbEntityValidationException(exceptionMessage, dbEx.EntityValidationErrors);
            }
        }
        public async Task<IList<ProjectAdvertisementShortlistConsultantProvider>> GetProjectAdvertisementShortlistConsultantProviderDetails(int ProjectAdvertisementShortListId)
        {
            try
            {
                return (await _ProjectAdvertisementShortlistConsultantProvider.FindAsync(x => x.ProjectAdvertisementShortListId == ProjectAdvertisementShortListId)).ToList();
            }
            catch (DbEntityValidationException dbEx)
            {
                var errorMessages = dbEx.EntityValidationErrors
                    .SelectMany(x => x.ValidationErrors)
                    .Select(x => x.ErrorMessage);

                var fullErrorMessage = string.Join("; ", errorMessages);

                var exceptionMessage = string.Concat(dbEx.Message, " The validation errors are: ", fullErrorMessage);

                throw new DbEntityValidationException(exceptionMessage, dbEx.EntityValidationErrors);
            }
        }
        public async Task<IList<ShortlistSubDetails>> GetShortlistSubDetails(int ConsultantProviderId)
        {
            try
            {
                return (await _ShortlistSubDetails.FindAsync(x => x.ProjectAdvertisementShortlistConsultantProviderId == ConsultantProviderId)).ToList();
            }
            catch (DbEntityValidationException dbEx)
            {
                var errorMessages = dbEx.EntityValidationErrors
                    .SelectMany(x => x.ValidationErrors)
                    .Select(x => x.ErrorMessage);

                var fullErrorMessage = string.Join("; ", errorMessages);

                var exceptionMessage = string.Concat(dbEx.Message, " The validation errors are: ", fullErrorMessage);

                throw new DbEntityValidationException(exceptionMessage, dbEx.EntityValidationErrors);
            }
        }

        public async Task<ProcessorMaster> CreateProcessor(ProcessorMaster processorMaster)
        {
            try
            {
                processorMaster.IsActive = true;
                _ProcessorMaster.Insert(processorMaster);
                await _unitOfWork.SaveAsync();
                return processorMaster;
            }
            catch (DbEntityValidationException dbEx)
            {
                var errorMessages = dbEx.EntityValidationErrors
                    .SelectMany(x => x.ValidationErrors)
                    .Select(x => x.ErrorMessage);

                var fullErrorMessage = string.Join("; ", errorMessages);

                var exceptionMessage = string.Concat(dbEx.Message, " The validation errors are: ", fullErrorMessage);

                throw new DbEntityValidationException(exceptionMessage, dbEx.EntityValidationErrors);
            }
        }


    }
}

