﻿using ContractsAndAgreements_Domain.Entity;
using ContractsAndAgreements_Domain.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContractsAndAgreements_DomainLogic.Services.Interfaces
{
    public interface ICommonService
    {
        Task<IList<ContractTypeMaster>> GetAllContractTypeList();
        Task<IList<FeeTypeMaster>> GetAllFeeTypeList();
        Task<IList<FeeTypeMasterr>> GetAllCompensationTypeList();
        Task<IList<ParishMaster>> GetAllParishList();
        Task<IList<SelectionMaster>> GetAllSelectionList();
        Task<IList<ProjectManagerMaster>> GetAllProjectManagerList();
        //Task<IList<ConsultantAreaSubTaxMaster>> GetAllConsultantAreaSubTaxList();
        Task<IList<ConsultantProviderMaster>> GetAllConsultantProviderList();
        Task<ConsultantProviderMaster> GetConsultantProviderDetail(int ConsultantProviderId);

        Task<CommunicationSettingMaster> GetCommunicationSettingMaster(string type);
        Task<IList<SuperVisorMaster>> GetAllSupervisorList();
        Task<IList<ProcessorMaster>> GetAllProcessorList();

        Task<SuperVisorMaster> GetSupervisorDetails(int SupervisorId);

        Task<StatusCode> SaveConsultantProvider(ConsultantProviderMaster consultant);
        Task<StatusCode> UpdateConsultantProvider(ConsultantProviderMaster consultant);
        Task<StatusCode> DeleteConsultantProvider(ConsultantProviderMaster consultant);

        Task<IList<CompensationTypeMaster>> GetCompensationTypeList();

        Task<IList<ContractAgencyMaster>> GetAllContractAgencyList();
    }
}
