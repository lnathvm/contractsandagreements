﻿using ContractsAndAgreements_Domain.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContractsAndAgreements_DomainLogic.Services.Interfaces
{
    public interface ICommunicationService
    {
        Task<bool> SendEmail(EmailDto email);
    }
}
