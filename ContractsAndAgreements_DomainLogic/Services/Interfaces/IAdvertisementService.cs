﻿using ContractsAndAgreements_Domain.Dtos;
using ContractsAndAgreements_Domain.Entity;
using ContractsAndAgreements_Domain.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContractsAndAgreements_DomainLogic.Services.Interfaces
{
    public interface IAdvertisementService
    {
        Task<ProjectAdvertisementDetails> CreateProjectAdvertisementDetails(ProjectAdvertisementDetails project);
        Task<ProjectWorkFlowTracking> CreateProjectWorkFlowTracking(ProjectWorkFlowTracking tracking);
        Task<ProjectAdvertisementDetails> UpdateProjectAdvertisementDetails(ProjectAdvertisementDetails project);
        Task<ProjectWorkFlowTracking> UpdateProjectWorkFlowTracking(ProjectWorkFlowTracking tracking);
        Task<ProjectAdvertisementDetails> GetProjectAdvertisementDetailsByProjectInfoId(int ProjectInfoId);
        Task<ProjectWorkFlowTracking> GetProjectWorkFlowTracking(int ProjectWorkFlowTrackingId);
        Task<ProjectAddendumDetails> CreateProjectAddendumDetails(ProjectAddendumDetails project);
        Task<ProjectAddendumWorkFlowTracking> CreateProjectAddendumWorkFlowTracking(ProjectAddendumWorkFlowTracking tracking);
        Task<ProjectPublishedDetails> CreateProjectPublishedDetails(ProjectPublishedDetails project);
        Task<IList<ProjectAddendumDetails>> GetProjectAddendumDetails(int ProjectInfoId);
        Task<ProjectAddendumWorkFlowTracking> GetProjectAddendumWorkFlowDetails(int ProjectAddendumWorkFlowId);



        Task<ProjectContractDetails> GetProjectContractDetailsByProjectInfoId(int ProjectInfoId);
        Task<ProjectContractDetails> UpdateProjectContractDetails(ProjectContractDetails project);
        Task<ProjectContractDetails> CreateProjectContractDetails(ProjectContractDetails project);

        Task<ProjectContractWorkFlowTracking> GetProjectContractWorkFlowTracking(int ProjectWorkFlowTrackingId);

        Task<ProjectContractWorkFlowTracking> UpdateProjectContrcatWorkFlowTracking(ProjectContractWorkFlowTracking tracking);
        Task<ProjectContractWorkFlowTracking> CreateProjectContractWorkFlowTracking(ProjectContractWorkFlowTracking tracking);
        Task<ProjectContractInformation> CreateProjectContractInformation(ProjectContractInformation tracking);
        Task<ProjectContractInformation> UpdateProjectContractatInformation(ProjectContractInformation tracking);
        Task<ProjectContractInformation> GetProjectContractInformation(int ProjectContractInformationId);

        Task<StatusCode> CreateContractInformationSubTaxDetails(IList<ContractInformationSubTaxDetails> subTaxDetails);
        Task<StatusCode> DeleteContractInformationSubTaxDetails(int ProjectContractInformationId);
        Task<IList<ContractInformationSubTaxDetails>> GetContractInformationSubTaxDetails(int ProjectConsultantAreaId);

        Task<ProjectTaskOrderDetails> GetProjectTaskOrderDetailsByProjectInfoId(int ProjectInfoId);
        Task<ProjectTaskOrderDetails> UpdateProjectTaskOrderDetails(ProjectTaskOrderDetails project);
        Task<ProjectTaskOrderDetails> CreateProjectTaskOrderDetails(ProjectTaskOrderDetails project);
        Task<ProjectTaskOrderInformation> CreateProjectTaskOrderInformation(ProjectTaskOrderInformation tracking);
        Task<ProjectTaskOrderInformation> UpdateProjectTaskOrderInformation(ProjectTaskOrderInformation tracking);
        Task<ProjectTaskOrderInformation> GetProjectTaskOrderInformation(int ProjectTaskOrderInformationId);

        Task<StatusCode> CreateTaskOrderInformationSubTaxDetails(IList<TaskOrderInformationSubTaxDetails> subTaxDetails);
        Task<StatusCode> DeleteTaskOrderInformationSubTaxDetails(int ProjectTaskOrderInformationId);
        Task<IList<TaskOrderInformationSubTaxDetails>> GetTaskOrderInformationSubTaxDetails(int ProjectTaskOrderInformationId);
        Task<IList<AllAdvertisementDetailsDto>> GetAllAdvertisementDetailsList();
        Task<GetAddendunDocumentDetailDto> GetAddendunDocumentDetail(int ProjectAddendumId);
        Task<AllAdvertisementDetailsDto> GetAdvertisementDocumentDetail(int projectadvertisementdetails);


        Task<ProjectAdvertisementSelection> CreateProjectAdvertisementSelectionDetails(ProjectAdvertisementSelection project);
        Task<ProjectAdvertisementSelection> UpdateProjectAdvertisementSelectionDetails(ProjectAdvertisementSelection project);
        Task<ProjectAdvertisementSelection> GetProjectAdvertisementSelectionDetailsByProjectInfoId(int ProjectInfoId);
        Task<IList<AllAdvertisementDetailsDto>> GetAllAdvertisementSelectionDetailsList();


        Task<ProjectAdvertisementShortlist> CreateProjectAdvertisementShortlistDetails(ProjectAdvertisementShortlist project);
        Task<ProjectAdvertisementShortlistConsultantProvider> CreateProjectAdvertisementShortlistConsultantProviderDetails(ProjectAdvertisementShortlistConsultantProvider project);

        Task<ProjectAdvertisementShortlist> UpdateProjectAdvertisementShortlistDetails(ProjectAdvertisementShortlist project);
        Task<ProjectAdvertisementShortlist> GetProjectAdvertisementShortlistDetailsByProjectInfoId(int ProjectInfoId);
        Task<IList<AllAdvertisementDetailsDto>> GetAllAdvertisementShortlistDetailsList();

        Task<ProjectAdvertisementApparentSelection> CreateProjectAdvertisementApparentSelectionDetails(ProjectAdvertisementApparentSelection project);
        Task<ProjectAdvertisementApparentSelection> UpdateProjectAdvertisementApparentSelectionDetails(ProjectAdvertisementApparentSelection project);
        Task<ProjectAdvertisementApparentSelection> GetProjectAdvertisementApparentSelectionDetailsByProjectInfoId(int ProjectInfoId);
        Task<IList<AllAdvertisementDetailsDto>> GetAllAdvertisementApparentSelectionDetailsList();
        Task<StatusCode> CreateShortlistSubDetails(IList<ShortlistSubDetails> subDetails);
        Task<StatusCode> CreateProjectAdvertisementDocument(AdvertisementDocumentMaster documentDto);
        Task<IList<CommonDropdownDto>> GetShortListConsultantProviderList(int ProjectInfoId);
        Task<IList<CommonDropdownDto>> GetSelectedShortListConsultantProviderDetail(int PjtAdvShortlistId);

        Task<AdvertisementShortListDocDto> GetAdvertisementShortListDocDetail(int ProjectInfoId);
        Task<AllAdvertisementDetailsDto> GetAdvertisementSelectionDocDetail(int ProjectSelId);
        Task<AllAdvertisementDetailsDto> GetAdvertisementApparentSelectionDocDetail(int ProjectAppSelId);
        Task<IList<ShortlistSubDetailsDto>> GetSubByShortListId(int ProjectSortId);
        Task<AdvertisementCommonDetailDto> GetAdvertisementDetail(int ProjectGeneralInformationId);

        Task<StatusCode> CreateProjectAddendumDocumentDetails(AddendumDocumentMaster project);
        Task<StatusCode> UpdateProjectAdvertisementDocument(AdvertisementDocumentMaster documentDto);
        Task<AdvertisementDocumentMaster> GetProjectAdvertisementDocument(int ProjectAdvertisementDetailsId);

        Task<ProjectSupplementalAgreementDetails> CreateProjectSupplementalAgreementDetails(ProjectSupplementalAgreementDetails project);

        Task<ProjectSupplementalAgreementDetails> UpdateProjectSupplementalAgreementDetails(ProjectSupplementalAgreementDetails project);

        Task<ProjectExtraWorkLetterDetails> CreateProjectExtraWorkLetterDetails(ProjectExtraWorkLetterDetails project);

        Task<ProjectExtraWorkLetterDetails> UpdateProjectExtraWorkLetterDetails(ProjectExtraWorkLetterDetails project);
        Task<ProjectOriginalContractDetails> CreateProjectOriginalContractDetails(ProjectOriginalContractDetails project);
        Task<ProjectOriginalContractDetails> UpdateProjectOriginalContractDetails(ProjectOriginalContractDetails project);
        Task<ProjectSupplementalAgreementDetails> GetProjectSupplementalAgreementDetails(int ProjectAdvertisementDetailsId);
        Task<ProjectExtraWorkLetterDetails> GetProjectExtraWorkLetterDetails(int ProjectExtraWorkLetterDetailId);
        Task<ProjectOriginalContractDetails> GetProjectOriginalContractDetails(int ProjectAdvertisementDetailsId);

        Task<ProjectSupplementalAgreementForIDIQ> GetProjectSupplementalAgreementForIDIQDetailsByProjectInfoId(int ProjectInfoId);
        Task<ProjectSupplementalAgreementForIDIQ> CreateProjectSupplementalAgreementForIDIQDetails(ProjectSupplementalAgreementForIDIQ project);
        Task<ProjectSupplementalAgreementForIDIQ> UpdateProjectSupplementalAgreementForIDIQDetails(ProjectSupplementalAgreementForIDIQ project);
        Task<ProjectSupplementalAgreementInformationForIDIQ> CreateProjectSupplementalAgreementForIDIQInformation(ProjectSupplementalAgreementInformationForIDIQ info);
        Task<ProjectSupplementalAgreementInformationForIDIQ> UpdateProjectSupplementalAgreementForIDIQInformation(ProjectSupplementalAgreementInformationForIDIQ info);
        Task<ProjectSupplementalAgreementInformationForIDIQ> GetProjectSupplementalAgreementForIDIQInformation(int ProjectSupplementalAgreementInformationForIDIQId);
        Task<StatusCode> CreateSupplementalAgreementForIDIQInformationSubTaxDetails(IList<ProjectSupplementalAgreementInformationForIDIQSubTaxDetails> subTaxDetails);
        Task<StatusCode> DeleteSupplementalAgreementForIDIQInformationSubTaxDetails(int ProjectTaskOrderInformationId);
        Task<IList<ProjectSupplementalAgreementInformationForIDIQSubTaxDetails>> GetSupplementalAgreementForIDIQInformationSubTaxDetails(int ProjectSupplementalAgreementInformationForIDIQId);

        Task<StatusCode> CreateProjectSupplementalAgreementSubTaxDetails(IList<ProjectSupplementalAgreementSubTaxDetails> subTaxDetails);
        Task<StatusCode> CreateProjectExtraWorkLetterSubTaxDetails(IList<ProjectExtraWorkLetterSubTaxDetails> subTaxDetails);
        Task<StatusCode> CreateProjectOriginalContractSubTaxDetails(IList<ProjectOriginalContractSubTaxDetails> subTaxDetails);
        Task<IList<ProjectOriginalContractSubTaxDetails>> GetProjectOriginalContractSubTaxDetails(int ProjectOriginalContractId);
        Task<IList<ProjectSupplementalAgreementSubTaxDetails>> GetProjectSupplementalAgreementSubTaxDetails(int ProjectSupplementalAgreementId);
        Task<IList<ProjectExtraWorkLetterSubTaxDetails>> GetProjectExtraWorkLetterSubTaxDetails(int ProjectExtraWorkLetterId);
        Task<StatusCode> DeleteProjectOriginalContractSubTaxDetails(IList<ProjectOriginalContractSubTaxDetails> subTaxDetails);
        Task<StatusCode> DeleteProjectSupplementalAgreementSubTaxDetails(IList<ProjectSupplementalAgreementSubTaxDetails> subTaxDetails);
        Task<StatusCode> DeleteProjectExtraWorkLetterSubTaxDetails(IList<ProjectExtraWorkLetterSubTaxDetails> subTaxDetails);
        Task<StatusCode> CreateProjectExtraWorkLetterInformationDetails(ProjectExtraWorkLetterInformationDetails Details);
        Task<StatusCode> CreateProjectSupplementalAgreementInformationDetails(ProjectSupplementalAgreementInformationDetails Details);
        Task<StatusCode> CreateProjectOriginalContractInformationDetails(ProjectOriginalContractInformationDetails Details);
        Task<StatusCode> UpdateProjectOriginalContractInformationDetails(ProjectOriginalContractInformationDetails Details);
        Task<StatusCode> UpdateProjectSupplementalAgreementInformationDetails(ProjectSupplementalAgreementInformationDetails Details);
        Task<StatusCode> UpdateProjectExtraWorkLetterInformationDetails(ProjectExtraWorkLetterInformationDetails Details);
        Task<ProjectOriginalContractInformationDetails> GetProjectOriginalContractInformationDetails(int ProjectOriginalContractId);
        Task<ProjectSupplementalAgreementInformationDetails> GetProjectSupplementalAgreementInformationDetails(int ProjectSupplementalAgreementId);
        Task<ProjectExtraWorkLetterInformationDetails> GetProjectExtraWorkLetterInformationDetails(int ProjectExtraWorkLetterId);

        Task<ProjectAddendumDetails> GetProjectAddendumDetailsById(int AddendumId);
        Task<ProjectAddendumDetails> UpdateProjectAddendumDetails(ProjectAddendumDetails project);
        Task<ProjectAdvertisementShortlist> GetProjectAdvertisementShortlistDetailsById(int ShorlistId);

        Task<IList<ProjectAdvertisementShortlistConsultantProvider>> GetProjectAdvertisementShortlistConsultantProviderDetails(int ProjectAdvertisementShortListId);
        Task<IList<ShortlistSubDetails>> GetShortlistSubDetails(int ConsultantProviderId);
        Task<ProcessorMaster> CreateProcessor(ProcessorMaster processorMaster);
    }
}
