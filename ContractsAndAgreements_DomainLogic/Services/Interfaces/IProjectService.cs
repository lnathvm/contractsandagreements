﻿using ContractsAndAgreements_Domain.Dtos;
using ContractsAndAgreements_Domain.Entity;
using ContractsAndAgreements_Domain.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContractsAndAgreements_DomainLogic.Services.Interfaces
{
    public interface IProjectService
    {
        Task<ProjectGeneralInformation> CreateProjectGeneralInformation(ProjectGeneralInformation projectGeneral);
        Task<ProjectConsultantArea> CreateProjectConsultantArea(ProjectConsultantArea consultantArea);
        Task<StatusCode> CreateConsultantAreaSubTaxDetails(IList<ConsultantAreaSubTaxDetails> subTaxDetails);
        Task<ProjectTracking> CreateProjectTracking(ProjectTracking projectTracking);
        Task<ProjectGeneralInformation> GetProjectGeneralInformation(int ProjectGeneralInformationId);
        Task<ProjectConsultantArea> GetProjectConsultantArea(int ProjectConsultantAreaId);
        Task<IList<ConsultantAreaSubTaxDetails>> GetConsultantAreaSubTaxDetails(int ProjectConsultantAreaId);
        Task<ProjectTracking> GetProjectTracking(int ProjectTrackingId);
        //Task<IList<ConsultantProviderMaster>> GetAllConsultantProviderList();
        //Task<ConsultantProviderMaster> GetConsultantProviderDetail(int ConsultantProviderId);

        Task<IList<DashboardInfoDto>> GetAllProjectGeneralInformationList();
        Task<ProjectConsultantArea> GetProjectConsultantAreaByProjectInfoId(int ProjectGeneralInformationId);
        Task<ProjectTracking> GetProjectTrackingByProjectGeneralInformationId(int ProjectGeneralInformationId);
        Task<ProjectGeneralInformation> UpdateProjectGeneralInformation(ProjectGeneralInformation projectGeneral);
        Task<ProjectConsultantArea> UpdateProjectConsultantArea(ProjectConsultantArea consultantArea);
        Task<StatusCode> DeleteConsultantAreaSubTaxDetails(int ProjectConsultantAreaId);
        Task<ProjectTracking> UpdateProjectTracking(ProjectTracking projectTracking);

        //Task<dynamic> GetProjectInfoFromLagov(string StateProjectNo);

        Task<IList<ContractAndTaskOrderListDto>> GetAllProjectContractAndTaskOrderList(int ProjectGeneralInformationId);
        Task<IList<ProjectContractTaskOrderMoneyBalanceDto>> GetAllProjectMoneyBalanceList();

        Task<IList<ProjectContractTaskOrderMoneyBalanceDto>> GetProjectContractTaskOrderMoneyBalanceList(int ProjectGeneralInformationId);

        


    }
}
